$("#diffButton_on").click(function() {

    $('.compare_border .diff').addClass('active');
    $(this).css({'display':'none'});
    $("#diffButton_off").css({'display':'block'});

});
$("#diffButton_off").click(function() {

    $('.compare_border .diff').removeClass('active');
    $(this).css({'display':'none'});
    $("#diffButton_on").css({'display':'block'});
});



$('#moveright').click(function() {

    if(canimoveit($(this))){
        var leftPos = $('div#movebox').scrollLeft();
        $("div#movebox").animate({
            scrollLeft: leftPos + 250
        }, 200);
        //always remove the class to left arrow
        if(($("#moveleft").hasClass('arrow_disabled'))){
            $("#moveleft").removeClass('arrow_disabled');
        }
        //also, i need to know if i can disable the arrow
        console.log($('div#movebox table').width() - $('div#movebox').width() - (leftPos+250));
        if( ($('div#movebox table').width() - $('div#movebox').width() - (leftPos+250) ) < 250 ){
            $(this).addClass('arrow_disabled');
        }
    }

});
$('#moveleft').click(function() {
    if(canimoveit($(this))){
        var leftPos = $('div#movebox').scrollLeft();
        $("div#movebox").animate({
            scrollLeft: leftPos - 250
        }, 200);
        //disable arrow?
        if(leftPos <= ($("#movebox tr").width() / $("#movebox tr td").length)) {
            $(this).addClass('arrow_disabled');
        }
        //always remove the class to right arrow
        if(($("#moveright").hasClass('arrow_disabled'))){
            $("#moveright").removeClass('arrow_disabled');
        }
    }
});

//you can move it only when the arrow is not disabled
function canimoveit(element){
    if(element.hasClass('arrow_disabled')) return false;
    return true;
}
