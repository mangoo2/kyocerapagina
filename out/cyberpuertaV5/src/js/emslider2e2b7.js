/**

USE:
$('.emslider2').emslider2();

**/

(function($) {
    $.fn.emslider2 = function(options) {
        options = $.extend({
            slidingeffect:'swing',
            start:'left', /* left, center */
        }, options);
    
        return this.each(function() {
            
            $.fn.mincontentwidth = function()
            {
                var box = $(this);
                var mincontentwidth=$('ul li', box).children().first().outerWidth(true);
                return mincontentwidth;
            }
            $.fn.itemsperview = function() {
                var box = $(this);
                var mincontentwidth=box.mincontentwidth();
                var maxviewwidth=$('.emslider2_items', box).width();

                var item = Math.round(maxviewwidth / mincontentwidth);
                return item;
            };
            
            $.fn.itemanimate = function(element, left, paddingleft, duration) {
                var box = $(this);
                
                $(element).stop().animate({
                'left':Math.floor(left) + 'px',
                'padding-left':paddingleft+'px' 
                }, 
                duration,
                options.slidingeffect);
            };
            
            $.fn.refresh = function() {
                var box = $(this);
                var maxviewwidth=$('.emslider2_items', box).width();
                var itemsperview = box.itemsperview();
                var itemcount = $('ul li', box).length;
                var itemwidth=Math.floor(maxviewwidth / 100 * (100 / itemsperview));
                var mincontentwidth=box.mincontentwidth();
                
                if(itemcount<itemsperview)
                    itemwidth=mincontentwidth;
                
                $('ul', box).css({
                    'width':(itemwidth * itemcount) + 'px'
                });
                $('ul li', box).css('width',itemwidth + 'px');
                
                var indexleft = box.attr('data-indexleft');
                if(indexleft==null || indexleft=="")
                    indexleft=0;
                
                $('ul li', box).each(function( index ) {
                    var left = parseInt((parseInt(index) + parseInt(indexleft)) * parseInt(itemwidth));
                    var paddingleft = Math.floor((itemwidth - mincontentwidth)/2);
                    box.itemanimate(this,left,paddingleft,0);
                });
                
                if((indexleft * -1) < (parseInt(itemcount)-parseInt(itemsperview)))
                {
                    button_prev.removeClass('emslider2_navi_inactive');
                }
                else
                {
                    button_next.addClass('emslider2_navi_inactive');
                }
                if((indexleft * -1) == (parseInt(itemcount)-parseInt(itemsperview)))
                {
                    button_next.addClass('emslider2_navi_inactive');
                }
                if(indexleft < 0)
                {
                    button_next.removeClass('emslider2_navi_inactive');
                }
                else
                {
                    button_prev.addClass('emslider2_navi_inactive');
                }
                if(indexleft==0)
                {
                    button_prev.addClass('emslider2_navi_inactive');
                }
                //CP-989 Fix this flicker issue on load
                box.css('visibility', 'visible');
            };
            
            
            var box = $(this);
            var mincontentwidth=box.mincontentwidth();
            var maxviewwidth=$('.emslider2_items', box).width();
            var itemcount = $('ul li', box).length;
            var itemsperview = box.itemsperview();
            
            var button_next = $('.emslider2_next',box);
            var button_prev = $('.emslider2_prev',box);

            var indexleft = box.attr('data-indexleft');
            if(indexleft==null || indexleft=="")
                indexleft=0;
                
            if(itemcount > itemsperview)
            {
                if(options.slidingeffect=='center')
                    indexleft=Math.floor((itemcount-itemsperview)/2) * -1;
                else if(options.slidingeffect=='left')
                    indexleft=0;
            }   
            else
            {
                button_next.addClass('emslider2_navi_inactive');
                button_prev.addClass('emslider2_navi_inactive');
            }
            box.attr('data-indexleft',indexleft);
            
            box.refresh();
            
            button_next.click(function() {
                
                var itemsperview = box.itemsperview();
                var itemcount = $('ul li', box).length;
                var maxviewwidth=$('.emslider2_items', box).width();
                var itemwidth=Math.floor(maxviewwidth / 100 * (100/itemsperview));
                var mincontentwidth=box.mincontentwidth();
                
                var indexleft = box.attr('data-indexleft');
                if(indexleft==null || indexleft=="")
                    indexleft=0;
                    
                if((indexleft * -1) < (parseInt(itemcount)-parseInt(itemsperview)))
                {
                    indexleft--;
                    button_prev.removeClass('emslider2_navi_inactive');
                }
                else
                {
                    button_next.addClass('emslider2_navi_inactive');
                    return;
                }
                if((indexleft * -1) == (parseInt(itemcount)-parseInt(itemsperview)))
                {
                    button_next.addClass('emslider2_navi_inactive');
                }
                
                $('ul li', box).each(function( index ) {
                    var left = parseInt((parseInt(index) + parseInt(indexleft)) * parseInt(itemwidth));
                    var paddingleft = Math.floor((itemwidth - mincontentwidth)/2);
                    box.itemanimate(this,left,paddingleft,500);
                });
                
                box.attr('data-indexleft',indexleft);
            });
            button_prev.click(function() {
                
                var itemsperview = box.itemsperview();
                var itemcount = $('ul li', box).length;
                var maxviewwidth=$('.emslider2_items', box).width();
                var itemwidth=Math.floor(maxviewwidth / 100 * (100/itemsperview));
                var mincontentwidth=box.mincontentwidth();
                
                var indexleft = box.attr('data-indexleft');
                if(indexleft==null || indexleft=="")
                    indexleft=0;
                
                if(indexleft < 0)
                {
                    indexleft++;
                    button_next.removeClass('emslider2_navi_inactive');
                }
                else
                {
                    button_prev.addClass('emslider2_navi_inactive');
                    return;
                }
                if(indexleft==0)
                {
                    button_prev.addClass('emslider2_navi_inactive');
                }
                
                $('ul li', box).each(function( index ) {
                    var left = parseInt((parseInt(index) + parseInt(indexleft)) * parseInt(itemwidth));
                    var paddingleft = Math.floor((itemwidth - mincontentwidth)/2);
                    box.itemanimate(this,left,paddingleft,500);
                });
                
                box.attr('data-indexleft',indexleft);
            });
            
            $( window ).resize(function() {
                box.refresh();
            });
        });
    };

})(jQuery);