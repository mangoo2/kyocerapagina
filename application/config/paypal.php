<? php   if (! defined ( 'BASEPATH' )) exit ( 'No se permite el acceso directo al script' ); 
// ------------------------------------------------ ------------------------ 
// Configuración de la biblioteca de Paypal 
// ------------------- -------------------------------------------------- --- 

// Entorno de PayPal, Sandbox o Live 
$config[ 'sandbox' ] =  TRUE ; // FALSE para el entorno en vivo 

// PayPal business email 
$config[ 'business' ] =  'sb-4moio26456688@business.example.com' ; 
// ¿Cuál es la moneda por defecto? 
$config['paypal_lib_currency_code' ] =  'MXN' ; 
// ¿Dónde está ubicado el botón? 
$config[ 'paypal_lib_button_path' ] =  'asset / images /' ; 
// Si (y donde) para registrar la respuesta ipn en un archivo 
$config[ 'paypal_lib_ipn_log' ] =  TRUE ; 
$config[ 'paypal_lib_ipn_log_file' ] =  BASEPATH  . 'logs / paypal_ipn.log' ;