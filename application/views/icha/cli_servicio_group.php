<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
    <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
    <link href="<?php echo base_url(); ?>public/css/styles.css?v=<?php echo date('YmdHis');?>" type="text/css" rel="stylesheet">
    <style type="text/css">
    	
        .servicio_cli{
                background: url(<?php echo base_url()?>/public/img/1impresora.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }
        .bloques_cli{
                background: url(<?php echo base_url()?>/public/img/bloques.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }
        .bloques_cli2{
                background: url(<?php echo base_url()?>/public/img/carrito.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }
            .servicio_cli{
            	font-size: 11px;
            }
            .container{
            	    padding-right: 1px;
            }

        
    </style>
</head>
<body class="container">
		<div class="row">
			<div class="col-md-12" style="text-align: center;">
				Numero de cliente: <b><?php echo $idcliente;?></b><br>
				Solicitud de servicio técnico
			</div>
		</div>
		<?php 	$html='';
			$row_reg=0;
			foreach ($result_eq_c as $itemc) {
				$row_reg++;
			}
			foreach ($result_eq_p as $itemc) {
				$row_reg++;
			}
			if(count($result_eq_v)>0){
				$row_reg++;
			}
			if(count($result_eq_h)>0){
				$row_reg++;
			}

			foreach ($result_eq_c as $itemc) {
				$html.='<div class="row">';
					$html.='<div class="col-md-12 min-height" style="padding-right: 1px;padding-left: 1px;">';
						$html.='<a class="option_ct servicio_cli shadowx vinculo_a" data-url="'.base_url().'Icha/cli_servicio_pre/'.$codigo.'/1/0/'.$itemc['idcontrato'].'?row='.$row_reg.'">Contrato activo ('.$itemc['idcontrato'].' '.$itemc['folio'].')</a>';
					$html.='</div>';
				$html.='</div>';
			}
			foreach ($result_eq_p as $itemc) {
				$html.='<div class="row">';
					$html.='<div class="col-md-12 min-height" style="padding-right: 1px;padding-left: 1px;">';
						$html.='<a class="option_ct bloques_cli shadowx vinculo_a" data-url="'.base_url().'Icha/cli_servicio_pre/'.$codigo.'/2/0/'.$itemc['id'].'?row='.$row_reg.'">Poliza activa '.$itemc['nombre'].' ('.$itemc['id'].')</a>';
					$html.='</div>';
				$html.='</div>';
			}
			if(count($result_eq_v)>0){
				$html.='<div class="row">';
					$html.='<div class="col-md-12 min-height" style="padding-right: 1px;padding-left: 1px;">';
						$html.='<a class="option_ct bloques_cli shadowx vinculo_a" data-url="'.base_url().'Icha/cli_servicio/'.$codigo.'/4/0/0?row='.$row_reg.'">Mis Compras</a>';//esta tambien abilitado cli_servicio_pre en dado caso que se requiera
					$html.='</div>';
				$html.='</div>';
			}
			if(count($result_eq_h)>0){
				$html.='<div class="row">';
					$html.='<div class="col-md-12 min-height" style="padding-right: 1px;padding-left: 1px;">';
						$html.='<a class="option_ct bloques_cli shadowx vinculo_a" data-url="'.base_url().'Icha/cli_servicio/'.$codigo.'/3/0/0?row='.$row_reg.'">Otros</a>';//esta tambien abilitado cli_servicio_pre en dado caso que se requiera
					$html.='</div>';
				$html.='</div>';
			}
			echo $html;
		?>
		
		<div class="row row_fixed">
			<div class="col-md-6 col-sm-6 col-6"><a href="<?php echo base_url().'Icha/cli_verif/'.$codigo;?>" class="btn btn-danger btn-sm shadowx">Regresar</a></div>
			<div class="col-md-6 col-sm-6 col-6"></div>
		</div>
	
	<script data-cp-orig="cdn.jquery.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <link href="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.js"></script>

    	<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">


    <script type="text/javascript">
    	$(document).ready(function($) {
    		<?php if($row_reg==1){ ?>
    			const elemento = document.querySelector('.option_ct');
    			elemento.click();
    		<?php } ?>
    		$('.vinculo_a').click(function(event) {
    			var url =$(this).data('url');
    			if(url!='#'){
    				$('body').loading({theme: 'dark',message: 'Procesando...'});
    			
	    			console.log(url);
	    			setTimeout(function(){ 
	    				$(location).attr('href',url);
	    			}, 1000);
    			}
    			
    			
    		});
    	});
    </script>
</body>
</html>