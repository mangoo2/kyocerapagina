<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
    <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
    <link href="<?php echo base_url(); ?>public/css/styles.css?v=<?php echo date('YmdHi');?>" type="text/css" rel="stylesheet">
    <style type="text/css">
    	.no_cli{
                background: url(<?php echo base_url()?>/public/img/cli_no.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }
        .yes_cli{
                background: url(<?php echo base_url()?>/public/img/cli_si.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }
    </style>
</head>
<body>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct no_cli shadowx" href="<?php echo base_url()?>Icha/chat_no_cli">No soy cliente</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct yes_cli shadowx" href="<?php echo base_url()?>Icha/verif_cli">Exclusivo clientes</a>
			</div>
		</div>
	
	<script data-cp-orig="cdn.jquery.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <link href="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.js"></script>

</body>
</html>