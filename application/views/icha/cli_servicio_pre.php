<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<meta content="width=device-width,initial-scale=1.0" name=viewport>
	<title></title>
	<link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
    <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
    <link href="<?php echo base_url(); ?>public/css/styles.css?v=<?php echo date('YmdHis');?>" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome/css/all.min.css" type="text/css" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/styles_ser.css" type="text/css" >
    <style type="text/css">
        .servicio_cli{
            background: url(<?php echo base_url()?>/public/img/1impresora.svg) white;
            background-size: 12%;background-repeat: no-repeat;background-position-x: 95%;background-position-y: center;
        }
    	.custom-checkbox {
	        width: 30px;height: 30px;
	        background-image: url('<?php echo base_url()?>/public/img/checkbox-unchecked.svg'); /* Imagen cuando no está seleccionado */
	        background-size: cover;display: inline-block;
	    }

	    .form-check-input:checked + .custom-checkbox {
	        background-image: url('<?php echo base_url()?>/public/img/check.svg'); /* Imagen cuando está seleccionado */
	    }
	    .option_ct {
		    height: auto;
		}
    </style>

</head>
<script type="text/javascript">
	        var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
	        var csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
	    </script>
<body class="container" style="padding-left: 8px;padding-right: 3px;">
	<?php echo form_open('Icha/verif_cli'); ?>
	<?php echo form_close(); ?>
	<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
	<input type="hidden" id="idc" value="<?php echo $idcliente;?>">
	<input type="hidden" id="codigo" value="<?php echo $codigo;?>">
	<input type="hidden" id="csrfName" value="<?php echo $this->security->get_csrf_token_name(); ?>">
	<input type="hidden" id="csrfHash" value="<?php echo $this->security->get_csrf_hash(); ?>">
	<input type="hidden" id="tiporcv" value="<?php echo $tiporcv;?>">
		<div class="row">
			<div class="col-md-12" >
				Numero de cliente: <b><?php echo $idcliente;?></b><br>
				Solicitud servicio tecnico<br>
				<?php 

					
					$idventa=0;
				?>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct servicio_cli shadowx c_red" style="font-size: 13px;padding-right: 38px;">Selecciona los equipos que requieran servicio</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct no_cli shadowx vinculo_a" data-url="<?php echo base_url().'Icha/cli_servicio_ss/'.$codigo.'/3/'.$group.'/'.$idg.'?row='.$row_reg;?>">No conozco mi serie o modelo</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct yes_cli shadowx vinculo_a" data-url="<?php echo base_url().'Icha/cli_servicio/'.$codigo.'/'.$tiporcv.'/'.$group.'/'.$idg.'?row='.$row_reg;?>">Tengo el modelo o serie</a>
			</div>
		</div>
		<div class="row row_fixed">
			<div class="col-md-6 col-sm-6 col-6"><a href="<?php 
				$url=base_url().'Icha/cli_servicio_group/'.$codigo;
				if(isset($_GET['row'])){
					if($_GET['row']==1){
						$url=base_url().'Icha/cli_verif/'.$codigo;
					}
				}
			echo $url;?>" class="btn btn-danger btn-sm shadowx">Regresar</a></div>
			
		</div>
		
		
	
	<script data-cp-orig="cdn.jquery.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <link href="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/datatable/jquery.dataTables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/datatable/jquery.dataTables.min.css">
	
	<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">

    <script src="<?php echo base_url(); ?>public/js/cli_servicio.js?v=<?php echo date('YmdHis')?>"></script>
    <script type="text/javascript">
    	$(document).ready(function($) {
    		<?php if($row_reg==1){ ?>
    			const elemento = document.querySelector('.option_ct');
    			elemento.click();
    		<?php } ?>
    		$('.vinculo_a').click(function(event) {
    			var url =$(this).data('url');
    			if(url!='#'){
    				$('body').loading({theme: 'dark',message: 'Procesando...'});
    			
	    			console.log(url);
	    			setTimeout(function(){ 
	    				$(location).attr('href',url);
	    			}, 1000);
    			}
    			
    			
    		});
    	});
    </script>
    	
</body>
</html>