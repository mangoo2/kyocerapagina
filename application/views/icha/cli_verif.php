<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
    <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
    <link href="<?php echo base_url(); ?>public/css/styles.css?v=<?php echo date('YmdHis');?>" type="text/css" rel="stylesheet">
    	
    <style type="text/css">
    	
        .servicio_cli{
                background: url(<?php echo base_url()?>/public/img/1impresora.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }
        .bloques_cli{
                background: url(<?php echo base_url()?>/public/img/bloques.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }
        .bloques_cli2{
                background: url(<?php echo base_url()?>/public/img/ejecutivo.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }
        .bloques_cli3{
                background: url(<?php echo base_url()?>/public/img/estatus_ser.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }

        
    </style>
</head>
<body class="container">
		<div class="row">
			<div class="col-md-12" style="text-align: center;">
				Numero de cliente: <b><?php echo $idcliente;?></b>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct servicio_cli shadowx vinculo_a" data-url="<?php echo base_url().'Icha/cli_servicio_group/'.$codigo;?>">Solicitar Servicio</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct bloques_cli shadowx vinculo_a" data-url="#">Ver mis contratos</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct bloques_cli shadowx vinculo_a" data-url="#">Ver mis facturas</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct bloques_cli2 shadowx vinculo_a" data-url="<?php echo base_url().'Icha/cli_chat/'.$codigo;?>">Chatear con un Asesor</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct bloques_cli3 shadowx vinculo_a" data-url="<?php echo base_url().'Icha/cli_estatus_ser/'.$codigo;?>">Estatus de mi servicio</a>
			</div>
		</div>
		
		
	
	<script data-cp-orig="cdn.jquery.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <link href="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.js"></script>

    	<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">
    <script type="text/javascript">
    	$(document).ready(function($) {
    		$('.vinculo_a').click(function(event) {
    			var url =$(this).data('url');
    			if(url!='#'){
    				$('body').loading({theme: 'dark',message: 'Procesando...'});
    			
	    			console.log(url);
	    			setTimeout(function(){ 
	    				$(location).attr('href',url);
	    			}, 1000);
    			}
    			
    			
    		});
    	});
    </script>
</body>
</html>