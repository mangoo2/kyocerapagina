<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
    <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
    <link href="<?php echo base_url(); ?>public/css/styles.css?v=<?php echo date('YmdHis');?>" type="text/css" rel="stylesheet">
    <style type="text/css">
    	
        .chat_venta{
                background: url(<?php echo base_url()?>/public/img/chat_venta.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }
        .chat_con{
                background: url(<?php echo base_url()?>/public/img/chat_con.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }
         .chat_pol{
                background: url(<?php echo base_url()?>/public/img/chat_pol.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }
        .bloques_cli2{
                background: url(<?php echo base_url()?>/public/img/ejecutivo.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;
                background-position-x: 95%;
                background-position-y: center;
            }

        
    </style>
</head>
<body class="container">
		<div class="row">
			<div class="col-md-12" style="text-align: center;">
				Numero de cliente: <b><?php echo $idcliente;?></b><br>
				Seleccione un departamento
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct chat_venta shadowx vinculo_a" data-url="<?php echo base_url().'Icha/cli_chat_select/1/'.$codigo;?>">Ventas</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct chat_con shadowx vinculo_a" data-url="<?php echo base_url().'Icha/cli_chat_select/2/'.$codigo;?>">Contratos / Arrendamiento</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct chat_pol shadowx vinculo_a" data-url="<?php echo base_url().'Icha/cli_chat_select/3/'.$codigo;?>">Pólizas</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct bloques_cli2 shadowx vinculo_a" data-url="<?php echo base_url().'Icha/cli_chat_select/4/'.$codigo;?>">Servicio Técnico</a>
			</div>
		</div>
		
	<div class="row row_fixed">
			<div class="col-md-6 col-sm-6 col-6"><a href="<?php echo base_url().'Icha/cli_verif/'.$codigo;?>" class="btn btn-danger btn-sm shadowx">Regresar</a></div>
			<div class="col-md-6 col-sm-6 col-6">
				
		</div>
	
	<script data-cp-orig="cdn.jquery.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <link href="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">
    <script type="text/javascript">
    	$(document).ready(function($) {
    		
    	});
    </script>
    <script type="text/javascript">
    	$(document).ready(function($) {
	    	<?php 
	    		$hora=date('G');
	    		//echo '//'.$hora;
	    		if($hora<8 || $hora>17){

	    			?>
	    			$('body').loading({theme: 'dark',message: 'No hay ningun asesor disponible.<br>Nuestro horario de atencion es de 08:00 am a 06:00 pm'});<?php
	    		}
	    	?>
	    	$('.vinculo_a').click(function(event) {
    			var url =$(this).data('url');
    			if(url!='#'){
    				$('body').loading({theme: 'dark',message: 'Conectando con un asesor, por favor espera…'});
    			
	    			console.log(url);
	    			setTimeout(function(){ 
	    				$(location).attr('href',url);
	    			}, 1000);
    			}
    			
    			
    		});
    	});
    </script>
    <style type="text/css">
    	<?php 
    		$hora=date('G');
    		if($hora<8 || $hora>17){
    			?>
    			.row_fixed{
    				z-index: 4;
    			}
    			#iniciarchat{
    				display: none;
    			}
    			<?php
    		}
    	?>
    </style>
</body>
</html>