<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
    <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
    <link href="<?php echo base_url(); ?>public/css/styles.css?v=<?php echo date('YmdHi');?>" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/styles_ser.css" type="text/css" >
    <style type="text/css">
    	.servicio_cli{
            background: url(<?php echo base_url()?>/public/img/1impresora.svg) white;
            background-size: 12%;background-repeat: no-repeat;background-position-x: 95%;background-position-y: center;
        }
    	.custom-checkbox {
	        width: 30px;height: 30px;
	        background-image: url('<?php echo base_url()?>/public/img/checkbox-unchecked.svg'); /* Imagen cuando no está seleccionado */
	        background-size: cover;display: inline-block;
	    }
	    .custom-checkbox1 {
	        width: 30px;height: 30px;
	        background-image: url('<?php echo base_url()?>/public/img/chat_venta.svg'); /* Imagen cuando no está seleccionado */
	        background-size: cover;display: inline-block;
	    }
	    .custom-checkbox4 {
	        width: 30px;height: 30px;
	        background-image: url('<?php echo base_url()?>/public/img/ejecutivo.svg'); /* Imagen cuando no está seleccionado */
	        background-size: cover;display: inline-block;
	    }

	    .form-check-input:checked + .custom-checkbox1,.form-check-input:checked + .custom-checkbox4 {
	        background-image: url('<?php echo base_url()?>/public/img/check.svg'); /* Imagen cuando está seleccionado */
	    }
	    label .error, label.error{
	    	color:red;
	    }
	    input .error, input.error{
	    	color: red;

	    }
	    #iniciarchat{
	    	width: 100%;
	    }
    </style>
</head>
<body class="container">
	<input type="hidden" id="base_url" value="<?php echo base_url();?>">
		<div class="row">
			<div class="col-md-12" style="text-align: center;">
				<!--Numero de cliente: <b><?php echo $idcliente;?></b><br>-->
				Seleccione Departamento
			</div>
		</div>
		<?php 
            $attributes = array('id' => 'form_newcli');
            echo form_open('#',$attributes); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label for="nom"></label>	
						<input type="tex" name="nom" id="nom" placeholder="Tu nombre"  class="form-control" required>
					</div>
					<div class="form-group">
						<label for="email"></label>	
						<input type="email" name="email" id="email" placeholder="Correo electrónico" class="form-control" required>
					</div>
					<div class="form-group">
						<label for="tel"></label>	
						<input type="tel" name="tel" id="tel" placeholder="Teléfono" class="form-control" required>
					</div>
					<div class="form-group">
						<label for="tel"></label>	
						<textarea type="text" name="motivo" id="motivo" placeholder="Motivo por el cual nos contactas" class="form-control" required></textarea>
					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-6" style="padding-right: 4px;">
					<label for="id_servicio_c_1">
						<div class="col-md-12 min-height option_ct shadowx">
							<div>
								<div class="col3">
									<p><span class="c_red">Ventas</span></p>
								</div>
								<div class="col2">
									<input class="form-check-input id_equipo_ser" type="radio" name="servicio_c" id="id_servicio_c_1" value="1" required>
									<span class="custom-checkbox1"></span>
								</div>
							</div>
						</div>
					</label>
				</div>
				<div class="col-md-6 col-sm-6 col-6" style="padding-left: 4px;">
					<label for="id_servicio_c_2">
						<div class="col-md-12 min-height option_ct shadowx">
							<div>
								<div class="col3">
									<p><span class="c_red">Servició técnico</span></p>
								</div>
								<div class="col2">
									<input class="form-check-input id_equipo_ser" type="radio" name="servicio_c" id="id_servicio_c_2" value="4" required>
									<span class="custom-checkbox4"></span>
								</div>
							</div>
						</div>
					</label>
				</div>
			</div>
		<?php echo form_close(); ?>
		<div class="row row_fixed">
			<div class="col-md-6 col-sm-6 col-6"><a href="<?php echo base_url().'Icha/?ses=1&ttp=1';?>" class="btn btn-danger btn-sm shadowx">Regresar</a></div>
			<div class="col-md-6 col-sm-6 col-6"><button id="iniciarchat" class="btn btn-danger btn-sm shadowx">Iniciar chat</a></button>
				
		</div>
		
	
	<script data-cp-orig="cdn.jquery.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <link href="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/jquery_validate/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">

    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/new_cli.js" ></script>

    <script type="text/javascript">
    	$(document).ready(function($) {
    	<?php 
    		$hora=date('G');
    		//echo '//'.$hora;
    		if($hora<8 || $hora>17){

    			?>
    			$('body').loading({theme: 'dark',message: 'No hay ningun asesor disponible.<br>Nuestro horario de atencion es de 08:00 am a 06:00 pm'});<?php
    		}
    	?>
    	});
    </script>
    <style type="text/css">
    	<?php 
    		$hora=date('G');
    		if($hora<8 || $hora>17){
    			?>
    			.row_fixed{
    				z-index: 4;
    			}
    			#iniciarchat{
    				display: none;
    			}
    			<?php
    		}
    	?>
    </style>
</body>
</html>