<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
    <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
    <link href="<?php echo base_url(); ?>public/css/styles.css?v=<?php echo date('YmdHis');?>" type="text/css" rel="stylesheet">
    <style type="text/css">
    	
        .yes_cli{
                background: url(<?php echo base_url()?>/public/img/cli_si.svg) white;
                background-size: 81%;
    background-repeat: no-repeat;
    background-position: center;
            }
        .t_c_red{
        	color:red;
        }
        .t_c_green{
        	color:green;
        }
        .not_img{
        	width: 41px;
        }
        .not_img_loading{
        	text-align: center;
        }
        .not_img_loading img{
        	width: 30px;
        }
    </style>
</head>
<body class="container">
		<div class="row">
			<div class="col-md-12 min-height">
				<?php echo form_open('Icha/verif_cli'); ?>
					<label style="font-size: 11px;">Introduce tú número de cliente y presiona enter</label>
				    <div class="input-group input_form_ct_group shadowx">
					  
					  <input type="text" name="codigo" id="codigo" class="input_form_ct  form-control" placeholder="Introduce tu numero de cliente" >
					  <div class="input-group-prepend icon_ct yes_cli">
					    
					  </div>
					</div>
					<div class="col-md-12" style="text-align:center;">
						<button type="submit" class="btn btn-secondary" style="margin-top: 10px;">Validar</button>
					</div>
				    

				<?php echo form_close(); ?>
			</div>
			<?php
				$html='';
					//$html='Estatus: '.$estatus;
				//if($estatus!=''){
					//$html='Estatus1: '.$estatus;
					if($estatus=='0'){
						$html.='<div class="col-md-9 col-sm-9 col-9 t_c_red">';
						$html.='El numero de cliente ingresado no existe';
						$html.='</div>';
						$html.='<div class="col-md-3 col-sm-3 col-3">';
						$html.='<img src="'.base_url().'public/img/equis.svg" class="not_img">';
						$html.='</div>';
					}
					if($estatus==1){
						$html.='<div class="col-md-9 col-sm-9 col-9 t_c_green">';
						$html.='El numero de cliente ingresado es correcto';
						$html.='</div>';
						$html.='<div class="col-md-3 col-sm-3 col-3">';
						$html.='<img src="'.base_url().'public/img/check.svg" class="not_img">';
						$html.='</div>';

						$html.='<div class="col-md-12 not_img_loading">';
						$html.='<img src="'.base_url().'public/img/ring.gif">';
						$html.='</div>';
					}
					
				//}
				echo $html;
			?>
		</div>
		<div class="row row_fixed">
			<div class="col-md-6 col-sm-6 col-6"><a href="<?php echo base_url().'Icha/?ses=1&ttp=1';?>" class="btn btn-danger btn-sm shadowx">Regresar</a></div>
			<!--<div class="col-md-6 col-sm-6 col-6"><a type="button" class="btn btn-danger shadowx">Siguiente</a></div>-->
		</div>
		
	
	<script data-cp-orig="cdn.jquery.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <link href="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.js"></script>
    <script type="text/javascript">
    	$(document).ready(function($) {
    		<?php 
    		if($estatus==1){
    			?>
    				setTimeout(function(){ 
    					location.href ="<?php echo base_url()?>Icha/cli_verif/<?php echo $codigo;?>";
    				}, 2000);
    			<?php
    		}
    		?>
    	});
    </script>
</body>
</html>