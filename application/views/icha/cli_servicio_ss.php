<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<meta content="width=device-width,initial-scale=1.0" name=viewport>
	<title></title>
	<link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
    <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
    <link href="<?php echo base_url(); ?>public/css/styles.css?v=<?php echo date('YmdHis');?>" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome/css/all.min.css" type="text/css" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/styles_ser.css" type="text/css" >
    <style type="text/css">
        .servicio_cli{
            background: url(<?php echo base_url()?>/public/img/1impresora.svg) white;
            background-size: 12%;background-repeat: no-repeat;background-position-x: 95%;background-position-y: center;
        }
    	.custom-checkbox {
	        width: 30px;height: 30px;
	        background-image: url('<?php echo base_url()?>/public/img/checkbox-unchecked.svg'); /* Imagen cuando no está seleccionado */
	        background-size: cover;display: inline-block;
	    }

	    .form-check-input:checked + .custom-checkbox {
	        background-image: url('<?php echo base_url()?>/public/img/check.svg'); /* Imagen cuando está seleccionado */
	    }
	    .option_ct {
		    height: auto;
		}
		thead{
			display: none;
		}
		table.dataTable.no-footer {
		    border-bottom: 1px solid rgb(0 0 0 / 0%);
		}
		.option_ct.selected {
			/* solo es para no tomar el estilo cuando este seleccionado en esta sseccion no aplica */
		    background: #fff;
		}
    </style>

</head>
<script type="text/javascript">
	        var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
	        var csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
	    </script>
<body class="container" style="padding-left: 8px;padding-right: 3px;">
	<?php echo form_open('Icha/verif_cli'); ?>
	<?php echo form_close(); ?>
	<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
	<input type="hidden" id="idc" value="<?php echo $idcliente;?>">
	<input type="hidden" id="codigo" value="<?php echo $codigo;?>">
	<input type="hidden" id="csrfName" value="<?php echo $this->security->get_csrf_token_name(); ?>">
	<input type="hidden" id="csrfHash" value="<?php echo $this->security->get_csrf_hash(); ?>">
	<input type="hidden" id="tiporcv" value="<?php echo $tiporcv;?>">
		<div class="row">
			<div class="col-md-12" >
				Numero de cliente: <b><?php echo $idcliente;?></b><br>
				Solicitud servicio tecnico<br>
				<?php 

					if($tiporcv==2){
						$pol_name='';
						$realizados=0;
						$disponibles=0;
						foreach ($result_eq as $item) {
							$pol_name=$item['nombre'];
							$realizados = $item['viewcont_realizados'];
							$disponibles = $item['viewcont_disponibles'];
						}
						echo "Poliza activa: <b>$pol_name</b><br>
								Servicios ocupados: <b>$realizados</b><br>
								Servicios disponibles: <b>$disponibles</b><br>
							 ";
					}
					$idventa=0;
				?>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct servicio_cli shadowx c_red" style="font-size: 13px;padding-right: 38px;">Selecciona los equipos que requieran servicio</a>
			</div>
		</div>
		<?php 
			$html='<table id="table_equipos"><thead><tr><th></th></tr></thead><tbody>';

				$html.='<tr><td class="td_equipo ocultartd"><span class="ocultaa">A</span>';
				$html.='<div class="option_ct shadowx">';
					$html.='<label for="id_eq_113_NA">';
						$html.='<div class="col-md-12 min-height ">';

							//$html.='<div class="col2">';
									//$html.='<img src="https://altaproductividadapr.com/uploads/equipos/r_-230111-135654-default.jpg" class="not_img">';
							//$html.='</div>';
							$html.='<div class="col3">';
								$html.='<p><b>Sin modelo y sin Modelo</b></p>';
								$html.='<p class="c_red"> </p>';
							$html.='</div>';
							$html.='<div class="col2">';
									$html.='<input class="form-check-input id_equipo_ser" type="checkbox" id="id_eq_113_NA" data-ideq="113" data-modelo="Sin Modelo" data-serie="" data-idser="0" 

											    data-tiporcv="3" data-idserv="0" data-idservd="0"  data-vc="0"
									>';
									$html.='<span class="custom-checkbox"></span>';
							$html.='</div>';
						$html.='</div>';
					$html.='</label>';
					$html.='<div class="">';
						$html.='<label>Motivo o falla del equipo.<b>Si es posible coloque la ubicacion del equipo</b></label>';
						$html.='<input type="text" class="form-control" id="motivo_equipo">';
					$html.='</div>';
				$html.='</div>';
				
				$html.='</td></tr>';
				//var_dump($result_eq);
			
			$html.='</tbody></table>';
			echo $html;
			
		?>
		
		<div class="row row_fixed_notificacion"></div>
		<div class="row row_fixed_date">
			<div class="col-md-12">
				<label>Fecha requeria de solicitud</label>
				<input type="date" id="date_servicio" class="form-control" style="max-width: 300px;">
			</div>
		</div>
		<div class="row row_fixed">
			<div class="col-md-6 col-sm-6 col-6"><a href="<?php 
				$url=base_url().'Icha/cli_servicio_group/'.$codigo;
				if(isset($_GET['row'])){
					if($_GET['row']==1){
						$url=base_url().'Icha/cli_verif/'.$codigo;
					}
				}
			echo $url;?>" class="btn btn-danger btn-sm shadowx">Regresar</a></div>
			<div class="col-md-6 col-sm-6 col-6">
				<a type="button" class="btn btn-danger btn-sm shadowx next_1" style="display: none;">Siguiente</a>
				<a type="button" class="btn btn-danger btn-sm shadowx next_2" style="display: none;">Generar Solicitud</a></div>
		</div>
		<div class="btb-search" id="btb-search" >
			<input type="tex" class="form-control" id="search_input" placeholder="Buscar" >
			<div class="btb-search_content" onclick="search_view()">
				<i class="fas fa-search"></i>
			</div>
			
		</div>
		
	
	<script data-cp-orig="cdn.jquery.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <link href="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/datatable/jquery.dataTables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/datatable/jquery.dataTables.min.css">
    <script src="<?php echo base_url(); ?>public/js/cli_servicio.js?v=<?php echo date('YmdHis')?>"></script>
    
    <script type="text/javascript">
    	$(document).ready(function($) {
    		$('#id_eq_113_NA').click();
    	});
    </script>
</body>
</html>