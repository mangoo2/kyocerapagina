<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
    <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
    <link href="<?php echo base_url(); ?>public/css/styles.css?v=<?php echo date('YmdHis');?>" type="text/css" rel="stylesheet">
    	
    	<script src="<?php echo base_url()?>public/plugins/time/modernizr.min.js"  ></script>
    <link href="<?php echo base_url(); ?>public/plugins/time/style.css" type="text/css" rel="stylesheet">

    <style type="text/css">
    	
        .t_c_r{
        	color: red;
        }
        .timeline-content p{
        	margin: 0px;
        }
        #timeline .timeline-item .timeline-icon {
		    /*background: #ee4d4d;*/
		    background: #9E9E9E;
		}
		#timeline .timeline-item .timeline-icon.activo {
		    background: #ee4d4d;
		}
		
		#timeline .timeline-item .timeline-content.right:before {
		    border-right: 7px solid #fff0 !important;
		}
		#timeline .timeline-item .timeline-content:before {
		    border-left: 7px solid #fff0 !important;
		}
		@media screen and (max-width: 768px) {
			#timeline .timeline-item .timeline-content:before, #timeline .timeline-item .timeline-content.right:before {
			    border-right: 7px solid #fff0 !important;
			}
			
		}
		

		#timeline .timeline-item .timeline-content{
			background: #fff0;
			font-size: 0px;
			-webkit-box-shadow: none;
			    -moz-box-shadow: none;
			    -ms-box-shadow: none;
			    box-shadow: none;
		}
		#timeline .timeline-item .timeline-content.activo{
			background: #fff;
			font-size: 12px;
			    -webkit-box-shadow: 0 3px 0 rgba(0, 0, 0, 0.1);
			    -moz-box-shadow: 0 3px 0 rgba(0, 0, 0, 0.1);
			    -ms-box-shadow: 0 3px 0 rgba(0, 0, 0, 0.1);
			    box-shadow: 0 3px 0 rgba(0, 0, 0, 0.1);
		}
        
    </style>
</head>
<body class="container">
		<div class="row">
			<div class="col-md-12" style="text-align: center;">
				Numero de cliente: <b><?php echo $idcliente;?></b>
			</div>
			<div class="col-md-12" style="text-align: center;">
				Servicio numero: <b><?php echo '<!--'.$tipo.'-->'.$idservicio;?></b> <img src="<?php echo base_url().'public/img/estatus_ser.svg'?>" style="width: 40px;">
				<?php
					/*
					echo '<<br>';
					echo '$tipo:'.$tipo.'<br>';
					echo '$personalid:'.$personalid.'<br>';
					echo '$g_status:'.$g_status.'<br>';
					echo '$idser:'.$idser.'<br>';
					*/
				?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 ">
				<!-------------------------------------------------------------------->
					<div id="timeline">
						<div class="timeline-item">
							<div class="timeline-icon activo">
								<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="21px" height="20px" viewBox="0 0 21 20" enable-background="new 0 0 21 20" xml:space="preserve">
								<g><path fill="#FFFFFF" d="M17.92,3.065l-1.669-2.302c-0.336-0.464-0.87-0.75-1.479-0.755C14.732,0.008,7.653,0,7.653,0v5.6 c0,0.096-0.047,0.185-0.127,0.237c-0.081,0.052-0.181,0.06-0.268,0.02l-1.413-0.64C5.773,5.183,5.69,5.183,5.617,5.215l-1.489,0.65 c-0.087,0.038-0.19,0.029-0.271-0.023c-0.079-0.052-0.13-0.141-0.13-0.235V0H2.191C1.655,0,1.233,0.434,1.233,0.97 c0,0,0.025,15.952,0.031,15.993c0.084,0.509,0.379,0.962,0.811,1.242l2.334,1.528C4.671,19.905,4.974,20,5.286,20h10.307 c1.452,0,2.634-1.189,2.634-2.64V4.007C18.227,3.666,18.12,3.339,17.92,3.065z M16.42,17.36c0,0.464-0.361,0.833-0.827,0.833H5.341 l-1.675-1.089h10.341c0.537,0,0.953-0.44,0.953-0.979V2.039l1.459,2.027V17.36L16.42,17.36z"/>
									</g>
								</svg>
							</div>
							<div class="timeline-content activo">
								<!--<h2>LOREM IPSUM DOLOR</h2>-->
								<p>En Proceso de asignacion</p>
							</div>
					</div>
					<div class="timeline-item">
						<div class="timeline-icon <?php if($personalid!=49){ echo 'activo';}?>">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="21px" height="20px" viewBox="0 0 21 20" enable-background="new 0 0 21 20" xml:space="preserve">
								<g><path fill="#FFFFFF" d="M17.92,3.065l-1.669-2.302c-0.336-0.464-0.87-0.75-1.479-0.755C14.732,0.008,7.653,0,7.653,0v5.6 c0,0.096-0.047,0.185-0.127,0.237c-0.081,0.052-0.181,0.06-0.268,0.02l-1.413-0.64C5.773,5.183,5.69,5.183,5.617,5.215l-1.489,0.65 c-0.087,0.038-0.19,0.029-0.271-0.023c-0.079-0.052-0.13-0.141-0.13-0.235V0H2.191C1.655,0,1.233,0.434,1.233,0.97 c0,0,0.025,15.952,0.031,15.993c0.084,0.509,0.379,0.962,0.811,1.242l2.334,1.528C4.671,19.905,4.974,20,5.286,20h10.307 c1.452,0,2.634-1.189,2.634-2.64V4.007C18.227,3.666,18.12,3.339,17.92,3.065z M16.42,17.36c0,0.464-0.361,0.833-0.827,0.833H5.341 l-1.675-1.089h10.341c0.537,0,0.953-0.44,0.953-0.979V2.039l1.459,2.027V17.36L16.42,17.36z"/>
									</g>
								</svg>
						</div>
						<div class="timeline-content right <?php if($personalid!=49){ echo 'activo';}?>">
							<!--<h2>LOREM IPSUM DOLOR</h2>-->
							<p>Servicio asignado</p>
							<p><span class="t_c_r">Tecnico</span>: <?php echo $tecnico;?></p>
							<p><span class="t_c_r">Fecha de atencion</span>:<?php echo $fecha;?></p>
							</div>
					</div>
					<div class="timeline-item">
							<div class="timeline-icon <?php if($g_status>=1){ echo 'activo';}?>">
								<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="21px" height="20px" viewBox="0 0 21 20" enable-background="new 0 0 21 20" xml:space="preserve">
								<g><path fill="#FFFFFF" d="M17.92,3.065l-1.669-2.302c-0.336-0.464-0.87-0.75-1.479-0.755C14.732,0.008,7.653,0,7.653,0v5.6 c0,0.096-0.047,0.185-0.127,0.237c-0.081,0.052-0.181,0.06-0.268,0.02l-1.413-0.64C5.773,5.183,5.69,5.183,5.617,5.215l-1.489,0.65 c-0.087,0.038-0.19,0.029-0.271-0.023c-0.079-0.052-0.13-0.141-0.13-0.235V0H2.191C1.655,0,1.233,0.434,1.233,0.97 c0,0,0.025,15.952,0.031,15.993c0.084,0.509,0.379,0.962,0.811,1.242l2.334,1.528C4.671,19.905,4.974,20,5.286,20h10.307 c1.452,0,2.634-1.189,2.634-2.64V4.007C18.227,3.666,18.12,3.339,17.92,3.065z M16.42,17.36c0,0.464-0.361,0.833-0.827,0.833H5.341 l-1.675-1.089h10.341c0.537,0,0.953-0.44,0.953-0.979V2.039l1.459,2.027V17.36L16.42,17.36z"/>
									</g>
								</svg>
							</div>
							<div class="timeline-content <?php if($g_status>=1){ echo 'activo';}?>">
								<!--<h2>LOREM IPSUM DOLOR</h2>-->
								<p>En proceso de atencion</p>
								<p><span class="t_c_r"><?php echo $tecnico;?>: </span>El tecnico arribo a la ubicacion y comenzo con su servicio</p>
							</div>
					</div>
					<div class="timeline-item">
						<div class="timeline-icon <?php if($g_status==2){ echo 'activo';}?>">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="21px" height="20px" viewBox="0 0 21 20" enable-background="new 0 0 21 20" xml:space="preserve"> <g> <path fill="#FFFFFF" d="M17.92,3.065l-1.669-2.302c-0.336-0.464-0.87-0.75-1.479-0.755C14.732,0.008,7.653,0,7.653,0v5.6 c0,0.096-0.047,0.185-0.127,0.237c-0.081,0.052-0.181,0.06-0.268,0.02l-1.413-0.64C5.773,5.183,5.69,5.183,5.617,5.215l-1.489,0.65 c-0.087,0.038-0.19,0.029-0.271-0.023c-0.079-0.052-0.13-0.141-0.13-0.235V0H2.191C1.655,0,1.233,0.434,1.233,0.97										c0,0,0.025,15.952,0.031,15.993c0.084,0.509,0.379,0.962,0.811,1.242l2.334,1.528C4.671,19.905,4.974,20,5.286,20h10.307 c1.452,0,2.634-1.189,2.634-2.64V4.007C18.227,3.666,18.12,3.339,17.92,3.065z M16.42,17.36c0,0.464-0.361,0.833-0.827,0.833H5.341 l-1.675-1.089h10.341c0.537,0,0.953-0.44,0.953-0.979V2.039l1.459,2.027V17.36L16.42,17.36z"/>
									</g>
								</svg>
						</div>
						<div class="timeline-content right <?php if($g_status==2){ echo 'activo';}?>">
								<!--<h2>LOREM IPSUM DOLOR</h2>-->
								<p>Servicio finalizado </p>
						</div>
					</div>
								
							</div>
				<!-------------------------------------------------------------------->
			</div>
		</div>
		
		
		
	
	<script data-cp-orig="cdn.jquery.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <link href="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.js"></script>

    	<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">
    <script type="text/javascript">
    	$(document).ready(function($) {
    		
    	});
    </script>
</body>
</html>