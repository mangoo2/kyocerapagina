<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<meta content="width=device-width,initial-scale=1.0" name=viewport>
	<title></title>
	<script data-cp-orig="cdn.jquery.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap_4_5.min.css" rel="stylesheet"  >
    <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle46.min.js"  ></script>
    <script src="<?php echo base_url()?>public/plugins/bootstrap/jquery.slim.min.js"  ></script>
    <link href="<?php echo base_url(); ?>public/css/styles.css?v=<?php echo date('YmdHis');?>" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome/css/all.min.css" type="text/css" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/styles_ser.css" type="text/css" >
    <style type="text/css">
        .servicio_cli{
            background: url(<?php echo base_url()?>/public/img/1impresora.svg) white;
            background-size: 12%;background-repeat: no-repeat;background-position-x: 95%;background-position-y: center;
        }
    	.custom-checkbox {
	        width: 25px;height: 22px;
	        background-image: url('<?php echo base_url()?>/public/img/checkbox-unchecked.svg'); /* Imagen cuando no está seleccionado */
	        background-size: cover;display: inline-block;
	    }

	    .form-check-input:checked + .custom-checkbox {
	        background-image: url('<?php echo base_url()?>/public/img/check.svg'); /* Imagen cuando está seleccionado */
	    }
	    .option_ct {
		    height: auto;
		}
		#accordionExample .card-header{
			padding: 5px 5px;
		}
		.title_direccion{
			padding: 3px 0px;
			font-size: 11px;
		}
		#accordionExample{
			margin-bottom: 58px;
		}
		#accordionExample .card-body{
			padding: 6px;
		}
		.option_ct_serie label{
			margin: 0px;
		}
		.col-series{
			display: none;
		}
		.col-series.activo{
			display: block !important;
		}
    </style>

</head>
<script type="text/javascript">
	        var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
	        var csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
	    </script>
<body class="container" style="padding-left: 8px;padding-right: 3px;">
	<?php echo form_open('Icha/verif_cli'); ?>
	<?php echo form_close(); ?>
	<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
	<input type="hidden" id="idc" value="<?php echo $idcliente;?>">
	<input type="hidden" id="codigo" value="<?php echo $codigo;?>">
	<input type="hidden" id="csrfName" value="<?php echo $this->security->get_csrf_token_name(); ?>">
	<input type="hidden" id="csrfHash" value="<?php echo $this->security->get_csrf_hash(); ?>">
	<input type="hidden" id="tiporcv" value="<?php echo $tiporcv;?>">
		<div class="row">
			<div class="col-md-12" >
				Numero de cliente: <b><?php echo $idcliente;?></b><br>
				Solicitud servicio tecnico<br>
				<?php 

					if($tiporcv==2){
						$pol_name='';
						$realizados=0;
						$disponibles=0;
						foreach ($result_eq as $item) {
							$pol_name=$item['nombre'];
							$realizados = $item['viewcont_realizados'];
							$disponibles = $item['viewcont_disponibles'];
						}
						echo "Poliza activa: <b>$pol_name</b><br>
								Servicios ocupados: <b>$realizados</b><br>
								Servicios disponibles: <b>$disponibles</b><br>
							 ";
					}
					$idventa=0;
					$eq_dir=0;
				?>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-12 min-height">
				<a class="option_ct servicio_cli shadowx c_red" style="font-size: 13px;padding-right: 38px;">Selecciona los equipos que requieran servicio</a>
			</div>
		</div>
<div class="accordion" id="accordionExample">
<?php 
	$iddir='g_0';
	$row=0;
	$totalregistros=count($result_eq);
	$html='';
	foreach ($result_eq as $item) {
		if($iddir!=$item['iddir']){
			$iddir=$item['iddir'];
			$collapsed_inicial=1;
		}else{
			$collapsed_inicial=0;
		}
		if($eq_dir!=$item['eq_dir']){
			$eq_dir=$item['eq_dir'];
			$row_inicial=1;
		}else{
			$row_inicial=0;
		}
		

		if($row>0){
			if($row_inicial==1){
			//$html.='<div class="c_red f-s-10" > '.$item['ubicacion'] .' / '.$item['iddirecc'] .'</b></div>';			
								$html.='</div></div>';
						$html.='</div>';

			
  			}
  			if($collapsed_inicial==1){
  				$html_collapsed='</div>
    </div>
  </div>';
  			$html.=$html_collapsed;
  			}
		}

		if($collapsed_inicial==1){
			$html_collapsed='<div class="card">
			    <div class="card-header" id="heading_'.$row.'">
			      <h2 class="mb-0">
			        <button class="btn btn-link btn-block text-left collapsed title_direccion" type="button" data-toggle="collapse" data-target="#direccion_'.$row.'" aria-expanded="false" aria-controls="direccion_'.$row.'">
			          '.$item['iddirecc'].'
			        </button>
			      </h2>
			    </div>

			    <div id="direccion_'.$row.'" class="collapse" aria-labelledby="direccion_'.$iddir.'" data-parent="#accordionExample" style="">
			      <div class="card-body">';
			      $html.=$html_collapsed;
		}
			//===========================================================
		if($row_inicial==1){
        $html.='';
        $html.='<div class="">';
          
            $html.='<div class="col-md-12 min-height option_ct shadowx">';

              //$html.='<div class="col2">';
              //    $html.='<img src="https://altaproductividadapr.com/uploads/equipos/'.$item['foto'].'" class="not_img">';
              //$html.='</div>';
              $html.='<div>';
              	$fun_clic="show_hide_ser('$eq_dir')";
                $html.='<div class="col3" onclick="'.$fun_clic.'">';
                  $html.='<p><b>'.$item['modelo'].'</b></p>';                                 
                $html.='</div>';
              $html.='</div>';
              $html.='<div class="col-series '.$eq_dir.'">';
        }
				$html.='<div class="option_ct_serie ocultartd">';
					$html.='<label for="id_eq_'.$item['idEquipo'].'_'.$item['serie'].'_'.$item['tiporcv'].'">';
						$html.='<div class="col3">';
							$html.='<p><span class="c_red">'.$item['serie'].'</span></p>';																	
						$html.='</div>';
						$html.='<div class="col1">';
								$html.='<input class="form-check-input id_equipo_ser" type="checkbox" id="id_eq_'.$item['idEquipo'].'_'.$item['serie'].'_'.$item['tiporcv'].'" data-ideq="'.$item['idEquipo'].'" data-modelo="'.$item['modelo'].'" data-serie="'.$item['serie'].'" data-idser="'.$item['serieId'].'" data-tiporcv="'.$item['tiporcv'].'" data-idserv="'.$item['id'].'" data-idservd="'.$item['polid'].'"  data-vc="'.$item['viewcont'].'"  >';
								$html.='<span class="custom-checkbox"></span>';
						$html.='</div>';
					$html.='</label>';
					$html.='<div class="div_motivo">';
								$html.='<label>Motivo o falla del equipo</label>';
								$html.='<input type="text" class="form-control" id="motivo_equipo">';
					$html.='</div>';
				$html.='</div>';
			//===========================================================
		$row++;
		if(intval($row)==intval($totalregistros)){
			//$html.='<div class="c_red f-s-10" >total'.$item['ubicacion'] .' / '.$item['iddirecc'] .'</b></div>';
						$html.='</div></div>';
				$html.='</div>';

			$html_collapsed='</div>
			    </div>
			  </div>';
			  $html.=$html_collapsed;
		}
	}
	echo $html;

	log_message('error', $html);
?>
</div>



		
		
		<div class="row row_fixed_notificacion">zzzzzzzzzzzzzzzz</div>
		<div class="row row_fixed_date">
			<div class="col-md-12">
				<label>Fecha requeria de solicitud</label>
				<input type="date" id="date_servicio" class="form-control" style="max-width: 300px;">
			</div>
		</div>
		<div class="row row_fixed">
			<div class="col-md-6 col-sm-6 col-6"><a href="<?php 
				$url=base_url().'Icha/cli_servicio_group/'.$codigo;
				if(isset($_GET['row'])){
					if($_GET['row']==1){
						$url=base_url().'Icha/cli_verif/'.$codigo;
					}
				}
			echo $url;?>" class="btn btn-danger btn-sm shadowx">Regresar</a></div>
			<div class="col-md-6 col-sm-6 col-6">
				<a type="button" class="btn btn-danger btn-sm shadowx next_1" style="display: none;">Siguiente</a>
				<a type="button" class="btn btn-danger btn-sm shadowx next_2" style="display: none;">Generar Solicitud</a></div>
		</div>
		<div class="btb-search" id="btb-search" >
			<input type="tex" class="form-control" id="search_input" placeholder="Buscar" >
			<div class="btb-search_content" onclick="search_view()">
				<i class="fas fa-search"></i>
			</div>
			
		</div>
		
	
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <link href="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/datatable/jquery.dataTables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/datatable/jquery.dataTables.min.css">
    <script src="<?php echo base_url(); ?>public/js/cli_servicio.js?v=<?php echo date('YmdHis')?>"></script>
    
    	
</body>
</html>