<style type="text/css">
	.emproduct.listitemline .emproduct_right .emproduct_right_attribute li:before {
content: "•";
}
</style>


				<!--<div id="breadCrumb"><span class="breadCrumb first  "><a href="https://www.kyoceraap.com/" title="Inicio"><span>Inicio</span></a></span><span class="breadCrumb  last "><a href="https://www.kyoceraap.com/Nuevos-articulos/" title="Nuevos artículos"><span> artículos</span></a></span></div>-->
				<div  class="start_topbox_right">
					<div class="row">
						<div class="col-md-12">
							<div id="breadCrumb"><span class="breadCrumb first  "><a href="https://www.kyoceraap.com/" title="Inicio"><span>Inicio</span></a></span><span class="breadCrumb  last "><a href="https://www.kyoceraap.com/Nuevos-articulos/" title="Nuevos artículos"><span> artículos</span></a></span></div>
						</div>
					</div>
					<div class="row bannerarea clear">
						<div class="col-md-12">
							<section class="lazy slider" id="lazy" data-sizes="50vw" style="height:auto;">
								<?php foreach ($infobaner as $itemb) { 
									$url_image='https://altaproductividadapr.com/uploads/baner/'.$itemb['imagen'];
									?>
									<div>
								      <img data-lazy="<?php echo $url_image;?>" data-srcset="<?php echo $url_image;?>" data-sizes="100vw">
								    </div>
								<?php }?>
							    
							  </section>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="margin-bottom: 113px; margin-top: 15px;">
							<table class="table" id="tablepro">
								<thead>
									<tr><th></th></tr>
								</thead>
								<!----------------------------------------->
									<?php
							foreach ($equipos as $item) {
								$title_equipo=$item['modelo'].' '.$item['especificaciones'];
								if($item['foto']==''){
									$url_image=base_url().'public/img/impresora.png';
								}else{
									$url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['foto'];	
								}
								
								$url_pro=base_url().'Equipos/articulo_equipo/'.$item['uuid'];
								$iditem=$item['id'];
						?>
						<tr><td>
						<li class="cell productData small-12 small-order-1">
							<div class="emproduct clear listitemline">
								<!--<form name="tobasket.productList-1" class="js-oxProductForm" action="<?php echo base_url();?>Account/addcar" method="post" >-->
									<?php echo form_open('Account/addcar'); ?>
									<input type="hidden" name="typeitem" value="1" >
                            <input type="hidden" name="iditem" value="<?php echo $iditem;?>" >
                            <input type="hidden" name="urlitem" value="<?php echo base_url();?>Inicio" >
                            <input type="hidden" name="am" value="1">
									<div class="emproduct_left_shadow"></div>
									<div class="emproduct_left">
										<a class="cp-picture-container cpGaProdproductList-1" href="<?php echo $url_pro;?>">
											<div class="picture-wrapper">
												<div class="line-cat-slider">
													<div  class="catSlider">
														<div data-cp-img-container="" class="cs-image" style="background-image: url(<?php echo $url_image;?>);"></div>
														
													</div>
												</div>
											</div>
											
										</a>
									</div>
									<div class="emproduct_right">
										<a id="productList-1" href="<?php echo $url_pro;?>" class="emproduct_right_title emsmoothtext cpGaProdproductList-1 productList-1" title="<?php echo $title_equipo;?>" style="margin-right: 40px; color: rgb(51, 51, 51);" data-color="rgb(51, 51, 51)">
											<?php echo $title_equipo;?>
										</a>
										<div class="clear emproduct_right_artnum_review">
											<div class="emproduct_right_artnum">
												SKU: <?php echo $item['noparte'];?>
											</div>
										</div>
										<div class="clear emproduct_left_attribute_price">
											<div class="emproduct_right_attribute">
												<ul><?php echo str_replace(array('<col1>','<col2>','<col3>'),array('<li><span class="title">',': </span><span class="content">','</span></li>'), $item['caracte'])?></ul>
											</div>
											<div class="emproduct_right_price">
												<?php if($this->session->userdata('logeado')==true){ ?>
												<div class="clear">
													<!--sse-->
													<div class="emproduct_right_price_left">
														<label class="price">$<?php echo round(($item['costo_renta']*1.16),2);?></label>
														<div class="emdeliverycost">
															<span class="deliverytext">Costo de envío:</span>
															<span class="deliveryvalue">$99.00</span>
														</div>
														<div class="emstock">Disponibles:<span><?php echo $item['stock'];?></span>pzas.</div>
													</div>
													<!--/sse-->
													<div class="emproduct_right_price_right">
														<div class="tobasketFunction clear">
															<div class="emproduct_tobasketbox  clear">
																<input type="hidden" name="am" value="1" wtx-context="B1FB0674-D7AC-49F3-8C6B-73DC054462C3">
																<?php if($item['stock']>0 and $item['costo_renta']>0){ ?>
																	<button id="toBasket_productList-1" type="submit" class="submitButton largeButton cartIcon">Agregar al carrito</button>
																<?php }else{ ?>
																	<a class="btn btn-primary btn-sm" onclick="addproconti(1,<?php echo $iditem;?>)">Solicitar Cotización</a>
																<?php } ?>
															</div>
															<div class="emproduct_comparebox clear">
																<div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_productList-1" data-id="listproduct_productList-1"></div>
															</div>
														</div>
													</div>
												</div>
												<?php }else{ ?>
					                                <div class="main-info clientelogueo">
					                                    Debes ser cliente para ver precios y existencias
					                                </div>
					                            <?php } ?> 
												
											</div>
										</div>
									</div>
								<!--</form>-->
								<?php echo form_close(); ?>
							</li></td></tr>
							<?php
							}
							?>
								<!----------------------------------------->
							</table>
						</div>
					</div>
					
					
					
					
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function($) {
				$(".lazy").slick({
				        autoplay: true,
				  autoplaySpeed: 10000,
      			});
				$('#tablepro').dataTable();
			});
		</script>