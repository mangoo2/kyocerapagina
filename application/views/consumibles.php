<style type="text/css">
	.emproduct.listitemline .emproduct_right .emproduct_right_attribute li:before {content: "•";}
</style>


				<!--<div id="breadCrumb"><span class="breadCrumb first  "><a href="https://www.kyoceraap.com/" title="Inicio"><span>Inicio</span></a></span><span class="breadCrumb  last "><a href="https://www.kyoceraap.com/Nuevos-articulos/" title="Nuevos artículos"><span> artículos</span></a></span></div>-->
				<div class="start_topbox_right">
					<div class="row">
						<div class="col-md-12">
							<div id="breadCrumb"><span class="breadCrumb first  "><a href="https://www.kyoceraap.com/" title="Inicio"><span>Inicio</span></a></span><span class="breadCrumb  last "><a href="https://www.kyoceraap.com/Nuevos-articulos/" title="Nuevos artículos"><span> artículos</span></a></span></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<section class="lazy slider" id="lazy" data-sizes="50vw" style="height:auto;">
								<?php foreach ($infobaner as $itemb) { 
									$url_image='https://altaproductividadapr.com/uploads/baner/'.$itemb['imagen'];
									?>
									<div>
								      <img data-lazy="<?php echo $url_image;?>" data-srcset="<?php echo $url_image;?>" data-sizes="100vw">
								    </div>
								<?php }?>
							    
							  </section>
						</div>
					</div>
					

					
					<div class="row">
						<div class="col-md-12" style="margin-bottom: 113px;">
							<table class="table" id="tablepro">
								<thead>
									<tr><th></th></tr>
								</thead>
								<!----------------------------------------->
									<?php
							foreach ($equipos as $item) {
								$html_pro='';
								$title_equipo=$item['modelo'];
								if($item['foto']==''){
									$url_image=base_url().'public/img/impresora.png';
								}else{
									$url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['foto'];	
								}
								
								$url_pro=base_url().'Consumibles/articulo/'.$item['uuid'];
								$iditem=$item['id'];
								$html_pro.='<tr><td>';
								$html_pro.='<li class="cell productData small-12 small-order-1">';
									$html_pro.='<div class="emproduct clear listitemline ">';
										//$html_pro.='<form name="tobasket.productList-1" class="js-oxProductForm" action="'.base_url().'Account/addcar" method="post" >';
											$html_pro.=form_open('Account/addcar');
											$html_pro.='<input type="hidden" name="typeitem" value="4" >';
				                            $html_pro.='<input type="hidden" name="iditem" value="'.$iditem.'" >';
				                            $html_pro.='<input type="hidden" name="urlitem" value="'.base_url().'Consumibles" >';
				                            $html_pro.='<input type="hidden" name="am" value="1">';
											$html_pro.='<div class="emproduct_left_shadow"></div>';
											$html_pro.='<div class="emproduct_left">';
												$html_pro.='<a class="cp-picture-container cpGaProdproductList-1" href="'.$url_pro.'">';
													$html_pro.='<div class="picture-wrapper">';
														$html_pro.='<div class="line-cat-slider">';
															$html_pro.='<div  class="catSlider">';
																$html_pro.='<div data-cp-img-container="" class="cs-image" style="background-image: url('.$url_image.');"></div>';
															$html_pro.='</div>';
														$html_pro.='</div>';
													$html_pro.='</div>';
												$html_pro.='</a>';
											$html_pro.='</div>';
											
											$html_pro.='<div class="emproduct_right">';
												$html_pro.='<a id="productList-'.$item['id'].'" href="'.$url_pro.'" class="emproduct_right_title emsmoothtext cpGaProdproductList-1 productList-1" title="'.$title_equipo.'" style="margin-right: 40px; color: rgb(51, 51, 51);" data-color="rgb(51, 51, 51)">'.$title_equipo.'</a>';
												$html_pro.='<div class="clear emproduct_right_artnum_review">';
													$html_pro.='<div class="emproduct_right_artnum">SKU: '.$item['parte'].'</div>';
												$html_pro.='</div>';
												$html_pro.='<div class="clear emproduct_left_attribute_price">';
													$html_pro.='<div class="emproduct_right_attribute">';
														$html_pro.='<ul>'.str_replace(array('<col1>','<col2>','<col3>'),array('<li><span class="title">',': </span><span class="content">','</span></li>'), $item['caracte']).'</ul>';
													$html_pro.='</div>';
													$html_pro.='<div class="emproduct_right_price">';
															if($this->session->userdata('logeado')==true){ 
														$html_pro.='<div class="clear">';
															$html_pro.='<!--sse-->';
															$html_pro.='<div class="emproduct_right_price_left">';
																$html_pro.='<label class="price">$ '.round($item['general']*1.16,2).'</label>';
																$html_pro.='<div class="emdeliverycost">';
																	$html_pro.='<span class="deliverytext">Costo de envío:</span>';
																	$html_pro.='<span class="deliveryvalue">$99.00</span>';
																$html_pro.='</div>';
																$html_pro.='<div class="emstock">Disponibles:<span>'.$item['stock'].'</span>pzas.</div>';
															$html_pro.='</div>';
															$html_pro.='<!--/sse-->';
															$html_pro.='<div class="emproduct_right_price_right">';
																$html_pro.='<div class="tobasketFunction clear">';
																	$html_pro.='<div class="emproduct_tobasketbox  clear">';
																		$html_pro.='<input type="hidden" name="am" value="1" wtx-context="B1FB0674-D7AC-49F3-8C6B-73DC054462C3">';
																		if($item['stock']>0 and $item['general']>0){ 
																			$html_pro.='<button id="toBasket_productList-'.$item['id'].'" type="submit" class="submitButton largeButton cartIcon">Agregar al carrito</button>';
																		}else{ 
																			$html_pro.='<a class="btn btn-primary btn-sm" onclick="addproconti(4,'.$iditem.')">Solicitar Cotización</a>';
																		}
																	$html_pro.='</div>';
																$html_pro.='</div>';
															$html_pro.='</div>';
														$html_pro.='</div>';
														 }else{ 
																	$html_pro.='<div class="main-info clientelogueo">';
									                                    $html_pro.='Debes ser cliente para ver precios y existencias';
									                                $html_pro.='</div>';
														} 
														
													$html_pro.=' </div>';
												$html_pro.=' </div>';
											$html_pro.=' </div>';
										//$html_pro.='</form>';
										$html_pro.=form_close();
										
									$html_pro.='</div>';
										
								$html_pro.='</li>';
								$html_pro.='</td></tr>';
								
								echo $html_pro;
							}
							?>
								<!----------------------------------------->

							</table>
						</div>
					</div>
					
					
					
						
						
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function($) {
				$(".lazy").slick({
				        autoplay: true,
				  autoplaySpeed: 10000,
      			});
				$('#tablepro').dataTable();
			});
		</script>