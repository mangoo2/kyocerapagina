<div id="page" class="sidebarLeft content ">
<div class="pageborder clear">
  <div id="sidebar" class="Left">
     <div class="cp-main-menu-place-holder">
        <div data-cp-vue-bind="main-menu" data-cp-options='{"css": ["desktop", "home"], "closeDelay" : 1000}'>
           <div class="cp-main-menu desktop home">
              <div class="main-menu"></div>
           </div>
        </div>
     </div>
  </div>
<div id="content">
<div class="cmsContent">
<h1>Quiénes somos</h1>
<div id="cms-banner">
<div id="fondo-banner"><br>
<br>
&nbsp;
<h3 style="margin-left:20px">¡Entra al mundo de la tecnología!</h3>
</div>
<div style="float:right"><br>
<img src="//www.cyberpuerta.mx/out/pictures/wysiwigpro/new/cyberpuerta.png" style="margin-right:20px"></div>
</div>
<div style="float:left">&nbsp;
<p>En Cyberpuerta creamos el lugar más conveniente para comprar electrónica en México. Nuestro sitio www.cyberpuerta.mx ya es el más funcional y surtido en electrónica de consumo / computación al nivel nacional. Junto con nuestros clientes, continuamos innovándonos para seguir ofreciéndote el lugar más conveniente para realizar sus compras.</p>
<div style="float:right; margin-left:20px"><img src="//www.cyberpuerta.mx/out/pictures/wysiwigpro/new/tablet-pc.png"></div>
<p>Constituida en el 2008, Cyberpuerta S.A. de C.V. participó en el programa de la Incubadora de Empresas del Tecnológico de Monterrey (ITESM) y fue certificada por la Asociación de Internet. Cyberpuerta es socio preferido de las mejores marcas y distribuye más de 40,000 productos nuevos, 100% originales y con la garantía del fabricante. Contamos con <a href="https://www.cyberpuerta.mx/Certificaciones/" target="_self">certificaciones</a> como canal de distribución de diferentes fabricantes.</p>
<p>Dentro de nuestra gama de productos encontrarás electrónica de consumo como son productos Apple, Televisores, Audio &amp; MP3, Foto &amp; Video, DVD &amp; Blu-Ray así como productos de computación como son Portátiles, PC’s, Cómputo, Monitores, Punto de Venta (POS), Impresoras &amp; Copiadoras, Telecomunicación y Software.</p>
</div>
<!--
<div style="clear:both">
<div style="float:left; width:46%">
<div style="float:left; margin-right:20px"><img alt="logotipo CMMI" src="//www.cyberpuerta.mx/out/pictures/wysiwigpro/certificaciones/CMMI-logotipo3.png" style="width:100%; max-width:120px;" /></div>
<p>Queremos brindar el mejor servicio para nuestros clientes, es por eso que obtuvimos la certificación CMMI, para asegurar que nuestros procesos de desarrollo se mantengan de acuerdo a los estándares de la industria.</p>
</div>
-->
<div style="float:left; width:46%; margin-bottom: 24px;">
<div style="float:left; margin-right:20px"><a href="https://www.ekomi.es/testimonios-cyberpuerta.html" target="new"><img src="//www.cyberpuerta.mx/out/pictures/wysiwigpro/new/ekomi_oro.png"></a></div>
<p>Miles de usuarios visitan nuestra página cada día y disfrutan de nuestro excelente nivel de servicio al cliente. Te invitamos a leer las experiencias de nuestros clientes al comprar en Cyberpuerta en el módulo independiente de <a href="https://www.ekomi.es/testimonios-cyberpuerta.html" target="new">eKomi</a>.</p>
</div>
<div style="clear:both">&nbsp;
<div style="float:left; margin-right:20px"><a href="https://www.facebook.com/Cyberpuerta"><img alt="Facebook Cyberpuerta" width="60" src="//www.cyberpuerta.mx/out/pictures/wysiwigpro/facebook.png"></a> <a href="https://twitter.com/CyberPuerta"><img alt="Twitter Cyberpuerta" width="60" src="//www.cyberpuerta.mx/out/pictures/wysiwigpro/twitter.png"></a>
<a href="https://discord.com/invite/wsbm5pZb7e"><img alt="Discord Cyberpuerta" width="60" src="https://www.cyberpuerta.mx/out/pictures/wysiwigpro/discord.png"></a>
</div>
<p style="line-height:60px">Búscanos en nuestras redes sociales y ponte en contacto con nosotros.</p>
</div>
<div id="special-quote" style="clear:both">&nbsp;
<p>Esperamos poder servirte en un futuro cercano,<br>
<strong>Tu equipo de Cyberpuerta.mx</strong></p>
</div>
<br>
<br>
</div>
</div>
</div>
</div>