<?php
require_once dirname(__FILE__) . '/TCPDF3/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF3/tcpdf.php';

  $logos = 'https://altaproductividadapr.com/public/img/alta.jpg';
  $logos2 = 'https://altaproductividadapr.com/public/img/kyocera.jpg';
  //=========================================================================
    //$GLOBALS['configuracionCotizacion']=$configuracionCotizacion;
    //$GLOBALS["fecha_crea"]=$fecha_crea;
    /*
    $GLOBALS["fecha_asig"]=$fecha_asig;
    $GLOBALS["tipo_serv"]=$tipo_serv;
    $GLOBALS["responsable_asig"]=$responsable_asig;
    $GLOBALS["hora_asig"]=$hora_asig;
    $GLOBALS["prioridad"]=$prioridad;
    $GLOBALS["descripcion_serv_fall"]=$descripcion_serv_fall;
    $GLOBALS["modelo"]=$modelo;
    $GLOBALS["serie"]=$serie;
    $GLOBALS["direcc_inst_serv"]=$direcc_inst_serv;
    $GLOBALS["tel"]=$tel;
    $GLOBALS["cel"]=$cel;
    $GLOBALS["correo"]=$correo;
    $GLOBALS["docum_acce"]=$docum_acce;
    $GLOBALS["observa_edi"]=$observa_edi;
    $GLOBALS["lectu_ini"]=$lectu_ini;
    $GLOBALS["km_veh_i"]=$km_veh_i;
    $GLOBALS["lectur_fin"]=$lectur_fin;
    $GLOBALS["km_veh_f"]=$km_veh_f;
    $GLOBALS["asesor_ser"]=$asesor_ser;
    $GLOBALS["gerencia"]=$gerencia;
    $GLOBALS["firma"]=$firma;
    $GLOBALS["comentarioejecutiva"]=$comentarioejecutiva;
    $GLOBALS["comentariotecnico"]=$comentariotecnico;
    $GLOBALS["nombre_empresa"]=$nombre_empresa;
    */

    
      
    
  //=========================================================================
//====================================================
class MYPDF extends TCPDF {
 
  public function Header() {

      
      //$this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';
    
      //<img src="http://facturacion33.adminfactura.com.mx/view/image/viamex_logob100.png" width="100px">
      //$this->writeHTML($html2, true, false, true, false, '');
      
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Servicio');
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('7', '10', '7');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('10');

// set auto page breaks
$pdf->SetAutoPageBreak(true, 10);//PDF_MARGIN_BOTTOM

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);
// add a page
//$pdf->AddPage('P', 'A4');

foreach ($infoser as $itemser) {
  $id_equipo=$itemser['idequipo'];
  $idRenta=$itemser['idRenta'];
  $idEquipo=$itemser['idEquipo_r'];
  $serieId=$itemser['serieId'];
  $pdf->AddPage('P', 'A4');
  $html='<style type="text/css">
          .t_c{text-align:center;}
          .t_b{font-weight: bold;}
          .bor_b{border-bottom: 1px solid black;}
          .bor_l{border-left: 1px solid black;}
          .bor_r{border-right: 1px solid black;}
          .bor_t{border-top: 1px solid black;}
          .tableb th{background-color:rgb(217,217,217);}
        </style>';
  $data['fecha_crea'] =$itemser['reg']; 
        
        if($itemser['tservicio_a']>0){
            $tipo_serv=$itemser['poliza'];
            //$tipoppp=0;
        }else{
            $tipo_serv=$itemser['servicio'];
            //$tipoppp=1;
        }
        $text_prioridad='';
        if ($itemser['prioridad']==1){
            $text_prioridad='Prioridad 1';
        }else if($itemser['prioridad']==2){
            $text_prioridad='Prioridad 2';
        }else if($itemser['prioridad']==3){
            $text_prioridad='Prioridad 3';
        }else if($itemser['prioridad']==4){
            $text_prioridad='Servicio Regular';
        }else{
            $text_prioridad='';
        } 
        $data['prioridad']=$text_prioridad;

  $html.='<table border="0">
            <tr>
              <td width="20%" class="t_c"><img src="'.$logos.'" width="80px"></td>
              <td width="60%" class="t_c">
                "ALTA PRODUCTIVIDAD, S.A DE C.V."<br>
                RFC: APR980122KZ6<br>
                Impresoras y Multifuncionales, Venta, Renta y Mantenimiento.<br>
                Domicilio Fiscal: Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. Mexico C.P.72090<br>
                Banco BANAMEX, Cuenta 7347132, Sucursal 826, CLABE 2668082673471320<br>
                Call Center 273 3400, 249 5393. www.kyoceraap.com

              </td>
              <td width="20%" class="t_c"><img src="'.$logos2.'" width="100px"></td>
            </tr>
          </table>
          <p></p>';
          /*
          $datosconsumiblesfolio = $this->ModeloServicio->getfoliosserviciosserie($idpdf,$serieId);
            if ($datosconsumiblesfolio->num_rows()>0) {
              $html.='<table class="tableb">
                        <tr>
                          <th>Cantidad</th>
                          <th>Marca</th>
                          <th>Modelo</th>
                          <th>parte</th>
                          <th>Folio</th>
                        </tr>
                      ';
                foreach ($datosconsumiblesfolio->result() as $item) {
                   $html.='
                        <tr>
                          <td>1</td>
                          <td>KYOCERA</td>
                          <td>'.$item->modelo.'</td>
                          <td>'.$item->parte.'</td>
                          <td>'.$item->foliotext.'</td>
                        </tr>
                      ';
                }
              $html.='</table>';
            }
            */
    
              


    $html.='<table class="tableb" cellpadding="4">
              <tr>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">INFORMACIÓN GENERAL DEL SERVICIO</th>
              </tr>
            </table>
            <table class="tableb" cellpadding="4">
              <tr>
                <th class="bor_l bor_t">FECHA DE CREACION </th>
                <td class="  bor_t">'.date('d-m-Y',strtotime($itemser['reg'])).'</td>
                <th class="  bor_t">RESPONSABLE ASIGNADO</th>
                <td class=" bor_r bor_t">'.$itemser['tecnico'].'</td>
              </tr>
              <tr>
                <th class="bor_l ">FECHA DE ASIGNACION </th>
                <td class="  ">'.date('d-m-Y',strtotime($itemser['fecha'])).'</td>
                <th class="  ">HORA DE ASIGNACION</th>
                <td class=" bor_r ">'.date('g:i a',strtotime($itemser['hora'])).'</td>
              </tr>
              <tr>
                <th class="bor_l bor_b">TIPO DE SERVICIO</th>
                <td class="  bor_b">'.$tipo_serv.'</td>
                <th class="  bor_b">PRIORIDAD</th>
                <td class=" bor_r bor_b">'.$text_prioridad.'</td>
              </tr>
            </table>
            <p></p>
            <table class="tableb" cellpadding="4">
              <tr>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">INFORMACIÓN DEL EQUIPO</th>
              </tr>
            </table>
            <table class="tableb" cellpadding="4">
              <tr>
                <th width="10%" class="bor_b bor_l bor_t">Modelo</th>
                <td width="15%" class="bor_b bor_t">'.$itemser["modelo"].'</td>
                <th width="10%" class="bor_b bor_t">Serie</th>
                <td width="10%" class="bor_b bor_t">'.$itemser["serie"].'</td>
                <th width="22%" class="bor_b bor_t">Descripción del servicio ó falla : </th>
                <td width="33%" class="bor_b bor_r bor_t">'.$itemser["tserviciomotivo"].'</td>
              </tr>';
              //if($comentarioinfo!=''){
              //  $html.='<tr><td colspan="6" class="bor_l bor_r">'.$comentarioinfo.'</td></tr>';
              //}
                if($itemser["tpoliza"]==22){
                  //$rentaequipos_accesorios = $this->ModeloCatalogos->rentaequipos_accesorios1($idRenta,$idEquipo,$id_equipo);
                  $url="https://altaproductividadapr.com/index.php/restserver/rentaequipos_accesorios1/$idRenta/$idEquipo/$id_equipo";
                  $rentaequipos_accesorios=$this->ModeloRest->consultaapiget($url);
                    
                    foreach ($rentaequipos_accesorios as $itemacc) {
                        //$equipos .='<tr><td>'.$itemacc->cantidad.' '.$itemacc->nombre.'</td><td>'.$itemacc->series.'</td><td></td></tr>';
                        $html.='<tr>
                                <th class="bor_b bor_l bor_t">Modelo Accesorio</th>
                                <td class="bor_b bor_t">'.$itemacc['nombre'].'</td>
                                <th class="bor_b bor_t">Serie</th>
                                <td class="bor_b bor_t bor_r" colspan="3">'.$itemacc['series'].'</td>
                              </tr>';
                    }
                    $arrayconsumibles=array();
                    //$rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible1($idRenta,$idEquipo,$id_equipo);
                    $url="https://altaproductividadapr.com/index.php/restserver/rentaequipos_consumible1/$idRenta/$idEquipo/$id_equipo";
                  $rentaequipos_consumible=$this->ModeloRest->consultaapiget($url);
                    
                    foreach ($rentaequipos_consumible as $item0) {
                        //$contrato_folio2 = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta3($idRenta,$item0->id_consumibles,$item0->id,$serieId); 
                      $id_consumibles=$item0['id_consumibles'];
                      $item0id=$item0['id'];
                        $url="https://altaproductividadapr.com/index.php/restserver/obtenerfoliosconsumiblesrenta3/$idRenta/$id_consumibles/$item0id/$serieId";
                  $contrato_folio2=$this->ModeloRest->consultaapiget($url);
                  //var_dump($contrato_folio2);
                        foreach ($contrato_folio2 as $itemx) {
                          $arrayconsumibles[$itemx->idcontratofolio]=array('modelo'=>$item0['modelo'],'parte'=>$item0['parte'],'folio'=>$itemx['foliotext']);
                            //if($idcontratofolio!=$itemx->idcontratofolio){
                                //$equipos .='<tr><td>'.$item0->modelo.' No° parte '.$item0->parte.'</td><td>'.$itemx->foliotext.'</td><td></td></tr>';
                                /*
                                $html.='<tr>
                                          <th class="bor_b bor_l bor_t">Modelo Consumible</th>
                                          <td class="bor_b bor_t">'.$item0->modelo.'</td>
                                          <th class="bor_b bor_t">Folio</th>
                                          <td class="bor_b bor_t bor_r" colspan="3">'.$itemx->foliotext.'</td>
                                        </tr>';
                                */
                            //}
                              
                              
                        }
                    }
                    foreach ($arrayconsumibles as $itemu) {
                        //$equipos .='<tr><td>'.$itemu['modelo'].' No° parte '.$itemu['parte'].'</td><td>'.$itemu['folio'].'</td><td></td></tr>';
                        $html.='<tr>
                                          <th class="bor_b bor_l bor_t">Modelo Consumible</th>
                                          <td class="bor_b bor_t">'.$itemu['modelo'].'</td>
                                          <th class="bor_b bor_t">Folio</th>
                                          <td class="bor_b bor_t bor_r" colspan="3">'.$itemu['folio'].'</td>
                                        </tr>';
                    }
                }
              
              
            $html.='</table>
            <table class="tableb" cellpadding="4">
              <tr>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">INFORMACION DEL CLIENTE </th>
              </tr>
            </table>
            <table class="tableb" cellpadding="4">
              <tr>
                <td width="30%" class=" bor_l bor_t">Nombre</td>
                <td width="70%" class=" bor_r bor_t">'.$itemser['empresa'].'</td>
              </tr>
              <tr>
                <td class=" bor_l ">Dirección de instalación/servicio</td>
                <td class=" bor_r ">'.$itemser['iddirecc'].'</td>
              </tr>
              <tr>
                <td class=" bor_l ">Teléfono</td>
                <td class=" bor_r ">'.$itemser['telefono'].'</td>
              </tr>
              <!--<tr>
                <td class=" bor_l ">Celular</td>
                <td class=" bor_r "></td>
              </tr>-->
              <tr>
                <td class=" bor_l ">Correo electrónico</td>
                <td class=" bor_r ">'.$itemser['correo'].'</td>
              </tr>';
              $docum_acce='';
              $docum_acce=$itemser['equipo_acceso'].' '.$itemser['documentacion_acceso'];
              if($itemser['equipo_acceso_info']!=null or $itemser['equipo_acceso_info']!=''){
                $docum_acce=$itemser['equipo_acceso_info'].' '.$itemser['documentacion_acceso_info'];
              }

      $html.='<tr>
                <td class="bor_b bor_l ">Documentos y accesorios para el ingreso</td>
                <td class="bor_b bor_r ">'.$docum_acce.'</td>
              </tr>
            </table>
            <p></p>
            <table class="tableb" cellpadding="4">
              <tr>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">Observaciones Técnico</th>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">Observaciones Ejecutiva</th>
              </tr>';
              $comentariotecnico='';
              if($itemser['pro_ser']>0){
                if($itemser['cot_refaccion']!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$itemser['pro_ser'].' '.$itemser['cot_refaccion'].'<br>';
                }
                if($itemser['cot_refaccion2']!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$itemser['pro_ser'].' '.$itemser['cot_refaccion2'].'<br>';
                }
                if($itemser['cot_refaccion3']!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$itemser['pro_ser'].' '.$itemser['cot_refaccion3'].'<br>';
                }
                
            }
            if($itemser['comentario']!=''){
                $comentariotecnico.='Observación General: '.$itemser['comentario'].'<br>';
            }
            if($itemser['comentario_tec']!=''){
                $comentariotecnico.='Observación Equipo: '.$itemser['comentario_tec'].'<br>';
            }
      $html.='<tr>
                <td class="t_c bor_b  bor_l bor_r bor_t">'.$comentariotecnico.'</td>
                <td class="t_c  bor_b bor_l bor_r bor_t">'.$itemser['comentariocal'].'</td>
              </tr>
              <tr>
                <td class="t_c   bor_l bor_r ">'.$itemser['qrecibio'].'</td>
                <td class="t_c   bor_l bor_r ">Asesor de servicio </td>
              </tr>
              <tr>
                <td class="t_c  bor_b bor_l bor_r ">Nombre de quien recibió</td>
                <td class="t_c  bor_b bor_l bor_r ">'.$itemser['tecnico'].'</td>
              </tr>

            </table>
            ';


$pdf->writeHTML($html, true, false, true, false, '');
}
  



$pdf->IncludeJS('print(true);');
$pdf->Output('Folios_.pdf', 'I');

?>