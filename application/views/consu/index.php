<link href="<?php echo base_url()?>public/css/styles_card_eq.css?v=<?php echo  date('Ymdgis');?>" rel="stylesheet"  >

<style type="text/css">
	.op_arre img{width: 100%;max-width: 160px;	}
	.op_arre{text-align: center;	}
	.op_arre p{padding: 0 15% 0 15%;font-weight: bold;font-size:15px;}
	.footer_eq{
		background: url(<?php echo base_url();?>public/img/logo2.svg);
		background-repeat: no-repeat;background-position: left;background-size: 100px;
	}
	.start_topbox .start_topbox_right {
		padding-top: 0px;
	}
	.eq_carac{
		width: 30px;
		height: 30px;
		margin-bottom: 12px;
	}
	.icon_1{

	}
	.icon_1{
		background: url(<?php echo base_url();?>public/img/equipo_caract_icon/1.webp);
		background-size: cover;
		margin-right: 25px;
    	margin-left: auto;
	}
	.icon_2{
		background: url(<?php echo base_url();?>public/img/equipo_caract_icon/2.png);
		background-size: cover;
		margin-right: 10px;
    	margin-left: auto;
	}
	.icon_3{
		background: url(<?php echo base_url();?>public/img/equipo_caract_icon/3.png);
		background-size: cover;
		margin-right: 0px;
    	margin-left: auto;
	}
	.icon_4{
		background: url(<?php echo base_url();?>public/img/equipo_caract_icon/4.png);
		background-size: cover;
		margin-right: 0px;
    	margin-left: auto;
	}
	.icon_5{
		background: url(<?php echo base_url();?>public/img/equipo_caract_icon/5.png);
		background-size: cover;
		margin-right: 10px;
    	margin-left: auto;
	}
	.icon_6{
		background: url(<?php echo base_url();?>public/img/equipo_caract_icon/6.png);
		background-size: cover;
		margin-right: 25px;
    	margin-left: auto;
	}
	.eq_carac.has-tip{
		display: block;
	}
	/* solo se coloco para ajustar lo quitado si se agrega mas quitarlo */
		.eq_c_1{
			height: 300px;
		}
		.cinta_espe{
			margin-top: -22px;
		}
		.cinta_espe0{
			margin-top: -32px;
		}
		.img_modelo {
		    height: 261px;
		}
	/******************/
	.view_consumibles{
		padding-left: 2px;
		padding-right: 2px;
	}
	.consu_eq.active{
		background: #dbdbdb;
		font-weight: bold;
	}
</style>
<div class="start_topbox_right"><!--
     	<div class="row">
			<div class="col-md-12">
				<div id="breadCrumb">
					<span class="breadCrumb first  "><a href="<?php echo base_url().'Inicio';?>" title="Inicio"><span>Inicio</span></a></span>
					<span class="breadCrumb  last "><a href="<?php echo base_url().'Inicio/rentaequipos';?>" title="Nuevos artículos"><span> Arrendamiento</span></a></span></div>
			</div>
		</div>-->
     <div class="emstartpagebox emstartpagenew clear">
         <!-- <div class="box start_sales_box"> -->
         <div class="">
         	<!-------------------------------------->
             <div class="row cmsContent">
        
                 <div class="col-md-12"style="background-image: url(<?php echo base_url()?>public/img/baner_consu.webp); min-height: 323px;background-size: contain; background-position: center; background-repeat: no-repeat;">
                 </div>
             </div>
             <div class="row">
             	<div class="col-md-4 op_arre">
             		<img src="<?php echo base_url();?>public/img/circulos/46.svg">
             		<p>Entregas programadas con tus servicios preventivos</p>
             	</div>
             	<div class="col-md-4 op_arre">
             		<img src="<?php echo base_url();?>public/img/circulos/49.svg">
             		<p>Tóner de repuesto de acuerdo con tu consumo</p>
             	</div>
             	<div class="col-md-4 op_arre">
             		<img src="<?php echo base_url();?>public/img/circulos/48.svg">
             		<p>Consumibles originales y compatibles</p>
             	</div>
             </div>
            <!---------------------------------------------> 
            <div class="row">
            	<div class="col-md-1">
            		
            	</div>
            	<div class="col-md-3">
            		<a class="option_ct_a bshadow" style="background: url(<?php echo base_url()?>public/img/eq_color.jpg);background-repeat: no-repeat;background-position-x: 95%;background-position-y: center;background-size: 15px;" onclick="view_eq(2)">Color</a>
            	</div>
            	<div class="col-md-3">
            		<a class="option_ct_a bshadow" style="background: url(<?php echo base_url()?>public/img/eq_mono.png);background-repeat: no-repeat;background-position-x: 95%;background-position-y: center;background-size: 15px;" onclick="view_eq(1)">Monocromaticos</a>
            	</div>
            	<div class="col-md-2">
            		<a class="option_ct_a bshadow"  onclick="view_eq(0)">Todo</a>
            	</div>
            </div>
            <!--<div class="row">
            	<div class="col-md-2" style="max-height: 346px;overflow: auto;">
            		<?php 
            			foreach ($lis_equi as $item_eq) {
            				echo '<a class="option_ct_a bshadow " onclick="consu_eq('.$item_eq['id'].')">'.$item_eq['modelo'].'</a>';
            			}
            		?>
            	</div>
            	<div class="col-md-10 view_consumibles"></div>
            </div>-->
         </div>
     </div>
</div>


<div class="row">
    	<div class="col-md-2" style="max-height: 346px;overflow: auto;">
    		<?php 
    			foreach ($lis_equi as $item_eq) {
    				echo '<a class="option_ct_a bshadow consu_eq consu_eq_'.$item_eq['id'].'" onclick="consu_eq('.$item_eq['id'].')">'.$item_eq['modelo'].'</a>';
    			}
    		?>
    	</div>
    	<div class="col-md-10 view_consumibles"></div>
    </div>



<script type="text/javascript">
	var cont_v=0;
	function mostrar_info(){
		if(cont_v==0){
            $('.formulario_txt').show(100);
            cont_v=1;
		}else{
            $('.formulario_txt').hide(100);
            cont_v=0;
		}

	}
</script>
