<link href="<?php echo base_url()?>public/css/styles_card_eq.css?v=<?php echo  date('Ymdgis');?>" rel="stylesheet"  >

<style type="text/css">
	.op_arre img{width: 100%;max-width: 160px;	}
	.op_arre{text-align: center;	}
	.op_arre p{padding: 0 15% 0 15%;font-weight: bold;font-size:15px;}
	.footer_eq{
		background: url(<?php echo base_url();?>public/img/logo2.svg);
		background-repeat: no-repeat;background-position: left;background-size: 100px;
	}
	.start_topbox .start_topbox_right {
		padding-top: 0px;
	}
	.eq_carac{
		width: 30px;
		height: 30px;
		margin-bottom: 12px;
	}
	.icon_1{

	}
	.icon_1{
		background: url(<?php echo base_url();?>public/img/equipo_caract_icon/1.webp);
		background-size: cover;
		margin-right: 25px;
    	margin-left: auto;
	}
	.icon_2{
		background: url(<?php echo base_url();?>public/img/equipo_caract_icon/2.png);
		background-size: cover;
		margin-right: 10px;
    	margin-left: auto;
	}
	.icon_3{
		background: url(<?php echo base_url();?>public/img/equipo_caract_icon/3.png);
		background-size: cover;
		margin-right: 0px;
    	margin-left: auto;
	}
	.icon_4{
		background: url(<?php echo base_url();?>public/img/equipo_caract_icon/4.png);
		background-size: cover;
		margin-right: 0px;
    	margin-left: auto;
	}
	.icon_5{
		background: url(<?php echo base_url();?>public/img/equipo_caract_icon/5.png);
		background-size: cover;
		margin-right: 10px;
    	margin-left: auto;
	}
	.icon_6{
		background: url(<?php echo base_url();?>public/img/equipo_caract_icon/6.png);
		background-size: cover;
		margin-right: 25px;
    	margin-left: auto;
	}
	.eq_carac.has-tip{
		display: block;
	}
</style>
<div class="start_topbox_right"><!--
     	<div class="row">
			<div class="col-md-12">
				<div id="breadCrumb">
					<span class="breadCrumb first  "><a href="<?php echo base_url().'Inicio';?>" title="Inicio"><span>Inicio</span></a></span>
					<span class="breadCrumb  last "><a href="<?php echo base_url().'Inicio/rentaequipos';?>" title="Nuevos artículos"><span> Arrendamiento</span></a></span></div>
			</div>
		</div>-->
     <div class="emstartpagebox emstartpagenew clear">
         <!-- <div class="box start_sales_box"> -->
         <div class="">
         	<!-------------------------------------->
             <div class="row cmsContent">
        
                 <div class="col-md-12"style="background-image: url(<?php echo base_url()?>public/img/baner.webp); min-height: 323px;background-size: contain; background-position: center; background-repeat: no-repeat;">
                 </div>
                 
             </div>
             <div class="row">
             	<div class="col-md-4 op_arre">
             		<img src="<?php echo base_url();?>public/img/circulos/46.svg">
             		<p>Equipos a la media de tú consumo</p>
             	</div>
             	<div class="col-md-4 op_arre">
             		<img src="<?php echo base_url();?>public/img/circulos/47.svg">
             		<p>Cobertura a nivel nacional</p>
             	</div>
             	<div class="col-md-4 op_arre">
             		<img src="<?php echo base_url();?>public/img/circulos/48.svg">
             		<p>Accesorios y refacciones originales</p>
             	</div>
             	<div class="col-md-4 op_arre">
             		<img src="<?php echo base_url();?>public/img/circulos/49.svg">
             		<p>Servicios preventivos y correctivos</p>
             	</div>
             	<div class="col-md-4 op_arre">
             		<img src="<?php echo base_url();?>public/img/circulos/50.svg">
             		<p>Tóner de repuesto de acuerdo con tu consumo</p>
             	</div>
             	<div class="col-md-4 op_arre">
             		<img src="<?php echo base_url();?>public/img/circulos/51.svg">
             		<p>Servicios mensuales programados</p>
             	</div>

             </div>
            <!---------------------------------------------> 
            <div class="row">
            	<div class="col-md-1">
            		
            	</div>
            	<div class="col-md-3">
            		<a class="option_ct_a bshadow" style="background: url(<?php echo base_url()?>public/img/eq_color.jpg);background-repeat: no-repeat;background-position-x: 95%;background-position-y: center;background-size: 15px;" onclick="view_eq(2)">Equipos a color</a>
            	</div>
            	<div class="col-md-3">
            		<a class="option_ct_a bshadow" style="background: url(<?php echo base_url()?>public/img/eq_mono.png);background-repeat: no-repeat;background-position-x: 95%;background-position-y: center;background-size: 15px;" onclick="view_eq(1)">Equipos a Monocromaticos</a>
            	</div>
            	<div class="col-md-3">
            		<a class="option_ct_a bshadow" style="background: url(<?php echo base_url()?>public/img/eq_hib.jpg);background-repeat: no-repeat;background-position-x: 95%;background-position-y: center;background-size: 15px;" onclick="view_eq(3)">Equipos a Hibridos</a>
            	</div>
            	<div class="col-md-2">
            		<a class="option_ct_a bshadow"  onclick="view_eq(0)">Todo</a>
            	</div>
            	
            </div>
            <div class="row">
            	<div class="col-md-12">
	            	<?php foreach ($lis_equi as $item) { 
	            		$idequipo = $item['id'];
	            		$span_tipo_m='<button style="background: url('.base_url().'public/img/eq_mono.png);background-repeat: no-repeat;background-size: 100%;"></button> Imp Mono';//1
	            		$span_tipo_c='<button style="background: url('.base_url().'public/img/eq_color.jpg);background-repeat: no-repeat;background-size: 100%;"></button> Imp Color';//2
	            		$span_tipo_h='<button style="background: url('.base_url().'public/img/eq_hib.jpg);background-repeat: no-repeat;background-size: 100%;"></button> Imp Hibrida';//3
	            		/*
	            		<select id="categoriaId" name="categoriaId" class="browser-default chosen-select" style="display: none;">
	                                  <option value="" disabled="" selected="">Selecciona una opción</option>
	                                                                                  Impresora monocromática
	                                                                                  Impresora de color
	                                                                                  Multifuncional monocromático
	                                                                                  Multifuncional de color
	                                                                                  Multifuncional Híbrido de color
	                                                                      </select>
	                                                                      */
	                    $tipo_c=1;
	            		if($item['categoriaId']==24){//Impresora monocromática
	            			$tipo_c=1;
	            		}
	            		if($item['categoriaId']==25){//Impresora de color
	            			$tipo_c=2;
	            		}
	            		if($item['categoriaId']==26){// Multifuncional monocromático
	            			$tipo_c=1;
	            		}
	            		if($item['categoriaId']==27){//Multifuncional de color
	            			$tipo_c=2;
	            		}
	            		if($item['categoriaId']==28){//Multifuncional Híbrido de color
	            			$tipo_c=3;
	            		}
	            	?>
	            		<div class="impresoras imp_cat_<?php echo $tipo_c;?> imp_id_<?php echo $item['id']?>">
	            			<div class="card_eq_contect">
	            				<div class="p_cont">
	            					<div class="price bshadow">
	            						<div class="p_c_1">
	            							$ <?php echo number_format(($item['pag_monocromo_incluidos']*$item['excedente']),2,'.',',');?>
	            						</div>
	            						<div class="p_c_2">
	            							Renta mensual +IVA
	            						</div>
	            					</div>
	            				</div>
	            				<div class="p_mod bshadow"><?php echo $item['modelo'];?></div>
	            				<div class="card_eq_body bshadow">
	            					<div class="eq_c_1">
	            						<div class="dll_clicks">Páginas mono incluidas <button class="bshadow"><?php echo $item['pag_monocromo_incluidos'];?></button> </div>
	            						<div class="dll_clicks">Páginas color incluidas <button class="bshadow"><?php echo $item['pag_color_incluidos'];?></button> </div>
	            						<div class="img_modelo" style="background:url(<?php echo 'https://altaproductividadapr.com/uploads/equipos/'.$item['foto']; ?>);background-repeat: no-repeat;background-size: 70%;">
	            							<?php 
	            									$re_caract=$this->ModeloConsultas->equipo_caracteristica($idequipo);
	            									foreach ($re_caract as $i_c) {
	            										echo '<div class="eq_carac icon_'.$i_c['orden'].' tooltip_icon" data-toggle="tooltip" data-placement="top" title="'.$i_c['name'].' '.$i_c['descripcion'].'" ></div>';
	            									}
	            								?>
	            						</div>
	            						<div class="eq_t_mc">
	            							<?php 
	            								if($tipo_c==1){
	            									echo $span_tipo_m;
	            								}
	            								if($tipo_c==2){
	            									echo $span_tipo_c;
	            								}
	            								if($tipo_c==3){
	            									echo $span_tipo_h;
	            								}
	            							?>
	            						</div>
	            					</div>
	            					<div class="eq_c_2">
	            						<div><div class="btn_i_red2">Incluye</div></div>
	            						<div>
	            							<ul>

	            								<li>Tóner, consumibles y refacciones.</li>
	            								<li>Un servicio mensual preventivo.</li>
	            								<li>Servicios correctivos ilimitados.</li>
	            								<li>Tiempo de respuesta 6 hrs. máximo.</li>
	            								<li>Capacitación de uso del equipo.</li>
	            								<li>Servicio técnico certificado por la marca.</li>
	            							</ul>
	            						</div>
	            						<div><div class="btn_i_red2">Extras</div></div>
	            						<div>
	            							<ul>
	            								<li>(excedente monocromático o color)</li>
	            								<li>‍‍Condiciones</li>
	            							</ul>
	            						</div>
	            						<div><div class="btn_i_red2">Condiciones</div></div>
	            						<div>
	            							<ul>
	            								<li>Contrato por 12, 24 o 36 meses</li>
	            								<li>(renta en depósito en garantía)</li>
	            							</ul>
	            						</div>
	            					</div>
	            					<div class="footer_eq">
	            						<div class="btns-infos">
	            							<button class="btn_i_red" onclick="view_dll(<?php echo $item['id']?>,1)"><i class="fa fa-plus"></i> Info</button>
	            						</div>
	            						
	            						<div class="cinta_espe"></div>
	            						<div class="cinta_espe0">
	            							<div class="cinta_espe2">Precio especial en línea</div>
	            						</div>
	            					</div>
	            					

	            					
	            				</div>
	            				<div class="p_cont">
		            					<div class="cotiza bshadow">Cotiza este equipo</div>
		            				</div>
	            			</div>
	            			
	            		</div>
	            	<?php } ?>
            	</div>
            </div>
         </div>
     </div>
</div>



<script type="text/javascript">
	var cont_v=0;
	function mostrar_info(){
		if(cont_v==0){
            $('.formulario_txt').show(100);
            cont_v=1;
		}else{
            $('.formulario_txt').hide(100);
            cont_v=0;
		}

	}
</script>
