<style type="text/css">
   .start_topbox .start_topbox_right{
      padding-top: 3px;
   }
   .baner_kyo{
      background-image: url(<?php echo base_url();?>public/img/baner_kyo.webp);

      text-align:right;
      padding-top: 20px;
    background-size: 84%;
    background-repeat: no-repeat;
}
   .td1{
      background: url(<?php echo base_url();?>public/img/comillas.png) black;
      background-repeat: no-repeat;
      background-position: center;
      background-size: 61%;

   }
</style>
<div class="start_topbox_right">
      <!--<div class="row">
         <div class="col-md-12">
            <div id="breadCrumb">
               <span class="breadCrumb first  "><a href="<?php echo base_url().'Inicio';?>" title="Inicio"><span>Inicio</span></a></span>
               <span class="breadCrumb  last "><a href="<?php echo base_url().'Inicio/quienes_somos';?>" title="Nuevos artículos"><span> Quiénes somos</span></a></span></div>
         </div>
      </div>-->
     <div class="emstartpagebox emstartpagenew clear">
         <!-- <div class="box start_sales_box"> -->
         <div class="">
            <!-------------------------------------->
               <!--
               <div class="row cmsContent">
                  <div class="col-md-12"><h1>Quiénes somos</h1></div>
                  
                  <div class="col-md-12"style="background-image: url(<?php echo base_url()?>public/img/vaner.png);background-repeat: no-repeat;min-height: 102px;background-size: contain;">
                  <h3 style="margin-left:20px;margin-top: 33px;"></h3>
                 </div>
               </div>
            -->
            <div class="row">
               <div class="col-md-12 baner_kyo">
                  <h1>El espiritu pionero<br>está en nuestra filosofia<br>y en nuestro<br>ADN</h1>
               </div>
               <div class="col-md-12">
                  <h2>Nuestra misión es lograr espacios de trabajo más inteligentes</h2>
                  <p>Le hacemos la vida más sencilla creando espacios de trabajo inteligentes, poniendo su conocimiento en práctica para obtener una ventaja competitiva. Contamos con más de 60 de años de experiencia como pioneros en innovación, liderando la transformación de la gestión documental para que las empresas sean más ágiles, funcionen con mayor eficiencia y estén centradas en los clientes.</p>
               </div>
            </div>
            <div class="row">
               <div class="col-md-1"></div>
               <div class="col-md-10">
                  <table class="info_ll">
                     <tr>
                        <td class="td0">
                           <p class="p1">“Hacer lo correcto como ser humano”</p>
                           <p class="p2">Dr. Kazuo Inamori</p>
                        </td>
                        <td class="td1"></td>
                     </tr>
                  </table>
               </div>
               <div class="col-md-1"></div>
            </div>
            <div class="row" style="margin-top:20px">
               <div class="col-md-6"><img src="<?php echo base_url()?>public/img/ban1.webp" width="100%"> </div>
               <div class="col-md-6">
                  <p class="p1">“Respetar lo divino y amar a las personas”</p>
                  <p>La filosofía de nuestro fundador, el Dr Kazuo Inamori, guía la misión de Kyocera hacía “el hacer lo correcto como ser humano”, un concepto que incluimos en todas nuestras decisiones. Demostrar la importancia de la injusticia y el esfuerzo diligente sirve como paradigma de nuestra conducta.</p>
               </div>
               <div class="col-md-6">
                  <p class="p1">Impacto positivo en la sociedad</p>
                  <p>Nos hemos convertido en una compañía internacional manteniéndonos fieles a la filosofía de nuestro fundador: lo que hacemos nace de nuestra propia predisposición a causar un impacto positivo en el individuo y en el conjunto de la sociedad. No nos hemos alejado de la idea que da sentido a todo lo que hacemos. Hemos recorrido a un largo camino desde que empezamos a caminar como una pequeña empresa de cerámica en Japón, pero mantenemos la misma mentalidad  -aprender, crecer, mejorar – como individuos, como empresa y como parte de la sociedad.</p>
               </div>
               <div class="col-md-6"><img src="<?php echo base_url()?>public/img/ban2.webp" width="100%"> </div>
            </div>


            <!---------------------------------------------> 
         </div>
     </div>
</div>