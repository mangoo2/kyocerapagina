
                <div class="start_topbox_right">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="checkoutSteps clear">
                                <li class="step1  active ">
                                    <a href="https://www.cyberpuerta.mx/carrito-de-compras/">
                                        Carrito
                                    </a>
                                </li>
                                <li class="step4">
                                    <span>
                                        3. Confirmar pedido
                                    </span>
                                </li>
                                <li class="step3">
                                    <span>
                                        2. Envío y pago
                                    </span>
                                </li>
                                <li class="step2">
                                    <span>
                                        <a href="<?php echo base_url();?>Carrito/direccion">1. Elegir dirección</a>
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <table id="basket" class="basketitems">
                                <colgroup>
                                <col class="editCol">
                                <col class="thumbCol">
                                <col>
                                <col class="titleCol">
                                <col class="countCol">
                                <col class="priceCol">
                                <col class="totalCol">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Producto</th>
                                        <th></th>
                                        <th></th>
                                        <th class="quantity">Cantidad</th>
                                        <th>Precio</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $totalgeneral=0;
                                    $totalcostoenvio=99;
                                    //var_dump($_SESSION['pro']);
                                    foreach ($result_car as $fila){
                                        $prorow=$fila['id'];
                                        $url_image=base_url().'public/img/producto-sin-imagen.png';
                                        $Cantidad=$fila['cant'];
                                        if($fila['typeitem']==1){
                                            $idcode=intval($fila['iditem']);
                                            $url="https://altaproductividadapr.com/index.php/restserver/productosinfoid/".$idcode;
                                            $equiposrow=$this->ModeloRest->consultaapiget($url);
                                            foreach ($equiposrow as $item) {
                                                $noparte=$item['noparte'];
                                                $costo_venta=round(($item['costo_renta']*1.16),2);
                                                $title_equipo=$item['modelo'].' '.$item['especificaciones'];
                                                $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                if($item['foto']==''){
                                                    $url_image=base_url().'public/img/impresora.png';
                                                }else{
                                                    $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['foto'];
                                                }       
                                                ?>
                                                <tr id="cartItem_1" class="">
                                                    <td class="checkbox" rowspan="2">
                                                        <div class="emremovelink" onclick="deletepro(<?php echo $prorow;?>)"></div>
                                                    </td>
                                                    <td class="basketImage" rowspan="2">
                                                        <a href="#"><img src="<?php echo $url_image;?>" alt="<?php echo $title_equipo;?>"></a>
                                                    </td>
                                                    <td class="shadowcolumn" rowspan="2">
                                                        <div class="shadow"></div>
                                                    </td>
                                                    <td class="title">
                                                        <div class="emtitlebox">
                                                            <div class="emtitle"><a href="#"><?php echo $title_equipo;?></a></div>
                                                            <div class="skustock">
                                                                <div class="emartnum">SKU: <?php echo $noparte;?></div>
                                                                <div class="emstock">Disponibles: <span>10</span> pzas. </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="quantity main-container">
                                                        <div id="am_1" class="embuttonamount_border">
                                                            <div class="embuttonamount_amount" style="width:27px">
                                                                <input type="text" name="aproducts[9add3409708e5fd4be90f8e1a9b64552][am]" value="<?php echo $Cantidad;?>" size="3" autocomplete="off" class="textbox" data-cp-orig-value="2" data-cp-amount-basket="" data-press-promo="" data-cp-group="e098337572a775cee027f6dd68039137" wtx-context="915AF3DD-8B76-40DF-8895-662855B711C7">
                                                            </div>
                                                            <button type="button" class="embuttonamount_minus" name="aproducts[9add3409708e5fd4be90f8e1a9b64552][am]" onclick="down_up(<?php echo $prorow;?>,'down')" data-increase="down" data-pre-process-add-to-cart="e098337572a775cee027f6dd68039137">-</button>
                                                            <button type="button" class="embuttonamount_plus" name="aproducts[9add3409708e5fd4be90f8e1a9b64552][am]" onclick="down_up(<?php echo $prorow;?>,'up')" data-increase="up" data-pre-process-add-to-cart="e098337572a775cee027f6dd68039137" style="margin-left: 50px;">+</button>
                                                            
                                                        </div>
                                                        
                                                    </td>
                                                    <td class="unitPrice"><?php echo number_format($costo_venta,2,".",",");?></td>
                                                    <td class="totalPrice"><?php echo number_format($costo_venta*$Cantidad,2,".",",");?></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" class="eta-line">
                                                        <div style="margin-bottom: 20px;margin-top: 5px;">
                                                            <div><div class="delivery-btn"><div><a href="#" class="cp-delivery-more-info">Recíbelo entre el 22 y el 27 de junio</a></div></div></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        if($fila['typeitem']==2){
                                            $idcode=intval($fila['iditem']);
                                            //===========================================================================
                                                $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                $arraycols=array();
                                                $arraycols[]=array('name'=>'id','value'=>$idcode);

                                                $array=array('tabla'=>'catalogo_accesorios','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                $equiposrow=$this->ModeloRest->consultaapipost($url,$array);
                                            //=================================================================
                                            //==============================================================
                                                $arraycols=array();
                                                $arraycols[]=array('name'=>'idaccesorio','value'=>$idcode);

                                                $array=array('tabla'=>'catalogo_accesorios_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                $equiposimg=$this->ModeloRest->consultaapipost($url,$array);

                                                $equiposimg=$equiposimg;
                                                foreach ($equiposimg as $item) { 
                                                   $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['imagen'];
                                                }
                                            //==============================================================



                                            
                                            foreach ($equiposrow as $item) {
                                                $noparte=$item['no_parte'];
                                                $costo_venta=round($item['costo']*1.16,2);
                                                $title_equipo=$item['nombre'];
                                                $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                    
                                                ?>
                                                <tr id="cartItem_1" class="">
                                                    <td class="checkbox" rowspan="2">
                                                        <div class="emremovelink" onclick="deletepro(<?php echo $prorow;?>)"></div>
                                                    </td>
                                                    <td class="basketImage" rowspan="2">
                                                        <a href="#"><img src="<?php echo $url_image;?>" alt="<?php echo $title_equipo;?>"></a>
                                                    </td>
                                                    <td class="shadowcolumn" rowspan="2">
                                                        <div class="shadow"></div>
                                                    </td>
                                                    <td class="title">
                                                        <div class="emtitlebox">
                                                            <div class="emtitle"><a href="#"><?php echo $title_equipo;?></a></div>
                                                            <div class="skustock">
                                                                <div class="emartnum">SKU: <?php echo $noparte;?></div>
                                                                <div class="emstock">Disponibles: <span>10</span> pzas. </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="quantity main-container">
                                                        <div id="am_1" class="embuttonamount_border">
                                                            <div class="embuttonamount_amount" style="width:27px">
                                                                <input type="text" name="aproducts[9add3409708e5fd4be90f8e1a9b64552][am]" value="<?php echo $Cantidad;?>" size="3" autocomplete="off" class="textbox" data-cp-orig-value="2" data-cp-amount-basket="" data-press-promo="" data-cp-group="e098337572a775cee027f6dd68039137" wtx-context="915AF3DD-8B76-40DF-8895-662855B711C7">
                                                            </div>
                                                            <button type="button" class="embuttonamount_minus" name="aproducts[9add3409708e5fd4be90f8e1a9b64552][am]" onclick="down_up(<?php echo $prorow;?>,'down')" data-increase="down" data-pre-process-add-to-cart="e098337572a775cee027f6dd68039137">-</button>
                                                            <button type="button" class="embuttonamount_plus" name="aproducts[9add3409708e5fd4be90f8e1a9b64552][am]" onclick="down_up(<?php echo $prorow;?>,'up')" data-increase="up" data-pre-process-add-to-cart="e098337572a775cee027f6dd68039137" style="margin-left: 50px;">+</button>
                                                            
                                                        </div>
                                                        
                                                    </td>
                                                    <td class="unitPrice"><?php echo number_format($costo_venta,2,".",",");?></td>
                                                    <td class="totalPrice"><?php echo number_format($costo_venta*$Cantidad,2,".",",");?></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" class="eta-line">
                                                        <div style="margin-bottom: 20px;margin-top: 5px;">
                                                            <div><div class="delivery-btn"><div><a href="#" class="cp-delivery-more-info">Recíbelo entre el 22 y el 27 de junio</a></div></div></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        if($fila['typeitem']==3){
                                            $idcode=intval($fila['iditem']);
                                            //===========================================================================
                                                //===========================================================================
                                                    $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                    $arraycols=array();
                                                    $arraycols[]=array('name'=>'id','value'=>$idcode);

                                                    $array=array('tabla'=>'refacciones','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                    $equiposrow=$this->ModeloRest->consultaapipost($url,$array);
                                                //=================================================================
                                            //=================================================================
                                            //==============================================================
                                                $arraycols=array();
                                                $arraycols[]=array('name'=>'idrefaccion','value'=>$idcode);

                                                $array=array('tabla'=>'refacciones_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                $equiposimg=$this->ModeloRest->consultaapipost($url,$array);

                                                $equiposimg=$equiposimg;
                                                foreach ($equiposimg as $item) { 
                                                   $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['imagen'];
                                                }
                                            //==============================================================



                                            
                                            foreach ($equiposrow as $item) {
                                                $noparte=$item['codigo'];
                                                //================================
                                                    //===========================================================================
                                                        $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'refacciones_id','value'=>$idcode);

                                                        $array=array('tabla'=>'refacciones_costos','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $costo_lis_precio=$this->ModeloRest->consultaapipost($url,$array);
                                                        foreach ($costo_lis_precio as $itemp) {
                                                            $costo_venta=round($itemp['general']*1.16,2);
                                                        }
                                                    //=================================================================
                                                //================================
                                                $title_equipo=$item['nombre'];
                                                $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                    
                                                ?>
                                                <tr id="cartItem_1" class="">
                                                    <td class="checkbox" rowspan="2">
                                                        <div class="emremovelink" onclick="deletepro(<?php echo $prorow;?>)"></div>
                                                    </td>
                                                    <td class="basketImage" rowspan="2">
                                                        <a href="#"><img src="<?php echo $url_image;?>" alt="<?php echo $title_equipo;?>"></a>
                                                    </td>
                                                    <td class="shadowcolumn" rowspan="2">
                                                        <div class="shadow"></div>
                                                    </td>
                                                    <td class="title">
                                                        <div class="emtitlebox">
                                                            <div class="emtitle"><a href="#"><?php echo $title_equipo;?></a></div>
                                                            <div class="skustock">
                                                                <div class="emartnum">SKU: <?php echo $noparte;?></div>
                                                                <div class="emstock">Disponibles: <span>10</span> pzas. </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="quantity main-container">
                                                        <div id="am_1" class="embuttonamount_border">
                                                            <div class="embuttonamount_amount" style="width:27px">
                                                                <input type="text" name="aproducts[9add3409708e5fd4be90f8e1a9b64552][am]" value="<?php echo $Cantidad;?>" size="3" autocomplete="off" class="textbox" data-cp-orig-value="2" data-cp-amount-basket="" data-press-promo="" data-cp-group="e098337572a775cee027f6dd68039137" wtx-context="915AF3DD-8B76-40DF-8895-662855B711C7">
                                                            </div>
                                                            <button type="button" class="embuttonamount_minus" name="aproducts[9add3409708e5fd4be90f8e1a9b64552][am]" onclick="down_up(<?php echo $prorow;?>,'down')" data-increase="down" data-pre-process-add-to-cart="e098337572a775cee027f6dd68039137">-</button>
                                                            <button type="button" class="embuttonamount_plus" name="aproducts[9add3409708e5fd4be90f8e1a9b64552][am]" onclick="down_up(<?php echo $prorow;?>,'up')" data-increase="up" data-pre-process-add-to-cart="e098337572a775cee027f6dd68039137" style="margin-left: 50px;">+</button>
                                                            
                                                        </div>
                                                        
                                                    </td>
                                                    <td class="unitPrice"><?php echo number_format($costo_venta,2,".",",");?></td>
                                                    <td class="totalPrice"><?php echo number_format($costo_venta*$Cantidad,2,".",",");?></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" class="eta-line">
                                                        <div style="margin-bottom: 20px;margin-top: 5px;">
                                                            <div><div class="delivery-btn"><div><a href="#" class="cp-delivery-more-info">Recíbelo entre el 22 y el 27 de junio</a></div></div></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        if($fila['typeitem']==4){
                                            $idcode=intval($fila['iditem']);
                                                    //===========================================================================
                                                        $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'id','value'=>$idcode);
                                                        $array=array('tabla'=>'consumibles','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $equipos=$this->ModeloRest->consultaapipost($url,$array);
                                                    //=================================================================
                                                    //==============================================================
                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'idconsumible','value'=>$idcode);
                                                        $array=array('tabla'=>'consumibles_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $equiposimg=$this->ModeloRest->consultaapipost($url,$array);
                                                        $equiposimg=$equiposimg;
                                                        foreach ($equiposimg as $item) {
                                                            $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['imagen'];
                                                        }
                                                    //==============================================================
                                                        foreach ($equipos as $item) {
                                                            $noparte=$item['parte'];
                                                        
                                                            //================================
                                                            //===========================================================================
                                                                $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                                $arraycols=array();
                                                                $arraycols[]=array('name'=>'consumibles_id','value'=>$idcode);
                                                                $array=array('tabla'=>'consumibles_costos','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                                $costo_lis_precio=$this->ModeloRest->consultaapipost($url,$array);
                                                                foreach ($costo_lis_precio as $itemp) {
                                                                    $costo_venta=round($itemp['general']*1.16,2);
                                                                }
                                                            //=================================================================
                                                            //================================
                                                                $title_equipo=$item['modelo'];
                                                                $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                                ?>
                                                        
                                                                <tr id="cartItem_1" class="">
                                                                    <td class="checkbox" rowspan="2">
                                                                        <div class="emremovelink" onclick="deletepro(<?php echo $prorow;?>)"></div>
                                                                    </td>
                                                                    <td class="basketImage" rowspan="2">
                                                                        <a href="#"><img src="<?php echo $url_image;?>" alt="<?php echo $title_equipo;?>"></a>
                                                                    </td>
                                                                    <td class="shadowcolumn" rowspan="2">
                                                                        <div class="shadow"></div>
                                                                    </td>
                                                                    <td class="title">
                                                                        <div class="emtitlebox">
                                                                            <div class="emtitle"><a href="#"><?php echo $title_equipo;?></a></div>
                                                                            <div class="skustock">
                                                                                <div class="emartnum">SKU: <?php echo $noparte;?></div>
                                                                                <div class="emstock">Disponibles: <span>10</span> pzas. </div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="quantity main-container">
                                                                        <div id="am_1" class="embuttonamount_border">
                                                                            <div class="embuttonamount_amount" style="width:27px">
                                                                                <input type="text" name="aproducts[9add3409708e5fd4be90f8e1a9b64552][am]" value="<?php echo $Cantidad;?>" size="3" autocomplete="off" class="textbox" data-cp-orig-value="2" data-cp-amount-basket="" data-press-promo="" data-cp-group="e098337572a775cee027f6dd68039137" wtx-context="915AF3DD-8B76-40DF-8895-662855B711C7">
                                                                            </div>
                                                                            <button type="button" class="embuttonamount_minus" name="aproducts[9add3409708e5fd4be90f8e1a9b64552][am]" onclick="down_up(<?php echo $prorow;?>,'down')" data-increase="down" data-pre-process-add-to-cart="e098337572a775cee027f6dd68039137">-</button>
                                                                            <button type="button" class="embuttonamount_plus" name="aproducts[9add3409708e5fd4be90f8e1a9b64552][am]" onclick="down_up(<?php echo $prorow;?>,'up')" data-increase="up" data-pre-process-add-to-cart="e098337572a775cee027f6dd68039137" style="margin-left: 50px;">+</button>
                                                                            
                                                                        </div>
                                                                        
                                                                    </td>
                                                                    <td class="unitPrice"><?php echo number_format($costo_venta,2,".",",");?></td>
                                                                    <td class="totalPrice"><?php echo number_format($costo_venta*$Cantidad,2,".",",");?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" class="eta-line">
                                                                        <div style="margin-bottom: 20px;margin-top: 5px;">
                                                                            <div><div class="delivery-btn"><div><a href="#" class="cp-delivery-more-info">Recíbelo entre el 22 y el 27 de junio</a></div></div></div>
                                                                        </div>
                                                                    </td>
                                                </tr>
                                                            <?php
                                                        }
                                        }
                                    }
                                    $totalgeneraltotal=$totalgeneral+$totalcostoenvio;
                                    ?>
                                </tbody>
                            </table>
                            <div class="clear basket-summary-line">
                                <div class="summarybox">
                                    <table cellpadding="0" cellspacing="0" style="height:100px; margin-top:15px; margin-bottom:15px; " width="100%">
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:middle; text-align:center; width:30%; ">
                                                    
                                                </td>
                                                <td style="vertical-align:middle; text-align:center; width:36%; ">
                                                    <table align="center">
                                                        <tbody><tr><td style="color: #0066BB; font-size: 16px; font-weight: bold; "><img alt="" src="<?php echo base_url();?>public/img/yes.webp" style="width: 18px; height: 18px;"></td><td style="text-align:left; font-size:13px; ">Productos nuevos y originales</td>
                                                        </tr><tr><td style="color: #0066BB; font-size: 16px; font-weight: bold; "><img alt="" src="<?php echo base_url();?>public/img/yes.webp" style="width: 18px; height: 18px;"></td><td style="text-align:left; font-size:13px; ">Envío asegurado</td>
                                                        </tr><tr><td style="color: #0066BB; font-size: 16px; font-weight: bold; "><img alt="" src="<?php echo base_url();?>public/img/yes.webp" style="width: 18px; height: 18px;"></td><td style="text-align:left; font-size:13px; ">Pago seguro con VeriSign SSL</td>
                                                        </tr><tr><td style="color: #0066BB; font-size: 16px; font-weight: bold; "><img alt="" src="<?php echo base_url();?>public/img/yes.webp" style="width: 18px; height: 18px;"></td><td style="text-align:left; font-size:13px; ">Cada venta contiene factura</td>
                                                    </tr></tbody></table>
                                                </td>
                                                <td style="vertical-align:middle; text-align:center; width:33%; "><a href="https://sellosdeconfianza.org.mx/MuestraCertificado.php?NUMERO_SERIE=MD_x042" onclick="newWindow = window.open('https://sellosdeconfianza.org.mx/MuestraCertificado.php?NUMERO_SERIE=MD_x042', 'Certificado', 'width=500,height=616,left=150,top=70,toolbar=0,menuBar=0,scrollBars=0 resizable=0' ); newWindow.focus(); return false;" target="_blank"><img alt="" src="<?php echo base_url();?>public/img/amipci-datospersonales1.png"></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="summary">
                                    <div class="basketcontents_mini_summary_box">
                                        <table cellpadding="0" cellspacing="0" class="basketcontents_mini_summary">
                                            <tr>
                                                <th>Total de productos:</th> 
                                                <td id="basketTotalGross">$<?php echo number_format($totalgeneral,2,".",",");?></td>
                                            </tr>
                                            <tr>
                                                <th>Costo de envío</th> 
                                                <td>$<?php echo number_format($totalcostoenvio,2,".",",");?></td>
                                            </tr>
                                            <tr>
                                                <th>Total (con IVA)</th> 
                                                <td id="basketGrandTotal">$<?php 
                                                    $_SESSION['totalgeneraltotal']=$totalgeneraltotal;
                                                    echo number_format($totalgeneraltotal,2,".",",");?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <a type="button" class="btn btn-secondary" href="<?php echo base_url();?>Carrito/direccion">Ir al siguiente paso</a>
                        </div>
                    </div>
                </div>