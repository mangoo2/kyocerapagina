<div id="page" class=" start ">
                                                        <div class="pageborder clear">
<div id="content">
    <div class="start_topbox clear">
        <div id="sidebar" class="start_topbox_left">
            <div class="cp-main-menu-place-holder">
                <div data-cp-vue-bind="main-menu" data-cp-options='{"css": ["desktop", "home"], "closeDelay" : 1000}'>
                    <div class="cp-main-menu desktop home">
                        <div class="main-menu"></div>
                    </div>
                </div>
            </div>
            <div class="box sidebar_security" style="margin-top:20px">
                <div class="box-content">
                    <h1>¡Somos tu mejor opción!</h1>
                    <ul>
                        <li>Más de 40,000 productos.</li>
                        <li>Compras y envíos seguros.</li>
                        <li>Comparte y revisa opiniones.</li>
                    </ul>
                    <a class="submitButton largeButton" href="Quienes-somos/index.html">
                        Conocer más
                    </a>
                </div>
            </div>
            <div class="box emloginbanner">
                <div class="boxcontent">
                    <div class="headline">¿Ya tienes cuenta en Cyberpuerta?</div>
                    <table style="border-spacing: 0;">
                        <tr>
                            <td></td>
                            <td>Crea listas de favoritos...</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Envíos fáciles con tu libreta de direcciones...</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Escribe preguntas y opiniones de productos...</td>
                        </tr>
                    </table>
                    <a href="Bienvenido/index.html" class="submitButton" style="margin-bottom: 6px;">Crear cuenta</a>
                </div>
            </div>
        </div>
        <div class="start_topbox_right">
            <div class="bannerarea clear" style="">
                <div class="bannerarea_STARTPAGETOP">
                    <div class="bannerarea_contentbox">
                        <div data-cp-banner-place="home-main"></div>
                    </div>
                </div>
                <div class="bannerarea_STARTPAGETOPLEFT">
                    <div class="bannerarea_contentbox">
                        <div data-cp-banner-place="home-top-left"></div>
                    </div>
                </div>
                <!--<div class="bannerarea_STARTPAGETOPRIGHT">
                    <div class="bannerarea_contentbox">
                        <div data-cp-banner-place="home-top-middle"></div>
                    </div>
                </div>-->
                <div class="bannerarea_STARTPAGETOPMIDDLE">
                    <div class="bannerarea_contentbox">
                        <div data-cp-banner-place="home-top-right"></div>
                    </div>
                </div>
            </div>
            <div class="emstartpagebox emstartpagenew clear">
                <div class="box start_sales_box">
                    <div class="start_sales daily_offers clear salesCnt">
                        <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.svg" alt="offer icon" class="offer-icon">
                        <h1>
                        Destacados del día ¡no los dejes pasar!
                        </h1>
                        <div class="until" id="until_start_sales_dyn"></div>
                    </div>
                    <div class="daily_pagination" id="cp-start-daily-offers-pagination">
                        <span class="daily_item" style="display: none" data-cp-page="0">1</span>
                        <span class="daily_item" style="display: none" data-cp-page="1">2</span>
                        <span class="daily_item" style="display: none" data-cp-page="2">3</span>
                        <span class="daily_item" style="display: none" data-cp-page="3">4</span>
                        <span class="daily_item" style="display: none" data-cp-page="4">5</span>
                    </div>
                    <div class="link daily_offers">
                        <a href="Destacados-del-dia/index.html?cl=alist&amp;cnid=emsalearticle_startdetail">
                            Ver todos los productos destacados de hoy
                        </a>
                    </div>
                    <div>
                        <ul class="infogridView grid-x small-up-3 xlarge-up-4" style="width:100%" id="cp-start-daily-offers">
                            <li class="productData cell" data-cp-start-daily-product style="display: none">
                                <div class="emproductborder listiteminfogridborder emsmoothborder clear">
                                    <form name="tobasketemstartpagenew-1"
                                        action="https://www.kyoceraap.com/index.php?" method="post"
                                        >
                                        <input type="hidden" name="actcontrol" value="start" />
                                        <input type="hidden" name="lang" value="0" />
                                        <input type="hidden" name="pgNr" value="0">
                                        <input type="hidden" name="cl" value="start">
                                        <input type="hidden" name="fnc" value="tobasket">
                                        <input type="hidden" name="aid" value="3e69637e033d21d9fe49f9307b45c545">
                                        <input type="hidden" name="anid" value="3e69637e033d21d9fe49f9307b45c545">            <input type="hidden" name="am" value="1">
                                        <input type="hidden" name="oxloadid" value="">
                                        <div class="emproduct clear listiteminfogrid">
                                            <div class="offerbox  hasuntil  hasdaily clear">
                                                <div class="percent">
                                                    -9%
                                                    <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.png" alt="daily offer icon"/>
                                                    <span class="onlyhome">
                                                        ($80.00)
                                                    </span>
                                                </div>
                                            </div>
                                            <a class="cp-picture-container cpGaProdemstartpagenew-1" href="Seguridad-y-Vigilancia/Camaras-y-Sistemas-de-Vigilancia/Camaras-de-Seguridad-IP/TP-Link-Camara-IP-Smart-WiFi-Bullet-IR-para-Interiores-Exteriores-Tapo-C320WS-Inalambrico-">
                                                <div class="picture-wrapper">
                                                    <div data-cp-prod-slider="[&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-TP-LINK-TAPOC320WS-6a6ea4.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-TP-LINK-TAPOC320WS-596f5a.jpg&quot;]" class="catSlider">
                                                        <div data-cp-img-container class="cs-image"></div>
                                                        <div class="csnav"></div>
                                                    </div>
                                                </div>
                                                <div class="picture-overlay">
                                                    <div class="badges-left">
                                                        <img alt="TP-LINK" class="product-logo" src="<?php echo base_url();?>cdn.kyoceraap.com/storage/brands/0edf5040c0aad9fe27afc61f90b81066_tplink.png" data-cp-brand-logo="Por-Marca/TP-LINK/index.html">
                                                    </div>
                                                    <div class="badges-middle">
                                                    </div>
                                                    <div class="badges-right">
                                                        <div class="badges-right-top">
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="emproduct_review">
                                                <div class="detailsInfo_right_review" style="margin-top:0; ">
                                                    <div class="cpreviews_popup">
                                                        <div style="float:left; position:relative; "
                                                            class="stars">
                                                            <div class="cpreviews_stars_box clear" title="
                                                                Me gusta
                                                                "
                                                                >
                                                                <div class="cpreviews_stars_box_percent" style="width:96%; "></div>
                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                            </div>                </div>
                                                            <div style="float:left; position:relative; " class="popupborder">
                                                                <div class="popuptitle"></div>
                                                                <div class="popupbridge"></div>
                                                                <div class="popupbox">
                                                                    <div class="cpreviews_box">
                                                                        <div class="cpreviews_box_header clear">
                                                                            <div class="left">
                                                                                <div class="stars">
                                                                                    <div class="cpreviews_stars_box clear" title="
                                                                                        Me gusta
                                                                                        "
                                                                                        >
                                                                                        <div class="cpreviews_stars_box_percent" style="width:96%; "></div>
                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                    </div>                </div>
                                                                                    <div class="desc">4.8  de 5 estrellas </div>
                                                                                </div>
                                                                                <div class="right">
                                                                                    4
                                                                                    opiniones
                                                                                </div>
                                                                            </div>
                                                                            <div class="cpreviews_starsdesc">
                                                                                <div class="cpreviews_starsdesc_row clear">
                                                                                    <div class="cpreviews_starsdesc_linebg"></div>
                                                                                    <div class="cpreviews_starsdesc_line"
                                                                                    style="width:75px; "></div>
                                                                                    <div class="cpreviews_starsdesc_text">5 estrellas: (3)
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cpreviews_starsdesc_row clear">
                                                                                    <div class="cpreviews_starsdesc_linebg"></div>
                                                                                    <div class="cpreviews_starsdesc_line"
                                                                                    style="width:25px; "></div>
                                                                                    <div class="cpreviews_starsdesc_text">4 estrellas: (1)
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cpreviews_starsdesc_row clear">
                                                                                    <div class="cpreviews_starsdesc_linebg"></div>
                                                                                    <div class="cpreviews_starsdesc_line"
                                                                                    style="width:0px; "></div>
                                                                                    <div class="cpreviews_starsdesc_text">3 estrellas: (0)
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cpreviews_starsdesc_row clear">
                                                                                    <div class="cpreviews_starsdesc_linebg"></div>
                                                                                    <div class="cpreviews_starsdesc_line"
                                                                                    style="width:0px; "></div>
                                                                                    <div class="cpreviews_starsdesc_text">2 estrellas: (0)
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cpreviews_starsdesc_row clear">
                                                                                    <div class="cpreviews_starsdesc_linebg"></div>
                                                                                    <div class="cpreviews_starsdesc_line"
                                                                                    style="width:0px; "></div>
                                                                                    <div class="cpreviews_starsdesc_text">1 estrellas: (0)
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="cpreviews_box_link">
                                                                                <a href="Opiniones-sobre-TP-Link-Camara-IP-Smart-WiFi-Bullet-IR-para-Interiores/Exteriores-Tapo-C320WS-Inalambrico-2160-x-1440-Pixeles-Dia/Noche/index.html">Ver todas las (4) opiniones</a>
                                                                            </div>
                                                                        </div>                                                    </div>
                                                                    </div>
                                                                    <div class="reviews-total-txt" >
                                                                        <a href="Opiniones-sobre-TP-Link-Camara-IP-Smart-WiFi-Bullet-IR-para-Interiores/Exteriores-Tapo-C320WS-Inalambrico-2160-x-1440-Pixeles-Dia/Noche/index.html">
                                                                            4
                                                                            opiniones
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-cp-complete-name="emproduct_cptitleBox">
                                                            <div class="emproduct_cptitleBox">
                                                                <a id="emstartpagenew-1" href="Seguridad-y-Vigilancia/Camaras-y-Sistemas-de-Vigilancia/Camaras-de-Seguridad-IP/TP-Link-Camara-IP-Smart-WiFi-Bullet-IR-para-Interiores-Exteriores-Tapo-C320WS-Inalambrico-"
                                                                    class="emproduct_title cpGaProdemstartpagenew-1"
                                                                    title="TP-Link Cámara IP Smart WiFi Bullet IR para Interiores/Exteriores Tapo C320WS, Inalámbrico, 2160 x 1440 Pixeles, Día/Noche"
                                                                    data-truncate="TP-Link Cámara IP Smart WiFi Bullet IR para Interiores/Exteriores Tapo C320WS, Inalámbr..."
                                                                    data-text>
                                                                    <span>TP-Link Cámara IP Smart WiFi Bullet IR para Interiores/Exteriores Tapo C320WS, Inalámbr...</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="grid-x align-middle background-white cp-pt-10" style="padding-bottom: 5px;">
                                                            <div class="cell auto">
                                                                <div class="emproduct_artnum">TAPO C320WS</div>
                                                            </div>
                                                            <div class="cell shrink">
                                                            </div>
                                                        </div>
                                                        <div class="main-info background-white">
                                                            <div class="addtoCart-section">
                                                                <div class="tobasketFunction">
                                                                    <button id="toBasket_emstartpagenew-1" type="submit"
                                                                    class="submitButton largeButton">Agregar
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="moreinfo-section">
                                                                <!-- Price -->
                                                                <!--sse-->
                                                                <div class="emproduct_price">
                                                                    <span class="oldPrice">
                                                                        <del>$909.00</del>
                                                                    </span>
                                                                    <label class="price">
                                                                        $829.00
                                                                    </label>
                                                                </div>
                                                                <!--/sse-->
                                                                <!-- Delivery Cost -->
                                                                <div class="emdeliverycost">
                                                                    <span class="deliverytext">Costo de envío:</span>
                                                                    <span class="deliveryvalue">$99.00</span>
                                                                </div>
                                                                <!--  Stock  -->
                                                                <div class="emstock">            Disponibles:
                                                                    <span>8</span>
                                                                    pzas.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="emproduct_compare">
                                                            <div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_emstartpagenew-1" data-id="listproduct_emstartpagenew-1">
                                                                <div id="compare_removelistproduct_emstartpagenew-1" style=display:none>
                                                                    <div class="emcomparelinkbox_link_ctn ">
                                                                        <a data-id="listproduct_emstartpagenew-1" class="emcomparelinkbox_link_remove" data-aid="3e69637e033d21d9fe49f9307b45c545"
                                                                            id="removeCmplistproduct_emstartpagenew-1" href="#" onclick="go_to('index2ab0.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=3e69637e033d21d9fe49f9307b45c545&amp;anid=3e69637e033d21d9fe49f9307b45c545&amp;pgNr=0&amp;searchparam=')">
                                                                            <span class="square"></span>
                                                                        </a>
                                                                        <a class="emcomparelinkbox_link_txt " href="lista-de-comparacion/index.html">
                                                                        Ver Comparación (0)            </a>
                                                                    </div>
                                                                </div>
                                                                <div id="compare_addlistproduct_emstartpagenew-1" >
                                                                    <a data-id="listproduct_emstartpagenew-1" class="emcomparelinkbox_link_add" data-aid="3e69637e033d21d9fe49f9307b45c545"
                                                                        id="toCmplistproduct_emstartpagenew-1" href="#" onclick="go_to('index2ab0.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=3e69637e033d21d9fe49f9307b45c545&amp;anid=3e69637e033d21d9fe49f9307b45c545&amp;pgNr=0&amp;searchparam=')">
                                                                        <span class="square"></span>
                                                                        Comparar
                                                                    </a>
                                                                </div>
                                                            </div>                </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </li>
                                            <li class="productData cell" data-cp-start-daily-product style="display: none">
                                                <div class="emproductborder listiteminfogridborder emsmoothborder clear">
                                                    <form name="tobasketemstartpagenew-2"
                                                        action="https://www.kyoceraap.com/index.php?" method="post"
                                                        >
                                                        <input type="hidden" name="actcontrol" value="start" />
                                                        <input type="hidden" name="lang" value="0" />
                                                        <input type="hidden" name="pgNr" value="0">
                                                        <input type="hidden" name="cl" value="start">
                                                        <input type="hidden" name="fnc" value="tobasket">
                                                        <input type="hidden" name="aid" value="a8490bf384a38d69b91f5f9a08499ad7">
                                                        <input type="hidden" name="anid" value="a8490bf384a38d69b91f5f9a08499ad7">            <input type="hidden" name="am" value="1">
                                                        <input type="hidden" name="oxloadid" value="">
                                                        <div class="emproduct clear listiteminfogrid">
                                                            <div class="offerbox  hasuntil  hasdaily clear">
                                                                <div class="percent">
                                                                    -9%
                                                                    <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.png" alt="daily offer icon"/>
                                                                    <span class="onlyhome">
                                                                        ($190.00)
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <a class="cp-picture-container cpGaProdemstartpagenew-2" href="Computo-Hardware/Componentes/Fuentes-de-Poder-para-PC-s/Fuente-de-Poder-MSI-MPG-A650GF-80-PLUS-Gold-24-pines-ATX-140mm-650W.html">
                                                                <div class="picture-wrapper">
                                                                    <div data-cp-prod-slider="[&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-MSI-MPGA650GF-1.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-MSI-MPGA650GF-2.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-MSI-MPGA650GF-3.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-MSI-MPGA650GF-4.jpg&quot;]" class="catSlider">
                                                                        <div data-cp-img-container class="cs-image"></div>
                                                                        <div class="csnav"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="picture-overlay">
                                                                    <div class="badges-left">
                                                                        <img alt="MSI" class="product-logo" src="<?php echo base_url();?>cdn.kyoceraap.com/storage/brands/8726e36e9d6d53646390d495b4ce9dc1_msi.png" data-cp-brand-logo="Por-Marca/MSI/index.html">
                                                                    </div>
                                                                    <div class="badges-middle">
                                                                    </div>
                                                                    <div class="badges-right">
                                                                        <div class="badges-right-top">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div class="emproduct_review">
                                                                <div class="detailsInfo_right_review" style="margin-top:0; ">
                                                                    <div class="cpreviews_popup">
                                                                        <div style="float:left; position:relative; "
                                                                            class="stars">
                                                                            <div class="cpreviews_stars_box clear" title="
                                                                                ¡Lo mejor!
                                                                                "
                                                                                >
                                                                                <div class="cpreviews_stars_box_percent" style="width:100%; "></div>
                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                            </div>                </div>
                                                                            <div style="float:left; position:relative; " class="popupborder">
                                                                                <div class="popuptitle"></div>
                                                                                <div class="popupbridge"></div>
                                                                                <div class="popupbox">
                                                                                    <div class="cpreviews_box">
                                                                                        <div class="cpreviews_box_header clear">
                                                                                            <div class="left">
                                                                                                <div class="stars">
                                                                                                    <div class="cpreviews_stars_box clear" title="
                                                                                                        ¡Lo mejor!
                                                                                                        "
                                                                                                        >
                                                                                                        <div class="cpreviews_stars_box_percent" style="width:100%; "></div>
                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                    </div>                </div>
                                                                                                    <div class="desc">5  de 5 estrellas </div>
                                                                                                </div>
                                                                                                <div class="right">
                                                                                                    1
                                                                                                    opinión
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="cpreviews_starsdesc">
                                                                                                <div class="cpreviews_starsdesc_row clear">
                                                                                                    <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                    <div class="cpreviews_starsdesc_line"
                                                                                                    style="width:100px; "></div>
                                                                                                    <div class="cpreviews_starsdesc_text">5 estrellas: (1)
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="cpreviews_starsdesc_row clear">
                                                                                                    <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                    <div class="cpreviews_starsdesc_line"
                                                                                                    style="width:0px; "></div>
                                                                                                    <div class="cpreviews_starsdesc_text">4 estrellas: (0)
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="cpreviews_starsdesc_row clear">
                                                                                                    <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                    <div class="cpreviews_starsdesc_line"
                                                                                                    style="width:0px; "></div>
                                                                                                    <div class="cpreviews_starsdesc_text">3 estrellas: (0)
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="cpreviews_starsdesc_row clear">
                                                                                                    <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                    <div class="cpreviews_starsdesc_line"
                                                                                                    style="width:0px; "></div>
                                                                                                    <div class="cpreviews_starsdesc_text">2 estrellas: (0)
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="cpreviews_starsdesc_row clear">
                                                                                                    <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                    <div class="cpreviews_starsdesc_line"
                                                                                                    style="width:0px; "></div>
                                                                                                    <div class="cpreviews_starsdesc_text">1 estrellas: (0)
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="cpreviews_box_link">
                                                                                                <a href="Opiniones-sobre-Fuente-de-Poder-MSI-MPG-A650GF-80-PLUS-Gold-24-pines-ATX-140mm-650W/index.html">Ver todas las (1) opiniones</a>
                                                                                            </div>
                                                                                        </div>                                                    </div>
                                                                                    </div>
                                                                                    <div class="reviews-total-txt" >
                                                                                        <a href="Opiniones-sobre-Fuente-de-Poder-MSI-MPG-A650GF-80-PLUS-Gold-24-pines-ATX-140mm-650W/index.html">
                                                                                            1
                                                                                            opinión
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div data-cp-complete-name="emproduct_cptitleBox">
                                                                            <div class="emproduct_cptitleBox">
                                                                                <a id="emstartpagenew-2" href="Computo-Hardware/Componentes/Fuentes-de-Poder-para-PC-s/Fuente-de-Poder-MSI-MPG-A650GF-80-PLUS-Gold-24-pines-ATX-140mm-650W.html"
                                                                                    class="emproduct_title cpGaProdemstartpagenew-2"
                                                                                    title="Fuente de Poder MSI MPG A650GF 80 PLUS Gold, 24-pines ATX, 140mm, 650W"
                                                                                    data-truncate="Fuente de Poder MSI MPG A650GF 80 PLUS Gold, 24-pines ATX, 140mm, 650W"
                                                                                    data-text>
                                                                                    <span>Fuente de Poder MSI MPG A650GF 80 PLUS Gold, 24-pines ATX, 140mm, 650W</span>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="grid-x align-middle background-white cp-pt-10" style="padding-bottom: 5px;">
                                                                            <div class="cell auto">
                                                                                <div class="emproduct_artnum">MPG A650GF</div>
                                                                            </div>
                                                                            <div class="cell shrink">
                                                                            </div>
                                                                        </div>
                                                                        <div class="main-info background-white">
                                                                            <div class="addtoCart-section">
                                                                                <div class="tobasketFunction">
                                                                                    <button id="toBasket_emstartpagenew-2" type="submit"
                                                                                    class="submitButton largeButton">Agregar
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                            <div class="moreinfo-section">
                                                                                <!-- Price -->
                                                                                <!--sse-->
                                                                                <div class="emproduct_price">
                                                                                    <span class="oldPrice">
                                                                                        <del>$2,209.00</del>
                                                                                    </span>
                                                                                    <label class="price">
                                                                                        $2,019.00
                                                                                    </label>
                                                                                </div>
                                                                                <!--/sse-->
                                                                                <!-- Delivery Cost -->
                                                                                <div class="emdeliverycost">
                                                                                    <span class="deliverytext">Costo de envío:</span>
                                                                                    <span class="deliveryvalue">$99.00</span>
                                                                                </div>
                                                                                <!--  Stock  -->
                                                                                <div class="emstock">            Disponibles:
                                                                                    <span>47</span>
                                                                                    pzas.
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="emproduct_compare">
                                                                            <div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_emstartpagenew-2" data-id="listproduct_emstartpagenew-2">
                                                                                <div id="compare_removelistproduct_emstartpagenew-2" style=display:none>
                                                                                    <div class="emcomparelinkbox_link_ctn ">
                                                                                        <a data-id="listproduct_emstartpagenew-2" class="emcomparelinkbox_link_remove" data-aid="a8490bf384a38d69b91f5f9a08499ad7"
                                                                                            id="removeCmplistproduct_emstartpagenew-2" href="#" onclick="go_to('index1272.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=a8490bf384a38d69b91f5f9a08499ad7&amp;anid=a8490bf384a38d69b91f5f9a08499ad7&amp;pgNr=0&amp;searchparam=')">
                                                                                            <span class="square"></span>
                                                                                        </a>
                                                                                        <a class="emcomparelinkbox_link_txt " href="lista-de-comparacion/index.html">
                                                                                        Ver Comparación (0)            </a>
                                                                                    </div>
                                                                                </div>
                                                                                <div id="compare_addlistproduct_emstartpagenew-2" >
                                                                                    <a data-id="listproduct_emstartpagenew-2" class="emcomparelinkbox_link_add" data-aid="a8490bf384a38d69b91f5f9a08499ad7"
                                                                                        id="toCmplistproduct_emstartpagenew-2" href="#" onclick="go_to('index1272.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=a8490bf384a38d69b91f5f9a08499ad7&amp;anid=a8490bf384a38d69b91f5f9a08499ad7&amp;pgNr=0&amp;searchparam=')">
                                                                                        <span class="square"></span>
                                                                                        Comparar
                                                                                    </a>
                                                                                </div>
                                                                            </div>                </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </li>
                                                            <li class="productData cell" data-cp-start-daily-product style="display: none">
                                                                <div class="emproductborder listiteminfogridborder emsmoothborder clear">
                                                                    <form name="tobasketemstartpagenew-3"
                                                                        action="https://www.kyoceraap.com/index.php?" method="post"
                                                                        >
                                                                        <input type="hidden" name="actcontrol" value="start" />
                                                                        <input type="hidden" name="lang" value="0" />
                                                                        <input type="hidden" name="pgNr" value="0">
                                                                        <input type="hidden" name="cl" value="start">
                                                                        <input type="hidden" name="fnc" value="tobasket">
                                                                        <input type="hidden" name="aid" value="a2688383bd022d9b4816075d62c882fa">
                                                                        <input type="hidden" name="anid" value="a2688383bd022d9b4816075d62c882fa">            <input type="hidden" name="am" value="1">
                                                                        <input type="hidden" name="oxloadid" value="">
                                                                        <div class="emproduct clear listiteminfogrid">
                                                                            <div class="offerbox  hasuntil  hasdaily clear">
                                                                                <div class="percent">
                                                                                    -12%
                                                                                    <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.png" alt="daily offer icon"/>
                                                                                    <span class="onlyhome">
                                                                                        ($775.00)
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <a class="cp-picture-container cpGaProdemstartpagenew-3" href="Computo-Hardware/Componentes/Tarjetas-Madre/Tarjeta-Madre-ASUS-ATX-TUF-Gaming-X670E-Plus-WiFi-S-AM5-AMD-X670-HDMI-128GB-DDR5-para-AMD.html">
                                                                                <div class="picture-wrapper">
                                                                                    <div data-cp-prod-slider="[&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-ASUS-90MB1BK0-M0AAY0-b10682.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-ASUS-90MB1BK0-M0AAY0-11f7fb.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-ASUS-90MB1BK0-M0AAY0-5b8397.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-ASUS-90MB1BK0-M0AAY0-7c1563.jpg&quot;]" class="catSlider">
                                                                                        <div data-cp-img-container class="cs-image"></div>
                                                                                        <div class="csnav"></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="picture-overlay">
                                                                                    <div class="badges-left">
                                                                                        <img alt="ASUS" class="product-logo" src="<?php echo base_url();?>cdn.kyoceraap.com/storage/brands/asuslogoa5705fdb70seeklogo.com.png" data-cp-brand-logo="Por-Marca/ASUS/index.html">
                                                                                    </div>
                                                                                    <div class="badges-middle">
                                                                                    </div>
                                                                                    <div class="badges-right">
                                                                                        <div class="badges-right-top">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            <div class="emproduct_review">
                                                                            </div>
                                                                            <div data-cp-complete-name="emproduct_cptitleBox">
                                                                                <div class="emproduct_cptitleBox">
                                                                                    <a id="emstartpagenew-3" href="Computo-Hardware/Componentes/Tarjetas-Madre/Tarjeta-Madre-ASUS-ATX-TUF-Gaming-X670E-Plus-WiFi-S-AM5-AMD-X670-HDMI-128GB-DDR5-para-AMD.html"
                                                                                        class="emproduct_title cpGaProdemstartpagenew-3"
                                                                                        title="Tarjeta Madre ASUS ATX TUF Gaming X670E Plus WiFi, S-AM5, AMD X670, HDMI, 128GB DDR5 para AMD"
                                                                                        data-truncate="Tarjeta Madre ASUS ATX TUF Gaming X670E Plus WiFi, S-AM5, AMD X670, HDMI, 128GB DDR5 pa..."
                                                                                        data-text>
                                                                                        <span>Tarjeta Madre ASUS ATX TUF Gaming X670E Plus WiFi, S-AM5, AMD X670, HDMI, 128GB DDR5 pa...</span>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="grid-x align-middle background-white cp-pt-10" style="padding-bottom: 5px;">
                                                                                <div class="cell auto">
                                                                                    <div class="emproduct_artnum">90MB1BK0-M0AAY0</div>
                                                                                </div>
                                                                                <div class="cell shrink">
                                                                                </div>
                                                                            </div>
                                                                            <div class="main-info background-white">
                                                                                <div class="addtoCart-section">
                                                                                    <div class="tobasketFunction">
                                                                                        <button id="toBasket_emstartpagenew-3" type="submit"
                                                                                        class="submitButton largeButton">Agregar
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="moreinfo-section">
                                                                                    <!-- Price -->
                                                                                    <!--sse-->
                                                                                    <div class="emproduct_price">
                                                                                        <span class="oldPrice">
                                                                                            <del>$6,524.00</del>
                                                                                        </span>
                                                                                        <label class="price">
                                                                                            $5,749.00
                                                                                        </label>
                                                                                    </div>
                                                                                    <!--/sse-->
                                                                                    <!-- Delivery Cost -->
                                                                                    <div class="emdeliverycost">
                                                                                        <span class="deliverytext">Costo de envío:</span>
                                                                                        <span class="deliveryvalue">$99.00</span>
                                                                                    </div>
                                                                                    <!--  Stock  -->
                                                                                    <div class="emstock">            Disponibles:
                                                                                        <span>7</span>
                                                                                        pzas.
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="emproduct_compare">
                                                                                <div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_emstartpagenew-3" data-id="listproduct_emstartpagenew-3">
                                                                                    <div id="compare_removelistproduct_emstartpagenew-3" style=display:none>
                                                                                        <div class="emcomparelinkbox_link_ctn ">
                                                                                            <a data-id="listproduct_emstartpagenew-3" class="emcomparelinkbox_link_remove" data-aid="a2688383bd022d9b4816075d62c882fa"
                                                                                                id="removeCmplistproduct_emstartpagenew-3" href="#" onclick="go_to('index4ed7.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=a2688383bd022d9b4816075d62c882fa&amp;anid=a2688383bd022d9b4816075d62c882fa&amp;pgNr=0&amp;searchparam=')">
                                                                                                <span class="square"></span>
                                                                                            </a>
                                                                                            <a class="emcomparelinkbox_link_txt " href="lista-de-comparacion/index.html">
                                                                                            Ver Comparación (0)            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="compare_addlistproduct_emstartpagenew-3" >
                                                                                        <a data-id="listproduct_emstartpagenew-3" class="emcomparelinkbox_link_add" data-aid="a2688383bd022d9b4816075d62c882fa"
                                                                                            id="toCmplistproduct_emstartpagenew-3" href="#" onclick="go_to('index4ed7.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=a2688383bd022d9b4816075d62c882fa&amp;anid=a2688383bd022d9b4816075d62c882fa&amp;pgNr=0&amp;searchparam=')">
                                                                                            <span class="square"></span>
                                                                                            Comparar
                                                                                        </a>
                                                                                    </div>
                                                                                </div>                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </li>
                                                                <li class="productData cell" data-cp-start-daily-product style="display: none">
                                                                    <div class="emproductborder listiteminfogridborder emsmoothborder clear">
                                                                        <form name="tobasketemstartpagenew-4"
                                                                            action="https://www.kyoceraap.com/index.php?" method="post"
                                                                            >
                                                                            <input type="hidden" name="actcontrol" value="start" />
                                                                            <input type="hidden" name="lang" value="0" />
                                                                            <input type="hidden" name="pgNr" value="0">
                                                                            <input type="hidden" name="cl" value="start">
                                                                            <input type="hidden" name="fnc" value="tobasket">
                                                                            <input type="hidden" name="aid" value="93d2549aa5d8f37a401df3ee5c24c293">
                                                                            <input type="hidden" name="anid" value="93d2549aa5d8f37a401df3ee5c24c293">            <input type="hidden" name="am" value="1">
                                                                            <input type="hidden" name="oxloadid" value="">
                                                                            <div class="emproduct clear listiteminfogrid">
                                                                                <div class="offerbox  hasuntil  hasdaily clear">
                                                                                    <div class="percent">
                                                                                        -10%
                                                                                        <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.png" alt="daily offer icon"/>
                                                                                        <span class="onlyhome">
                                                                                            ($80.00)
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <a class="cp-picture-container cpGaProdemstartpagenew-4" href="Computo-Hardware/Componentes/Gabinetes/Gabinete-Ocelot-Gaming-OGMC01-con-Ventana-ATX-ATX-Micro-ATX-Mini-ITX-USB-3-2-sin-Fuente-Negro.html">
                                                                                    <div class="picture-wrapper">
                                                                                        <div data-cp-prod-slider="[&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-OCELOTGAMING-OGMC01-1.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-OCELOTGAMING-OGMC01-2.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-OCELOTGAMING-OGMC01-3.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-OCELOTGAMING-OGMC01-4.jpg&quot;]" class="catSlider">
                                                                                            <div data-cp-img-container class="cs-image"></div>
                                                                                            <div class="csnav"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="picture-overlay">
                                                                                        <div class="badges-left">
                                                                                            <img alt="OCELOT GAMING" class="product-logo" src="<?php echo base_url();?>cdn.kyoceraap.com/storage/brands/LOGOTIPOOCELOT01.png" data-cp-brand-logo="Por-Marca/OCELOT-GAMING/index.html">
                                                                                        </div>
                                                                                        <div class="badges-middle">
                                                                                        </div>
                                                                                        <div class="badges-right">
                                                                                            <div class="badges-right-top">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <div class="emproduct_review">
                                                                                    <div class="detailsInfo_right_review" style="margin-top:0; ">
                                                                                        <div class="cpreviews_popup">
                                                                                            <div style="float:left; position:relative; "
                                                                                                class="stars">
                                                                                                <div class="cpreviews_stars_box clear" title="
                                                                                                    Me gusta
                                                                                                    "
                                                                                                    >
                                                                                                    <div class="cpreviews_stars_box_percent" style="width:92%; "></div>
                                                                                                    <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                    <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                    <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                    <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                    <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                </div>                </div>
                                                                                                <div style="float:left; position:relative; " class="popupborder">
                                                                                                    <div class="popuptitle"></div>
                                                                                                    <div class="popupbridge"></div>
                                                                                                    <div class="popupbox">
                                                                                                        <div class="cpreviews_box">
                                                                                                            <div class="cpreviews_box_header clear">
                                                                                                                <div class="left">
                                                                                                                    <div class="stars">
                                                                                                                        <div class="cpreviews_stars_box clear" title="
                                                                                                                            Me gusta
                                                                                                                            "
                                                                                                                            >
                                                                                                                            <div class="cpreviews_stars_box_percent" style="width:92%; "></div>
                                                                                                                            <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                            <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                            <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                            <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                            <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                        </div>                </div>
                                                                                                                        <div class="desc">4.6  de 5 estrellas </div>
                                                                                                                    </div>
                                                                                                                    <div class="right">
                                                                                                                        176
                                                                                                                        opiniones
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="cpreviews_starsdesc">
                                                                                                                    <div class="cpreviews_starsdesc_row clear">
                                                                                                                        <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                        <div class="cpreviews_starsdesc_line"
                                                                                                                        style="width:61.931818181818px; "></div>
                                                                                                                        <div class="cpreviews_starsdesc_text">5 estrellas: (109)
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="cpreviews_starsdesc_row clear">
                                                                                                                        <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                        <div class="cpreviews_starsdesc_line"
                                                                                                                        style="width:34.090909090909px; "></div>
                                                                                                                        <div class="cpreviews_starsdesc_text">4 estrellas: (60)
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="cpreviews_starsdesc_row clear">
                                                                                                                        <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                        <div class="cpreviews_starsdesc_line"
                                                                                                                        style="width:3.4090909090909px; "></div>
                                                                                                                        <div class="cpreviews_starsdesc_text">3 estrellas: (6)
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="cpreviews_starsdesc_row clear">
                                                                                                                        <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                        <div class="cpreviews_starsdesc_line"
                                                                                                                        style="width:0px; "></div>
                                                                                                                        <div class="cpreviews_starsdesc_text">2 estrellas: (0)
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="cpreviews_starsdesc_row clear">
                                                                                                                        <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                        <div class="cpreviews_starsdesc_line"
                                                                                                                        style="width:0.56818181818182px; "></div>
                                                                                                                        <div class="cpreviews_starsdesc_text">1 estrellas: (1)
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="cpreviews_box_link">
                                                                                                                    <a href="Opiniones-sobre-Gabinete-Ocelot-Gaming-OGMC01-con-Ventana-ATX-ATX/Micro-ATX/Mini-ITX-USB-3-2-sin-Fuente/sin-Ventiladores-Instalados-Negro/index.html">Ver todas las (176) opiniones</a>
                                                                                                                </div>
                                                                                                            </div>                                                    </div>
                                                                                                        </div>
                                                                                                        <div class="reviews-total-txt" >
                                                                                                            <a href="Opiniones-sobre-Gabinete-Ocelot-Gaming-OGMC01-con-Ventana-ATX-ATX/Micro-ATX/Mini-ITX-USB-3-2-sin-Fuente/sin-Ventiladores-Instalados-Negro/index.html">
                                                                                                                176
                                                                                                                opiniones
                                                                                                            </a>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div data-cp-complete-name="emproduct_cptitleBox">
                                                                                                <div class="emproduct_cptitleBox">
                                                                                                    <a id="emstartpagenew-4" href="Computo-Hardware/Componentes/Gabinetes/Gabinete-Ocelot-Gaming-OGMC01-con-Ventana-ATX-ATX-Micro-ATX-Mini-ITX-USB-3-2-sin-Fuente-Negro.html"
                                                                                                        class="emproduct_title cpGaProdemstartpagenew-4"
                                                                                                        title="Gabinete Ocelot Gaming OGMC01 con Ventana, ATX, ATX/Micro-ATX/Mini-ITX, USB 3.2, sin Fuente/sin Ventiladores Instalados, Negro"
                                                                                                        data-truncate="Gabinete Ocelot Gaming OGMC01 con Ventana, ATX, ATX/Micro-ATX/Mini-ITX, USB 3.2, sin Fu..."
                                                                                                        data-text>
                                                                                                        <span>Gabinete Ocelot Gaming OGMC01 con Ventana, ATX, ATX/Micro-ATX/Mini-ITX, USB 3.2, sin Fu...</span>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="grid-x align-middle background-white cp-pt-10" style="padding-bottom: 5px;">
                                                                                                <div class="cell auto">
                                                                                                    <div class="emproduct_artnum">OGMC01</div>
                                                                                                </div>
                                                                                                <div class="cell shrink">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="main-info background-white">
                                                                                                <div class="addtoCart-section">
                                                                                                    <div class="tobasketFunction">
                                                                                                        <button id="toBasket_emstartpagenew-4" type="submit"
                                                                                                        class="submitButton largeButton">Agregar
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="moreinfo-section">
                                                                                                    <!-- Price -->
                                                                                                    <!--sse-->
                                                                                                    <div class="emproduct_price">
                                                                                                        <span class="oldPrice">
                                                                                                            <del>$789.00</del>
                                                                                                        </span>
                                                                                                        <label class="price">
                                                                                                            $709.00
                                                                                                        </label>
                                                                                                    </div>
                                                                                                    <!--/sse-->
                                                                                                    <!-- Delivery Cost -->
                                                                                                    <div class="emdeliverycost">
                                                                                                        <span class="deliverytext">Costo de envío:</span>
                                                                                                        <span class="deliveryvalue">$149.00</span>
                                                                                                    </div>
                                                                                                    <!--  Stock  -->
                                                                                                    <div class="emstock">            Disponibles:
                                                                                                        <span>246</span>
                                                                                                        pzas.
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="emproduct_compare">
                                                                                                <div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_emstartpagenew-4" data-id="listproduct_emstartpagenew-4">
                                                                                                    <div id="compare_removelistproduct_emstartpagenew-4" style=display:none>
                                                                                                        <div class="emcomparelinkbox_link_ctn ">
                                                                                                            <a data-id="listproduct_emstartpagenew-4" class="emcomparelinkbox_link_remove" data-aid="93d2549aa5d8f37a401df3ee5c24c293"
                                                                                                                id="removeCmplistproduct_emstartpagenew-4" href="#" onclick="go_to('index6ace.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=93d2549aa5d8f37a401df3ee5c24c293&amp;anid=93d2549aa5d8f37a401df3ee5c24c293&amp;pgNr=0&amp;searchparam=')">
                                                                                                                <span class="square"></span>
                                                                                                            </a>
                                                                                                            <a class="emcomparelinkbox_link_txt " href="lista-de-comparacion/index.html">
                                                                                                            Ver Comparación (0)            </a>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div id="compare_addlistproduct_emstartpagenew-4" >
                                                                                                        <a data-id="listproduct_emstartpagenew-4" class="emcomparelinkbox_link_add" data-aid="93d2549aa5d8f37a401df3ee5c24c293"
                                                                                                            id="toCmplistproduct_emstartpagenew-4" href="#" onclick="go_to('index6ace.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=93d2549aa5d8f37a401df3ee5c24c293&amp;anid=93d2549aa5d8f37a401df3ee5c24c293&amp;pgNr=0&amp;searchparam=')">
                                                                                                            <span class="square"></span>
                                                                                                            Comparar
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </div>                </div>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </li>
                                                                                <li class="productData cell" data-cp-start-daily-product style="display: none">
                                                                                    <div class="emproductborder listiteminfogridborder emsmoothborder clear">
                                                                                        <form name="tobasketemstartpagenew-5"
                                                                                            action="https://www.kyoceraap.com/index.php?" method="post"
                                                                                            >
                                                                                            <input type="hidden" name="actcontrol" value="start" />
                                                                                            <input type="hidden" name="lang" value="0" />
                                                                                            <input type="hidden" name="pgNr" value="0">
                                                                                            <input type="hidden" name="cl" value="start">
                                                                                            <input type="hidden" name="fnc" value="tobasket">
                                                                                            <input type="hidden" name="aid" value="72940b35fd126fe57beac27b915b7802">
                                                                                            <input type="hidden" name="anid" value="72940b35fd126fe57beac27b915b7802">            <input type="hidden" name="am" value="1">
                                                                                            <input type="hidden" name="oxloadid" value="">
                                                                                            <div class="emproduct clear listiteminfogrid">
                                                                                                <div class="offerbox  hasuntil  hasdaily clear">
                                                                                                    <div class="percent">
                                                                                                        -10%
                                                                                                        <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.png" alt="daily offer icon"/>
                                                                                                        <span class="onlyhome">
                                                                                                            ($80.00)
                                                                                                        </span>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <a class="cp-picture-container cpGaProdemstartpagenew-5" href="Computo-Hardware/Componentes/Gabinetes/Gabinete-Yeyian-Blade-2101-con-Ventana-LED-Rojo-Midi-Tower-ATX-Micro-ATX-USB-2-0-3-1-sin-Fuente-Negro.html">
                                                                                                    <div class="picture-wrapper">
                                                                                                        <div data-cp-prod-slider="[&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-YEYIAN-YNH-B2101-1.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-YEYIAN-YNH-B2101-2.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-YEYIAN-YNH-B2101-3.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-YEYIAN-YNH-B2101-4.jpg&quot;]" class="catSlider">
                                                                                                            <div data-cp-img-container class="cs-image"></div>
                                                                                                            <div class="csnav"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="picture-overlay">
                                                                                                        <div class="badges-left">
                                                                                                            <img alt="YEYIAN" class="product-logo" src="<?php echo base_url();?>cdn.kyoceraap.com/storage/brands/4d96a5d1feea6fd99445193a6b8b82db_YEYIAN.png" data-cp-brand-logo="Por-Marca/YEYIAN/index.html">
                                                                                                        </div>
                                                                                                        <div class="badges-middle">
                                                                                                        </div>
                                                                                                        <div class="badges-right">
                                                                                                            <div class="badges-right-top">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </a>
                                                                                                <div class="emproduct_review">
                                                                                                    <div class="detailsInfo_right_review" style="margin-top:0; ">
                                                                                                        <div class="cpreviews_popup">
                                                                                                            <div style="float:left; position:relative; "
                                                                                                                class="stars">
                                                                                                                <div class="cpreviews_stars_box clear" title="
                                                                                                                    Me gusta
                                                                                                                    "
                                                                                                                    >
                                                                                                                    <div class="cpreviews_stars_box_percent" style="width:86%; "></div>
                                                                                                                    <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                    <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                    <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                    <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                    <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                </div>                </div>
                                                                                                                <div style="float:left; position:relative; " class="popupborder">
                                                                                                                    <div class="popuptitle"></div>
                                                                                                                    <div class="popupbridge"></div>
                                                                                                                    <div class="popupbox">
                                                                                                                        <div class="cpreviews_box">
                                                                                                                            <div class="cpreviews_box_header clear">
                                                                                                                                <div class="left">
                                                                                                                                    <div class="stars">
                                                                                                                                        <div class="cpreviews_stars_box clear" title="
                                                                                                                                            Me gusta
                                                                                                                                            "
                                                                                                                                            >
                                                                                                                                            <div class="cpreviews_stars_box_percent" style="width:86%; "></div>
                                                                                                                                            <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                            <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                            <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                            <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                            <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                        </div>                </div>
                                                                                                                                        <div class="desc">4.3  de 5 estrellas </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="right">
                                                                                                                                        85
                                                                                                                                        opiniones
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="cpreviews_starsdesc">
                                                                                                                                    <div class="cpreviews_starsdesc_row clear">
                                                                                                                                        <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                        <div class="cpreviews_starsdesc_line"
                                                                                                                                        style="width:49.411764705882px; "></div>
                                                                                                                                        <div class="cpreviews_starsdesc_text">5 estrellas: (42)
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="cpreviews_starsdesc_row clear">
                                                                                                                                        <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                        <div class="cpreviews_starsdesc_line"
                                                                                                                                        style="width:36.470588235294px; "></div>
                                                                                                                                        <div class="cpreviews_starsdesc_text">4 estrellas: (31)
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="cpreviews_starsdesc_row clear">
                                                                                                                                        <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                        <div class="cpreviews_starsdesc_line"
                                                                                                                                        style="width:11.764705882353px; "></div>
                                                                                                                                        <div class="cpreviews_starsdesc_text">3 estrellas: (10)
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="cpreviews_starsdesc_row clear">
                                                                                                                                        <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                        <div class="cpreviews_starsdesc_line"
                                                                                                                                        style="width:2.3529411764706px; "></div>
                                                                                                                                        <div class="cpreviews_starsdesc_text">2 estrellas: (2)
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="cpreviews_starsdesc_row clear">
                                                                                                                                        <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                        <div class="cpreviews_starsdesc_line"
                                                                                                                                        style="width:0px; "></div>
                                                                                                                                        <div class="cpreviews_starsdesc_text">1 estrellas: (0)
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="cpreviews_box_link">
                                                                                                                                    <a href="Opiniones-sobre-Gabinete-Yeyian-Blade-2101-con-Ventana-LED-Rojo-Midi-Tower-ATX/Micro-ATX-USB-2-0/3-1-sin-Fuente-1-Ventilador-LED-Instalado-Negro/index.html">Ver todas las (85) opiniones</a>
                                                                                                                                </div>
                                                                                                                            </div>                                                    </div>
                                                                                                                        </div>
                                                                                                                        <div class="reviews-total-txt" >
                                                                                                                            <a href="Opiniones-sobre-Gabinete-Yeyian-Blade-2101-con-Ventana-LED-Rojo-Midi-Tower-ATX/Micro-ATX-USB-2-0/3-1-sin-Fuente-1-Ventilador-LED-Instalado-Negro/index.html">
                                                                                                                                85
                                                                                                                                opiniones
                                                                                                                            </a>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div data-cp-complete-name="emproduct_cptitleBox">
                                                                                                                <div class="emproduct_cptitleBox">
                                                                                                                    <a id="emstartpagenew-5" href="Computo-Hardware/Componentes/Gabinetes/Gabinete-Yeyian-Blade-2101-con-Ventana-LED-Rojo-Midi-Tower-ATX-Micro-ATX-USB-2-0-3-1-sin-Fuente-Negro.html"
                                                                                                                        class="emproduct_title cpGaProdemstartpagenew-5"
                                                                                                                        title="Gabinete Yeyian Blade 2101 con Ventana LED Rojo, Midi-Tower, ATX/Micro-ATX, USB 2.0/3.1, sin Fuente, 1 Ventilador LED Instalado, Negro"
                                                                                                                        data-truncate="Gabinete Yeyian Blade 2101 con Ventana LED Rojo, Midi-Tower, ATX/Micro-ATX, USB 2.0/3.1..."
                                                                                                                        data-text>
                                                                                                                        <span>Gabinete Yeyian Blade 2101 con Ventana LED Rojo, Midi-Tower, ATX/Micro-ATX, USB 2.0/3.1...</span>
                                                                                                                    </a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="grid-x align-middle background-white cp-pt-10" style="padding-bottom: 5px;">
                                                                                                                <div class="cell auto">
                                                                                                                    <div class="emproduct_artnum">YNH-B2101</div>
                                                                                                                </div>
                                                                                                                <div class="cell shrink">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="main-info background-white">
                                                                                                                <div class="addtoCart-section">
                                                                                                                    <div class="tobasketFunction">
                                                                                                                        <button id="toBasket_emstartpagenew-5" type="submit"
                                                                                                                        class="submitButton largeButton">Agregar
                                                                                                                        </button>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="moreinfo-section">
                                                                                                                    <!-- Price -->
                                                                                                                    <!--sse-->
                                                                                                                    <div class="emproduct_price">
                                                                                                                        <span class="oldPrice">
                                                                                                                            <del>$769.00</del>
                                                                                                                        </span>
                                                                                                                        <label class="price">
                                                                                                                            $689.00
                                                                                                                        </label>
                                                                                                                    </div>
                                                                                                                    <!--/sse-->
                                                                                                                    <!-- Delivery Cost -->
                                                                                                                    <div class="emdeliverycost">
                                                                                                                        <span class="deliverytext">Costo de envío:</span>
                                                                                                                        <span class="deliveryvalue">$189.00</span>
                                                                                                                    </div>
                                                                                                                    <!--  Stock  -->
                                                                                                                    <div class="emstock">            Disponibles:
                                                                                                                        <span>13</span>
                                                                                                                        pzas.
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="emproduct_compare">
                                                                                                                <div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_emstartpagenew-5" data-id="listproduct_emstartpagenew-5">
                                                                                                                    <div id="compare_removelistproduct_emstartpagenew-5" style=display:none>
                                                                                                                        <div class="emcomparelinkbox_link_ctn ">
                                                                                                                            <a data-id="listproduct_emstartpagenew-5" class="emcomparelinkbox_link_remove" data-aid="72940b35fd126fe57beac27b915b7802"
                                                                                                                                id="removeCmplistproduct_emstartpagenew-5" href="#" onclick="go_to('index5895.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=72940b35fd126fe57beac27b915b7802&amp;anid=72940b35fd126fe57beac27b915b7802&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                <span class="square"></span>
                                                                                                                            </a>
                                                                                                                            <a class="emcomparelinkbox_link_txt " href="lista-de-comparacion/index.html">
                                                                                                                            Ver Comparación (0)            </a>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="compare_addlistproduct_emstartpagenew-5" >
                                                                                                                        <a data-id="listproduct_emstartpagenew-5" class="emcomparelinkbox_link_add" data-aid="72940b35fd126fe57beac27b915b7802"
                                                                                                                            id="toCmplistproduct_emstartpagenew-5" href="#" onclick="go_to('index5895.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=72940b35fd126fe57beac27b915b7802&amp;anid=72940b35fd126fe57beac27b915b7802&amp;pgNr=0&amp;searchparam=')">
                                                                                                                            <span class="square"></span>
                                                                                                                            Comparar
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                </div>                </div>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                    </div>
                                                                                                </li>
                                                                                                <li class="productData cell" data-cp-start-daily-product style="display: none">
                                                                                                    <div class="emproductborder listiteminfogridborder emsmoothborder clear">
                                                                                                        <form name="tobasketemstartpagenew-6"
                                                                                                            action="https://www.kyoceraap.com/index.php?" method="post"
                                                                                                            >
                                                                                                            <input type="hidden" name="actcontrol" value="start" />
                                                                                                            <input type="hidden" name="lang" value="0" />
                                                                                                            <input type="hidden" name="pgNr" value="0">
                                                                                                            <input type="hidden" name="cl" value="start">
                                                                                                            <input type="hidden" name="fnc" value="tobasket">
                                                                                                            <input type="hidden" name="aid" value="1554dd0380471bc1cdd1301e722c4ccb">
                                                                                                            <input type="hidden" name="anid" value="1554dd0380471bc1cdd1301e722c4ccb">            <input type="hidden" name="am" value="1">
                                                                                                            <input type="hidden" name="oxloadid" value="">
                                                                                                            <div class="emproduct clear listiteminfogrid">
                                                                                                                <div class="offerbox  hasuntil  hasdaily clear">
                                                                                                                    <div class="percent">
                                                                                                                        -9%
                                                                                                                        <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.png" alt="daily offer icon"/>
                                                                                                                        <span class="onlyhome">
                                                                                                                            ($210.00)
                                                                                                                        </span>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <a class="cp-picture-container cpGaProdemstartpagenew-6" href="Computo-Hardware/Monitores/Monitores/Monitor-Gamer-Hyundai-24FGM-LED-24-Full-HD-Widescreen-75Hz-HDMI-Bocinas-Integradas-Negro.html">
                                                                                                                    <div class="picture-wrapper">
                                                                                                                        <div data-cp-prod-slider="[&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-HYUNDAI-HT24FGMBK01-3d3fdd.jpg&quot;]" class="catSlider">
                                                                                                                            <div data-cp-img-container class="cs-image"></div>
                                                                                                                            <div class="csnav"></div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="picture-overlay">
                                                                                                                        <div class="badges-left">
                                                                                                                            <img alt="HYUNDAI" class="product-logo" src="<?php echo base_url();?>cdn.kyoceraap.com/storage/brands/fb1939d8b52f6a7a04a68f75b4ceefe6_HYUNDAI.png" data-cp-brand-logo="Por-Marca/HYUNDAI/index.html">
                                                                                                                        </div>
                                                                                                                        <div class="badges-middle">
                                                                                                                        </div>
                                                                                                                        <div class="badges-right">
                                                                                                                            <div class="badges-right-top">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                                <div class="emproduct_review">
                                                                                                                </div>
                                                                                                                <div data-cp-complete-name="emproduct_cptitleBox">
                                                                                                                    <div class="emproduct_cptitleBox">
                                                                                                                        <a id="emstartpagenew-6" href="Computo-Hardware/Monitores/Monitores/Monitor-Gamer-Hyundai-24FGM-LED-24-Full-HD-Widescreen-75Hz-HDMI-Bocinas-Integradas-Negro.html"
                                                                                                                            class="emproduct_title cpGaProdemstartpagenew-6"
                                                                                                                            title="Monitor Gamer Hyundai 24FGM LED 24&quot;, Full HD, 75Hz, HDMI, Bocinas Integradas, Negro"
                                                                                                                            data-truncate="Monitor Gamer Hyundai 24FGM LED 24", Full HD, 75Hz, HDMI, Bocinas Integradas, Negro"
                                                                                                                            data-text>
                                                                                                                            <span>Monitor Gamer Hyundai 24FGM LED 24", Full HD, 75Hz, HDMI, Bocinas Integradas, Negro</span>
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="grid-x align-middle background-white cp-pt-10" style="padding-bottom: 5px;">
                                                                                                                    <div class="cell auto">
                                                                                                                        <div class="emproduct_artnum">HT24FGMBK01</div>
                                                                                                                    </div>
                                                                                                                    <div class="cell shrink">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="main-info background-white">
                                                                                                                    <div class="addtoCart-section">
                                                                                                                        <div class="tobasketFunction">
                                                                                                                            <button id="toBasket_emstartpagenew-6" type="submit"
                                                                                                                            class="submitButton largeButton">Agregar
                                                                                                                            </button>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="moreinfo-section">
                                                                                                                        <!-- Price -->
                                                                                                                        <!--sse-->
                                                                                                                        <div class="emproduct_price">
                                                                                                                            <span class="oldPrice">
                                                                                                                                <del>$2,449.00</del>
                                                                                                                            </span>
                                                                                                                            <label class="price">
                                                                                                                                $2,239.00
                                                                                                                            </label>
                                                                                                                        </div>
                                                                                                                        <!--/sse-->
                                                                                                                        <!-- Delivery Cost -->
                                                                                                                        <div class="emdeliverycost">
                                                                                                                            <span class="deliverytext">Costo de envío:</span>
                                                                                                                            <span class="deliveryvalue">$149.00</span>
                                                                                                                        </div>
                                                                                                                        <!--  Stock  -->
                                                                                                                        <div class="emstock">            Disponibles:
                                                                                                                            <span>160</span>
                                                                                                                            pzas.
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="emproduct_compare">
                                                                                                                    <div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_emstartpagenew-6" data-id="listproduct_emstartpagenew-6">
                                                                                                                        <div id="compare_removelistproduct_emstartpagenew-6" style=display:none>
                                                                                                                            <div class="emcomparelinkbox_link_ctn ">
                                                                                                                                <a data-id="listproduct_emstartpagenew-6" class="emcomparelinkbox_link_remove" data-aid="1554dd0380471bc1cdd1301e722c4ccb"
                                                                                                                                    id="removeCmplistproduct_emstartpagenew-6" href="#" onclick="go_to('indexf2b2.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=1554dd0380471bc1cdd1301e722c4ccb&amp;anid=1554dd0380471bc1cdd1301e722c4ccb&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                    <span class="square"></span>
                                                                                                                                </a>
                                                                                                                                <a class="emcomparelinkbox_link_txt " href="lista-de-comparacion/index.html">
                                                                                                                                Ver Comparación (0)            </a>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div id="compare_addlistproduct_emstartpagenew-6" >
                                                                                                                            <a data-id="listproduct_emstartpagenew-6" class="emcomparelinkbox_link_add" data-aid="1554dd0380471bc1cdd1301e722c4ccb"
                                                                                                                                id="toCmplistproduct_emstartpagenew-6" href="#" onclick="go_to('indexf2b2.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=1554dd0380471bc1cdd1301e722c4ccb&amp;anid=1554dd0380471bc1cdd1301e722c4ccb&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                <span class="square"></span>
                                                                                                                                Comparar
                                                                                                                            </a>
                                                                                                                        </div>
                                                                                                                    </div>                </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                        </div>
                                                                                                    </li>
                                                                                                    <li class="productData cell" data-cp-start-daily-product style="display: none">
                                                                                                        <div class="emproductborder listiteminfogridborder emsmoothborder clear">
                                                                                                            <form name="tobasketemstartpagenew-7"
                                                                                                                action="https://www.kyoceraap.com/index.php?" method="post"
                                                                                                                >
                                                                                                                <input type="hidden" name="actcontrol" value="start" />
                                                                                                                <input type="hidden" name="lang" value="0" />
                                                                                                                <input type="hidden" name="pgNr" value="0">
                                                                                                                <input type="hidden" name="cl" value="start">
                                                                                                                <input type="hidden" name="fnc" value="tobasket">
                                                                                                                <input type="hidden" name="aid" value="77d620c8eee5c037ea2113964aef75d0">
                                                                                                                <input type="hidden" name="anid" value="77d620c8eee5c037ea2113964aef75d0">            <input type="hidden" name="am" value="1">
                                                                                                                <input type="hidden" name="oxloadid" value="">
                                                                                                                <div class="emproduct clear listiteminfogrid">
                                                                                                                    <div class="offerbox  hasuntil  hasdaily clear">
                                                                                                                        <div class="percent">
                                                                                                                            -42%
                                                                                                                            <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.png" alt="daily offer icon"/>
                                                                                                                            <span class="onlyhome">
                                                                                                                                ($560.00)
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <a class="cp-picture-container cpGaProdemstartpagenew-7" href="Computo-Hardware/Memorias-RAM-y-Flash/Memorias-RAM-para-PC/Memoria-RAM-XPG-Spectrix-D45G-White-RGB-DDR4-3600MHz-16GB-CL18-XMP.html">
                                                                                                                        <div class="picture-wrapper">
                                                                                                                            <div data-cp-prod-slider="[&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-XPG-AX4U360016G18I-CWHD45G-0cb371.png&quot;]" class="catSlider">
                                                                                                                                <div data-cp-img-container class="cs-image"></div>
                                                                                                                                <div class="csnav"></div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="picture-overlay">
                                                                                                                            <div class="badges-left">
                                                                                                                                <img alt="XPG" class="product-logo" src="<?php echo base_url();?>cdn.kyoceraap.com/storage/brands/xpglogo2.png" data-cp-brand-logo="Por-Marca/XPG/index.html">
                                                                                                                            </div>
                                                                                                                            <div class="badges-middle">
                                                                                                                            </div>
                                                                                                                            <div class="badges-right">
                                                                                                                                <div class="badges-right-top">
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </a>
                                                                                                                    <div class="emproduct_review">
                                                                                                                        <div class="detailsInfo_right_review" style="margin-top:0; ">
                                                                                                                            <div class="cpreviews_popup">
                                                                                                                                <div style="float:left; position:relative; "
                                                                                                                                    class="stars">
                                                                                                                                    <div class="cpreviews_stars_box clear" title="
                                                                                                                                        ¡Lo mejor!
                                                                                                                                        "
                                                                                                                                        >
                                                                                                                                        <div class="cpreviews_stars_box_percent" style="width:100%; "></div>
                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                    </div>                </div>
                                                                                                                                    <div style="float:left; position:relative; " class="popupborder">
                                                                                                                                        <div class="popuptitle"></div>
                                                                                                                                        <div class="popupbridge"></div>
                                                                                                                                        <div class="popupbox">
                                                                                                                                            <div class="cpreviews_box">
                                                                                                                                                <div class="cpreviews_box_header clear">
                                                                                                                                                    <div class="left">
                                                                                                                                                        <div class="stars">
                                                                                                                                                            <div class="cpreviews_stars_box clear" title="
                                                                                                                                                                ¡Lo mejor!
                                                                                                                                                                "
                                                                                                                                                                >
                                                                                                                                                                <div class="cpreviews_stars_box_percent" style="width:100%; "></div>
                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                                            </div>                </div>
                                                                                                                                                            <div class="desc">5  de 5 estrellas </div>
                                                                                                                                                        </div>
                                                                                                                                                        <div class="right">
                                                                                                                                                            3
                                                                                                                                                            opiniones
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                    <div class="cpreviews_starsdesc">
                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                            style="width:100px; "></div>
                                                                                                                                                            <div class="cpreviews_starsdesc_text">5 estrellas: (3)
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                            <div class="cpreviews_starsdesc_text">4 estrellas: (0)
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                            <div class="cpreviews_starsdesc_text">3 estrellas: (0)
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                            <div class="cpreviews_starsdesc_text">2 estrellas: (0)
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                            <div class="cpreviews_starsdesc_text">1 estrellas: (0)
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                    <div class="cpreviews_box_link">
                                                                                                                                                        <a href="Opiniones-sobre-Memoria-RAM-XPG-Spectrix-D45G-RGB-DDR4-3600MHz-16GB-CL18-XMP-Blanco/index.html">Ver todas las (3) opiniones</a>
                                                                                                                                                    </div>
                                                                                                                                                </div>                                                    </div>
                                                                                                                                            </div>
                                                                                                                                            <div class="reviews-total-txt" >
                                                                                                                                                <a href="Opiniones-sobre-Memoria-RAM-XPG-Spectrix-D45G-RGB-DDR4-3600MHz-16GB-CL18-XMP-Blanco/index.html">
                                                                                                                                                    3
                                                                                                                                                    opiniones
                                                                                                                                                </a>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div data-cp-complete-name="emproduct_cptitleBox">
                                                                                                                                    <div class="emproduct_cptitleBox">
                                                                                                                                        <a id="emstartpagenew-7" href="Computo-Hardware/Memorias-RAM-y-Flash/Memorias-RAM-para-PC/Memoria-RAM-XPG-Spectrix-D45G-White-RGB-DDR4-3600MHz-16GB-CL18-XMP.html"
                                                                                                                                            class="emproduct_title cpGaProdemstartpagenew-7"
                                                                                                                                            title="Memoria RAM XPG Spectrix D45G RGB DDR4, 3600MHz, 16GB, CL18, XMP, Blanco"
                                                                                                                                            data-truncate="Memoria RAM XPG Spectrix D45G RGB DDR4, 3600MHz, 16GB, CL18, XMP, Blanco"
                                                                                                                                            data-text>
                                                                                                                                            <span>Memoria RAM XPG Spectrix D45G RGB DDR4, 3600MHz, 16GB, CL18, XMP, Blanco</span>
                                                                                                                                        </a>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="grid-x align-middle background-white cp-pt-10" style="padding-bottom: 5px;">
                                                                                                                                    <div class="cell auto">
                                                                                                                                        <div class="emproduct_artnum">AX4U360016G18I-CWHD45G</div>
                                                                                                                                    </div>
                                                                                                                                    <div class="cell shrink">
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="main-info background-white">
                                                                                                                                    <div class="addtoCart-section">
                                                                                                                                        <div class="tobasketFunction">
                                                                                                                                            <button id="toBasket_emstartpagenew-7" type="submit"
                                                                                                                                            class="submitButton largeButton">Agregar
                                                                                                                                            </button>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="moreinfo-section">
                                                                                                                                        <!-- Price -->
                                                                                                                                        <!--sse-->
                                                                                                                                        <div class="emproduct_price">
                                                                                                                                            <span class="oldPrice">
                                                                                                                                                <del>$1,319.00</del>
                                                                                                                                            </span>
                                                                                                                                            <label class="price">
                                                                                                                                                $759.00
                                                                                                                                            </label>
                                                                                                                                        </div>
                                                                                                                                        <!--/sse-->
                                                                                                                                        <!-- Delivery Cost -->
                                                                                                                                        <div class="emdeliverycost">
                                                                                                                                            <span class="deliverytext">Costo de envío:</span>
                                                                                                                                            <span class="deliveryvalue">$99.00</span>
                                                                                                                                        </div>
                                                                                                                                        <!--  Stock  -->
                                                                                                                                        <div class="emstock">            Disponibles:
                                                                                                                                            <span>9</span>
                                                                                                                                            pzas.
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="emproduct_compare">
                                                                                                                                    <div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_emstartpagenew-7" data-id="listproduct_emstartpagenew-7">
                                                                                                                                        <div id="compare_removelistproduct_emstartpagenew-7" style=display:none>
                                                                                                                                            <div class="emcomparelinkbox_link_ctn ">
                                                                                                                                                <a data-id="listproduct_emstartpagenew-7" class="emcomparelinkbox_link_remove" data-aid="77d620c8eee5c037ea2113964aef75d0"
                                                                                                                                                    id="removeCmplistproduct_emstartpagenew-7" href="#" onclick="go_to('indexdd31.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=77d620c8eee5c037ea2113964aef75d0&amp;anid=77d620c8eee5c037ea2113964aef75d0&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                                    <span class="square"></span>
                                                                                                                                                </a>
                                                                                                                                                <a class="emcomparelinkbox_link_txt " href="lista-de-comparacion/index.html">
                                                                                                                                                Ver Comparación (0)            </a>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                        <div id="compare_addlistproduct_emstartpagenew-7" >
                                                                                                                                            <a data-id="listproduct_emstartpagenew-7" class="emcomparelinkbox_link_add" data-aid="77d620c8eee5c037ea2113964aef75d0"
                                                                                                                                                id="toCmplistproduct_emstartpagenew-7" href="#" onclick="go_to('indexdd31.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=77d620c8eee5c037ea2113964aef75d0&amp;anid=77d620c8eee5c037ea2113964aef75d0&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                                <span class="square"></span>
                                                                                                                                                Comparar
                                                                                                                                            </a>
                                                                                                                                        </div>
                                                                                                                                    </div>                </div>
                                                                                                                                </div>
                                                                                                                            </form>
                                                                                                                        </div>
                                                                                                                    </li>
                                                                                                                    <li class="productData cell" data-cp-start-daily-product style="display: none">
                                                                                                                        <div class="emproductborder listiteminfogridborder emsmoothborder clear">
                                                                                                                            <form name="tobasketemstartpagenew-8"
                                                                                                                                action="https://www.kyoceraap.com/index.php?" method="post"
                                                                                                                                >
                                                                                                                                <input type="hidden" name="actcontrol" value="start" />
                                                                                                                                <input type="hidden" name="lang" value="0" />
                                                                                                                                <input type="hidden" name="pgNr" value="0">
                                                                                                                                <input type="hidden" name="cl" value="start">
                                                                                                                                <input type="hidden" name="fnc" value="tobasket">
                                                                                                                                <input type="hidden" name="aid" value="71b611d02ca158e8f3f44ab61d85b2ba">
                                                                                                                                <input type="hidden" name="anid" value="71b611d02ca158e8f3f44ab61d85b2ba">            <input type="hidden" name="am" value="1">
                                                                                                                                <input type="hidden" name="oxloadid" value="">
                                                                                                                                <div class="emproduct clear listiteminfogrid">
                                                                                                                                    <div class="offerbox  hasuntil  hasdaily clear">
                                                                                                                                        <div class="percent">
                                                                                                                                            -10%
                                                                                                                                            <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.png" alt="daily offer icon"/>
                                                                                                                                            <span class="onlyhome">
                                                                                                                                                ($90.00)
                                                                                                                                            </span>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <a class="cp-picture-container cpGaProdemstartpagenew-8" href="Computo-Hardware/Memorias-RAM-y-Flash/Memorias-RAM-para-PC/Kit-Memoria-RAM-Patriot-Viper-Steel-DDR4-3200MHz-16GB-2x-8-GB-Non-ECC-CL16.html">
                                                                                                                                        <div class="picture-wrapper">
                                                                                                                                            <div data-cp-prod-slider="[&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-PATRIOT-PVS416G320C6K-1.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-PATRIOT-PVS416G320C6K-2.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-PATRIOT-PVS416G320C6K-3.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-PATRIOT-PVS416G320C6K-4.jpg&quot;]" class="catSlider">
                                                                                                                                                <div data-cp-img-container class="cs-image"></div>
                                                                                                                                                <div class="csnav"></div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                        <div class="picture-overlay">
                                                                                                                                            <div class="badges-left">
                                                                                                                                                <img alt="PATRIOT" class="product-logo" src="<?php echo base_url();?>cdn.kyoceraap.com/storage/brands/patriot.png" data-cp-brand-logo="Por-Marca/PATRIOT/index.html">
                                                                                                                                            </div>
                                                                                                                                            <div class="badges-middle">
                                                                                                                                            </div>
                                                                                                                                            <div class="badges-right">
                                                                                                                                                <div class="badges-right-top">
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    </a>
                                                                                                                                    <div class="emproduct_review">
                                                                                                                                        <div class="detailsInfo_right_review" style="margin-top:0; ">
                                                                                                                                            <div class="cpreviews_popup">
                                                                                                                                                <div style="float:left; position:relative; "
                                                                                                                                                    class="stars">
                                                                                                                                                    <div class="cpreviews_stars_box clear" title="
                                                                                                                                                        Me gusta
                                                                                                                                                        "
                                                                                                                                                        >
                                                                                                                                                        <div class="cpreviews_stars_box_percent" style="width:96%; "></div>
                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                                    </div>                </div>
                                                                                                                                                    <div style="float:left; position:relative; " class="popupborder">
                                                                                                                                                        <div class="popuptitle"></div>
                                                                                                                                                        <div class="popupbridge"></div>
                                                                                                                                                        <div class="popupbox">
                                                                                                                                                            <div class="cpreviews_box">
                                                                                                                                                                <div class="cpreviews_box_header clear">
                                                                                                                                                                    <div class="left">
                                                                                                                                                                        <div class="stars">
                                                                                                                                                                            <div class="cpreviews_stars_box clear" title="
                                                                                                                                                                                Me gusta
                                                                                                                                                                                "
                                                                                                                                                                                >
                                                                                                                                                                                <div class="cpreviews_stars_box_percent" style="width:96%; "></div>
                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                                                            </div>                </div>
                                                                                                                                                                            <div class="desc">4.8  de 5 estrellas </div>
                                                                                                                                                                        </div>
                                                                                                                                                                        <div class="right">
                                                                                                                                                                            79
                                                                                                                                                                            opiniones
                                                                                                                                                                        </div>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="cpreviews_starsdesc">
                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                            style="width:81.012658227848px; "></div>
                                                                                                                                                                            <div class="cpreviews_starsdesc_text">5 estrellas: (64)
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                            style="width:18.987341772152px; "></div>
                                                                                                                                                                            <div class="cpreviews_starsdesc_text">4 estrellas: (15)
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                            <div class="cpreviews_starsdesc_text">3 estrellas: (0)
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                            <div class="cpreviews_starsdesc_text">2 estrellas: (0)
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                            <div class="cpreviews_starsdesc_text">1 estrellas: (0)
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="cpreviews_box_link">
                                                                                                                                                                        <a href="Opiniones-sobre-Kit-Memoria-RAM-Patriot-Viper-Steel-DDR4-3200MHz-16GB-2x-8-GB-Non-ECC-CL16/index.html">Ver todas las (79) opiniones</a>
                                                                                                                                                                    </div>
                                                                                                                                                                </div>                                                    </div>
                                                                                                                                                            </div>
                                                                                                                                                            <div class="reviews-total-txt" >
                                                                                                                                                                <a href="Opiniones-sobre-Kit-Memoria-RAM-Patriot-Viper-Steel-DDR4-3200MHz-16GB-2x-8-GB-Non-ECC-CL16/index.html">
                                                                                                                                                                    79
                                                                                                                                                                    opiniones
                                                                                                                                                                </a>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                                <div data-cp-complete-name="emproduct_cptitleBox">
                                                                                                                                                    <div class="emproduct_cptitleBox">
                                                                                                                                                        <a id="emstartpagenew-8" href="Computo-Hardware/Memorias-RAM-y-Flash/Memorias-RAM-para-PC/Kit-Memoria-RAM-Patriot-Viper-Steel-DDR4-3200MHz-16GB-2x-8-GB-Non-ECC-CL16.html"
                                                                                                                                                            class="emproduct_title cpGaProdemstartpagenew-8"
                                                                                                                                                            title="Kit Memoria RAM Patriot Viper Steel DDR4, 3200MHz, 16GB (2x 8 GB), Non-ECC, CL16"
                                                                                                                                                            data-truncate="Kit Memoria RAM Patriot Viper Steel DDR4, 3200MHz, 16GB (2x 8 GB), Non-ECC, CL16"
                                                                                                                                                            data-text>
                                                                                                                                                            <span>Kit Memoria RAM Patriot Viper Steel DDR4, 3200MHz, 16GB (2x 8 GB), Non-ECC, CL16</span>
                                                                                                                                                        </a>
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                                <div class="grid-x align-middle background-white cp-pt-10" style="padding-bottom: 5px;">
                                                                                                                                                    <div class="cell auto">
                                                                                                                                                        <div class="emproduct_artnum">PVS416G320C6K</div>
                                                                                                                                                    </div>
                                                                                                                                                    <div class="cell shrink">
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                                <div class="main-info background-white">
                                                                                                                                                    <div class="addtoCart-section">
                                                                                                                                                        <div class="tobasketFunction">
                                                                                                                                                            <button id="toBasket_emstartpagenew-8" type="submit"
                                                                                                                                                            class="submitButton largeButton">Agregar
                                                                                                                                                            </button>
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                    <div class="moreinfo-section">
                                                                                                                                                        <!-- Price -->
                                                                                                                                                        <!--sse-->
                                                                                                                                                        <div class="emproduct_price">
                                                                                                                                                            <span class="oldPrice">
                                                                                                                                                                <del>$899.00</del>
                                                                                                                                                            </span>
                                                                                                                                                            <label class="price">
                                                                                                                                                                $809.00
                                                                                                                                                            </label>
                                                                                                                                                        </div>
                                                                                                                                                        <!--/sse-->
                                                                                                                                                        <!-- Delivery Cost -->
                                                                                                                                                        <div class="emdeliverycost">
                                                                                                                                                            <span class="deliverytext">Costo de envío:</span>
                                                                                                                                                            <span class="deliveryvalue">$99.00</span>
                                                                                                                                                        </div>
                                                                                                                                                        <!--  Stock  -->
                                                                                                                                                        <div class="emstock">            Disponibles:
                                                                                                                                                            <span>15</span>
                                                                                                                                                            pzas.
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                                <div class="emproduct_compare">
                                                                                                                                                    <div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_emstartpagenew-8" data-id="listproduct_emstartpagenew-8">
                                                                                                                                                        <div id="compare_removelistproduct_emstartpagenew-8" style=display:none>
                                                                                                                                                            <div class="emcomparelinkbox_link_ctn ">
                                                                                                                                                                <a data-id="listproduct_emstartpagenew-8" class="emcomparelinkbox_link_remove" data-aid="71b611d02ca158e8f3f44ab61d85b2ba"
                                                                                                                                                                    id="removeCmplistproduct_emstartpagenew-8" href="#" onclick="go_to('index8fda.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=71b611d02ca158e8f3f44ab61d85b2ba&amp;anid=71b611d02ca158e8f3f44ab61d85b2ba&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                                                    <span class="square"></span>
                                                                                                                                                                </a>
                                                                                                                                                                <a class="emcomparelinkbox_link_txt " href="lista-de-comparacion/index.html">
                                                                                                                                                                Ver Comparación (0)            </a>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                        <div id="compare_addlistproduct_emstartpagenew-8" >
                                                                                                                                                            <a data-id="listproduct_emstartpagenew-8" class="emcomparelinkbox_link_add" data-aid="71b611d02ca158e8f3f44ab61d85b2ba"
                                                                                                                                                                id="toCmplistproduct_emstartpagenew-8" href="#" onclick="go_to('index8fda.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=71b611d02ca158e8f3f44ab61d85b2ba&amp;anid=71b611d02ca158e8f3f44ab61d85b2ba&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                                                <span class="square"></span>
                                                                                                                                                                Comparar
                                                                                                                                                            </a>
                                                                                                                                                        </div>
                                                                                                                                                    </div>                </div>
                                                                                                                                                </div>
                                                                                                                                            </form>
                                                                                                                                        </div>
                                                                                                                                    </li>
                                                                                                                                    <li class="productData cell" data-cp-start-daily-product style="display: none">
                                                                                                                                        <div class="emproductborder listiteminfogridborder emsmoothborder clear">
                                                                                                                                            <form name="tobasketemstartpagenew-9"
                                                                                                                                                action="https://www.kyoceraap.com/index.php?" method="post"
                                                                                                                                                >
                                                                                                                                                <input type="hidden" name="actcontrol" value="start" />
                                                                                                                                                <input type="hidden" name="lang" value="0" />
                                                                                                                                                <input type="hidden" name="pgNr" value="0">
                                                                                                                                                <input type="hidden" name="cl" value="start">
                                                                                                                                                <input type="hidden" name="fnc" value="tobasket">
                                                                                                                                                <input type="hidden" name="aid" value="0e33b0487b3a0230fbb88e9a165cb5ac">
                                                                                                                                                <input type="hidden" name="anid" value="0e33b0487b3a0230fbb88e9a165cb5ac">            <input type="hidden" name="am" value="1">
                                                                                                                                                <input type="hidden" name="oxloadid" value="">
                                                                                                                                                <div class="emproduct clear listiteminfogrid">
                                                                                                                                                    <div class="offerbox  hasuntil  hasdaily clear">
                                                                                                                                                        <div class="percent">
                                                                                                                                                            -9%
                                                                                                                                                            <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.png" alt="daily offer icon"/>
                                                                                                                                                            <span class="onlyhome">
                                                                                                                                                                ($80.00)
                                                                                                                                                            </span>
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                    <a class="cp-picture-container cpGaProdemstartpagenew-9" href="Computo-Hardware/Discos-Duros-SSD-NAS/SSD/SSD-Adata-Ultimate-SU800-512GB-SATA-III-2-5-7mm.html">
                                                                                                                                                        <div class="picture-wrapper">
                                                                                                                                                            <div data-cp-prod-slider="[&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-ADATA-ASU800SS-512GT-C-1.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-ADATA-ASU800SS-512GT-C-2.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-ADATA-ASU800SS-512GT-C-3.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-ADATA-ASU800SS-512GT-C-4.jpg&quot;]" class="catSlider">
                                                                                                                                                                <div data-cp-img-container class="cs-image"></div>
                                                                                                                                                                <div class="csnav"></div>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                        <div class="picture-overlay">
                                                                                                                                                            <div class="badges-left">
                                                                                                                                                                <img alt="ADATA" class="product-logo" src="<?php echo base_url();?>cdn.kyoceraap.com/storage/brands/f976fb2ecf4847ecb507697a8d02ed3d_ADATA.png" data-cp-brand-logo="Por-Marca/ADATA/index.html">
                                                                                                                                                            </div>
                                                                                                                                                            <div class="badges-middle">
                                                                                                                                                            </div>
                                                                                                                                                            <div class="badges-right">
                                                                                                                                                                <div class="badges-right-top">
                                                                                                                                                                </div>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                    </a>
                                                                                                                                                    <div class="emproduct_review">
                                                                                                                                                        <div class="detailsInfo_right_review" style="margin-top:0; ">
                                                                                                                                                            <div class="cpreviews_popup">
                                                                                                                                                                <div style="float:left; position:relative; "
                                                                                                                                                                    class="stars">
                                                                                                                                                                    <div class="cpreviews_stars_box clear" title="
                                                                                                                                                                        Me gusta
                                                                                                                                                                        "
                                                                                                                                                                        >
                                                                                                                                                                        <div class="cpreviews_stars_box_percent" style="width:96%; "></div>
                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                                                    </div>                </div>
                                                                                                                                                                    <div style="float:left; position:relative; " class="popupborder">
                                                                                                                                                                        <div class="popuptitle"></div>
                                                                                                                                                                        <div class="popupbridge"></div>
                                                                                                                                                                        <div class="popupbox">
                                                                                                                                                                            <div class="cpreviews_box">
                                                                                                                                                                                <div class="cpreviews_box_header clear">
                                                                                                                                                                                    <div class="left">
                                                                                                                                                                                        <div class="stars">
                                                                                                                                                                                            <div class="cpreviews_stars_box clear" title="
                                                                                                                                                                                                Me gusta
                                                                                                                                                                                                "
                                                                                                                                                                                                >
                                                                                                                                                                                                <div class="cpreviews_stars_box_percent" style="width:96%; "></div>
                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                                                                            </div>                </div>
                                                                                                                                                                                            <div class="desc">4.8  de 5 estrellas </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                        <div class="right">
                                                                                                                                                                                            467
                                                                                                                                                                                            opiniones
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                    <div class="cpreviews_starsdesc">
                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                            style="width:81.370449678801px; "></div>
                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">5 estrellas: (380)
                                                                                                                                                                                            </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                            style="width:16.27408993576px; "></div>
                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">4 estrellas: (76)
                                                                                                                                                                                            </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                            style="width:1.7130620985011px; "></div>
                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">3 estrellas: (8)
                                                                                                                                                                                            </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                            style="width:0.42826552462527px; "></div>
                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">2 estrellas: (2)
                                                                                                                                                                                            </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                            style="width:0.21413276231263px; "></div>
                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">1 estrellas: (1)
                                                                                                                                                                                            </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                    <div class="cpreviews_box_link">
                                                                                                                                                                                        <a href="Opiniones-sobre-SSD-Adata-Ultimate-SU800-512GB-SATA-III-2-5-7mm/index.html">Ver todas las (467) opiniones</a>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>                                                    </div>
                                                                                                                                                                            </div>
                                                                                                                                                                            <div class="reviews-total-txt" >
                                                                                                                                                                                <a href="Opiniones-sobre-SSD-Adata-Ultimate-SU800-512GB-SATA-III-2-5-7mm/index.html">
                                                                                                                                                                                    467
                                                                                                                                                                                    opiniones
                                                                                                                                                                                </a>
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                    </div>
                                                                                                                                                                </div>
                                                                                                                                                                <div data-cp-complete-name="emproduct_cptitleBox">
                                                                                                                                                                    <div class="emproduct_cptitleBox">
                                                                                                                                                                        <a id="emstartpagenew-9" href="Computo-Hardware/Discos-Duros-SSD-NAS/SSD/SSD-Adata-Ultimate-SU800-512GB-SATA-III-2-5-7mm.html"
                                                                                                                                                                            class="emproduct_title cpGaProdemstartpagenew-9"
                                                                                                                                                                            title="SSD Adata Ultimate SU800, 512GB, SATA III, 2.5&#039;&#039;, 7mm"
                                                                                                                                                                            data-truncate="SSD Adata Ultimate SU800, 512GB, SATA III, 2.5'', 7mm"
                                                                                                                                                                            data-text>
                                                                                                                                                                            <span>SSD Adata Ultimate SU800, 512GB, SATA III, 2.5'', 7mm</span>
                                                                                                                                                                        </a>
                                                                                                                                                                    </div>
                                                                                                                                                                </div>
                                                                                                                                                                <div class="grid-x align-middle background-white cp-pt-10" style="padding-bottom: 5px;">
                                                                                                                                                                    <div class="cell auto">
                                                                                                                                                                        <div class="emproduct_artnum">ASU800SS-512GT-C</div>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="cell shrink">
                                                                                                                                                                    </div>
                                                                                                                                                                </div>
                                                                                                                                                                <div class="main-info background-white">
                                                                                                                                                                    <div class="addtoCart-section">
                                                                                                                                                                        <div class="tobasketFunction">
                                                                                                                                                                            <button id="toBasket_emstartpagenew-9" type="submit"
                                                                                                                                                                            class="submitButton largeButton">Agregar
                                                                                                                                                                            </button>
                                                                                                                                                                        </div>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="moreinfo-section">
                                                                                                                                                                        <!-- Price -->
                                                                                                                                                                        <!--sse-->
                                                                                                                                                                        <div class="emproduct_price">
                                                                                                                                                                            <span class="oldPrice">
                                                                                                                                                                                <del>$899.00</del>
                                                                                                                                                                            </span>
                                                                                                                                                                            <label class="price">
                                                                                                                                                                                $819.00
                                                                                                                                                                            </label>
                                                                                                                                                                        </div>
                                                                                                                                                                        <!--/sse-->
                                                                                                                                                                        <!-- Delivery Cost -->
                                                                                                                                                                        <div class="emdeliverycost">
                                                                                                                                                                            <span class="deliverytext">Costo de envío:</span>
                                                                                                                                                                            <span class="deliveryvalue">$99.00</span>
                                                                                                                                                                        </div>
                                                                                                                                                                        <!--  Stock  -->
                                                                                                                                                                        <div class="emstock">            Disponibles:
                                                                                                                                                                            <span>45</span>
                                                                                                                                                                            pzas.
                                                                                                                                                                        </div>
                                                                                                                                                                    </div>
                                                                                                                                                                </div>
                                                                                                                                                                <div class="emproduct_compare">
                                                                                                                                                                    <div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_emstartpagenew-9" data-id="listproduct_emstartpagenew-9">
                                                                                                                                                                        <div id="compare_removelistproduct_emstartpagenew-9" style=display:none>
                                                                                                                                                                            <div class="emcomparelinkbox_link_ctn ">
                                                                                                                                                                                <a data-id="listproduct_emstartpagenew-9" class="emcomparelinkbox_link_remove" data-aid="0e33b0487b3a0230fbb88e9a165cb5ac"
                                                                                                                                                                                    id="removeCmplistproduct_emstartpagenew-9" href="#" onclick="go_to('index00a8.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=0e33b0487b3a0230fbb88e9a165cb5ac&amp;anid=0e33b0487b3a0230fbb88e9a165cb5ac&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                                                                    <span class="square"></span>
                                                                                                                                                                                </a>
                                                                                                                                                                                <a class="emcomparelinkbox_link_txt " href="lista-de-comparacion/index.html">
                                                                                                                                                                                Ver Comparación (0)            </a>
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                        <div id="compare_addlistproduct_emstartpagenew-9" >
                                                                                                                                                                            <a data-id="listproduct_emstartpagenew-9" class="emcomparelinkbox_link_add" data-aid="0e33b0487b3a0230fbb88e9a165cb5ac"
                                                                                                                                                                                id="toCmplistproduct_emstartpagenew-9" href="#" onclick="go_to('index00a8.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=0e33b0487b3a0230fbb88e9a165cb5ac&amp;anid=0e33b0487b3a0230fbb88e9a165cb5ac&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                                                                <span class="square"></span>
                                                                                                                                                                                Comparar
                                                                                                                                                                            </a>
                                                                                                                                                                        </div>
                                                                                                                                                                    </div>                </div>
                                                                                                                                                                </div>
                                                                                                                                                            </form>
                                                                                                                                                        </div>
                                                                                                                                                    </li>
                                                                                                                                                    <li class="productData cell" data-cp-start-daily-product style="display: none">
                                                                                                                                                        <div class="emproductborder listiteminfogridborder emsmoothborder clear">
                                                                                                                                                            <form name="tobasketemstartpagenew-10"
                                                                                                                                                                action="https://www.kyoceraap.com/index.php?" method="post"
                                                                                                                                                                >
                                                                                                                                                                <input type="hidden" name="actcontrol" value="start" />
                                                                                                                                                                <input type="hidden" name="lang" value="0" />
                                                                                                                                                                <input type="hidden" name="pgNr" value="0">
                                                                                                                                                                <input type="hidden" name="cl" value="start">
                                                                                                                                                                <input type="hidden" name="fnc" value="tobasket">
                                                                                                                                                                <input type="hidden" name="aid" value="279ca9ef1423ccb3b7f69f5e8d765995">
                                                                                                                                                                <input type="hidden" name="anid" value="279ca9ef1423ccb3b7f69f5e8d765995">            <input type="hidden" name="am" value="1">
                                                                                                                                                                <input type="hidden" name="oxloadid" value="">
                                                                                                                                                                <div class="emproduct clear listiteminfogrid">
                                                                                                                                                                    <div class="offerbox  hasuntil  hasdaily clear">
                                                                                                                                                                        <div class="percent">
                                                                                                                                                                            -10%
                                                                                                                                                                            <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.png" alt="daily offer icon"/>
                                                                                                                                                                            <span class="onlyhome">
                                                                                                                                                                                ($80.00)
                                                                                                                                                                            </span>
                                                                                                                                                                        </div>
                                                                                                                                                                    </div>
                                                                                                                                                                    <a class="cp-picture-container cpGaProdemstartpagenew-10" href="Computo-Hardware/Memorias-RAM-y-Flash/Memorias-RAM-para-PC/Memoria-RAM-XPG-Gammix-D30-Red-DDR4-3200MHz-16GB-Non-ECC-CL16-XMP.html">
                                                                                                                                                                        <div class="picture-wrapper">
                                                                                                                                                                            <div data-cp-prod-slider="[&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-XPG-AX4U320016G16A-SR30-1.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-XPG-AX4U320016G16A-SR30-2.jpg&quot;]" class="catSlider">
                                                                                                                                                                                <div data-cp-img-container class="cs-image"></div>
                                                                                                                                                                                <div class="csnav"></div>
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                        <div class="picture-overlay">
                                                                                                                                                                            <div class="badges-left">
                                                                                                                                                                                <img alt="XPG" class="product-logo" src="<?php echo base_url();?>cdn.kyoceraap.com/storage/brands/xpglogo2.png" data-cp-brand-logo="Por-Marca/XPG/index.html">
                                                                                                                                                                            </div>
                                                                                                                                                                            <div class="badges-middle">
                                                                                                                                                                            </div>
                                                                                                                                                                            <div class="badges-right">
                                                                                                                                                                                <div class="badges-right-top">
                                                                                                                                                                                </div>
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                    </a>
                                                                                                                                                                    <div class="emproduct_review">
                                                                                                                                                                        <div class="detailsInfo_right_review" style="margin-top:0; ">
                                                                                                                                                                            <div class="cpreviews_popup">
                                                                                                                                                                                <div style="float:left; position:relative; "
                                                                                                                                                                                    class="stars">
                                                                                                                                                                                    <div class="cpreviews_stars_box clear" title="
                                                                                                                                                                                        Me gusta
                                                                                                                                                                                        "
                                                                                                                                                                                        >
                                                                                                                                                                                        <div class="cpreviews_stars_box_percent" style="width:98%; "></div>
                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                                                                    </div>                </div>
                                                                                                                                                                                    <div style="float:left; position:relative; " class="popupborder">
                                                                                                                                                                                        <div class="popuptitle"></div>
                                                                                                                                                                                        <div class="popupbridge"></div>
                                                                                                                                                                                        <div class="popupbox">
                                                                                                                                                                                            <div class="cpreviews_box">
                                                                                                                                                                                                <div class="cpreviews_box_header clear">
                                                                                                                                                                                                    <div class="left">
                                                                                                                                                                                                        <div class="stars">
                                                                                                                                                                                                            <div class="cpreviews_stars_box clear" title="
                                                                                                                                                                                                                Me gusta
                                                                                                                                                                                                                "
                                                                                                                                                                                                                >
                                                                                                                                                                                                                <div class="cpreviews_stars_box_percent" style="width:98%; "></div>
                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                                                                                            </div>                </div>
                                                                                                                                                                                                            <div class="desc">4.9  de 5 estrellas </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="right">
                                                                                                                                                                                                            8
                                                                                                                                                                                                            opiniones
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                    <div class="cpreviews_starsdesc">
                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                            style="width:87.5px; "></div>
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">5 estrellas: (7)
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                            style="width:12.5px; "></div>
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">4 estrellas: (1)
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">3 estrellas: (0)
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">2 estrellas: (0)
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">1 estrellas: (0)
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                    <div class="cpreviews_box_link">
                                                                                                                                                                                                        <a href="Opiniones-sobre-Memoria-RAM-XPG-Gammix-D30-DDR4-3200MHz-16GB-Non-ECC-CL16-XMP-Rojo/index.html">Ver todas las (8) opiniones</a>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                </div>                                                    </div>
                                                                                                                                                                                            </div>
                                                                                                                                                                                            <div class="reviews-total-txt" >
                                                                                                                                                                                                <a href="Opiniones-sobre-Memoria-RAM-XPG-Gammix-D30-DDR4-3200MHz-16GB-Non-ECC-CL16-XMP-Rojo/index.html">
                                                                                                                                                                                                    8
                                                                                                                                                                                                    opiniones
                                                                                                                                                                                                </a>
                                                                                                                                                                                            </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>
                                                                                                                                                                                <div data-cp-complete-name="emproduct_cptitleBox">
                                                                                                                                                                                    <div class="emproduct_cptitleBox">
                                                                                                                                                                                        <a id="emstartpagenew-10" href="Computo-Hardware/Memorias-RAM-y-Flash/Memorias-RAM-para-PC/Memoria-RAM-XPG-Gammix-D30-Red-DDR4-3200MHz-16GB-Non-ECC-CL16-XMP.html"
                                                                                                                                                                                            class="emproduct_title cpGaProdemstartpagenew-10"
                                                                                                                                                                                            title="Memoria RAM XPG Gammix D30 DDR4, 3200MHz, 16GB, Non-ECC, CL16, XMP, Rojo"
                                                                                                                                                                                            data-truncate="Memoria RAM XPG Gammix D30 DDR4, 3200MHz, 16GB, Non-ECC, CL16, XMP, Rojo"
                                                                                                                                                                                            data-text>
                                                                                                                                                                                            <span>Memoria RAM XPG Gammix D30 DDR4, 3200MHz, 16GB, Non-ECC, CL16, XMP, Rojo</span>
                                                                                                                                                                                        </a>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>
                                                                                                                                                                                <div class="grid-x align-middle background-white cp-pt-10" style="padding-bottom: 5px;">
                                                                                                                                                                                    <div class="cell auto">
                                                                                                                                                                                        <div class="emproduct_artnum">AX4U320016G16A-SR30</div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                    <div class="cell shrink">
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>
                                                                                                                                                                                <div class="main-info background-white">
                                                                                                                                                                                    <div class="addtoCart-section">
                                                                                                                                                                                        <div class="tobasketFunction">
                                                                                                                                                                                            <button id="toBasket_emstartpagenew-10" type="submit"
                                                                                                                                                                                            class="submitButton largeButton">Agregar
                                                                                                                                                                                            </button>
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                    <div class="moreinfo-section">
                                                                                                                                                                                        <!-- Price -->
                                                                                                                                                                                        <!--sse-->
                                                                                                                                                                                        <div class="emproduct_price">
                                                                                                                                                                                            <span class="oldPrice">
                                                                                                                                                                                                <del>$769.00</del>
                                                                                                                                                                                            </span>
                                                                                                                                                                                            <label class="price">
                                                                                                                                                                                                $689.00
                                                                                                                                                                                            </label>
                                                                                                                                                                                        </div>
                                                                                                                                                                                        <!--/sse-->
                                                                                                                                                                                        <!-- Delivery Cost -->
                                                                                                                                                                                        <div class="emdeliverycost">
                                                                                                                                                                                            <span class="deliverytext">Costo de envío:</span>
                                                                                                                                                                                            <span class="deliveryvalue">$99.00</span>
                                                                                                                                                                                        </div>
                                                                                                                                                                                        <!--  Stock  -->
                                                                                                                                                                                        <div class="emstock">            Disponibles:
                                                                                                                                                                                            <span>123</span>
                                                                                                                                                                                            pzas.
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>
                                                                                                                                                                                <div class="emproduct_compare">
                                                                                                                                                                                    <div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_emstartpagenew-10" data-id="listproduct_emstartpagenew-10">
                                                                                                                                                                                        <div id="compare_removelistproduct_emstartpagenew-10" style=display:none>
                                                                                                                                                                                            <div class="emcomparelinkbox_link_ctn ">
                                                                                                                                                                                                <a data-id="listproduct_emstartpagenew-10" class="emcomparelinkbox_link_remove" data-aid="279ca9ef1423ccb3b7f69f5e8d765995"
                                                                                                                                                                                                    id="removeCmplistproduct_emstartpagenew-10" href="#" onclick="go_to('index9372.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=279ca9ef1423ccb3b7f69f5e8d765995&amp;anid=279ca9ef1423ccb3b7f69f5e8d765995&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                                                                                    <span class="square"></span>
                                                                                                                                                                                                </a>
                                                                                                                                                                                                <a class="emcomparelinkbox_link_txt " href="lista-de-comparacion/index.html">
                                                                                                                                                                                                Ver Comparación (0)            </a>
                                                                                                                                                                                            </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                        <div id="compare_addlistproduct_emstartpagenew-10" >
                                                                                                                                                                                            <a data-id="listproduct_emstartpagenew-10" class="emcomparelinkbox_link_add" data-aid="279ca9ef1423ccb3b7f69f5e8d765995"
                                                                                                                                                                                                id="toCmplistproduct_emstartpagenew-10" href="#" onclick="go_to('index9372.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=279ca9ef1423ccb3b7f69f5e8d765995&amp;anid=279ca9ef1423ccb3b7f69f5e8d765995&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                                                                                <span class="square"></span>
                                                                                                                                                                                                Comparar
                                                                                                                                                                                            </a>
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </div>                </div>
                                                                                                                                                                                </div>
                                                                                                                                                                            </form>
                                                                                                                                                                        </div>
                                                                                                                                                                    </li>
                                                                                                                                                                    <li class="productData cell" data-cp-start-daily-product style="display: none">
                                                                                                                                                                        <div class="emproductborder listiteminfogridborder emsmoothborder clear">
                                                                                                                                                                            <form name="tobasketemstartpagenew-11"
                                                                                                                                                                                action="https://www.kyoceraap.com/index.php?" method="post"
                                                                                                                                                                                >
                                                                                                                                                                                <input type="hidden" name="actcontrol" value="start" />
                                                                                                                                                                                <input type="hidden" name="lang" value="0" />
                                                                                                                                                                                <input type="hidden" name="pgNr" value="0">
                                                                                                                                                                                <input type="hidden" name="cl" value="start">
                                                                                                                                                                                <input type="hidden" name="fnc" value="tobasket">
                                                                                                                                                                                <input type="hidden" name="aid" value="60594b2b87db78580f524ece85d23a86">
                                                                                                                                                                                <input type="hidden" name="anid" value="60594b2b87db78580f524ece85d23a86">            <input type="hidden" name="am" value="1">
                                                                                                                                                                                <input type="hidden" name="oxloadid" value="">
                                                                                                                                                                                <div class="emproduct clear listiteminfogrid">
                                                                                                                                                                                    <div class="offerbox  hasuntil  hasdaily clear">
                                                                                                                                                                                        <div class="percent">
                                                                                                                                                                                            -19%
                                                                                                                                                                                            <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.png" alt="daily offer icon"/>
                                                                                                                                                                                            <span class="onlyhome">
                                                                                                                                                                                                ($807.00)
                                                                                                                                                                                            </span>
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                    <a class="cp-picture-container cpGaProdemstartpagenew-11" href="Computo-Hardware/Componentes/Tarjetas-de-Video/Tarjeta-de-Video-Sapphire-AMD-Radeon-RX-6500-XT-4GB-64-bit-GDDR6-PCI-Express-4-0.html">
                                                                                                                                                                                        <div class="picture-wrapper">
                                                                                                                                                                                            <div data-cp-prod-slider="[&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-SAPPHIRE-11314-01-20G-1eef8f.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-SAPPHIRE-11314-01-20G-610464.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-SAPPHIRE-11314-01-20G-5c4b0a.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-SAPPHIRE-11314-01-20G-02adc0.jpg&quot;]" class="catSlider">
                                                                                                                                                                                                <div data-cp-img-container class="cs-image"></div>
                                                                                                                                                                                                <div class="csnav"></div>
                                                                                                                                                                                            </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                        <div class="picture-overlay">
                                                                                                                                                                                            <div class="badges-left">
                                                                                                                                                                                                <img alt="SAPPHIRE" class="product-logo" src="<?php echo base_url();?>cdn.kyoceraap.com/storage/brands/579d7ad5df33469ab949933371f24586_sapphire1.png" data-cp-brand-logo="Por-Marca/SAPPHIRE/index.html">
                                                                                                                                                                                            </div>
                                                                                                                                                                                            <div class="badges-middle">
                                                                                                                                                                                            </div>
                                                                                                                                                                                            <div class="badges-right">
                                                                                                                                                                                                <div class="badges-right-top">
                                                                                                                                                                                                    <div class="cp-processor-badge" style="background-image: url('<?php echo base_url();?>out/pictures/badge-manager/rx-radeon47c3.png?1598661319');background-repeat: no-repeat;background-size: contain;"></div>
                                                                                                                                                                                                </div>
                                                                                                                                                                                            </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </a>
                                                                                                                                                                                    <div class="emproduct_review">
                                                                                                                                                                                        <div class="detailsInfo_right_review" style="margin-top:0; ">
                                                                                                                                                                                            <div class="cpreviews_popup">
                                                                                                                                                                                                <div style="float:left; position:relative; "
                                                                                                                                                                                                    class="stars">
                                                                                                                                                                                                    <div class="cpreviews_stars_box clear" title="
                                                                                                                                                                                                        Me gusta
                                                                                                                                                                                                        "
                                                                                                                                                                                                        >
                                                                                                                                                                                                        <div class="cpreviews_stars_box_percent" style="width:92%; "></div>
                                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                                                                                    </div>                </div>
                                                                                                                                                                                                    <div style="float:left; position:relative; " class="popupborder">
                                                                                                                                                                                                        <div class="popuptitle"></div>
                                                                                                                                                                                                        <div class="popupbridge"></div>
                                                                                                                                                                                                        <div class="popupbox">
                                                                                                                                                                                                            <div class="cpreviews_box">
                                                                                                                                                                                                                <div class="cpreviews_box_header clear">
                                                                                                                                                                                                                    <div class="left">
                                                                                                                                                                                                                        <div class="stars">
                                                                                                                                                                                                                            <div class="cpreviews_stars_box clear" title="
                                                                                                                                                                                                                                Me gusta
                                                                                                                                                                                                                                "
                                                                                                                                                                                                                                >
                                                                                                                                                                                                                                <div class="cpreviews_stars_box_percent" style="width:92%; "></div>
                                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                                                                                                            </div>                </div>
                                                                                                                                                                                                                            <div class="desc">4.6  de 5 estrellas </div>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                        <div class="right">
                                                                                                                                                                                                                            24
                                                                                                                                                                                                                            opiniones
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                    <div class="cpreviews_starsdesc">
                                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                                            style="width:58.333333333333px; "></div>
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">5 estrellas: (14)
                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                                            style="width:41.666666666667px; "></div>
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">4 estrellas: (10)
                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">3 estrellas: (0)
                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">2 estrellas: (0)
                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">1 estrellas: (0)
                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                    <div class="cpreviews_box_link">
                                                                                                                                                                                                                        <a href="Opiniones-sobre-Tarjeta-de-Video-Sapphire-AMD-Radeon-RX-6500-XT-4GB-64-bit-GDDR6-PCI-Express-4-0/index.html">Ver todas las (24) opiniones</a>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                </div>                                                    </div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="reviews-total-txt" >
                                                                                                                                                                                                                <a href="Opiniones-sobre-Tarjeta-de-Video-Sapphire-AMD-Radeon-RX-6500-XT-4GB-64-bit-GDDR6-PCI-Express-4-0/index.html">
                                                                                                                                                                                                                    24
                                                                                                                                                                                                                    opiniones
                                                                                                                                                                                                                </a>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                </div>
                                                                                                                                                                                                <div data-cp-complete-name="emproduct_cptitleBox">
                                                                                                                                                                                                    <div class="emproduct_cptitleBox">
                                                                                                                                                                                                        <a id="emstartpagenew-11" href="Computo-Hardware/Componentes/Tarjetas-de-Video/Tarjeta-de-Video-Sapphire-AMD-Radeon-RX-6500-XT-4GB-64-bit-GDDR6-PCI-Express-4-0.html"
                                                                                                                                                                                                            class="emproduct_title cpGaProdemstartpagenew-11"
                                                                                                                                                                                                            title="Tarjeta de Video Sapphire AMD Radeon RX 6500 XT,  4GB 64 bit GDDR6, PCI Express 4.0 ― ¡Compra y recibe un código de regalo The Last of US™ Part I!"
                                                                                                                                                                                                            data-truncate="Tarjeta de Video Sapphire AMD Radeon RX 6500 XT,  4GB 64 bit GDDR6, PCI Express 4.0 ― ¡..."
                                                                                                                                                                                                            data-text>
                                                                                                                                                                                                            <span>Tarjeta de Video Sapphire AMD Radeon RX 6500 XT,  4GB 64 bit GDDR6, PCI Express 4.0 ― ¡...</span>
                                                                                                                                                                                                        </a>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                </div>
                                                                                                                                                                                                <div class="grid-x align-middle background-white cp-pt-10" style="padding-bottom: 5px;">
                                                                                                                                                                                                    <div class="cell auto">
                                                                                                                                                                                                        <div class="emproduct_artnum">11314-01-20G</div>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                    <div class="cell shrink">
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                </div>
                                                                                                                                                                                                <div class="main-info background-white">
                                                                                                                                                                                                    <div class="addtoCart-section">
                                                                                                                                                                                                        <div class="tobasketFunction">
                                                                                                                                                                                                            <button id="toBasket_emstartpagenew-11" type="submit"
                                                                                                                                                                                                            class="submitButton largeButton">Agregar
                                                                                                                                                                                                            </button>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                    <div class="moreinfo-section">
                                                                                                                                                                                                        <!-- Price -->
                                                                                                                                                                                                        <!--sse-->
                                                                                                                                                                                                        <div class="emproduct_price">
                                                                                                                                                                                                            <span class="oldPrice">
                                                                                                                                                                                                                <del>$4,156.00</del>
                                                                                                                                                                                                            </span>
                                                                                                                                                                                                            <label class="price">
                                                                                                                                                                                                                $3,349.00
                                                                                                                                                                                                            </label>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <!--/sse-->
                                                                                                                                                                                                        <!-- Delivery Cost -->
                                                                                                                                                                                                        <div class="emdeliverycost">
                                                                                                                                                                                                            <span class="deliverytext">Costo de envío:</span>
                                                                                                                                                                                                            <span class="deliveryvalue">$99.00</span>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <!--  Stock  -->
                                                                                                                                                                                                        <div class="emstock">            Disponibles:
                                                                                                                                                                                                            <span>74</span>
                                                                                                                                                                                                            pzas.
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                </div>
                                                                                                                                                                                                <div class="emproduct_compare">
                                                                                                                                                                                                    <div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_emstartpagenew-11" data-id="listproduct_emstartpagenew-11">
                                                                                                                                                                                                        <div id="compare_removelistproduct_emstartpagenew-11" style=display:none>
                                                                                                                                                                                                            <div class="emcomparelinkbox_link_ctn ">
                                                                                                                                                                                                                <a data-id="listproduct_emstartpagenew-11" class="emcomparelinkbox_link_remove" data-aid="60594b2b87db78580f524ece85d23a86"
                                                                                                                                                                                                                    id="removeCmplistproduct_emstartpagenew-11" href="#" onclick="go_to('indexddf4.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=60594b2b87db78580f524ece85d23a86&amp;anid=60594b2b87db78580f524ece85d23a86&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                                                                                                    <span class="square"></span>
                                                                                                                                                                                                                </a>
                                                                                                                                                                                                                <a class="emcomparelinkbox_link_txt " href="lista-de-comparacion/index.html">
                                                                                                                                                                                                                Ver Comparación (0)            </a>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div id="compare_addlistproduct_emstartpagenew-11" >
                                                                                                                                                                                                            <a data-id="listproduct_emstartpagenew-11" class="emcomparelinkbox_link_add" data-aid="60594b2b87db78580f524ece85d23a86"
                                                                                                                                                                                                                id="toCmplistproduct_emstartpagenew-11" href="#" onclick="go_to('indexddf4.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=60594b2b87db78580f524ece85d23a86&amp;anid=60594b2b87db78580f524ece85d23a86&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                                                                                                <span class="square"></span>
                                                                                                                                                                                                                Comparar
                                                                                                                                                                                                            </a>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                    </div>                </div>
                                                                                                                                                                                                </div>
                                                                                                                                                                                            </form>
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </li>
                                                                                                                                                                                    <li class="productData cell" data-cp-start-daily-product style="display: none">
                                                                                                                                                                                        <div class="emproductborder listiteminfogridborder emsmoothborder clear">
                                                                                                                                                                                            <form name="tobasketemstartpagenew-12"
                                                                                                                                                                                                action="https://www.kyoceraap.com/index.php?" method="post"
                                                                                                                                                                                                >
                                                                                                                                                                                                <input type="hidden" name="actcontrol" value="start" />
                                                                                                                                                                                                <input type="hidden" name="lang" value="0" />
                                                                                                                                                                                                <input type="hidden" name="pgNr" value="0">
                                                                                                                                                                                                <input type="hidden" name="cl" value="start">
                                                                                                                                                                                                <input type="hidden" name="fnc" value="tobasket">
                                                                                                                                                                                                <input type="hidden" name="aid" value="927c4ace2c4fb2d25197f217295096c0">
                                                                                                                                                                                                <input type="hidden" name="anid" value="927c4ace2c4fb2d25197f217295096c0">            <input type="hidden" name="am" value="1">
                                                                                                                                                                                                <input type="hidden" name="oxloadid" value="">
                                                                                                                                                                                                <div class="emproduct clear listiteminfogrid">
                                                                                                                                                                                                    <div class="offerbox  hasuntil  hasdaily clear">
                                                                                                                                                                                                        <div class="percent">
                                                                                                                                                                                                            -26%
                                                                                                                                                                                                            <img src="<?php echo base_url();?>out/cyberpuertaV5/img/featured-products-white.png" alt="daily offer icon"/>
                                                                                                                                                                                                            <span class="onlyhome">
                                                                                                                                                                                                                ($2,892.00)
                                                                                                                                                                                                            </span>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                    <a class="cp-picture-container cpGaProdemstartpagenew-12" href="Computadoras/Laptops/Laptop-Lenovo-ThinkBook-14-G2-ITL-14-Full-HD-Intel-Core-i3-1115G4-3GHz-8GB-256GB-SSD-Windows-10-Pro-64-bit-Espanol-Gris.html">
                                                                                                                                                                                                        <div class="picture-wrapper">
                                                                                                                                                                                                            <div data-cp-prod-slider="[&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-LENOVO-20VD00KBLM-1.png&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-LENOVO-20VD00KBLM-2.png&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-LENOVO-20VD00KBLM-83f23d.jpg&quot;,&quot;https:\/\/www.kyoceraap.com\/img\/product\/S\/CP-LENOVO-20VD00KBLM-4.png&quot;]" class="catSlider">
                                                                                                                                                                                                                <div data-cp-img-container class="cs-image"></div>
                                                                                                                                                                                                                <div class="csnav"></div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="picture-overlay">
                                                                                                                                                                                                            <div class="badges-left">
                                                                                                                                                                                                                <img alt="LENOVO" class="product-logo" src="<?php echo base_url();?>cdn.kyoceraap.com/storage/brands/685903ef83f7a7c8a165f77eed160d9e_lenovo.png" data-cp-brand-logo="Por-Marca/LENOVO/index.html">
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="badges-middle">
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="badges-right">
                                                                                                                                                                                                                <div class="badges-right-top">
                                                                                                                                                                                                                    <div class="cp-processor-badge" style="background-image: url('<?php echo base_url();?>out/pictures/badge-manager/Intel_Corei3%20(11th).jpg?1616168363');background-repeat: no-repeat;background-size: contain;"></div>
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                    </a>
                                                                                                                                                                                                    <div class="emproduct_review">
                                                                                                                                                                                                        <div class="detailsInfo_right_review" style="margin-top:0; ">
                                                                                                                                                                                                            <div class="cpreviews_popup">
                                                                                                                                                                                                                <div style="float:left; position:relative; "
                                                                                                                                                                                                                    class="stars">
                                                                                                                                                                                                                    <div class="cpreviews_stars_box clear" title="
                                                                                                                                                                                                                        ¡Lo mejor!
                                                                                                                                                                                                                        "
                                                                                                                                                                                                                        >
                                                                                                                                                                                                                        <div class="cpreviews_stars_box_percent" style="width:100%; "></div>
                                                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                                                                                                        <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                                                                                                    </div>                </div>
                                                                                                                                                                                                                    <div style="float:left; position:relative; " class="popupborder">
                                                                                                                                                                                                                        <div class="popuptitle"></div>
                                                                                                                                                                                                                        <div class="popupbridge"></div>
                                                                                                                                                                                                                        <div class="popupbox">
                                                                                                                                                                                                                            <div class="cpreviews_box">
                                                                                                                                                                                                                                <div class="cpreviews_box_header clear">
                                                                                                                                                                                                                                    <div class="left">
                                                                                                                                                                                                                                        <div class="stars">
                                                                                                                                                                                                                                            <div class="cpreviews_stars_box clear" title="
                                                                                                                                                                                                                                                ¡Lo mejor!
                                                                                                                                                                                                                                                "
                                                                                                                                                                                                                                                >
                                                                                                                                                                                                                                                <div class="cpreviews_stars_box_percent" style="width:100%; "></div>
                                                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star1"></div>
                                                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star2"></div>
                                                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star3"></div>
                                                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star4"></div>
                                                                                                                                                                                                                                                <div class="cpreviews_stars_box_star cpreviews_stars_box_star5"></div>
                                                                                                                                                                                                                                            </div>                </div>
                                                                                                                                                                                                                                            <div class="desc">5  de 5 estrellas </div>
                                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                                        <div class="right">
                                                                                                                                                                                                                                            1
                                                                                                                                                                                                                                            opinión
                                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                    <div class="cpreviews_starsdesc">
                                                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                                                            style="width:100px; "></div>
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">5 estrellas: (1)
                                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">4 estrellas: (0)
                                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">3 estrellas: (0)
                                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">2 estrellas: (0)
                                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                                        <div class="cpreviews_starsdesc_row clear">
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_linebg"></div>
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_line"
                                                                                                                                                                                                                                            style="width:0px; "></div>
                                                                                                                                                                                                                                            <div class="cpreviews_starsdesc_text">1 estrellas: (0)
                                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                    <div class="cpreviews_box_link">
                                                                                                                                                                                                                                        <a href="Opiniones-sobre-Laptop-Lenovo-ThinkBook-14-G2-ITL-14-Full-HD-Intel-Core-i3-1115G4-3GHz-8GB-256GB-SSD-Windows-10-Pro-64-bit-Espanol-Gris/index.html">Ver todas las (1) opiniones</a>
                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                </div>                                                    </div>
                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                            <div class="reviews-total-txt" >
                                                                                                                                                                                                                                <a href="Opiniones-sobre-Laptop-Lenovo-ThinkBook-14-G2-ITL-14-Full-HD-Intel-Core-i3-1115G4-3GHz-8GB-256GB-SSD-Windows-10-Pro-64-bit-Espanol-Gris/index.html">
                                                                                                                                                                                                                                    1
                                                                                                                                                                                                                                    opinión
                                                                                                                                                                                                                                </a>
                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                <div data-cp-complete-name="emproduct_cptitleBox">
                                                                                                                                                                                                                    <div class="emproduct_cptitleBox">
                                                                                                                                                                                                                        <a id="emstartpagenew-12" href="Computadoras/Laptops/Laptop-Lenovo-ThinkBook-14-G2-ITL-14-Full-HD-Intel-Core-i3-1115G4-3GHz-8GB-256GB-SSD-Windows-10-Pro-64-bit-Espanol-Gris.html"
                                                                                                                                                                                                                            class="emproduct_title cpGaProdemstartpagenew-12"
                                                                                                                                                                                                                            title="Laptop Lenovo ThinkBook 14 G2 ITL 14&quot; Full HD, Intel Core i3-1115G4 3GHz, 8GB, 256GB SSD, Windows 10 Pro 64-bit, Español, Gris"
                                                                                                                                                                                                                            data-truncate="Laptop Lenovo ThinkBook 14 G2 ITL 14&quot; Full HD, Intel Core i3-1115G4 3GHz, 8GB, 256GB SS..."
                                                                                                                                                                                                                            data-text>
                                                                                                                                                                                                                            <span>Laptop Lenovo ThinkBook 14 G2 ITL 14&quot; Full HD, Intel Core i3-1115G4 3GHz, 8GB, 256GB SS...</span>
                                                                                                                                                                                                                        </a>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                <div class="grid-x align-middle background-white cp-pt-10" style="padding-bottom: 5px;">
                                                                                                                                                                                                                    <div class="cell auto">
                                                                                                                                                                                                                        <div class="emproduct_artnum">20VD00KBLM</div>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                    <div class="cell shrink">
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                <div class="main-info background-white">
                                                                                                                                                                                                                    <div class="addtoCart-section">
                                                                                                                                                                                                                        <div class="tobasketFunction">
                                                                                                                                                                                                                            <button id="toBasket_emstartpagenew-12" type="submit"
                                                                                                                                                                                                                            class="submitButton largeButton">Agregar
                                                                                                                                                                                                                            </button>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                    <div class="moreinfo-section">
                                                                                                                                                                                                                        <!-- Price -->
                                                                                                                                                                                                                        <!--sse-->
                                                                                                                                                                                                                        <div class="emproduct_price">
                                                                                                                                                                                                                            <span class="oldPrice">
                                                                                                                                                                                                                                <del>$11,241.00</del>
                                                                                                                                                                                                                            </span>
                                                                                                                                                                                                                            <label class="price">
                                                                                                                                                                                                                                $8,349.00
                                                                                                                                                                                                                            </label>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                        <!--/sse-->
                                                                                                                                                                                                                        <!-- Delivery Cost -->
                                                                                                                                                                                                                        <div class="emdeliverycost">
                                                                                                                                                                                                                            <span class="deliverytext">Costo de envío:</span>
                                                                                                                                                                                                                            <span class="deliveryvalue">$99.00</span>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                        <!--  Stock  -->
                                                                                                                                                                                                                        <div class="emstock">            Disponibles:
                                                                                                                                                                                                                            <span>105</span>
                                                                                                                                                                                                                            pzas.
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                <div class="emproduct_compare">
                                                                                                                                                                                                                    <div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_emstartpagenew-12" data-id="listproduct_emstartpagenew-12">
                                                                                                                                                                                                                        <div id="compare_removelistproduct_emstartpagenew-12" style=display:none>
                                                                                                                                                                                                                            <div class="emcomparelinkbox_link_ctn ">
                                                                                                                                                                                                                                <a data-id="listproduct_emstartpagenew-12" class="emcomparelinkbox_link_remove" data-aid="927c4ace2c4fb2d25197f217295096c0"
                                                                                                                                                                                                                                    id="removeCmplistproduct_emstartpagenew-12" href="#" onclick="go_to('index13c2.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=927c4ace2c4fb2d25197f217295096c0&amp;anid=927c4ace2c4fb2d25197f217295096c0&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                                                                                                                    <span class="square"></span>
                                                                                                                                                                                                                                </a>
                                                                                                                                                                                                                                <a class="emcomparelinkbox_link_txt " href="lista-de-comparacion/index.html">
                                                                                                                                                                                                                                Ver Comparación (0)            </a>
                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                        <div id="compare_addlistproduct_emstartpagenew-12" >
                                                                                                                                                                                                                            <a data-id="listproduct_emstartpagenew-12" class="emcomparelinkbox_link_add" data-aid="927c4ace2c4fb2d25197f217295096c0"
                                                                                                                                                                                                                                id="toCmplistproduct_emstartpagenew-12" href="#" onclick="go_to('index13c2.html?cl=start&amp;am=1&amp;addcompare=1&amp;fnc=tocomparelist&amp;aid=927c4ace2c4fb2d25197f217295096c0&amp;anid=927c4ace2c4fb2d25197f217295096c0&amp;pgNr=0&amp;searchparam=')">
                                                                                                                                                                                                                                <span class="square"></span>
                                                                                                                                                                                                                                Comparar
                                                                                                                                                                                                                            </a>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                    </div>                </div>
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                            </form>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                    </li>
                                                                                                                                                                                                </ul>
                                                                                                                                                                                            </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>