
                <div class="start_topbox_right">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="checkoutSteps clear">
                                <li class="step1   ">
                                    <a href="<?php echo base_url();?>Carrito">
                                        Carrito
                                    </a>
                                </li>
                                <li class="step4">
                                    <span>
                                        3. Confirmar pedido
                                    </span>
                                </li>
                                <li class="step3">
                                    <span>
                                        2. Envío y pago
                                    </span>
                                </li>
                                <li class="step2 active">
                                    <span>
                                        <a href="<?php echo base_url();?>Carrito/direccion">1. Elegir dirección</a>
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a type="button" class="btn btn-success" onclick="agregardir()" data-target="#direccionModal">Agregar Dirección</a>
                                <a type="button" class="btn btn-success" onclick="agregarcon()" data-target="#direccionModal">Agregar Datos de contacto</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <!--<form method="post" action="./enviopago" id="formenviopago">-->
                            <?php echo form_open('Carrito/enviopago'); ?>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Direcciones</th>
                                            <th>Entre calles</th>
                                            <th>Codigo Postal</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($direcciones as $item) { 
                                            $idclientedirecc=$item['idclientedirecc'];
                                            ?>
                                            <tr>
                                                <td><input type="radio" name="direccion" id="dir_<?php echo $item['idclientedirecc']; ?>" value="<?php echo $item['idclientedirecc']; ?>" required></td>
                                                <td for="dir_<?php echo $item['idclientedirecc']; ?>"><label for="dir_<?php echo $item['idclientedirecc']; ?>"><?php echo $item['direccion']; ?></label></td>
                                                <td for="dir_<?php echo $item['idclientedirecc']; ?>"><label for="dir_<?php echo $item['idclientedirecc']; ?>"><?php echo $item['entrecalles']; ?></label></td>
                                                <td for="dir_<?php echo $item['idclientedirecc']; ?>"><label for="dir_<?php echo $item['idclientedirecc']; ?>"><?php echo $item['cp']; ?></label></td>
                                                <td ><a type="button" class="btn btn-danger btn-sm" onclick="deletedir('<?php echo base64_encode($idclientedirecc); ?>')" data-target="#direccionModal"><i class="fa fa-trash fa-fw"></i></a></td>
                                            </tr>
                                        <?php } ?>
                                        <tr></tr>
                                    </tbody>
                                </table>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Contacto</th>
                                            <th>Teléfono</th>
                                            <th>Celular</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($contactos as $item) { ?>
                                            <tr>
                                                <td><input type="radio" name="contacto" id="con_<?php echo $item['datosId']; ?>" value="<?php echo $item['datosId']; ?>" required></td>
                                                <td for="con_<?php echo $item['datosId']; ?>"><label for="dir_<?php echo $item['datosId']; ?>"><?php echo $item['atencionpara']; ?></label></td>
                                                <td for="con_<?php echo $item['datosId']; ?>"><label for="dir_<?php echo $item['datosId']; ?>"><?php echo $item['telefono']; ?></label></td>
                                                <td for="con_<?php echo $item['datosId']; ?>"><label for="dir_<?php echo $item['datosId']; ?>"><?php echo $item['celular']; ?></label></td>
                                                <td ></td>
                                            </tr>
                                        <?php } ?>
                                        <tr></tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-secondary" >Ir al siguiente paso</button>
                                    </div>
                                </div>
                            <!--</form>-->
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                    
                </div>







                

                <div id="direccionModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLiveLabel">Agregar Dirección</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Introduce una nueva dirección de envió:</label>
                                <label>Dirección(calle,numero exterior y/o exterior, colonia, estado, código postal):</label>
                                <textarea class="form-control" id="adddireccion"></textarea>
                                <label>Entre calles:</label>
                                <textarea class="form-control" id="entrecalles"></textarea>
                                <label>Codigo Postal:</label>
                                <input class="form-control" type="" id="cp">
                            </div>
                        </div>

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="savedir_close">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="savedir">Guardar</button>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="contactoModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLiveLabel">Agregar Datos de contacto</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Nombre / Quien recibe:</label>
                                <input class="form-control" type="" id="atencionpara">
                                <label>Teléfono:</label>
                                <input class="form-control" type="phone" id="telefono">
                                <label>Celular:</label>
                                <input class="form-control" type="phone" id="celular">
                            </div>
                        </div>

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="savecont_close">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="savecont">Guardar</button>
                      </div>
                    </div>
                  </div>
                </div>