<input type="hidden" id="idemp" value="<?php echo $idemp; ?>">


			<!--<div id="breadCrumb"><span class="breadCrumb first  "><a href="https://www.kyoceraap.com/" title="Inicio"><span>Inicio</span></a></span><span class="breadCrumb  last "><a href="https://www.kyoceraap.com/Nuevos-articulos/" title="Nuevos artículos"><span> artículos</span></a></span></div>-->
			<div  class="start_topbox_right">
				<div class="row">
					<div class="col-md-12">
						<div id="breadCrumb">
							<span class="breadCrumb first"><a href="<?php echo base_url()?>" title="Inicio"><span>Inicio</span></a></span>
							<span class="breadCrumb first"><a href="<?php echo base_url()?>Mi_perfil" title="Mi perfil"><span> Mi Perfil</span></a></span>
							<span class="breadCrumb last"><a href="<?php echo base_url()?>Mi_perfil/servicios" title="Servicios"><span> Servicios</span></a></span>
						</div>
					</div>
				</div>
				
				<div class="row " style="margin-bottom: 113px; margin-top: 15px;">
					<!------------------------------->
						<div class="row">
							<div class="col-md-12">
								<table class="table table-hover" id="table_servicios">
									<thead>
										<tr>
											<th>Tipo</th>
											<th>Servicio</th>
											<th>Fecha</th>
											<th>Hora</th>
											<th>Tecnico</th>
											<th>Estatus</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$html='';
											foreach ($serviciosall as $item_s) {
												$tipo='';
												$btn_doc='';
												$btn_doc='<a type="button" class="btn btn-secondary" href="'.base_url().'Mi_perfil/servicios_doc/'.$item_s['asignacionId'].'/'.$item_s['tipo'].'" target="_blank"><i class="fas fa-file-pdf"></i></a>';
												if($item_s['tipo']==1){
													$tipo='Rentas';
												}
												if($item_s['tipo']==2){
													$tipo='Poliza';
												}
												if($item_s['tipo']==3){
													$tipo='Evento';
												}
												if($item_s['tipo']==4){
													$tipo='Venta';
												}
												$g_status = $this->ModeloGeneral->get_info_status($item_s['g_status']);
												$g_nr = $this->ModeloGeneral->get_info_status_nr($item_s['g_nr']);

												$g_s_color='';
												$g_s_value='';

												$g_nr_color='';
												$g_nr_value='';

												if($g_status==2){
													$g_s_color=' style="background: #71c375;" ';
													$g_s_value='Completado';
												}
												if($g_status==1){
													$g_s_color=' style="background: #FFEB3B;" ';
													$g_s_value='En proceso';
												}
												if($g_status==0){
													$g_s_color=' style="background: #d7d7d7;" ';
													$g_s_value='En Espera';
												}
												if($g_nr==1){
													$g_s_color=' style="background: #f17a7a;" ';
													$g_s_value='Cancelado';

													
												}
												$html.='<tr data-gstatus="'.$item_s['g_status'].'" data-gnr="'.$item_s['g_nr'].'" >';
													$html.='<td>'.$tipo.'</td>';
													$html.='<td>'.$item_s['servicio'].'</td>';
													$html.='<td>'.$item_s['fecha'].'</td>';
													$html.='<td>'.$item_s['hora'].'</td>';
													$html.='<td>'.$item_s['tecnico'].' '.$item_s['tecnico2'].'</td>';
													$html.='<td '.$g_s_color.' >'.$g_s_value.'</td>';
													

													$html.='<td>'.$btn_doc.'</td>';
												$html.='</tr>';
											}
											echo $html;
										?>
									</tbody>
								</table>
							</div>
						</div>
						
					<!------------------------------->
				</div>			
			</div>
		</div>
	</div>
</div>
