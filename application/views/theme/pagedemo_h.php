<!DOCTYPE HTML>
<html lang="es" >
    <!-- Mirrored from www.kyoceraap.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 Mar 2023 23:28:22 GMT -->
    <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <?php $ver=date('ymdhms');?>
    <head>
        <link rel="icon" href="<?php echo base_url(); ?>public/img/favicon_kyocera.png" sizes="32x32">
        <meta content="width=device-width,initial-scale=1.0" name=viewport>
        <title>kyoceraap.com: Impresoras</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="Venta en línea de Hardware, Computadoras, Laptops, Impresoras, Monitores y más">
        <link rel="canonical" href="<?php echo base_url()?>">
        <link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome/css/all.min.css" type="text/css" >
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/generated/cms-pagesebeb.css?v=<?php echo $ver;?>" />-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/cyberpuertaV5/src/css/emslider20497.css?v=<?php echo $ver;?>"><!--requerido-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/cyberpuertaV5/src/css/oxwidget_headerlogin0497.css?v=<?php echo $ver;?>">
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/cyberpuertaV5/src/css/oxwidget_headernotice0497.css?v=<?php echo $ver;?>">-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/cyberpuertaV5/src/css/oxwidget_headercompare0497.css?v=<?php echo $ver;?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/cyberpuertaV5/src/css/oxwidget_headerhelp0497.css?v=<?php echo $ver;?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/cyberpuertaV5/src/css/oxwidget_headerminibasket0497.css?v=<?php echo $ver;?>"><!--requerido-->
        <link data-cp-orig="cdn.Swiper.css" rel="stylesheet" type="text/css" href="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css">
        <link data-cp-orig="cp.app.legacy.css" rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/dist/cp.app.legacy.2a1306440cf66f0e3efa.g.css?v=<?php echo date('ymdhms') ?>">
        <!--<link data-cp-orig="cp.app.foundation.css" rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/dist/cp.app.foundation.e29b346a69794f0187d9.g.css">-->
        <link data-cp-orig="cp.app.css" rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/dist/cp.app.3551495a5a8c567d440e.g.css"><!--requerido--->
        <link data-cp-orig="vendors~cp.app.css" rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/dist/vendors_cp.app.f79cffac28db61b5a671.g.css">
        <link data-cp-orig="cdn.material-icons.css" rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link data-cp-orig="cdn.open-sans.css" rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;display=swap">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/style_theme.css?1593624763" />
        <link href="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
        <input type="hidden" id="base_url" value="<?php echo base_url()?>">
        <link href="<?php echo base_url(); ?>public/css/styles.css?v=<?php echo $ver;?>" type="text/css" rel="stylesheet">


        <link rel="shortcut icon" href="<?php echo base_url();?>public/img/favkyo.png">
        <script>window.cp = {}</script>            <script data-cp-orig="cdn.jquery.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script data-cp-orig="cdn.foundation.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/foundation/6.5.3/js/foundation.min.js"></script>
        <script data-cp-orig="cdn.knockout.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/knockout/3.5.0/knockout-min.js"></script>
        <script data-cp-orig="cdn.moment.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
        <script data-cp-orig="cdn.moment-es.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/locale/es.min.js"></script>
        <script data-cp-orig="cdn.moment-timezone.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.26/moment-timezone-with-data-10-year-range.min.js"></script>
        <script data-cp-orig="cdn.Swiper.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js"></script>
        <script data-cp-orig="cdn.vue.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/vue/2.7.10/vue.runtime.min.js"></script>
        <script data-cp-orig="cdn.nouislider.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/noUiSlider/14.6.3/nouislider.min.js"></script>
        <script data-cp-orig="google.account" src="https://accounts.google.com/gsi/client"></script>
        <script data-cp-orig="apple.account" src="<?php echo base_url();?>appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/es_MX/appleid.auth.js"></script>
        <script data-cp-orig="manifest.js" src="<?php echo base_url();?>out/dist/manifest.c0f5f9e1e31a23450397.js"></script>
        <script data-cp-orig="cp.app.js" src="<?php echo base_url();?>out/dist/cp.app.de4ec193647b4d7c0e1e.js"></script>
        <script data-cp-orig="cp.app.foundation.js" src="<?php echo base_url();?>out/dist/cp.app.foundation.5aec65c868e55fdcfafa.js"></script>
        <script data-cp-orig="cp.app.legacy.js" src="<?php echo base_url();?>out/dist/cp.app.legacy.6bbaed39abf879b63359.js"></script>
        <script data-cp-orig="vendors~cp.app.js" src="<?php echo base_url();?>out/dist/vendors_cp.app.d380a273ca993ef3e5ed.js"></script>
        <script src="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.js"></script>
        <!--<script src="<?php echo base_url()?>public/plugins/code.jquery.com_jquery-2.2.0.min.js"  ></script>-->
        <link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
        <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
        
        <link href="<?php echo base_url()?>public/plugins/slick/slick-1.8.1/slick/slick.css?v=2" rel="stylesheet"  >
        <link href="<?php echo base_url()?>public/plugins/slick/slick-1.8.1/slick/slick-theme.css?v=2" rel="stylesheet"  >
        <script src="<?php echo base_url()?>public/plugins/slick/slick-1.8.1/slick/slick.js"  ></script>
        <link href="<?php echo base_url(); ?>public/css/styles_movil.css?v=<?php echo $ver;?>" type="text/css" rel="stylesheet">
        <style type="text/css">
            .logos.logo_1{
                background: url(<?php echo base_url()?>/public/img/logo2_white.svg);
                background-size: 100%;
                background-repeat: no-repeat;background-position: center;
            }
            .logos.logo_2{
                background: url(<?php echo base_url()?>/public/img/alta_white.svg);
                background-size: 60%;
                background-repeat: no-repeat;background-position: center;
            }
            .cabezera_info{
                background: url(<?php echo base_url()?>/public/img/ejecutivo.svg) white;
                background-size: 12%;
                background-repeat: no-repeat;background-position-x: right;background-position-y: center;
            }
            
.menus_has a,.clas_a{    text-decoration: none;    color: black !important;
}
.menus_has a:hover,.clas_a:hover{    color: red !important;}
.h_emp{    overflow-wrap: break-word; /* Permitir que el texto se ajuste automáticamente */    word-break: break-word;    white-space: normal !important; }
.img_per{    width: 50px;    height: 34px;}
.img_sesion{    width: 25px;}
.div_menu{    display: block;    float: inline-end;}
#emheaderlogin{    position: relative;    right: auto;}

a.submitButton, span.submitButton {    color: white !important;    background: linear-gradient(180deg, #f3402e 0, #ff0202); }
a.submitButton:hover, span.submitButton:hover {    color: red !important;}
#form_sesion label.error{    width: 100% !important;    padding-top: 0px;    background: red;    color: black;    text-align: center !important;}
#chat-what a{
    background: url(<?php echo base_url()?>/public/img/icons8-whatsapp.svg);
}
.p_menu_conte .headernotice_title1 {
    padding: 0;
    margin: 0;
    width: 120px;
    height: 24px;
    line-height: 27px;
    box-sizing: border-box;
    padding-left: 10px;
    font-size: 14px;
    cursor: pointer;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    background-image: url(<?php echo base_url();?>public/img/down-gray-arrow.png);
    background-repeat: no-repeat;
    background-position: calc(100% - 10px) center;
    color: #999999;
}
.p_menu_conte .headernotice_title1:hover{
    /*background-image: url(<?php echo base_url();?>public/img/down-blue-arrow.png);*/
    background-image: url(<?php echo base_url();?>public/img/down-red-arrow.png);
    color: red;
}
#oxwidget_headerlogin.hover .oxwidget_headerlogin_header .oxwidget_headerlogin_title1 {
    background-image: url(<?php echo base_url();?>public/img/down-red-arrow.png);
    color: red;
}
.img_footer{
    background-image: url(<?php echo base_url().'public/img/Imagen1.svg';?>);
    background-repeat: no-repeat;
    background-position-x: center;
    background-position-y: bottom;
    background-size: contain;
}
.menu-contact .dropdown-content{
    max-width: 180px;
}
.menu-contact .dropdown-content a{
    font-size: 13px !important;
}
        </style>
    </head>
    <body class="start ">
            <input type="hidden" id="csrfName" value="<?php echo $this->security->get_csrf_token_name(); ?>">
            <input type="hidden" id="csrfHash" value="<?php echo $this->security->get_csrf_hash(); ?>">
            <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
        <div class="general-overlay cp-overlay1 hide">
            <div class="cp-overlay-cnt" style="background-image:url('<?php echo base_url();?>out/cyberpuertaV5/img/loading_md.gif')">
                <div class="inner-cnt">
                    <div class='cp-overlay-loader'></div>
                    <div class='cp-overlay-title'></div>
                    <div class='cp-overlay-subtitle'></div>
                </div>
            </div>
        </div>
        <div data-cp-banner-place="side-left"></div>
        <div data-cp-banner-place="side-right"></div>
        <div class="cp-page-bg">
            <div data-cp-banner-place="page-top"></div>
            <a id="cpsaldo_newsletter_modal_popup_link" data-open="cpsaldo_newsletter_modal_popup" style="display:none;">cpsaldo_newsletter</a>

            
            <div id="headerstatic" class="">
                <div id="headerbackground"></div>
                <div id="header" class="clear">
                    <div class="headerborder">
                        <a class="logo" href="<?php echo base_url();?>Inicio" title="" style="width: 192px; text-decoration: auto; text-align: center;">
                            
                            
                            <img src="<?php echo base_url();?>public/img/alta.webp" alt="logo alta" class="img_log_2">
                        </a>
                        <div  class="cp-header-search desktop" style="max-width: 790px; left: 381px;">
                            <form class="search-form" action="https://www.kyoceraap.com/index.php?" method="get" name="search" autocomplete="off">
                                <input maxlength="255" required autocomplete="off" type="text" name="searchparam" id="searchparam" value="" data-cp-placeholder="¿Qué producto buscas el día de hoy?" >
                                <button class="submitButton largeButton" type="submit">Buscar</button>
                            </form>
                            
                        </div>
                        <div id="cp-main-menu-btn" class="cp-main-menu-btn">
                            <div class="title">Categorías</div>
                            
                        </div>
                        <div class="p_menu">
                            <?php if($this->session->userdata('logeado')==true){$class_login=' class_login ';}else{$class_login='';}?>
                            <div class="p_menu_conte <?php echo $class_login; ?>">
                                <div class="headernotice_title1 menus_ki" style="width: 90px;">
                                    <div class="oxwidget_headerhelp_title1 clas_a" ><a href="http://localhost/kyocerapagina/Inicio/quienes_somos">Kyocera</a></div>
                                </div>
                                <div id="oxwidget_headerhelp" class="menus_ki menus_has has">
                                    <div class="headernotice_title1">
                                        <div class="oxwidget_headerhelp_title1 clas_a">Categorías</div>
                                    </div>
                                    <div class="oxwidget_headerhelp_popup">
                                        <div class="oxwidget_headerhelp_helper"></div>
                                        <ul>
                                            <li><a href="Quienes-somos/index.html">Venta de equipos</a></li>
                                            <li><a href="Quienes-somos/index.html">Venta de refacciones</a></li>
                                            <li><a href="Contacto/index.html">Venta de consumibles</a></li>
                                            <li><a href="Contacto/index.html">Venta de accesorios</a></li>
                                            <li><a href="Preguntas-frequentes/index.html">Renta de equipos</a></li>
                                            <li><a href="Modos-de-Pago/index.html">Servicio técnico</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="menus_ki headernotice_title1" style="min-width: 125px;">
                                    <div class="oxwidget_headerhelp_title1 clas_a" ><a href="<?php echo base_url() ?>Inicio/rentaequipos">Arrendamiento</a></div>
                                </div>
                                <div class="menus_ki headernotice_title1" style="min-width: 125px;">
                                    <div class="oxwidget_headerhelp_title1 clas_a" ><a href="<?php echo base_url() ?>Inicio/contacto">Digitalización</a></div>
                                </div>
                                <?php if($this->session->userdata('logeado')==true){ ?>
                                    <div class="menus_ki headernotice_title1">
                                        <div class="oxwidget_headerhelp_title1 clas_a" ><a href="<?php echo base_url() ?>Inicio/consumibles">Consumibles</a></div>
                                    </div>
                                    <div class="menus_ki headernotice_title1" style="width: 83px;">
                                        <div class="oxwidget_headerhelp_title1 clas_a" ><a href="<?php echo base_url() ?>Inicio/contacto">Outlet</a></div>
                                    </div>
                                <?php } ?>
                                <?php if($this->session->userdata('logeado')==true){ ?>
                                    <div id="emheaderlogin" class="menus_ki div_menu" >
                                        <div id="oxwidget_headerlogin" class="menus_has">
                                            <div class="oxwidget_headerlogin_header">
                                                <div class="oxwidget_headerlogin_title1" title="<?php echo $this->session->userdata('emp');?>"><a>Mi Cuenta</a></div>
                                            </div>
                                            <div class="oxwidget_headerlogin_popup">
                                                <div class="oxwidget_headerlogin_helper "></div>
                                                <div class="header_popup">
                                                    <ul style="padding: 0px;">
                                                        <li><a href="#" class="h_emp">¡Hola! <b><?php echo $this->session->userdata('emp');?></b></a></li>
                                                        <li><a href="<?php echo base_url();?>Mi_perfil">Mi Perfil <img class="img_per" alt="img_per" src="<?php echo base_url().'public/img/miperfil.svg';?>"></a></li>
                                                        <!--<li><a href="<?php echo base_url();?>Cotizacion">Cotización</a></li>-->
                                                        <li><a href="<?php echo base_url();?>Account/exit">Cerrar Sesión <img class="img_sesion" alt="img_sesion" src="<?php echo base_url().'public/img/salir_session.svg';?>"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <div id="emheaderlogin" class="menus_ki div_menu">
                                        <div id="oxwidget_headerlogin" class="">
                                            <div class="oxwidget_headerlogin_header">
                                                <div class="oxwidget_headerlogin_title1 clas_a">Tu sesión</div>
                                            </div>
                                            <div class="oxwidget_headerlogin_popup">
                                                <div class="oxwidget_headerlogin_helper "></div>
                                                <div class="oxwidget_headerlogin_loginform">
                                                    <div class="info_login_error"></div>
                                                    <!--<form action="<?php echo base_url()?>Account/login" method="post">-->
                                                        <?php 
                                                            $attributes = array('id' => 'form_sesion');
                                                            echo form_open('Account/login',$attributes); ?>
                                                        <input type="hidden" name="lang" value="0" />
                                                        <input type="hidden" name="actcontrol" value="start" />
                                                        <input type="hidden" name="pgNr" value="0">
                                                        <input type="hidden" name="errorDest" value="loginBoxErrors">
                                                        <ul class="form">
                                                            <li>
                                                                <label>E-Mail:</label>
                                                                <input id="loginEmail" type="text" name="user" value="" class="textbox" autocomplete="header username" required>
                                                            </li>
                                                            <li>
                                                                <label>Contraseña:</label>
                                                                <input id="loginPasword" type="password" name="password" class="textbox passwordbox" value="" autocomplete="header password" required>
                                                            </li>
                                                            <li class="seperatororange"></li>
                                                            <div>
                                                                <a style="text-align: center;color:red;font-weight: 700; line-height: 20px;padding: 8px 0 7px 0;" href="olvide-mi-contrasena/index.html" title="Olvidé mi contraseña">Olvidé mi contraseña</a>
                                                            </div>
                                                            <li class="formSubmit">
                                                                <a class="submitButton largeButton" style="margin-right:-3px;" id="inicarsesion">Iniciar sesión</a>
                                                            </li>
                                                            <li class="formSubmit">
                                                                <a class="normalButton largeButton oxwidget_headerlogin_register_button" href="crear-cuenta/index.html" style="margin-right:-3px;">Registrarse</a>
                                                            </li>
                                                        </ul>
                                                    <!--</form>-->
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>



                            </div>
                            <?php if($this->session->userdata('logeado')==true){ ?>
                           <!-- 
                            <div id="emheaderlogin" class="div_menu" >
                                <div id="oxwidget_headerlogin" class="menus_has">
                                    <div class="oxwidget_headerlogin_header">
                                        <div class="oxwidget_headerlogin_title1" title="<?php echo $this->session->userdata('emp');?>"><a>Mi Cuenta</a></div>
                                    </div>
                                    <div class="oxwidget_headerlogin_popup">
                                        <div class="oxwidget_headerlogin_helper "></div>
                                        <div class="header_popup">
                                            <ul style="padding: 0px;">
                                                <li><a href="#" class="h_emp">¡Hola! <b><?php echo $this->session->userdata('emp');?></b></a></li>
                                                <li><a href="<?php echo base_url();?>Mi_perfil">Mi Perfil <img class="img_per" alt="img_per" src="<?php echo base_url().'public/img/miperfil.svg';?>"></a></li>
                                                <li><a href="<?php echo base_url();?>Account/exit">Cerrar Sesión <img class="img_sesion" alt="img_sesion" src="<?php echo base_url().'public/img/salir_session.svg';?>"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        -->
                            <?php }else{ ?>
                                <!--
                            <div id="emheaderlogin" class="div_menu">
                                <div id="oxwidget_headerlogin" class="">
                                    <div class="oxwidget_headerlogin_header">
                                        <div class="oxwidget_headerlogin_title1 clas_a">Tu sesión</div>
                                    </div>
                                    <div class="oxwidget_headerlogin_popup">
                                        <div class="oxwidget_headerlogin_helper "></div>
                                        <div class="oxwidget_headerlogin_loginform">
                                            <div class="info_login_error"></div>
                                                <?php 
                                                    $attributes = array('id' => 'form_sesion');
                                                    echo form_open('Account/login',$attributes); ?>
                                                <input type="hidden" name="lang" value="0" />
                                                <input type="hidden" name="actcontrol" value="start" />
                                                <input type="hidden" name="pgNr" value="0">
                                                <input type="hidden" name="errorDest" value="loginBoxErrors">
                                                <ul class="form">
                                                    <li>
                                                        <label>E-Mail:</label>
                                                        <input id="loginEmail" type="text" name="user" value="" class="textbox" autocomplete="header username" required>
                                                    </li>
                                                    <li>
                                                        <label>Contraseña:</label>
                                                        <input id="loginPasword" type="password" name="password" class="textbox passwordbox" value="" autocomplete="header password" required>
                                                    </li>
                                                    <li class="seperatororange"></li>
                                                    <div>
                                                        <a style="text-align: center;color:red;font-weight: 700; line-height: 20px;padding: 8px 0 7px 0;" href="olvide-mi-contrasena/index.html" title="Olvidé mi contraseña">Olvidé mi contraseña</a>
                                                    </div>
                                                    <li class="formSubmit">
                                                        <a class="submitButton largeButton" style="margin-right:-3px;" id="inicarsesion">Iniciar sesión</a>
                                                    </li>
                                                    <li class="formSubmit">
                                                        <a class="normalButton largeButton oxwidget_headerlogin_register_button" href="crear-cuenta/index.html" style="margin-right:-3px;">Registrarse</a>
                                                    </li>
                                                </ul>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        -->
                            <?php } ?>

                            <?php if($this->session->userdata('logeado')==true){ ?>
                                <!--
                            <div class="div_menu" >
                                <div id="oxwidget_headercompare" class="menus_has has">
                                    <div class="oxwidget_headercompare_header">
                                        <div class="oxwidget_headercompare_header_wait"></div>
                                        <div class="oxwidget_headercompare_title1" style="width: 103px;"><a href="<?php echo base_url() ?>Inicio/contacto">Outlet</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="div_menu" >
                                <div id="oxwidget_headercompare" class="menus_has has">
                                    <div class="oxwidget_headercompare_header">
                                        <div class="oxwidget_headercompare_header_wait"></div>
                                        <div class="oxwidget_headercompare_title1" style="width: 103px;"><a href="<?php echo base_url() ?>Inicio/contacto">Consumibles</a></div>
                                    </div>
                                </div>
                            </div>
                        -->
                            <?php } ?>
                            <!--
                            <div class="div_menu"  >
                                <div id="oxwidget_headercompare" class="menus_has has">
                                    <div class="oxwidget_headercompare_header">
                                        <div class="oxwidget_headercompare_header_wait"></div>
                                        <div class="oxwidget_headercompare_title1" style="width: 108px;"><a href="<?php echo base_url() ?>Inicio/contacto">Digitalización</a></div>
                                    </div>
                                </div>
                            </div>
                            <!--
                            <div class="div_menu"  >
                                <div id="oxwidget_headercompare" class="menus_has has">
                                    <div class="oxwidget_headercompare_header">
                                        <div class="oxwidget_headercompare_header_wait"></div>
                                        <div class="oxwidget_headercompare_title1" style="width: 130px;"><a href="<?php echo base_url() ?>Inicio/rentaequipos">Arrendamiento</a></div>
                                    </div>
                                </div>
                            </div>
                            <!--
                            <div class="div_menu"  >
                                <div id="oxwidget_headerhelp" class="menus_has has">
                                    <div class="oxwidget_headerhelp_header">
                                        <div class="oxwidget_headerhelp_title1 clas_a" style="width: 94px;">Categorías</div>
                                    </div>
                                    <div class="oxwidget_headerhelp_popup">
                                        <div class="oxwidget_headerhelp_helper"></div>
                                        <ul>
                                            <li><a href="Quienes-somos/index.html">Venta de equipos</a></li>
                                            <li><a href="Quienes-somos/index.html">Venta de refacciones</a></li>
                                            <li><a href="Contacto/index.html">Venta de consumibles</a></li>
                                            <li><a href="Contacto/index.html">Venta de accesorios</a></li>
                                            <li><a href="Preguntas-frequentes/index.html">Renta de equipos</a></li>
                                            <li><a href="Modos-de-Pago/index.html">Servicio técnico</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--
                            <div class="div_menu"  >
                                <div id="oxwidget_headernotice" class="menus_has has">
                                    <div class="oxwidget_headernotice_header">
                                        <div class="oxwidget_headernotice_header_wait"></div>
                                        <div class="oxwidget_headernotice_title1" style="width: 99px;">
                                            <a href="<?php echo base_url() ?>Inicio/quienes_somos">Kyocera</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            -->

                        </div>
                        <?php
                            $result_car=$this->ModeloConsultas->listado_carrito();
                            //var_dump($result_car);
                            if(count($result_car)>0){
                                $count_pro=count($result_car);
                                $class_hass='has';
                            }else{
                                $count_pro=0;
                                $class_hass='';
                            }
                        ?>
                        <div id="emheaderminibasket" class=" ">
                            <div id="oxwidget_headerminibasket" class="emsmoothhover  <?php echo $class_hass;?>">
                                <a class="oxwidget_headerminibasket_header" href="carrito-de-compras/index.html">
                                    <span class="count"><?php echo $count_pro;?></span>
                                    <span class="icon"></span>
                                    <span class="total totalgeneraltotal">$0.00</span>
                                </a>
                                <?php if($count_pro>0){
                                    $totalgeneral=0;
                                    $totalcostoenvio=99;
                                ?>
                                <div class="oxwidget_headerminibasket_popup">
                                    <div class="oxwidget_headerminibasket_popup_connect "></div>
                                    <div class="oxwidget_headerminibasket_popup_header">
                                        <div class="oxwidget_headerminibasket_popup_title">Tienes <span class="bigtext"><?php echo $count_pro;?> artículos</span> en tu carrito</div>
                                        <a href="<?php echo base_url();?>Account/clearprocard" style="text-decoration: none;color: red;">Borrar todos</a>
                                    </div>
                                    <div class="row">
                                        <?php
                                            foreach ($result_car as $fila) {
                                                $prorow=$fila['id'];
                                                $url_image=base_url().'public/img/producto-sin-imagen.png';
                                                $Cantidad=$fila['cant'];
                                                if($fila['typeitem']==1){
                                                    $idcode=intval($fila['iditem']);
                                                    $url="https://altaproductividadapr.com/index.php/restserver/productosinfoid/".$idcode;
                                                    $equiposrow=$this->ModeloRest->consultaapiget($url);
                                                    foreach ($equiposrow as $item) {
                                                        $noparte=$item['noparte'];
                                                        $costo_venta=round(($item['costo_renta']*1.16),2);
                                                        $title_equipo=$item['modelo'].' '.$item['especificaciones'];
                                                        $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                        if($item['foto']!=''){
                                                            $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['foto'];
                                                        }
                                                        ?>
                                        
                                                        <div class="col-md-12">
                                                            <div class="iconremove" onclick="deletepro(<?php echo $prorow;?>)">X<i class="fa fa-trash fa-fw"></i></div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div style="width: 95px;height: 85px; float: left; background: url(<?php echo $url_image;?>);background-size: 100%;background-repeat: no-repeat;">
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div><?php echo $title_equipo;?></div>
                                                            <div style="color: grey;"><?php echo $noparte;?></div>
                                                            <div style="width:50%; float:left; "><?php echo $Cantidad;?> Piezas</div>
                                                            <div style="width:50%; float:left; ">C/U $ <?php echo number_format($costo_venta,2,".",",");?></div>
                                                        </div>
                                                            <?php
                                                    }
                                                }
                                                if($fila['typeitem']==2){
                                                    $idcode=intval($fila['iditem']);
                                                    //===========================================================================
                                                    $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                    $arraycols=array();
                                                    $arraycols[]=array('name'=>'id','value'=>$idcode);
                                                    $array=array('tabla'=>'catalogo_accesorios','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                    $equipos=$this->ModeloRest->consultaapipost($url,$array);
                                                    //=================================================================
                                                    //==============================================================
                                                    $arraycols=array();
                                                    $arraycols[]=array('name'=>'idaccesorio','value'=>$idcode);
                                                    $array=array('tabla'=>'catalogo_accesorios_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                    $equiposimg=$this->ModeloRest->consultaapipost($url,$array);
                                                    $equiposimg=$equiposimg;
                                                    foreach ($equiposimg as $item) {
                                                        $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['imagen'];
                                                    }
                                                    //==============================================================
                                                    foreach ($equipos as $item) {
                                                        $noparte=$item['no_parte'];
                                                        $costo_venta=round($item['costo']*1.16,2);
                                                        $title_equipo=$item['nombre'];
                                                        $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                
                                                    ?>
                                                    
                                                    <div class="col-md-12">
                                                        <div class="iconremove" onclick="deletepro(<?php echo $prorow;?>)">X<i class="fa fa-trash fa-fw"></i></div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div style="width: 95px;height: 85px; float: left; background: url(<?php echo $url_image;?>);background-size: 100%;background-repeat: no-repeat;">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div ><?php echo $title_equipo;?></div>
                                                        <div style="color: grey;"><?php echo $noparte;?></div>
                                                        <div style="width:50%; float:left; "><?php echo $Cantidad;?> Piezas</div>
                                                        <div style="width:50%; float:left; ">C/U $ <?php echo number_format($costo_venta,2,".",",");?></div>
                                                    </div>
                                                    <?php
                                                    }
                                                }
                                                if($fila['typeitem']==3){
                                                    $idcode=intval($fila['iditem']);
                                                    //===========================================================================
                                                        $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'id','value'=>$idcode);
                                                        $array=array('tabla'=>'refacciones','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $equipos=$this->ModeloRest->consultaapipost($url,$array);
                                                    //=================================================================
                                                    //==============================================================
                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'idrefaccion','value'=>$idcode);
                                                        $array=array('tabla'=>'refacciones_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $equiposimg=$this->ModeloRest->consultaapipost($url,$array);
                                                        $equiposimg=$equiposimg;
                                                        foreach ($equiposimg as $item) {
                                                            $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['imagen'];
                                                        }
                                                    //==============================================================
                                                        foreach ($equipos as $item) {
                                                            $noparte=$item['codigo'];
                                                        
                                                            //================================
                                                            //===========================================================================
                                                                $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                                $arraycols=array();
                                                                $arraycols[]=array('name'=>'refacciones_id','value'=>$idcode);
                                                                $array=array('tabla'=>'refacciones_costos','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                                $costo_lis_precio=$this->ModeloRest->consultaapipost($url,$array);
                                                                foreach ($costo_lis_precio as $itemp) {
                                                                    $costo_venta=round($itemp['general']*1.16,2);
                                                                }
                                                            //=================================================================
                                                            //================================
                                                                $title_equipo=$item['nombre'];
                                                                $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                                ?>
                                                        
                                                                <div class="col-md-12">
                                                                    <div class="iconremove" onclick="deletepro(<?php echo $prorow;?>)">X<i class="fa fa-trash fa-fw"></i></div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div style="width: 95px;height: 85px; float: left; background: url(<?php echo $url_image;?>);background-size: 100%;background-repeat: no-repeat;">
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div ><?php echo $title_equipo;?></div>
                                                                    <div style="color: grey;"><?php echo $noparte;?></div>
                                                                    <div style="width:50%; float:left; "><?php echo $Cantidad;?> Piezas</div>
                                                                    <div style="width:50%; float:left; ">C/U $ <?php echo number_format($costo_venta,2,".",",");?></div>
                                                                </div>
                                                            <?php
                                                        }
                                                
                                                }
                                                if($fila['typeitem']==4){
                                                    $idcode=intval($fila['iditem']);
                                                    //===========================================================================
                                                        $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'id','value'=>$idcode);
                                                        $array=array('tabla'=>'consumibles','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $equipos=$this->ModeloRest->consultaapipost($url,$array);
                                                    //=================================================================
                                                    //==============================================================
                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'idconsumible','value'=>$idcode);
                                                        $array=array('tabla'=>'consumibles_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $equiposimg=$this->ModeloRest->consultaapipost($url,$array);
                                                        $equiposimg=$equiposimg;
                                                        foreach ($equiposimg as $item) {
                                                            $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['imagen'];
                                                        }
                                                    //==============================================================
                                                        foreach ($equipos as $item) {
                                                            $noparte=$item['parte'];
                                                        
                                                            //================================
                                                            //===========================================================================
                                                                $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                                $arraycols=array();
                                                                $arraycols[]=array('name'=>'consumibles_id','value'=>$idcode);
                                                                $array=array('tabla'=>'consumibles_costos','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                                $costo_lis_precio=$this->ModeloRest->consultaapipost($url,$array);
                                                                foreach ($costo_lis_precio as $itemp) {
                                                                    $costo_venta=round($itemp['general']*1.16,2);
                                                                }
                                                            //=================================================================
                                                            //================================
                                                                $title_equipo=$item['modelo'];
                                                                $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                                ?>
                                                        
                                                                <div class="col-md-12">
                                                                    <div class="iconremove" onclick="deletepro(<?php echo $prorow;?>)">X<i class="fa fa-trash fa-fw"></i></div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div style="width: 95px;height: 85px; float: left; background: url(<?php echo $url_image;?>);background-size: 100%;background-repeat: no-repeat;">
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div ><?php echo $title_equipo;?></div>
                                                                    <div style="color: grey;"><?php echo $noparte;?></div>
                                                                    <div style="width:50%; float:left; "><?php echo $Cantidad;?> Piezas</div>
                                                                    <div style="width:50%; float:left; ">C/U $ <?php echo number_format($costo_venta,2,".",",");?></div>
                                                                </div>
                                                            <?php
                                                        }
                                                
                                                }
                                            }



                                            
                                            //var_dump($_SESSION['pro']);
                                            foreach ($_SESSION['pro'] as $fila){
                                                $url_image=base_url().'public/img/producto-sin-imagen.png';
                                                $Cantidad=$_SESSION['can'][$prorow];
                                                
                                                if($fila['typeitem']==1){
                                                    $idcode=intval($fila['iditem']);
                                                    $url="https://altaproductividadapr.com/index.php/restserver/productosinfoid/".$idcode;
                                                    $equiposrow=$this->ModeloRest->consultaapiget($url);
                                                    foreach ($equiposrow as $item) {
                                                        $noparte=$item['noparte'];
                                                        $costo_venta=round(($item['costo_renta']*1.16),2);
                                                        $title_equipo=$item['modelo'].' '.$item['especificaciones'];
                                                        $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                        if($item['foto']!=''){
                                                            $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['foto'];
                                                        }
                                                        ?>
                                        
                                                        <div class="col-md-12">
                                                            <div class="iconremove" onclick="deletepro(<?php echo $prorow;?>)">X<i class="fa fa-trash fa-fw"></i></div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div style="width: 95px;height: 85px; float: left; background: url(<?php echo $url_image;?>);background-size: 100%;background-repeat: no-repeat;">
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div><?php echo $title_equipo;?></div>
                                                            <div style="color: grey;"><?php echo $noparte;?></div>
                                                            <div style="width:50%; float:left; "><?php echo $Cantidad;?> Piezas</div>
                                                            <div style="width:50%; float:left; ">C/U $ <?php echo number_format($costo_venta,2,".",",");?></div>
                                                        </div>
                                                            <?php
                                                        $prorow++;
                                                    }
                                                }
                                                if($fila['typeitem']==2){
                                                    $idcode=intval($fila['iditem']);
                                                    //===========================================================================
                                                    $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                    $arraycols=array();
                                                    $arraycols[]=array('name'=>'id','value'=>$idcode);
                                                    $array=array('tabla'=>'catalogo_accesorios','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                    $equipos=$this->ModeloRest->consultaapipost($url,$array);
                                                    //=================================================================
                                                    //==============================================================
                                                    $arraycols=array();
                                                    $arraycols[]=array('name'=>'idaccesorio','value'=>$idcode);
                                                    $array=array('tabla'=>'catalogo_accesorios_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                    $equiposimg=$this->ModeloRest->consultaapipost($url,$array);
                                                    $equiposimg=$equiposimg;
                                                    foreach ($equiposimg as $item) {
                                                        $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['imagen'];
                                                    }
                                                    //==============================================================
                                                    foreach ($equipos as $item) {
                                                        $noparte=$item['no_parte'];
                                                        $costo_venta=round($item['costo']*1.16,2);
                                                        $title_equipo=$item['nombre'];
                                                        $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                
                                                    ?>
                                                    
                                                    <div class="col-md-12">
                                                        <div class="iconremove" onclick="deletepro(<?php echo $prorow;?>)">X<i class="fa fa-trash fa-fw"></i></div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div style="width: 95px;height: 85px; float: left; background: url(<?php echo $url_image;?>);background-size: 100%;background-repeat: no-repeat;">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div ><?php echo $title_equipo;?></div>
                                                        <div style="color: grey;"><?php echo $noparte;?></div>
                                                        <div style="width:50%; float:left; "><?php echo $Cantidad;?> Piezas</div>
                                                        <div style="width:50%; float:left; ">C/U $ <?php echo number_format($costo_venta,2,".",",");?></div>
                                                    </div>
                                                    <?php
                                                    $prorow++;
                                                    }
                                                }
                                                if($fila['typeitem']==3){
                                                    $idcode=intval($fila['iditem']);
                                                    //===========================================================================
                                                        $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'id','value'=>$idcode);
                                                        $array=array('tabla'=>'refacciones','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $equipos=$this->ModeloRest->consultaapipost($url,$array);
                                                    //=================================================================
                                                    //==============================================================
                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'idrefaccion','value'=>$idcode);
                                                        $array=array('tabla'=>'refacciones_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $equiposimg=$this->ModeloRest->consultaapipost($url,$array);
                                                        $equiposimg=$equiposimg;
                                                        foreach ($equiposimg as $item) {
                                                            $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['imagen'];
                                                        }
                                                    //==============================================================
                                                        foreach ($equipos as $item) {
                                                            $noparte=$item['codigo'];
                                                        
                                                            //================================
                                                            //===========================================================================
                                                                $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                                $arraycols=array();
                                                                $arraycols[]=array('name'=>'refacciones_id','value'=>$idcode);
                                                                $array=array('tabla'=>'refacciones_costos','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                                $costo_lis_precio=$this->ModeloRest->consultaapipost($url,$array);
                                                                foreach ($costo_lis_precio as $itemp) {
                                                                    $costo_venta=round($itemp['general']*1.16,2);
                                                                }
                                                            //=================================================================
                                                            //================================
                                                                $title_equipo=$item['nombre'];
                                                                $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                                ?>
                                                        
                                                                <div class="col-md-12">
                                                                    <div class="iconremove" onclick="deletepro(<?php echo $prorow;?>)">X<i class="fa fa-trash fa-fw"></i></div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div style="width: 95px;height: 85px; float: left; background: url(<?php echo $url_image;?>);background-size: 100%;background-repeat: no-repeat;">
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div ><?php echo $title_equipo;?></div>
                                                                    <div style="color: grey;"><?php echo $noparte;?></div>
                                                                    <div style="width:50%; float:left; "><?php echo $Cantidad;?> Piezas</div>
                                                                    <div style="width:50%; float:left; ">C/U $ <?php echo number_format($costo_venta,2,".",",");?></div>
                                                                </div>
                                                            <?php
                                                            $prorow++;
                                                        }
                                                
                                                }
                                                if($fila['typeitem']==4){
                                                    $idcode=intval($fila['iditem']);
                                                    //===========================================================================
                                                        $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'id','value'=>$idcode);
                                                        $array=array('tabla'=>'consumibles','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $equipos=$this->ModeloRest->consultaapipost($url,$array);
                                                    //=================================================================
                                                    //==============================================================
                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'idconsumible','value'=>$idcode);
                                                        $array=array('tabla'=>'consumibles_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $equiposimg=$this->ModeloRest->consultaapipost($url,$array);
                                                        $equiposimg=$equiposimg;
                                                        foreach ($equiposimg as $item) {
                                                            $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['imagen'];
                                                        }
                                                    //==============================================================
                                                        foreach ($equipos as $item) {
                                                            $noparte=$item['parte'];
                                                        
                                                            //================================
                                                            //===========================================================================
                                                                $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                                $arraycols=array();
                                                                $arraycols[]=array('name'=>'consumibles_id','value'=>$idcode);
                                                                $array=array('tabla'=>'consumibles_costos','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                                $costo_lis_precio=$this->ModeloRest->consultaapipost($url,$array);
                                                                foreach ($costo_lis_precio as $itemp) {
                                                                    $costo_venta=round($itemp['general']*1.16,2);
                                                                }
                                                            //=================================================================
                                                            //================================
                                                                $title_equipo=$item['modelo'];
                                                                $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                                ?>
                                                        
                                                                <div class="col-md-12">
                                                                    <div class="iconremove" onclick="deletepro(<?php echo $prorow;?>)">X<i class="fa fa-trash fa-fw"></i></div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div style="width: 95px;height: 85px; float: left; background: url(<?php echo $url_image;?>);background-size: 100%;background-repeat: no-repeat;">
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div ><?php echo $title_equipo;?></div>
                                                                    <div style="color: grey;"><?php echo $noparte;?></div>
                                                                    <div style="width:50%; float:left; "><?php echo $Cantidad;?> Piezas</div>
                                                                    <div style="width:50%; float:left; ">C/U $ <?php echo number_format($costo_venta,2,".",",");?></div>
                                                                </div>
                                                            <?php
                                                            $prorow++;
                                                        }
                                                
                                                }
                                            }

                                        $totalgeneraltotal=$totalgeneral+$totalcostoenvio;
                                        ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            
                                        </div>
                                        <div class="col-md-4 card_montos">Subtotal:</div>
                                        <div class="col-md-4 card_montos"><b>$ <?php echo number_format($totalgeneral,2,".",",");?></b></div>
                                        <div class="col-md-4">
                                            
                                        </div>
                                        <div class="col-md-4 card_montos">Costo envio: </div>
                                        <div class="col-md-4 card_montos"><b>$ <?php echo number_format($totalcostoenvio,2,".",",");?></b></div>
                                        <div class="col-md-4">
                                            
                                        </div>
                                        <div class="col-md-4 card_montos">Total: </div>
                                        <div class="col-md-4 card_montos"><b>$ <?php echo number_format($totalgeneraltotal,2,".",",");?></b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a type="button" class="btn btn-secondary" href="<?php echo base_url();?>Carrito">Ir a caja</a>
                                        </div>
                                        
                                    </div>
                                </div>
                                <script type="text/javascript">
                                $(document).ready(function($) {
                                $('.totalgeneraltotal').html('$ <?php echo number_format($totalgeneraltotal,2,".",",");?>');
                                });
                                </script>
                                <?php } ?>
                            </div>
                        </div>
                        <!----------------->
                        <!----------------->
                        <div class="cp-header-overlay"></div>
                    </div>
                </div>
            </div>
            
            <div id="page" class=" start ">
                <div class="pageborder clear">
                    <div id="content">
                        <div class="start_topbox clear">
                            <div id="sidebar" class="start_topbox_left ">
                                <div class="cp-main-menu-place-holder">
                                    <div data-cp-vue-bind="main-menu" data-cp-options='{"css": ["desktop", "home"], "closeDelay" : 1000}'>
                                        <div class="cp-main-menu desktop home">
                                            <div class="main-menu bshadow">
                                                <div class="root-categories">
                                                    <div style="text-align: center;padding-top: 20px;"><img src="<?php echo base_url();?>public/img/logo2.svg" alt="logo kyocera" class="img_log_1" style="height: 45px;"></div>
                                                    <!--<div class="dropdown dropend">
                                                        
                                                        <a class="dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">Equipos</a>
                                                        <ul class="dropdown-menu">
                                                            <!--<li><a class="dropdown-item" href="<?php echo base_url();?>Equipos">Equipos</a></li>--
                                                            <li><a class="dropdown-item" href="<?php echo base_url();?>Equipos/Monocromaticos">Impresora monocromática</a></li>
                                                            <li><a class="dropdown-item" href="<?php echo base_url();?>Equipos/Color">Impresora de color</a></li>
                                                            <li><a class="dropdown-item" href="<?php echo base_url();?>Equipos/Multifuncional_monocromatico">Multifuncional monocromático</a></li>
                                                            <li><a class="dropdown-item" href="<?php echo base_url();?>Equipos/Multifuncional_de_color">Multifuncional de color</a></li>
                                                            <li><a class="dropdown-item" href="<?php echo base_url();?>Equipos/Multifuncional_Hibrido_de_color">Multifuncional Híbrido de color</a></li>
                                                        </ul>
                                                    </div>-->
                                                    <?php 
                                                        $html_m='';
                                                        $html_m.='<a class="menu bshadow" href="#" class="" onclick="eject_view(1)">Solicitud de servicio</a>';
                                                        $html_m.='<a class="menu bshadow" href="'.base_url().'Kits" class="">Kits</a>';
                                                        $html_m.='<a class="menu bshadow" href="'.base_url().'Accesorios" class="">Accesorios</a>';
                                                        $html_m.='<a class="menu bshadow" href="'.base_url().'Consumibles" class="">Consumibles</a>';

                                                        $html_m.='<div class="dropdown menu-contact">';
                                                          $html_m.='<button class="dropbtn bshadow">¡Contáctenos! <i class="fa-solid fa-caret-down"></i></button>';
                                                          $html_m.='<div class="dropdown-content">';
                                                            $html_m.='<!--<a href="#">Link 1</a>';
                                                            $html_m.='<a href="#">Link 2</a>-->';
                                                            $html_m.='<!--<a href="#">Link 3</a>-->';
                                                            $html_m.='<div class="info_contact">';
                                                                $html_m.='<p>Llámenos al:</p>';
                                                                $html_m.='<a class="a_tel" href="tel:+522292667121">Pue 229 266 7121</a>';
                                                                $html_m.='<a class="a_tel" href="tel:+522292667121">Tla 229 266 7121</a>';
                                                                $html_m.='<a class="a_tel" href="tel:+522292667121">MTY 229 266 7121</a>';
                                                                $html_m.='<p class="p_grey">L-V 8:00 AM - 6:00 PM</p>';
                                                                $html_m.='<p>O escribanos a:</p>';
                                                                $html_m.='<a class="a_mail" href="mailto:helpdesk@kyoceraap.com">helpdesk@kyoceraap.com</a>';
                                                            $html_m.='</div>';
                                                          $html_m.='</div>';
                                                        $html_m.='</div>';
                                                        echo $html_m;
                                                    ?>
                                                    
                                                    <!---------------------->
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <style type="text/css">
                            .slider {
                            background-color: transparent;
                            margin-left: 24px;
                            margin-right: 24px;
                            }
                            </style>
                            <!----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->