<!DOCTYPE HTML>
<html lang="es" >
    <!-- Mirrored from www.kyoceraap.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 Mar 2023 23:28:22 GMT -->
    <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <head>
        <link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome/css/all.css" type="text/css" >
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/generated/cms-pagesebeb.css?1593624763" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/cyberpuertaV5/src/css/emslider20497.css?1678389110">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/cyberpuertaV5/src/css/oxwidget_headerlogin0497.css?1678389110">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/cyberpuertaV5/src/css/oxwidget_headernotice0497.css?1678389110">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/cyberpuertaV5/src/css/oxwidget_headercompare0497.css?1678389110">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/cyberpuertaV5/src/css/oxwidget_headerhelp0497.css?1678389110">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/cyberpuertaV5/src/css/oxwidget_headerminibasket0497.css?1678389110">
        <link data-cp-orig="cdn.Swiper.css" rel="stylesheet" type="text/css" href="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css">
        <link data-cp-orig="cp.app.legacy.css" rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/dist/cp.app.legacy.2a1306440cf66f0e3efa.g.css">
        <link data-cp-orig="cp.app.foundation.css" rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/dist/cp.app.foundation.e29b346a69794f0187d9.g.css">
        <link data-cp-orig="cp.app.css" rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/dist/cp.app.3551495a5a8c567d440e.g.css">
        <link data-cp-orig="vendors~cp.app.css" rel="stylesheet" type="text/css" href="<?php echo base_url();?>out/dist/vendors_cp.app.f79cffac28db61b5a671.g.css">
        <link data-cp-orig="cdn.material-icons.css" rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link data-cp-orig="cdn.open-sans.css" rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;display=swap">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/style_theme.css?1593624763" />
        <link href="<?php echo base_url(); ?>public/plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
        <input type="hidden" id="base_url" value="<?php echo base_url()?>">
        
        

        <style type="text/css">
    #page {padding-bottom: 75px;    }
    #footer{position: fixed;height: 104px;box-shadow: inset 0 3px 3px #e7e7e7;z-index: 1;}
    
    body {font-family: 'Helvetica', 'Arial', sans-serif;    }
    
    .basketButton2 {position: absolute;    right: 0;}
    .card_montos{background-color: #e7e7e7;        text-align: right;    }
    .iconremove{cursor: pointer;        background: linear-gradient(to bottom, #ffffff 0%,#e8e8e8 100%);        width: 15px !important;        height: 15px !important;    }
    .embuttonamount_border {position: relative;}
.embuttonamount_amount{margin-left: 23px;}
.embuttonamount_border .embuttonamount_amount {left: 20px;    width: 25px;    border-left: 0;    border-right: 0;}
.embuttonamount_border .embuttonamount_amount input {    border: 0;    margin: 0;    padding: 0;    width: 25px;    height: 26px;    line-height: 30px;    color: black;    font-weight: normal;    font-size: 14px;    text-align: center;}
    .basketitems .embuttonamount_border {margin-left: 25px;    margin-bottom: 15px;    height: 30px;    width: 99px;}
    .embuttonamount_border .embuttonamount_minus {    left: 0px;}
.embuttonamount_border .embuttonamount_plus {left: 45px;}
.embuttonamount_border .embuttonamount_minus, .embuttonamount_border .embuttonamount_plus, .embuttonamount_border .embuttonamount_refresh, .embuttonamount_border .embuttonamount_refreshcancel, .embuttonamount_border .embuttonamount_amount {
    position: absolute;left: 0px;width: 23px; height: 30px; line-height: 30px; vertical-align: middle; border: 0; font-weight: bold; cursor: pointer; z-index: 1; text-align: center; background-color: white; border: 1px solid #adadad;box-sizing: border-box; -moz-box-sizing: border-box; display: block; color: #adadad; font-size: 14px; padding: 0; border-radius: 0;}
    .submitButton{
        color: white;
    }
    .submitButton:hover{
        color:red;
    }
</style>
        
        
        <!--sse-->
        <!-- Customizing Knowledge Graph: page-specific structured data -->
        <script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"Organization","name":"kyoceraap, S.A. de C.V.","url":"https:\/\/www.kyoceraap.com\/","logo":"https:\/\/www.kyoceraap.com\/out\/cyberpuertaV5\/img\/logo2.png","contactPoint":[{"@type":"ContactPoint","telephone":"+52-33 4737 1360","contactType":"customer service"},{"@type":"ContactPoint","telephone":"+52-55 4737 1360","contactType":"customer service"},{"@type":"ContactPoint","telephone":"+52-81 4737 1360","contactType":"customer service"}],"sameAs":["http:\/\/www.facebook.com\/kyoceraap","http:\/\/twitter.com\/kyoceraap"]}</script>
        <script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","url":"https:\/\/www.kyoceraap.com\/","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.kyoceraap.com\/index.php?cl=search&searchparam={search_term_string}","query-input":"required name=search_term_string"}}</script>
        <!--/sse-->
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <title>kyoceraap.com: Impresoras</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge"><script type="text/javascript">(window.NREUM||(NREUM={})).init={ajax:{deny_list:["bam.nr-data.net"]}};(window.NREUM||(NREUM={})).loader_config={licenseKey:"4879248977",applicationID:"66344412"};;(()=>{var e,t,r={9071:(e,t,r)=>{"use strict";r.d(t,{I:()=>n});var n=0,i=navigator.userAgent.match(/Firefox[\/\s](\d+\.\d+)/);i&&(n=+i[1])},6562:(e,t,r)=>{"use strict";r.d(t,{P_:()=>p,Mt:()=>v,C5:()=>d,DL:()=>y,OP:()=>N,lF:()=>B,Yu:()=>A,Dg:()=>h,CX:()=>f,GE:()=>w,sU:()=>M});var n={};r.r(n),r.d(n,{agent:()=>x,match:()=>j,version:()=>O});var i=r(6797),o=r(909),a=r(8610);class s{constructor(e,t){try{if(!e||"object"!=typeof e)return(0,a.Z)("New setting a Configurable requires an object as input");if(!t||"object"!=typeof t)return(0,a.Z)("Setting a Configurable requires a model to set its initial properties");Object.assign(this,t),Object.entries(e).forEach((e=>{let[t,r]=e;const n=(0,o.q)(t);n.length&&r&&"object"==typeof r&&n.forEach((e=>{e in r&&((0,a.Z)('"'.concat(e,'" is a protected attribute and can not be changed in feature ').concat(t,".  It will have no effect.")),delete r[e])})),this[t]=r}))}catch(e){(0,a.Z)("An error occured while setting a Configurable",e)}}}const c={beacon:i.ce.beacon,errorBeacon:i.ce.errorBeacon,licenseKey:void 0,applicationID:void 0,sa:void 0,queueTime:void 0,applicationTime:void 0,ttGuid:void 0,user:void 0,account:void 0,product:void 0,extra:void 0,jsAttributes:{},userAttributes:void 0,atts:void 0,transactionName:void 0,tNamePlain:void 0},u={};function d(e){if(!e)throw new Error("All info objects require an agent identifier!");if(!u[e])throw new Error("Info for ".concat(e," was never set"));return u[e]}function f(e,t){if(!e)throw new Error("All info objects require an agent identifier!");u[e]=new s(t,c),(0,i.Qy)(e,u[e],"info")}const l={allow_bfcache:!0,privacy:{cookies_enabled:!0},ajax:{deny_list:void 0,enabled:!0,harvestTimeSeconds:10},distributed_tracing:{enabled:void 0,exclude_newrelic_header:void 0,cors_use_newrelic_header:void 0,cors_use_tracecontext_headers:void 0,allowed_origins:void 0},ssl:void 0,obfuscate:void 0,jserrors:{enabled:!0,harvestTimeSeconds:10},metrics:{enabled:!0,harvestTimeSeconds:10},page_action:{enabled:!0,harvestTimeSeconds:30},page_view_event:{enabled:!0},page_view_timing:{enabled:!0,harvestTimeSeconds:30},session_trace:{enabled:!0,harvestTimeSeconds:10},spa:{enabled:!0,harvestTimeSeconds:10}},g={};function p(e){if(!e)throw new Error("All configuration objects require an agent identifier!");if(!g[e])throw new Error("Configuration for ".concat(e," was never set"));return g[e]}function h(e,t){if(!e)throw new Error("All configuration objects require an agent identifier!");g[e]=new s(t,l),(0,i.Qy)(e,g[e],"config")}function v(e,t){if(!e)throw new Error("All configuration objects require an agent identifier!");var r=p(e);if(r){for(var n=t.split("."),i=0;i<n.length-1;i++)if("object"!=typeof(r=r[n[i]]))return;r=r[n[n.length-1]]}return r}const m={accountID:void 0,trustKey:void 0,agentID:void 0,licenseKey:void 0,applicationID:void 0,xpid:void 0},b={};function y(e){if(!e)throw new Error("All loader-config objects require an agent identifier!");if(!b[e])throw new Error("LoaderConfig for ".concat(e," was never set"));return b[e]}function w(e,t){if(!e)throw new Error("All loader-config objects require an agent identifier!");b[e]=new s(t,m),(0,i.Qy)(e,b[e],"loader_config")}const A=(0,i.mF)().o;var E=r(2053),x=null,O=null;if(navigator.userAgent){var T=navigator.userAgent,S=T.match(/Version\/(\S+)\s+Safari/);S&&-1===T.indexOf("Chrome")&&-1===T.indexOf("Chromium")&&(x="Safari",O=S[1])}function j(e,t){if(!x)return!1;if(e!==x)return!1;if(!t)return!0;if(!O)return!1;for(var r=O.split("."),n=t.split("."),i=0;i<n.length;i++)if(n[i]!==r[i])return!1;return!0}var D=r(5526),_=r(2374);const k="NRBA_SESSION_ID";function P(){if(!_.il)return null;try{let e;return null===(e=window.sessionStorage.getItem(k))&&(e=(0,D.ky)(16),window.sessionStorage.setItem(k,e)),e}catch(e){return null}}var I=r(8226);const C=e=>({customTransaction:void 0,disabled:!1,isolatedBacklog:!1,loaderType:void 0,maxBytes:3e4,offset:(0,E.yf)(),onerror:void 0,origin:""+_._A.location,ptid:void 0,releaseIds:{},sessionId:1==v(e,"privacy.cookies_enabled")?P():null,xhrWrappable:"function"==typeof _._A.XMLHttpRequest?.prototype?.addEventListener,userAgent:n,version:I.q}),R={};function N(e){if(!e)throw new Error("All runtime objects require an agent identifier!");if(!R[e])throw new Error("Runtime for ".concat(e," was never set"));return R[e]}function M(e,t){if(!e)throw new Error("All runtime objects require an agent identifier!");R[e]=new s(t,C(e)),(0,i.Qy)(e,R[e],"runtime")}function B(e){return function(e){try{const t=d(e);return!!t.licenseKey&&!!t.errorBeacon&&!!t.applicationID}catch(e){return!1}}(e)}},8226:(e,t,r)=>{"use strict";r.d(t,{q:()=>n});const n="1226.PROD"},9557:(e,t,r)=>{"use strict";r.d(t,{w:()=>o});var n=r(8610);const i={agentIdentifier:""};class o{constructor(e){try{if("object"!=typeof e)return(0,n.Z)("shared context requires an object as input");this.sharedContext={},Object.assign(this.sharedContext,i),Object.entries(e).forEach((e=>{let[t,r]=e;Object.keys(i).includes(t)&&(this.sharedContext[t]=r)}))}catch(e){(0,n.Z)("An error occured while setting SharedContext",e)}}}},4329:(e,t,r)=>{"use strict";r.d(t,{L:()=>d,R:()=>c});var n=r(3752),i=r(7022),o=r(4045),a=r(2325);const s={};function c(e,t){const r={staged:!1,priority:a.p[t]||0};u(e),s[e].get(t)||s[e].set(t,r)}function u(e){e&&(s[e]||(s[e]=new Map))}function d(){let e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"",t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:"feature";if(u(e),!e||!s[e].get(t))return a(t);s[e].get(t).staged=!0;const r=Array.from(s[e]);function a(t){const r=e?n.ee.get(e):n.ee,a=o.X.handlers;if(r.backlog&&a){var s=r.backlog[t],c=a[t];if(c){for(var u=0;s&&u<s.length;++u)f(s[u],c);(0,i.D)(c,(function(e,t){(0,i.D)(t,(function(t,r){r[0].on(e,r[1])}))}))}delete a[t],r.backlog[t]=null,r.emit("drain-"+t,[])}}r.every((e=>{let[t,r]=e;return r.staged}))&&(r.sort(((e,t)=>e[1].priority-t[1].priority)),r.forEach((e=>{let[t]=e;a(t)})))}function f(e,t){var r=e[1];(0,i.D)(t[r],(function(t,r){var n=e[0];if(r[0]===n){var i=r[1],o=e[3],a=e[2];i.apply(o,a)}}))}},3752:(e,t,r)=>{"use strict";r.d(t,{c:()=>f,ee:()=>u});var n=r(6797),i=r(3916),o=r(7022),a=r(6562),s="nr@context";let c=(0,n.fP)();var u;function d(){}function f(e){return(0,i.X)(e,s,l)}function l(){return new d}function g(){(u.backlog.api||u.backlog.feature)&&(u.aborted=!0,u.backlog={})}c.ee?u=c.ee:(u=function e(t,r){var n={},c={},f={},p=!1;try{p=16===r.length&&(0,a.OP)(r).isolatedBacklog}catch(e){}var h={on:b,addEventListener:b,removeEventListener:y,emit:m,get:A,listeners:w,context:v,buffer:E,abort:g,aborted:!1,isBuffering:x,debugId:r,backlog:p?{}:t&&"object"==typeof t.backlog?t.backlog:{}};return h;function v(e){return e&&e instanceof d?e:e?(0,i.X)(e,s,l):l()}function m(e,r,n,i,o){if(!1!==o&&(o=!0),!u.aborted||i){t&&o&&t.emit(e,r,n);for(var a=v(n),s=w(e),d=s.length,f=0;f<d;f++)s[f].apply(a,r);var l=O()[c[e]];return l&&l.push([h,e,r,a]),a}}function b(e,t){n[e]=w(e).concat(t)}function y(e,t){var r=n[e];if(r)for(var i=0;i<r.length;i++)r[i]===t&&r.splice(i,1)}function w(e){return n[e]||[]}function A(t){return f[t]=f[t]||e(h,t)}function E(e,t){var r=O();h.aborted||(0,o.D)(e,(function(e,n){t=t||"feature",c[n]=t,t in r||(r[t]=[])}))}function x(e){return!!O()[c[e]]}function O(){return h.backlog}}(void 0,"globalEE"),c.ee=u)},9252:(e,t,r)=>{"use strict";r.d(t,{E:()=>n,p:()=>i});var n=r(3752).ee.get("handle");function i(e,t,r,i,o){o?(o.buffer([e],i),o.emit(e,t,r)):(n.buffer([e],i),n.emit(e,t,r))}},4045:(e,t,r)=>{"use strict";r.d(t,{X:()=>o});var n=r(9252);o.on=a;var i=o.handlers={};function o(e,t,r,o){a(o||n.E,i,e,t,r)}function a(e,t,r,i,o){o||(o="feature"),e||(e=n.E);var a=t[o]=t[o]||{};(a[r]=a[r]||[]).push([e,i])}},8544:(e,t,r)=>{"use strict";r.d(t,{bP:()=>s,iz:()=>c,m$:()=>a});var n=r(2374);let i=!1,o=!1;try{const e={get passive(){return i=!0,!1},get signal(){return o=!0,!1}};n._A.addEventListener("test",null,e),n._A.removeEventListener("test",null,e)}catch(e){}function a(e,t){return i||o?{capture:!!e,passive:i,signal:t}:!!e}function s(e,t){let r=arguments.length>2&&void 0!==arguments[2]&&arguments[2];window.addEventListener(e,t,a(r))}function c(e,t){let r=arguments.length>2&&void 0!==arguments[2]&&arguments[2];document.addEventListener(e,t,a(r))}},5526:(e,t,r)=>{"use strict";r.d(t,{Ht:()=>a,M:()=>o,Rl:()=>i,ky:()=>s});var n=r(2374);function i(){var e=null,t=0,r=n._A?.crypto||n._A?.msCrypto;function i(){return e?15&e[t++]:16*Math.random()|0}r&&r.getRandomValues&&(e=r.getRandomValues(new Uint8Array(31)));for(var o,a="xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx",s="",c=0;c<a.length;c++)s+="x"===(o=a[c])?i().toString(16):"y"===o?(o=3&i()|8).toString(16):o;return s}function o(){return s(16)}function a(){return s(32)}function s(e){var t=null,r=0,n=self.crypto||self.msCrypto;n&&n.getRandomValues&&Uint8Array&&(t=n.getRandomValues(new Uint8Array(31)));for(var i=[],o=0;o<e;o++)i.push(a().toString(16));return i.join("");function a(){return t?15&t[r++]:16*Math.random()|0}}},2053:(e,t,r)=>{"use strict";r.d(t,{nb:()=>c,os:()=>u,yf:()=>s,zO:()=>a});var n=r(7145),i=(new Date).getTime(),o=i;function a(){return n.G&&performance.now?Math.round(performance.now()):(i=Math.max((new Date).getTime(),i))-o}function s(){return i}function c(e){o=e}function u(){return o}},7145:(e,t,r)=>{"use strict";r.d(t,{G:()=>n});const n=void 0!==r(2374)._A?.performance?.timing?.navigationStart},6625:(e,t,r)=>{"use strict";r.d(t,{s:()=>c,v:()=>u});var n=r(8283),i=r(9071),o=r(2053),a=r(7145),s=r(2374);let c=!0;function u(e){var t=function(){if(i.I&&i.I<9)return;if(a.G)return c=!1,s._A?.performance?.timing?.navigationStart}();t&&((0,n.B)(e,"starttime",t),(0,o.nb)(t))}},8283:(e,t,r)=>{"use strict";r.d(t,{B:()=>o,L:()=>a});var n=r(2053),i={};function o(e,t,r){void 0===r&&(r=(0,n.zO)()+(0,n.os)()),i[e]=i[e]||{},i[e][t]=r}function a(e,t,r,n){const o=e.sharedContext.agentIdentifier;var a=i[o]?.[r],s=i[o]?.[n];void 0!==a&&void 0!==s&&e.store("measures",t,{value:s-a})}},9548:(e,t,r)=>{"use strict";r.d(t,{T:()=>i});var n=r(2374);const i={isFileProtocol:function(){let e=Boolean("file:"===(0,n.lW)()?.location?.protocol);e&&(i.supportabilityMetricSent=!0);return e},supportabilityMetricSent:!1}},8610:(e,t,r)=>{"use strict";function n(e,t){console&&console.warn&&"function"==typeof console.warn&&(console.warn("New Relic: ".concat(e)),t&&console.warn(t))}r.d(t,{Z:()=>n})},3916:(e,t,r)=>{"use strict";r.d(t,{X:()=>i});var n=Object.prototype.hasOwnProperty;function i(e,t,r){if(n.call(e,t))return e[t];var i=r();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:i,writable:!0,enumerable:!1}),i}catch(e){}return e[t]=i,i}},2374:(e,t,r)=>{"use strict";r.d(t,{_A:()=>o,il:()=>n,lW:()=>a,v6:()=>i});const n=Boolean("undefined"!=typeof window&&window.document),i=Boolean("undefined"!=typeof WorkerGlobalScope&&self.navigator instanceof WorkerNavigator);let o=(()=>{if(n)return window;if(i){if("undefined"!=typeof globalThis&&globalThis instanceof WorkerGlobalScope)return globalThis;if(self instanceof WorkerGlobalScope)return self}throw new Error('New Relic browser agent shutting down due to error: Unable to locate global scope. This is possibly due to code redefining browser global variables like "self" and "window".')})();function a(){return o}},7022:(e,t,r)=>{"use strict";r.d(t,{D:()=>i});var n=Object.prototype.hasOwnProperty;function i(e,t){var r=[],i="",o=0;for(i in e)n.call(e,i)&&(r[o]=t(i,e[i]),o+=1);return r}},9226:(e,t,r)=>{"use strict";r.d(t,{$c:()=>u,Ng:()=>d,RR:()=>c});var n=r(6562),i=r(9557),o=r(9548),a=r(8610),s={regex:/^file:\/\/(.*)/,replacement:"file://OBFUSCATED"};class c extends i.w{constructor(e){super(e)}shouldObfuscate(){return u(this.sharedContext.agentIdentifier).length>0}obfuscateString(e){if(!e||"string"!=typeof e)return e;for(var t=u(this.sharedContext.agentIdentifier),r=e,n=0;n<t.length;n++){var i=t[n].regex,o=t[n].replacement||"*";r=r.replace(i,o)}return r}}function u(e){var t=[],r=(0,n.Mt)(e,"obfuscate")||[];return t=t.concat(r),o.T.isFileProtocol()&&t.push(s),t}function d(e){for(var t=!1,r=!1,n=0;n<e.length;n++){"regex"in e[n]?"string"!=typeof e[n].regex&&e[n].regex.constructor!==RegExp&&((0,a.Z)('An obfuscation replacement rule contains a "regex" value with an invalid type (must be a string or RegExp)'),r=!0):((0,a.Z)('An obfuscation replacement rule was detected missing a "regex" value.'),r=!0);var i=e[n].replacement;i&&"string"!=typeof i&&((0,a.Z)('An obfuscation replacement rule contains a "replacement" value with an invalid type (must be a string)'),t=!0)}return!t&&!r}},2650:(e,t,r)=>{"use strict";r.d(t,{K:()=>a,b:()=>o});var n=r(8544);function i(){return"undefined"==typeof document||"complete"===document.readyState}function o(e,t){if(i())return e();(0,n.bP)("load",e,t)}function a(e){if(i())return e();(0,n.iz)("DOMContentLoaded",e)}},6797:(e,t,r)=>{"use strict";r.d(t,{EZ:()=>u,Qy:()=>c,ce:()=>o,fP:()=>a,gG:()=>d,mF:()=>s});var n=r(2053),i=r(2374);const o={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net"};function a(){return i._A.NREUM||(i._A.NREUM={}),void 0===i._A.newrelic&&(i._A.newrelic=i._A.NREUM),i._A.NREUM}function s(){let e=a();return e.o||(e.o={ST:i._A.setTimeout,SI:i._A.setImmediate,CT:i._A.clearTimeout,XHR:i._A.XMLHttpRequest,REQ:i._A.Request,EV:i._A.Event,PR:i._A.Promise,MO:i._A.MutationObserver,FETCH:i._A.fetch}),e}function c(e,t,r){let i=a();const o=i.initializedAgents||{},s=o[e]||{};return Object.keys(s).length||(s.initializedAt={ms:(0,n.zO)(),date:new Date}),i.initializedAgents={...o,[e]:{...s,[r]:t}},i}function u(e,t){a()[e]=t}function d(){return function(){let e=a();const t=e.info||{};e.info={beacon:o.beacon,errorBeacon:o.errorBeacon,...t}}(),function(){let e=a();const t=e.init||{};e.init={...t}}(),s(),function(){let e=a();const t=e.loader_config||{};e.loader_config={...t}}(),a()}},6998:(e,t,r)=>{"use strict";r.d(t,{N:()=>i,e:()=>o});var n=r(8544);function i(e){let t=arguments.length>1&&void 0!==arguments[1]&&arguments[1];return void(0,n.iz)("visibilitychange",(function(){if(t){if("hidden"!=document.visibilityState)return;e()}e(document.visibilityState)}))}function o(){return"hidden"===document.visibilityState?-1:1/0}},6034:(e,t,r)=>{"use strict";r.d(t,{t:()=>n});const n=r(2325).D.metrics},600:(e,t,r)=>{"use strict";r.r(t),r.d(t,{Instrument:()=>O,constants:()=>T});var n=r(9252),i=r(4045),o=r(6114),a=r(2374),s={REACT:"React",ANGULAR:"Angular",ANGULARJS:"AngularJS",BACKBONE:"Backbone",EMBER:"Ember",VUE:"Vue",METEOR:"Meteor",ZEPTO:"Zepto",JQUERY:"Jquery"};function c(){if(!a.il)return[];var e=[];try{(function(){try{if(window.React||window.ReactDOM||window.ReactRedux)return!0;if(document.querySelector("[data-reactroot], [data-reactid]"))return!0;for(var e=document.querySelectorAll("body > div"),t=0;t<e.length;t++)if(Object.keys(e[t]).indexOf("_reactRootContainer")>=0)return!0;return!1}catch(e){return!1}})()&&e.push(s.REACT),function(){try{return!!window.angular||(!!document.querySelector(".ng-binding, [ng-app], [data-ng-app], [ng-controller], [data-ng-controller], [ng-repeat], [data-ng-repeat]")||!!document.querySelector('script[src*="angular.js"], script[src*="angular.min.js"]'))}catch(e){return!1}}()&&e.push(s.ANGULARJS),function(){try{return!!(window.hasOwnProperty("ng")&&window.ng.hasOwnProperty("coreTokens")&&window.ng.coreTokens.hasOwnProperty("NgZone"))||!!document.querySelectorAll("[ng-version]").length}catch(e){return!1}}()&&e.push(s.ANGULAR),window.Backbone&&e.push(s.BACKBONE),window.Ember&&e.push(s.EMBER),window.Vue&&e.push(s.VUE),window.Meteor&&e.push(s.METEOR),window.Zepto&&e.push(s.ZEPTO),window.jQuery&&e.push(s.JQUERY)}catch(e){}return e}var u=r(9548),d=r(9226),f=r(8226),l=r(2650),g=r(8544),p=r(6562),h=r(8610);const v={dedicated:Boolean(a._A?.Worker),shared:Boolean(a._A?.SharedWorker),service:Boolean(a._A?.navigator?.serviceWorker)};let m,b,y;var w=r(6034);var A,E,x;class O extends o.S{constructor(e,t){var r;let n=!(arguments.length>2&&void 0!==arguments[2])||arguments[2];super(e,t,w.t,n),r=this,this.singleChecks(),this.eachSessionChecks(),(0,i.X)("record-supportability",(function(){return r.recordSupportability(...arguments)}),this.featureName,this.ee),(0,i.X)("record-custom",(function(){return r.recordCustom(...arguments)}),this.featureName,this.ee),this.importAggregator()}recordSupportability(e,t){var r=["sm",e,{name:e},t];return(0,n.p)("storeMetric",r,null,this.featureName,this.ee),r}recordCustom(e,t){var r=["cm",e,{name:e},t];return(0,n.p)("storeEventMetrics",r,null,this.featureName,this.ee),r}singleChecks(){this.recordSupportability("Generic/Version/".concat(f.q,"/Detected"));const{loaderType:e}=(0,p.OP)(this.agentIdentifier);e&&this.recordSupportability("Generic/LoaderType/".concat(e,"/Detected")),a.il&&(0,l.K)((()=>{c().forEach((e=>{this.recordSupportability("Framework/"+e+"/Detected")}))})),u.T.isFileProtocol()&&(this.recordSupportability("Generic/FileProtocol/Detected"),u.T.supportabilityMetricSent=!0);const t=(0,d.$c)(this.agentIdentifier);t.length>0&&this.recordSupportability("Generic/Obfuscate/Detected"),t.length>0&&!(0,d.Ng)(t)&&this.recordSupportability("Generic/Obfuscate/Invalid"),function(e){if(!m){if(v.dedicated){m=Worker;try{a._A.Worker=r(m,"Dedicated")}catch(e){o(e,"Dedicated")}if(v.shared){b=SharedWorker;try{a._A.SharedWorker=r(b,"Shared")}catch(e){o(e,"Shared")}}else n("Shared");if(v.service){y=navigator.serviceWorker.register;try{a._A.navigator.serviceWorker.register=(t=y,function(){for(var e=arguments.length,r=new Array(e),n=0;n<e;n++)r[n]=arguments[n];return i("Service",r[1]?.type),t.apply(navigator.serviceWorker,r)})}catch(e){o(e,"Service")}}else n("Service");var t;return}n("All")}function r(e,t){return"undefined"==typeof Proxy?e:new Proxy(e,{construct:(e,r)=>(i(t,r[1]?.type),new e(...r))})}function n(t){a.v6||e("Workers/".concat(t,"/Unavailable"))}function i(t,r){e("Workers/".concat(t,"module"===r?"/Module":"/Classic"))}function o(t,r){e("Workers/".concat(r,"/SM/Unsupported")),(0,h.Z)("NR Agent: Unable to capture ".concat(r," workers."),t)}}(this.recordSupportability.bind(this))}eachSessionChecks(){a.il&&(0,g.bP)("pageshow",(e=>{e.persisted&&this.recordSupportability("Generic/BFCache/PageRestored")}))}}A=O,E="featureName",x=w.t,(E=function(e){var t=function(e,t){if("object"!=typeof e||null===e)return e;var r=e[Symbol.toPrimitive];if(void 0!==r){var n=r.call(e,t||"default");if("object"!=typeof n)return n;throw new TypeError("@@toPrimitive must return a primitive value.")}return("string"===t?String:Number)(e)}(e,"string");return"symbol"==typeof t?t:String(t)}(E))in A?Object.defineProperty(A,E,{value:x,enumerable:!0,configurable:!0,writable:!0}):A[E]=x;var T={SUPPORTABILITY_METRIC:"sm",CUSTOM_METRIC:"cm"}},2484:(e,t,r)=>{"use strict";r.d(t,{t:()=>n});const n=r(2325).D.pageViewEvent},5637:(e,t,r)=>{"use strict";r.r(t),r.d(t,{Instrument:()=>h});var n,i,o,a=r(9252),s=r(2053),c=r(8283),u=r(6625),d=r(6114),f=r(2650),l=r(2484),g=r(2325),p=r(2374);class h extends d.S{constructor(e,t){let r=!(arguments.length>2&&void 0!==arguments[2])||arguments[2];super(e,t,l.t,r),p.il&&((0,u.v)(e),(0,c.B)(e,"firstbyte",(0,s.yf)()),(0,f.K)((()=>this.measureDomContentLoaded())),(0,f.b)((()=>this.measureWindowLoaded()),!0),this.importAggregator())}measureWindowLoaded(){var e=(0,s.zO)();(0,c.B)(this.agentIdentifier,"onload",e+(0,s.os)()),(0,a.p)("timing",["load",e],void 0,g.D.pageViewTiming,this.ee)}measureDomContentLoaded(){(0,c.B)(this.agentIdentifier,"domContent",(0,s.zO)()+(0,s.os)())}}n=h,i="featureName",o=l.t,(i=function(e){var t=function(e,t){if("object"!=typeof e||null===e)return e;var r=e[Symbol.toPrimitive];if(void 0!==r){var n=r.call(e,t||"default");if("object"!=typeof n)return n;throw new TypeError("@@toPrimitive must return a primitive value.")}return("string"===t?String:Number)(e)}(e,"string");return"symbol"==typeof t?t:String(t)}(i))in n?Object.defineProperty(n,i,{value:o,enumerable:!0,configurable:!0,writable:!0}):n[i]=o},6382:(e,t,r)=>{"use strict";r.d(t,{t:()=>n});const n=r(2325).D.pageViewTiming},7817:(e,t,r)=>{"use strict";r.r(t),r.d(t,{Instrument:()=>h});var n,i,o,a=r(9252),s=r(6998),c=r(8544),u=r(2053),d=r(6562),f=r(6114),l=r(6382),g=r(2325),p=r(2374);class h extends f.S{constructor(e,t){var r;let n=!(arguments.length>2&&void 0!==arguments[2])||arguments[2];if(super(e,t,l.t,n),r=this,p.il){if(this.pageHiddenTime=(0,s.e)(),this.performanceObserver,this.lcpPerformanceObserver,this.clsPerformanceObserver,this.fiRecorded=!1,"PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){this.performanceObserver=new PerformanceObserver((function(){return r.perfObserver(...arguments)}));try{this.performanceObserver.observe({entryTypes:["paint"]})}catch(e){}this.lcpPerformanceObserver=new PerformanceObserver((function(){return r.lcpObserver(...arguments)}));try{this.lcpPerformanceObserver.observe({entryTypes:["largest-contentful-paint"]})}catch(e){}this.clsPerformanceObserver=new PerformanceObserver((function(){return r.clsObserver(...arguments)}));try{this.clsPerformanceObserver.observe({type:"layout-shift",buffered:!0})}catch(e){}}this.fiRecorded=!1;["click","keydown","mousedown","pointerdown","touchstart"].forEach((e=>{(0,c.iz)(e,(function(){return r.captureInteraction(...arguments)}))})),(0,s.N)((()=>{this.pageHiddenTime=(0,u.zO)(),(0,a.p)("docHidden",[this.pageHiddenTime],void 0,g.D.pageViewTiming,this.ee)}),!0),(0,c.bP)("pagehide",(()=>(0,a.p)("winPagehide",[(0,u.zO)()],void 0,g.D.pageViewTiming,this.ee))),this.importAggregator()}}perfObserver(e,t){e.getEntries().forEach((e=>{"first-paint"===e.name?(0,a.p)("timing",["fp",Math.floor(e.startTime)],void 0,g.D.pageViewTiming,this.ee):"first-contentful-paint"===e.name&&(0,a.p)("timing",["fcp",Math.floor(e.startTime)],void 0,g.D.pageViewTiming,this.ee)}))}lcpObserver(e,t){var r=e.getEntries();if(r.length>0){var n=r[r.length-1];if(this.pageHiddenTime<n.startTime)return;var i=[n],o=this.addConnectionAttributes({});o&&i.push(o),(0,a.p)("lcp",i,void 0,g.D.pageViewTiming,this.ee)}}clsObserver(e){e.getEntries().forEach((e=>{e.hadRecentInput||(0,a.p)("cls",[e],void 0,g.D.pageViewTiming,this.ee)}))}addConnectionAttributes(e){var t=navigator.connection||navigator.mozConnection||navigator.webkitConnection;if(t)return t.type&&(e["net-type"]=t.type),t.effectiveType&&(e["net-etype"]=t.effectiveType),t.rtt&&(e["net-rtt"]=t.rtt),t.downlink&&(e["net-dlink"]=t.downlink),e}captureInteraction(e){if(e instanceof d.Yu.EV&&!this.fiRecorded){var t=Math.round(e.timeStamp),r={type:e.type};this.addConnectionAttributes(r),t<=(0,u.zO)()?r.fid=(0,u.zO)()-t:t>(0,u.os)()&&t<=Date.now()?(t-=(0,u.os)(),r.fid=(0,u.zO)()-t):t=(0,u.zO)(),this.fiRecorded=!0,(0,a.p)("timing",["fi",t,r],void 0,g.D.pageViewTiming,this.ee)}}}n=h,i="featureName",o=l.t,(i=function(e){var t=function(e,t){if("object"!=typeof e||null===e)return e;var r=e[Symbol.toPrimitive];if(void 0!==r){var n=r.call(e,t||"default");if("object"!=typeof n)return n;throw new TypeError("@@toPrimitive must return a primitive value.")}return("string"===t?String:Number)(e)}(e,"string");return"symbol"==typeof t?t:String(t)}(i))in n?Object.defineProperty(n,i,{value:o,enumerable:!0,configurable:!0,writable:!0}):n[i]=o},1509:(e,t,r)=>{"use strict";r.d(t,{W:()=>s});var n=r(6562),i=r(3752),o=r(2384),a=r(6797);class s{constructor(e,t,r){this.agentIdentifier=e,this.aggregator=t,this.ee=i.ee.get(e,(0,n.OP)(this.agentIdentifier).isolatedBacklog),this.featureName=r,this.blocked=!1,this.checkConfiguration()}checkConfiguration(){if(!(0,n.lF)(this.agentIdentifier)){let e={...(0,a.gG)().info?.jsAttributes};try{e={...e,...(0,n.C5)(this.agentIdentifier)?.jsAttributes}}catch(e){}(0,o.j)(this.agentIdentifier,{...(0,a.gG)(),info:{...(0,a.gG)().info,jsAttributes:e}})}}}},6114:(e,t,r)=>{"use strict";r.d(t,{S:()=>c});var n=r(4329),i=r(1509),o=r(2650),a=r(2374),s=r(8610);class c extends i.W{constructor(e,t,r){let i=!(arguments.length>3&&void 0!==arguments[3])||arguments[3];super(e,t,r),this.hasAggregator=!1,this.auto=i,this.abortHandler,i&&(0,n.R)(e,r)}importAggregator(){if(this.hasAggregator||!this.auto)return;this.hasAggregator=!0;const e=async()=>{try{const{lazyLoader:e}=await r.e(729).then(r.bind(r,8110)),{Aggregate:t}=await e(this.featureName,"aggregate");new t(this.agentIdentifier,this.aggregator)}catch(e){(0,s.Z)("Downloading ".concat(this.featureName," failed...")),this.abortHandler?.()}};a.v6?e():(0,o.b)((()=>e()),!0)}}},2384:(e,t,r)=>{"use strict";r.d(t,{j:()=>y});var n=r(8683),i=r.n(n),o=r(2325),a=r(6562),s=r(9252),c=r(7022),u=r(3752),d=r(2053),f=r(4329),l=r(2650),g=r(2374),p=r(8610);function h(e){["setErrorHandler","finished","addToTrace","inlineHit","addRelease","addPageAction","setCurrentRouteName","setPageViewName","setCustomAttribute","interaction","noticeError"].forEach((t=>{e[t]=function(){for(var r=arguments.length,n=new Array(r),i=0;i<r;i++)n[i]=arguments[i];return function(t){for(var r=arguments.length,n=new Array(r>1?r-1:0),i=1;i<r;i++)n[i-1]=arguments[i];Object.values(e.initializedAgents).forEach((e=>{e.exposed&&e.api[t]&&e.api[t](...n)}))}(t,...n)}}))}var v=r(6797);const m={stn:[o.D.sessionTrace],err:[o.D.jserrors,o.D.metrics],ins:[o.D.pageAction],spa:[o.D.spa]};const b={};function y(e){let t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},n=arguments.length>2?arguments[2]:void 0,y=arguments.length>3?arguments[3]:void 0,{init:w,info:A,loader_config:E,runtime:x={loaderType:n},exposed:O=!0}=t;const T=(0,v.gG)();let S={};return A||(w=T.init,A=T.info,E=T.loader_config,S=T),g.v6&&(A.jsAttributes={...A.jsAttributes,isWorker:!0}),(0,a.CX)(e,A),(0,a.Dg)(e,w||{}),(0,a.GE)(e,E||{}),(0,a.sU)(e,x),function(e,t,n){n||(0,f.R)(e,"api"),h(t);var v=u.ee.get(e),m=v.get("tracer"),b="api-",y=b+"ixn-";function w(){}(0,c.D)(["setErrorHandler","finished","addToTrace","inlineHit","addRelease"],(function(e,r){t[r]=E(b,r,!0,"api")})),t.addPageAction=E(b,"addPageAction",!0,o.D.pageAction),t.setCurrentRouteName=E(b,"routeName",!0,o.D.spa),t.setPageViewName=function(t,r){if("string"==typeof t)return"/"!==t.charAt(0)&&(t="/"+t),(0,a.OP)(e).customTransaction=(r||"http://custom.transaction")+t,E(b,"setPageViewName",!0,"api")()},t.setCustomAttribute=function(t,r){const n=(0,a.C5)(e);return(0,a.CX)(e,{...n,jsAttributes:{...n.jsAttributes,[t]:r}}),E(b,"setCustomAttribute",!0,"api")()},t.interaction=function(){return(new w).get()};var A=w.prototype={createTracer:function(e,t){var r={},n=this,i="function"==typeof t;return(0,s.p)(y+"tracer",[(0,d.zO)(),e,r],n,o.D.spa,v),function(){if(m.emit((i?"":"no-")+"fn-start",[(0,d.zO)(),n,i],r),i)try{return t.apply(this,arguments)}catch(e){throw m.emit("fn-err",[arguments,this,"string"==typeof e?new Error(e):e],r),e}finally{m.emit("fn-end",[(0,d.zO)()],r)}}}};function E(e,t,r,n){return function(){return(0,s.p)("record-supportability",["API/"+t+"/called"],void 0,o.D.metrics,v),(0,s.p)(e+t,[(0,d.zO)()].concat(i()(arguments)),r?null:this,n,v),r?void 0:this}}function x(){r.e(439).then(r.bind(r,5692)).then((t=>{let{setAPI:r}=t;r(e),(0,f.L)(e,"api")})).catch((()=>(0,p.Z)("Downloading runtime APIs failed...")))}(0,c.D)("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),(function(e,t){A[t]=E(y,t,void 0,o.D.spa)})),t.noticeError=function(e,t){"string"==typeof e&&(e=new Error(e)),(0,s.p)("record-supportability",["API/noticeError/called"],void 0,o.D.metrics,v),(0,s.p)("err",[e,(0,d.zO)(),!1,t],void 0,o.D.jserrors,v)},g.v6?x():(0,l.b)((()=>x()),!0)}(e,S,y),(0,v.Qy)(e,T,"api"),(0,v.Qy)(e,O,"exposed"),g.v6||((0,v.EZ)("activatedFeatures",b),(0,v.EZ)("setToken",(t=>function(e,t){var r=u.ee.get(t);e&&"object"==typeof e&&((0,c.D)(e,(function(e,t){if(!t)return(m[e]||[]).forEach((t=>{(0,s.p)("block-"+e,[],void 0,t,r)}));b[e]||((0,s.p)("feat-"+e,[],void 0,m[e],r),b[e]=!0)})),(0,f.L)(t,o.D.pageViewEvent))}(t,e)))),S}},909:(e,t,r)=>{"use strict";r.d(t,{Z:()=>i,q:()=>o});var n=r(2325);function i(e){switch(e){case n.D.ajax:return[n.D.jserrors];case n.D.sessionTrace:return[n.D.ajax,n.D.pageViewEvent];case n.D.pageViewTiming:return[n.D.pageViewEvent];default:return[]}}function o(e){return e===n.D.jserrors?[]:["auto"]}},2325:(e,t,r)=>{"use strict";r.d(t,{D:()=>n,p:()=>i});const n={ajax:"ajax",jserrors:"jserrors",metrics:"metrics",pageAction:"page_action",pageViewEvent:"page_view_event",pageViewTiming:"page_view_timing",sessionTrace:"session_trace",spa:"spa"},i={[n.pageViewEvent]:1,[n.pageViewTiming]:2,[n.metrics]:3,[n.jserrors]:4,[n.ajax]:5,[n.sessionTrace]:6,[n.pageAction]:7,[n.spa]:8}},8683:e=>{e.exports=function(e,t,r){t||(t=0),void 0===r&&(r=e?e.length:0);for(var n=-1,i=r-t||0,o=Array(i<0?0:i);++n<i;)o[n]=e[t+n];return o}}},n={};function i(e){var t=n[e];if(void 0!==t)return t.exports;var o=n[e]={exports:{}};return r[e](o,o.exports,i),o.exports}i.m=r,i.n=e=>{var t=e&&e.__esModule?()=>e.default:()=>e;return i.d(t,{a:t}),t},i.d=(e,t)=>{for(var r in t)i.o(t,r)&&!i.o(e,r)&&Object.defineProperty(e,r,{enumerable:!0,get:t[r]})},i.f={},i.e=e=>Promise.all(Object.keys(i.f).reduce(((t,r)=>(i.f[r](e,t),t)),[])),i.u=e=>(({78:"page_action-aggregate",147:"metrics-aggregate",193:"session_trace-aggregate",225:"ajax-instrument",317:"jserrors-aggregate",348:"page_view_timing-aggregate",439:"async-api",578:"jserrors-instrument",729:"lazy-loader",757:"session_trace-instrument",786:"page_view_event-aggregate",873:"spa-aggregate",876:"spa-instrument",898:"ajax-aggregate",908:"page_action-instrument"}[e]||e)+"."+{78:"92657d87",118:"34a59fa6",147:"7dcaee1b",193:"401d5d17",225:"25fe7245",264:"bcaf68fc",317:"d078b949",348:"6b3fec7f",439:"6bb277af",578:"c8f23c89",729:"48127245",757:"98d3280d",786:"29613e65",873:"58d1fc78",876:"364406a9",898:"178bdaa3",908:"64360627"}[e]+"-1226.min.js"),i.o=(e,t)=>Object.prototype.hasOwnProperty.call(e,t),e={},t="NRBA:",i.l=(r,n,o,a)=>{if(e[r])e[r].push(n);else{var s,c;if(void 0!==o)for(var u=document.getElementsByTagName("script"),d=0;d<u.length;d++){var f=u[d];if(f.getAttribute("src")==r||f.getAttribute("data-webpack")==t+o){s=f;break}}s||(c=!0,(s=document.createElement("script")).charset="utf-8",s.timeout=120,i.nc&&s.setAttribute("nonce",i.nc),s.setAttribute("data-webpack",t+o),s.src=r),e[r]=[n];var l=(t,n)=>{s.onerror=s.onload=null,clearTimeout(g);var i=e[r];if(delete e[r],s.parentNode&&s.parentNode.removeChild(s),i&&i.forEach((e=>e(n))),t)return t(n)},g=setTimeout(l.bind(null,void 0,{type:"timeout",target:s}),12e4);s.onerror=l.bind(null,s.onerror),s.onload=l.bind(null,s.onload),c&&document.head.appendChild(s)}},i.r=e=>{"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},i.p="https://js-agent.newrelic.com/",(()=>{var e={831:0,744:0};i.f.j=(t,r)=>{var n=i.o(e,t)?e[t]:void 0;if(0!==n)if(n)r.push(n[2]);else{var o=new Promise(((r,i)=>n=e[t]=[r,i]));r.push(n[2]=o);var a=i.p+i.u(t),s=new Error;i.l(a,(r=>{if(i.o(e,t)&&(0!==(n=e[t])&&(e[t]=void 0),n)){var o=r&&("load"===r.type?"missing":r.type),a=r&&r.target&&r.target.src;s.message="Loading chunk "+t+" failed.\n("+o+": "+a+")",s.name="ChunkLoadError",s.type=o,s.request=a,n[1](s)}}),"chunk-"+t,t)}};var t=(t,r)=>{var n,o,[a,s,c]=r,u=0;if(a.some((t=>0!==e[t]))){for(n in s)i.o(s,n)&&(i.m[n]=s[n]);if(c)c(i)}for(t&&t(r);u<a.length;u++)o=a[u],i.o(e,o)&&e[o]&&e[o][0](),e[o]=0},r=window.webpackChunkNRBA=window.webpackChunkNRBA||[];r.forEach(t.bind(null,0)),r.push=t.bind(null,r.push.bind(r))})();var o={};(()=>{"use strict";i.r(o);var e=i(2325),t=i(6562);const r=Object.values(e.D);function n(e){const n={};return r.forEach((r=>{n[r]=function(e,r){return!1!==(0,t.Mt)(r,"".concat(e,".enabled"))}(r,e)})),n}var a=i(2384),s=i(909),c=i(9557),u=i(7022);class d extends c.w{constructor(e){super(e),this.aggregatedData={}}store(e,t,r,n,i){var o=this.getBucket(e,t,r,i);return o.metrics=function(e,t){t||(t={count:0});return t.count+=1,(0,u.D)(e,(function(e,r){t[e]=f(r,t[e])})),t}(n,o.metrics),o}merge(e,t,r,n,i){var o=this.getBucket(e,t,n,i);if(o.metrics){var a=o.metrics;a.count+=r.count,(0,u.D)(r,(function(e,t){if("count"!==e){var n=a[e],i=r[e];i&&!i.c?a[e]=f(i.t,n):a[e]=function(e,t){if(!t)return e;t.c||(t=l(t.t));return t.min=Math.min(e.min,t.min),t.max=Math.max(e.max,t.max),t.t+=e.t,t.sos+=e.sos,t.c+=e.c,t}(i,a[e])}}))}else o.metrics=r}storeMetric(e,t,r,n){var i=this.getBucket(e,t,r);return i.stats=f(n,i.stats),i}getBucket(e,t,r,n){this.aggregatedData[e]||(this.aggregatedData[e]={});var i=this.aggregatedData[e][t];return i||(i=this.aggregatedData[e][t]={params:r||{}},n&&(i.custom=n)),i}get(e,t){return t?this.aggregatedData[e]&&this.aggregatedData[e][t]:this.aggregatedData[e]}take(e){for(var t={},r="",n=!1,i=0;i<e.length;i++)t[r=e[i]]=g(this.aggregatedData[r]),t[r].length&&(n=!0),delete this.aggregatedData[r];return n?t:null}}function f(e,t){return null==e?function(e){e?e.c++:e={c:1};return e}(t):t?(t.c||(t=l(t.t)),t.c+=1,t.t+=e,t.sos+=e*e,e>t.max&&(t.max=e),e<t.min&&(t.min=e),t):{t:e}}function l(e){return{t:e,min:e,max:e,sos:e*e,c:1}}function g(e){return"object"!=typeof e?[]:(0,u.D)(e,p)}function p(e,t){return t}var h=i(6797),v=i(5526),m=i(8610);var b=i(5637),y=i(7817),w=i(600);new class{constructor(t){let r=arguments.length>1&&void 0!==arguments[1]?arguments[1]:(0,v.ky)(16);this.agentIdentifier=r,this.sharedAggregator=new d({agentIdentifier:this.agentIdentifier}),this.features={},this.desiredFeatures=t.features||[],this.desiredFeatures.sort(((t,r)=>e.p[t.featureName]-e.p[r.featureName])),Object.assign(this,(0,a.j)(this.agentIdentifier,t,t.loaderType||"agent")),this.start()}get config(){return{info:(0,t.C5)(this.agentIdentifier),init:(0,t.P_)(this.agentIdentifier),loader_config:(0,t.DL)(this.agentIdentifier),runtime:(0,t.OP)(this.agentIdentifier)}}start(){const e="features";try{const t=n(this.agentIdentifier);this.desiredFeatures.forEach((e=>{if(t[e.featureName]){const r=(0,s.Z)(e.featureName);r.every((e=>t[e]))||(0,m.Z)("".concat(e.featureName," is enabled but one or more dependent features has been disabled (").concat(JSON.stringify(r),"). This may cause unintended consequences or missing data...")),this.features[e.featureName]=new e(this.agentIdentifier,this.sharedAggregator)}})),(0,h.Qy)(this.agentIdentifier,this.features,e)}catch(t){(0,m.Z)("Failed to initialize all enabled instrument classes (agent aborted) -",t);for(const e in this.features)this.features[e].abortHandler?.();const r=(0,h.fP)();return delete r.initializedAgents[this.agentIdentifier]?.api,delete r.initializedAgents[this.agentIdentifier]?.[e],delete this.sharedAggregator,delete r.ee?.get(this.agentIdentifier),!1}}}({features:[b.Instrument,y.Instrument,w.Instrument],loaderType:"lite"})})(),window.NRBA=o})();</script>
        <meta name="description" content="Venta en línea de Hardware, Computadoras, Laptops, Impresoras, Monitores y más.
        &#10003;Precios accesibles &#10003;Excelente servicio
        &#10003;40,000 productos &#10003;14 años en el mercado &#10003;500,000+ clientes">
        <link rel="canonical" href="index.html">
        <link rel="shortcut icon" href="<?php echo base_url();?>public/img/favkyo.png">
        <script type="application/json" id="cp-env-data">
        {"cl":"start","isMobile":false,"isCpx":false,"enableCpNotify":false,"shopUrl":"https:\/\/www.kyoceraap.com","enableBanners":1678341600,"bannersFindUrl":"https:\/\/us-central1-cyberpuerta-144818.cloudfunctions.net\/cp-banner-find","bannersExpires":1678383307,"bannerPlaces":["side-left","side-right","cpx-home-main","cpx-home-middle-1","cpx-home-middle-2","cpx-home-middle-3","page-top","home-main","home-top-left","home-top-middle","home-top-right","home-bottom","side-left","side-right","page-top"],"bannerContext":[],"baseApiData":"*,*,4f1f198301cd6","shopPrefix":"cp","env":"prod","restrictionGroup":"1","restrictionGroupName":"Cyberpuerta","sessionToken":"","sessionId":null,"apiVersion":"v0.1","etaCalculatorUri":"https:\/\/cp-eta-calculator-e346qjngfq-uc.a.run.app","etaCalculatorExpires":1678189896,"additional":null,"noSpa":false,"showVat":true,"rai":true,"fid":{"enabled":true,"gsi_clientId":"1044917490556-kg2simrb5fm8ml0s0uubmgeri9rrfhf1.apps.googleusercontent.com","gsi_clientId_android":"1044917490556-9kvrnb85iao90rr8ejf6l4ksd5idut33.apps.googleusercontent.com","gsi_clientId_ios":"1044917490556-nqt66sdvhcel94loopng83jvjle8g4va.apps.googleusercontent.com","apple_clientId":"mx.cyberpuerta","autoLink":false,"autoRegister":true,"canAddPasswordAgain":true,"requirePasswordToLink":false},"cpAdditionalTaxInfo":"hide"}
        </script>
        <script type="application/json" id="cp-layout-view-data">
            {"initialCategories":[
                {
                    "id":"99748977a0a9f40e2.09126772",
                    "title":"C\u00f3mputo (Hardware)",
                    "link":"https://www.kyoceraap.com/Computo-Hardware/",
                    "image":"cp-menu-icon1","parentId":"",
                    "subCategories":[],
                    "path":[],
                    "hasSubCategories":true
                },{
                "id":"07a49b1b2f27967a1.24876408",
                "title":"Impresi\u00f3n y Copiado",
                "link":"https://www.kyoceraap.com/Impresion-y-Copiado/",
                "image":"cp-menu-icon2",
                "parentId":"",
                "subCategories":[],
                "path":[],
                "hasSubCategories":true
                },{
                "id":"4fe38c01729fc",
                "title":"Computadoras",
                "link":"https://www.kyoceraap.com/Computadoras/",
                "image":"cp-menu-icon6",
                "parentId":"",
                "subCategories":[],"path":[],"hasSubCategories":true
                },{
                "id":"4fe353e55a8d4",
                "title":"Audio y Video",
                "link":"https://www.kyoceraap.com/Audio-y-Video/","image":"cp-menu-icon4","parentId":"","subCategories":[],"path":[],"hasSubCategories":true
                },{
                    "id":"5e8f92ef7a3a3",
                    "title":"Home Office",
                    "link":"https://www.kyoceraap.com/Home-Office/",
                    "image":"cp-menu-icon9",
                    "parentId":"",
                    "subCategories":[],
                    "path":[],
                    "hasSubCategories":true
                }
            ],
            "moreCategoriesUrl":"https://www.kyoceraap.com/out/generated/main-menu/6-oxrootid-1677708244.json",
            "menuUrl":"<?php echo base_url() ?>out/main-1677708244.json",
            "basket":{
                "empty":true,"articles":[],"linkNextStep":null,"numberOfItems":null,"savedItems":[],"numberOfSavedArticles":null,"linkToOffers":null,"saldoData":null,"messagesValidation":null,"errors":null,"forceOpen":null,"msiData":null,"summary":null,"paymentMethods":[],"CheckedPaymentId":""},
            "beforeCategories":[{"title":"Inicio","icon":"cp-menu-home-image","href":"https://www.kyoceraap.com/","template":""},{"title":"Mi cuenta","icon":"cp-menu-icon5","href":"","template":"cp-ko-user-menu"}],
            "afterCategories":[{"title":"Otras\u003Cbr\u003Ecategor\u00edas","icon":"cp-menu-icon7","href":"","template":"cp-ko-category-menu2"},{"title":"M\u00e1s\u003Cbr\u003Einformaci\u00f3n","icon":"cp-menu-icon3","href":"","template":"cp-ko-moreinfo-menu"}],
            "simpleHeader":false,"noHeader":false,"accountUrls":[{"url":"https://www.kyoceraap.com/mi-cuenta/","lang":"Mi cuenta"},{"url":"https://www.kyoceraap.com/historial-de-pedidos/","lang":"Historial de pedidos"},{"url":"https://www.kyoceraap.com/mis-direcciones/","lang":"Datos de env\u00edo, pago y facturaci\u00f3n"},{"url":"https://www.kyoceraap.com/mis-favoritos/","lang":"Mis favoritos"},{"url":"https://www.kyoceraap.com/mis-opiniones/","lang":"Mis opiniones"},{"url":"https://www.kyoceraap.com/mis-preguntas/","lang":"Mis preguntas"},{"url":"https://www.kyoceraap.com/mis-respuestas/","lang":"Mis respuestas"},{"url":"https://www.kyoceraap.com/mi-contrasena/","lang":"Configuraci\u00f3n de cuenta"},{"url":"https://www.kyoceraap.com/preferencias-de-correo/","lang":"Suscripciones"},{"url":"js-dynamic","lang":""},{"url":"#user-form=logout","lang":"Salir de cuenta","class":"button primary hollow expanded"}],
            "loginOptions":{"activeClass":"start","navParams":"\u003Cinput type=\"hidden\" name=\"actcontrol\" value=\"start\" /\u003E\n","action":"https://www.kyoceraap.com/index.php?","errors":[],"sizeOptions":[],"loginLink":"https://www.kyoceraap.com/web/account/login","logoutLink":"https://www.kyoceraap.com/web/account/logout","createAccountLink":"https://www.kyoceraap.com/crear-cuenta/","forgotPasswordLink":"https://www.kyoceraap.com/olvide-mi-contrasena/"},
            "cmsUrls":[],"basketUrl":"https://www.kyoceraap.com/carrito-de-compras/","homeLink":"https://www.kyoceraap.com/","actionLink":"https://www.kyoceraap.com/","searchParam":"","fidLink":null}
        </script>
        <!-- <script type="application/json" id="cp-view-data">
        {"dailyOffers":[],"brands":[{"id":"2ae6fda0ccd034cab3bee65bbfd1904d","title":"ACER","link":"https://www.kyoceraap.com/Por-Marca/ACER/","image":"https://cdn.kyoceraap.com/storage/brands/acernewlogo.jpg","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"146ae729839fab95f5876d8aeb381608","title":"EPSON","link":"https://www.kyoceraap.com/Por-Marca/EPSON/","image":"https://cdn.kyoceraap.com/storage/brands/epson.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"20e1b3688e94df3f62d35b44df5f7954","title":"SAMSUNG","link":"https://www.kyoceraap.com/Por-Marca/SAMSUNG/","image":"https://cdn.kyoceraap.com/storage/brands/samsung.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"e6fc8ce107f2bdf0955f021a391514ce","title":"HP","link":"https://www.kyoceraap.com/Por-Marca/HP/","image":"https://cdn.kyoceraap.com/storage/brands/e6fc8ce107f2bdf0955f021a391514ce_hp.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"1ec2744b3a948fae2382b1ec7bdb962b","title":"GIGABYTE","link":"https://www.kyoceraap.com/Por-Marca/GIGABYTE/","image":"https://cdn.kyoceraap.com/storage/brands/1ec2744b3a948fae2382b1ec7bdb962b_gigabyte.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"f9232ef48e81c8fa4eb9a0cafbe1f13d","title":"INTEL","link":"https://www.kyoceraap.com/Por-Marca/INTEL/","image":"https://cdn.kyoceraap.com/storage/brands/intellogo.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"67243b4384dbf4c379f1b7ea112742bd","title":"WESTERN DIGITAL","link":"https://www.kyoceraap.com/Por-Marca/WESTERN-DIGITAL/","image":"https://cdn.kyoceraap.com/storage/brands/westernDigital2022.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"1ec9de0c06c0d089bff3d3f70f237ae1","title":"XTREME PC GAMING","link":"https://www.kyoceraap.com/Por-Marca/XTREME-PC-GAMING/","image":"https://cdn.kyoceraap.com/storage/brands/5da100a24aafc_logoxtreme.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"40183069c6b7a5144ac7e7fdffb4f13b","title":"KINGSTON","link":"https://www.kyoceraap.com/Por-Marca/KINGSTON/","image":"https://cdn.kyoceraap.com/storage/brands/kingstonlogofcc32d090fseeklogo.com.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"f976fb2ecf4847ecb507697a8d02ed3d","title":"ADATA","link":"https://www.kyoceraap.com/Por-Marca/ADATA/","image":"https://cdn.kyoceraap.com/storage/brands/f976fb2ecf4847ecb507697a8d02ed3d_ADATA.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"c230b14bfa6e4bfdbf75b5432eb55d8c","title":"CORSAIR","link":"https://www.kyoceraap.com/Por-Marca/CORSAIR/","image":"https://cdn.kyoceraap.com/storage/brands/CORSAIRLogo2020_horiz_K.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"6a71ff9d77211464763f6e2edadc3b4c","title":"ZOTAC","link":"https://www.kyoceraap.com/Por-Marca/ZOTAC/","image":"https://cdn.kyoceraap.com/storage/brands/zotac.jpg","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"685903ef83f7a7c8a165f77eed160d9e","title":"LENOVO","link":"https://www.kyoceraap.com/Por-Marca/LENOVO/","image":"https://cdn.kyoceraap.com/storage/brands/685903ef83f7a7c8a165f77eed160d9e_lenovo.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"03b9d9b7b907a3cb052b661ef03e1ed1","title":"BROTHER","link":"https://www.kyoceraap.com/Por-Marca/BROTHER/","image":"https://cdn.kyoceraap.com/storage/brands/brother.jpg","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"4c462d6dd59d782386bb1cdad0060c70","title":"APPLE","link":"https://www.kyoceraap.com/Por-Marca/APPLE/","image":"https://cdn.kyoceraap.com/storage/brands/shops/apple-autorized.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"04c74ad625352f7093356e441ef69610","title":"DELL","link":"https://www.kyoceraap.com/Por-Marca/DELL/","image":"https://cdn.kyoceraap.com/storage/brands/04c74ad625352f7093356e441ef69610_logodell.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"43ee0a7ff98dae4ebf41c6bec8a4ae0e","title":"LOGITECH","link":"https://www.kyoceraap.com/Por-Marca/LOGITECH/","image":"https://cdn.kyoceraap.com/storage/brands/43ee0a7ff98dae4ebf41c6bec8a4ae0e_logitech.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"8726e36e9d6d53646390d495b4ce9dc1","title":"MSI","link":"https://www.kyoceraap.com/Por-Marca/MSI/","image":"https://cdn.kyoceraap.com/storage/brands/8726e36e9d6d53646390d495b4ce9dc1_msi.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"48af4341f745163f945fa838eeabb062","title":"AMD","link":"https://www.kyoceraap.com/Por-Marca/AMD/","image":"https://cdn.kyoceraap.com/storage/brands/48af4341f745163f945fa838eeabb062_amdlogo.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false},{"id":"17773c6a82fc40999707cd2c69f7d900","title":"XPG","link":"https://www.kyoceraap.com/Por-Marca/XPG/","image":"https://cdn.kyoceraap.com/storage/brands/xpglogo2.png","parentId":null,"subCategories":null,"path":null,"hasSubCategories":false}],"shoppingTools":[{"index1":"58c342ad4870c","cptitle":"Configurador de PC","cplink":"https://www.kyoceraap.com/configurador-de-pc/","cppicture":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/20230112_63c0181ee6f2e.png","cppicture_pdp":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/cp_63c0181ee7137.png","cpdescription":"","cpbutton_text":"Ir a configurador","f_oxmanufacturer":null},{"index1":"5ddd9e545fbd4","cptitle":"Buscador de consumibles HP","cplink":"https://www.kyoceraap.com/hp-buscador-de-consumibles/","cppicture":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/hp-buscador-hometool.png","cppicture_pdp":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/cp_5ebb0a0d1d226.jpg","cpdescription":"Encuentra f\u00e1cilmente el t\u00f3ner o cartucho que requieres para tu impresora HP.","cpbutton_text":"Ir a buscador","f_oxmanufacturer":null},{"index1":"530cbcf2dd800","cptitle":"Configurador de memoria","cplink":"https://www.kyoceraap.com/Configurador-de-Memoria-Kingston/","cppicture":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/20200618_5eebd62637392.png","cppicture_pdp":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/cp_5ebb1161e7866.jpg","cpdescription":"Aumenta el desempe\u00f1o de tus equipos con la memoria adecuada para cada uno.","cpbutton_text":"Ir a configurador","f_oxmanufacturer":null},{"index1":"530cbd545e662","cptitle":"Configurador de conectividad","cplink":"https://www.kyoceraap.com/StarTech-com-ConXit-Configurador/","cppicture":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/530cbd545e662-5453badb4a360.png","cppicture_pdp":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/cp_5ebb10e7b0e9b.jpg","cpdescription":"Busca los adaptadores y conectores que te facilitan la conexi\u00f3n de tus dispositivos.","cpbutton_text":"Ir a configurador","f_oxmanufacturer":null},{"index1":"5eebdeb49b65b","cptitle":"Connect-Pro","cplink":"https://www.kyoceraap.com/Configurador-de-Cables-Manhattan/","cppicture":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/20200622_5ef12cb7b2ecd.png","cppicture_pdp":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/cp_5ef215c65a300.png","cpdescription":"Busca y encuentra el producto que necesitas con el buscador de Manhattan.","cpbutton_text":"Ir a buscador","f_oxmanufacturer":null},{"index1":"530cbda16e0ba","cptitle":"Gu\u00edas de Compra","cplink":"https://www.kyoceraap.com/Guias-de-Compra/","cppicture":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/20200618_5eebe83d786de.png","cppicture_pdp":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/cp_5eebe83d788dd.png","cpdescription":"Le brindamos la informaci\u00f3n necesaria para que realice la mejor compra.","cpbutton_text":"Ir a gu\u00edas","f_oxmanufacturer":null},{"index1":"5c015d59f0a86","cptitle":"Tripp Lite Cable Finder","cplink":"https://www.kyoceraap.com/Tripp-Lite-Cable-Finder/","cppicture":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/tripplite-cable-finder.png","cppicture_pdp":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/cp_5ebb1192834d2.jpg","cpdescription":"Busca y encuentra el cable Tripp Lite que necesitas en dos sencillos pasos.","cpbutton_text":"Ir a buscador","f_oxmanufacturer":null},{"index1":"5d0abafaae814","cptitle":"Encuentra la laptop perfecta","cplink":"https://www.kyoceraap.com/pc-chooser-journey/","cppicture":"https://www.kyoceraap.com/out/pictures/emstartcontentslider/boton-chooser.png","cppicture_pdp":"","cpdescription":"","cpbutton_text":"Ir a configurador","f_oxmanufacturer":null}],"nostoData":null,"countdown":null}
        </script> -->
        <!-- <script type="application/json" data-cp-lang>
        {"cpx-compare":"Comparar","cpx-offer":"Oferta","cpx-new":"Nuevo","cpx-just-today":"\u00a1S\u00f3lo hoy!","cpx-from":"Desde","cpx-pcs":"Pza.|Pzs.","cpx-pcs-stock1":"Pieza en|Piezas en","cpx-pcs-stock2":"bodega|bodegas","cpx-shipping":"Env\u00edo","cpx-free-signs":"\u00a1Gratis!","cpx-out-of-stock":"Agotado","cpx-create-alert":"Crear alerta","cpx-see-more":"Ver m\u00e1s","cpx-add-to-cart":"Agregar a carrito","cpx-high-eta":"+%d d\u00edas","cpx-sku":"SKU","cpx-in-existence":"En existencia","cpx-weekly-offers-title":"Ofertas de la semana: \u00a1Descuento de volumen a partir de una sola pieza!","cpx-clock-title":"Ofertas validas por tiempo limitado","cpx-clock-are-left":"Quedan:","cpx-clock-day":"Dia|Dias","cpx-clock-hr":"Hr|Hrs","cpx-clock-min":"Min|Mins","cpx-clock-seg":"Seg|Segs","cpx-clock-button":"Ver todas las ofertas","cpx-categories-buy-by-brands":"Comprar por marca","cpx-categories-title":"Algunas de nuestras principales categorias","cpx-tools-title":"Utilice nuestras herramientas de compra","cpx-certificates-title":"Certificados","cpx-reviews-title":"Algunos de nuestros clientes","cpx-certificates-button":"Ver todos los certificados","cpx-links-about-abasteo":"Acerca de abasteo","cpx-links-about-us":"Quienes Somos","cpx-links-why-abasteo":"Por que abasteo","cpx-links-payment-methods":"Formas de pago","cpx-links-shipping-methods":"Formas de envio","cpx-links-information-center":"Centro de informacion","cpx-links-shortcuts":"Accesos rapidos","cpx-links-my-account":"Mi cuenta","cpx-links-my-orders":"Mis Pedidos","cpx-links-my-quotes":"Mis Cotizaciones","cpx-links-address-book":"Libreta de direcciones","cpx-links-purchasing-advice":"Asesoria de compras","cpx-links-how-to-buy":"Como comprar","cpx-links-shopping-guides":"Guias de compra","cpx-links-shopping-tools":"Herramientas de compra","cpx-links-assurances":"Garantias","cpx-links-contact":"Contacto","cpx-links-tel-mex":"MEX (55) 47 800 901","cpx-links-tel-gdl":"GDL (33) 47 800 901","cpx-links-tel-mty":"MTY (81) 47 800 901","cpx-links-schedule":"L-V 9AM A 6PM","cpx-links-email":"INFO@ABASTEO.MX","cpx-payment-prices-with-vat":"Precios incluyendo IVA","cpx-footer-general-conditions":"Condiciones generales","cpx-footer-notice-of-privacy":"Aviso de privacidad","cpx-footer-copyright":"\u00a9 2012 - 2019 ABASTEO.MX","cpx-about-abasteo":"Sobre Abasteo"}
        </script> -->
        <script>window.cp = {}</script>            <script data-cp-orig="cdn.jquery.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script data-cp-orig="cdn.foundation.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/foundation/6.5.3/js/foundation.min.js"></script>
        <script data-cp-orig="cdn.knockout.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/knockout/3.5.0/knockout-min.js"></script>
        <script data-cp-orig="cdn.moment.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
        <script data-cp-orig="cdn.moment-es.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/locale/es.min.js"></script>
        <script data-cp-orig="cdn.moment-timezone.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.26/moment-timezone-with-data-10-year-range.min.js"></script>
        <script data-cp-orig="cdn.Swiper.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js"></script>
        <script data-cp-orig="cdn.vue.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/vue/2.7.10/vue.runtime.min.js"></script>
        <script data-cp-orig="cdn.nouislider.js" src="<?php echo base_url();?>cdnjs.cloudflare.com/ajax/libs/noUiSlider/14.6.3/nouislider.min.js"></script>
        <script data-cp-orig="google.account" src="https://accounts.google.com/gsi/client"></script>
        <script data-cp-orig="apple.account" src="<?php echo base_url();?>appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/es_MX/appleid.auth.js"></script>
        <script data-cp-orig="manifest.js" src="<?php echo base_url();?>out/dist/manifest.c0f5f9e1e31a23450397.js"></script>
        <script data-cp-orig="cp.app.js" src="<?php echo base_url();?>out/dist/cp.app.de4ec193647b4d7c0e1e.js"></script>
        <script data-cp-orig="cp.app.foundation.js" src="<?php echo base_url();?>out/dist/cp.app.foundation.5aec65c868e55fdcfafa.js"></script>
        <script data-cp-orig="cp.app.legacy.js" src="<?php echo base_url();?>out/dist/cp.app.legacy.6bbaed39abf879b63359.js"></script>
        <script data-cp-orig="vendors~cp.app.js" src="<?php echo base_url();?>out/dist/vendors_cp.app.d380a273ca993ef3e5ed.js"></script>
        <!--<script src="<?php echo base_url()?>public/plugins/code.jquery.com_jquery-2.2.0.min.js"  ></script>-->

        <link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
        <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
        
        <link href="<?php echo base_url()?>public/plugins/slick/slick-1.8.1/slick/slick.css?v=2" rel="stylesheet"  >
        <link href="<?php echo base_url()?>public/plugins/slick/slick-1.8.1/slick/slick-theme.css?v=2" rel="stylesheet"  >
        <script src="<?php echo base_url()?>public/plugins/slick/slick-1.8.1/slick/slick.js"  ></script>
    </head>
    <!-- OXID eShop Community Edition, Version 4, Shopping Cart System (c) OXID eSales AG 2003 - 2023 - http://www.oxid-esales.com -->
    <body class="start ">
        <!-- Google Tag Manager --><noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-TZF4FD" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <div class="general-overlay cp-overlay1 hide">
            <div class="cp-overlay-cnt" style="background-image:url('<?php echo base_url();?>out/cyberpuertaV5/img/loading_md.gif')">
                <div class="inner-cnt">
                    <div class='cp-overlay-loader'></div>
                    <div class='cp-overlay-title'></div>
                    <div class='cp-overlay-subtitle'></div>
                </div>
            </div>
        </div>
        <div data-cp-banner-place="side-left"></div>
        <div data-cp-banner-place="side-right"></div>
        <div class="cp-page-bg">
            <div data-cp-banner-place="page-top"></div>
            <a id="cpsaldo_newsletter_modal_popup_link" data-open="cpsaldo_newsletter_modal_popup" style="display:none;">cpsaldo_newsletter</a>
            <div id="cpsaldo_newsletter_modal_popup" class="reveal" data-reveal style="width: 720px">
                <div class="cpsaldo_newsletter ">
                    <div>
                        <!-- <img alt="" src="//www.kyoceraap.com/out/pictures/wysiwigpro/layovers/newsletter-pop-up-image-100-2020.png" /> -->
                        <img alt="" src="<?php echo base_url();?>out/banners/welcome/bannerwelcome-saldo-new%402x.jpg" />
                    </div>
                    <div>
                        <div class="cpsaldo_newsletter_register">
                            <form id="cpsaldo_newsletter_registration" action="https://www.kyoceraap.com/index.php?" method="post" class="js-oxValidate inline-error">
                                <input type="hidden" name="lang" value="0" />
                                <input type="hidden" name="actcontrol" value="start" />
                                <input type="hidden" name="fnc" value="cpsaldo_newsletter_register">
                                <input type="hidden" name="cl" value="start">
                                <input type="hidden" name="tpl" value="">
                                <input type="hidden" name="oxloadid" value="">
                                <input type="hidden" name="CustomError" value=cpsaldo_newsletter_registration>
                                <input type="hidden" name="option" value="4">
                                <input type="hidden" data-cp-password-length value="6">
                                <input type="hidden" name="pgNr" value="0">
                                <div class="login_account clear">
                                    <input type="hidden" name="blnewssubscribed" value="1">
                                    <ul class="emvalidateform form" data-cp-password-strength-tool data-cp-register-form>
                                        <li>
                                            <div class="form_field_box oxuser__oxfname_box">
                                                <label class="req">Nombre:</label>
                                                <input class="js-oxValidate js-oxValidate_notEmpty" type="text" maxlength="255" name="lgn_name" value="" >
                                            </div>
                                            <div data-cp-error-msg>
                                                <span class="form-invalid-input">
                                                    <span class="js-oxError_notEmpty">Favor de llenar el campo</span>
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form_field_box oxuser__oxusername_box">
                                                <label class="req">Ingresa tu email:</label>
                                                <input class="js-oxValidate js-oxValidate_notEmpty js-oxValidate_email" type="text" maxlength="73" name="lgn_usr" value="" >
                                            </div>
                                            <div data-cp-error-msg>
                                                <span class="form-invalid-input">
                                                    <span class="js-oxError_notEmpty">Favor de llenar el campo</span>
                                                    <span class="js-oxError_email">Por favor ingresa un e-mail válido</span>
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form_field_box oxuser__oxpassword_box">
                                                <label class="req">Crea una contraseña:</label>
                                                <input class="js-oxValidate js-oxValidate_notEmpty js-oxValidate_length js-oxValidate_match js-oxValidate_pwdStrength" type="password" name="lgn_pwd" >
                                            </div>
                                            <div data-cp-error-msg>
                                                <span class="form-invalid-input">
                                                    <span class="js-oxError_notEmpty">Favor de llenar el campo</span>
                                                    <span class="js-oxError_length">Tu contraseña debe tener mínimo 6 dígitos</span>
                                                    <span class="js-oxError_pwdStrength">Seguridad insuficiente</span>
                                                    <span class="js-oxError_match">Las contraseñas no coinciden</span>
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form_field_box oxuser__oxpassword2_box">
                                                <label class="req">Confirma la contraseña: </label>
                                                <input class="js-oxValidate js-oxValidate_notEmpty js-oxValidate_match" type="password" name="lgn_pwd2">
                                            </div>
                                            <div data-cp-error-msg>
                                                <span class="form-invalid-input">
                                                    <span class="js-oxError_notEmpty">Favor de llenar el campo</span>
                                                    <span class="js-oxError_match">Las contraseñas no coinciden</span>
                                                </span>
                                            </div>
                                        </li>
                                        <li class="password-strength-container password-strength-container--two-column">
                                            <input type="hidden" data-cp-lang="empty" value="vacía">
                                            <input type="hidden" data-cp-lang="weak" value="debil">
                                            <input type="hidden" data-cp-lang="enough" value="suficiente">
                                            <input type="hidden" data-cp-lang="strong" value="fuerte">
                                            <input type="hidden" data-cp-lang="very-strong" value="muy fuerte">
                                            <input type="hidden" data-cp-lang="pwd-measure-tool-title" value="Requerimos que la seguridad de tu contraseña llegue a 'suficiente' y tenga mínimo 6 caracteres.">
                                            <input type="hidden" data-cp-lang="password-strength-recommendation-register" value="La seguridad debe de ser por lo menos 'suficiente' para poder crear tu cuenta">
                                            <input type="hidden" data-cp-lang="password-security" value="Seguridad de contraseña">
                                            <div>
                                                <div data-cp-vue-bind="password-strength-tool"></div>
                                                <div class="password-strength-tool-msg-req password-strength-tool-msg-req--no-pb">
                                                    La seguridad debe de ser por lo menos 'suficiente' para poder crear tu cuenta
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <input type="hidden" name="blnewssubscribed" value="0">
                                </div>
                                <div class="cpsaldo_newsletter_footer_box">
                                    <div class="cpsaldo_newsletter_footertext">
                                        Al crear una cuenta, se registrarán tus $100 y comenzarás a recibir nuestras promociones en tu correo electrónico. El saldo sólo es aplicable en pedidos con valor de productos mayor a $700.00 MXN
                                    </div>
                                    <div class="cpsaldo_newsletter_buttons_container">
                                        <div class="cpsaldo_button_box">
                                            <button type="submit" class="button-blue">
                                            Crear cuenta y recibir $100
                                            </button>
                                        </div>
                                        <div class="cpsaldo_newsletter_or_container">
                                            <div class="cpsaldo_newsletter_or_item"></div>
                                            <div>
                                                <span style="color: #A0A0A0">o</span>
                                            </div>
                                            <div class="cpsaldo_newsletter_or_item"></div>
                                        </div>
                                        <div class="cpsaldo_newsletter_fid_container">
                                            <!-- 2022 - google sign in button -->
                                            <div class="cpsaldo_newsletter_fid_item">
                                                <div data-cp-vue-bind="gsi-button" data-width="215px"></div>
                                            </div>
                                            <!-- 2022 - Apple sign in button -->
                                            <div class="cpsaldo_newsletter_fid_item">
                                                <div data-cp-vue-bind="asi-button" data-width="215px" data-height="40"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>    <button class="close-button cpsaldo-modal_close-btn" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- <div id="cpsaldo_newslettersticker" data-open="0" class="">
                <div class="bg10"></div>
                <div class="bg3"></div>
                <div class="bg1"></div>
                <div class="bg2"></div>
                <div class="close">NO VOLVER A MOSTRAR</div>
            </div> -->
            <form id="cpsaldo_newsletter_cookie" action="https://www.kyoceraap.com/index.php?" method="post" style="display:none; ">
                <input type="hidden" name="lang" value="0" />
                <input type="hidden" name="actcontrol" value="start" />
                <input type="hidden" name="fnc" value="cpsaldo_setnewslettercookie">
                <input type="hidden" name="cl" value="start">
                <input type="hidden" name="tpl" value="">
                <input type="hidden" name="oxloadid" value="">
                <input type="hidden" name="pgNr" value="0">
            </form>
            <div id="headerstatic" class="">
                <div id="headerbackground"></div>
                <div id="header" class="clear">
                    <div class="headerborder">
                        <a class="logo" href="<?php echo base_url();?>Inicio" title="">
                            <img src="<?php echo base_url();?>out/cyberpuertaV5/img/logo2.svg" alt="">
                        </a>
                        <div id="cp-header-search" class="cp-header-search desktop">
                            <form class="search-form" action="https://www.kyoceraap.com/index.php?" method="get" name="search" autocomplete="off">
                                <input type="hidden" name="cl" value="search">
                                <input
                                maxlength="255"
                                required
                                autocomplete="off"
                                type="text"
                                name="searchparam"
                                value=""
                                data-cp-placeholder="¿Qué producto buscas el día de hoy?"
                                >
                                <button class="submitButton largeButton" type="submit" style="color:">
                                Buscar
                                </button>
                            </form>
                            <div data-cp-vue-bind="cp-search-suggest"
                                data-cp-alt-search=""
                                data-cp-cnid=""
                                data-cp-user-email=""
                                data-cp-mnid=""
                            ></div>
                            <input type="hidden" data-cp-lang=search_in_category_suggest value="Buscar en la categoría">
                            <input type="hidden" data-cp-lang=search_in_manufacturer_suggest value="Buscar en la marca">
                            <input type="hidden" data-cp-lang=cpsolrproductfilter_search_options value="¿Qué producto buscas el día de hoy?">
                            <input type="hidden" data-cp-lang=cpsolrproductfilter_deleterecentsearches value="Borrar búsquedas recientes">
                            <input type="hidden" data-cp-lang=cpsolrproductfilter_recentsearchesdeleted value="Búsquedas recientes eliminadas">
                        </div>
                        <div id="cp-main-menu-btn" class="cp-main-menu-btn">
                            <div class="title">Categorías</div>
                            
                        </div>
                        <div id="emheadernotice" style="right: 535px;">
                            <div id="oxwidget_headernotice" class="has">
                                <div class="oxwidget_headernotice_header">
                                    <div class="oxwidget_headernotice_header_wait"></div>
                                    <div class="oxwidget_headernotice_title1" style="width: 164px;">
                                       <a href="<?php echo base_url() ?>Inicio/quienes_somos">Quiénes somos</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="emheadercompare" style="right: 447px;">
                            <div id="oxwidget_headercompare" class="has ">
                            <div class="oxwidget_headercompare_header">
                                <div class="oxwidget_headercompare_header_wait"></div>
                                <div class="oxwidget_headercompare_title1" style="width: 94px;">Servicios</div>
                            </div>
                            </div>
                        </div>

                        <div id="emheadercompare" style="right: 447px;">
                            <div id="oxwidget_headerhelp" class="">
                                <div class="oxwidget_headerhelp_header">
                                    <div class="oxwidget_headerhelp_title1" style="width: 94px;">Servicios</div>
                                </div>
                                <div class="oxwidget_headerhelp_popup">
                                    <div class="oxwidget_headerhelp_helper"></div>
                                    <ul>
                                        <li><a href="Quienes-somos/index.html">Venta de equipos</a></li>    
                                        <li><a href="Quienes-somos/index.html">Venta de refacciones</a></li>
                                        <li><a href="Contacto/index.html">Venta de consumibles</a></li>
                                        <li><a href="Contacto/index.html">Venta de accesorios</a></li>
                                        <li><a href="Preguntas-frequentes/index.html">Renta de equipos</a></li>
                                        <li><a href="Modos-de-Pago/index.html">Servicio tecnico</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div id="emheadercompare" style="right: 301px;">
                            <div id="oxwidget_headercompare" class="has ">
                                <div class="oxwidget_headercompare_header">
                                    <div class="oxwidget_headercompare_header_wait"></div>
                                    <div class="oxwidget_headercompare_title1" style="width: 151px;"><a href="<?php echo base_url() ?>Inicio/rentaequipos">Renta de Equipos</a></div>
                                </div>
                            </div>
                        </div>

                        <div id="emheadercompare" style="right: 210px !important;">
                            <div id="oxwidget_headercompare" class="has ">
                                <div class="oxwidget_headercompare_header">
                                    <div class="oxwidget_headercompare_header_wait"></div>
                                    <div class="oxwidget_headercompare_title1" style="width: 94px;"><a href="<?php echo base_url() ?>Inicio/contacto">Contacto</a></div>
                                </div>
                            </div>
                        </div>
                        <?php if($this->session->userdata('logeado')==true){ ?>
                        
                        <div id="emheaderlogin" style="right: 119px !important;">
                            <div id="oxwidget_headerlogin" class="">
                                <div class="oxwidget_headerlogin_header">
                                    <div class="oxwidget_headerlogin_title1" title="<?php echo $this->session->userdata('emp');?>">Cuenta</div>
                                </div>
                                <div class="oxwidget_headerlogin_popup">
                                    <div class="oxwidget_headerlogin_helper "></div>
                                    <div class="header_popup">
                                        <ul>
                                            <li><a href="#">Perfil</a></li>
                                            <li><a href="<?php echo base_url();?>Account/exit">Salir</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php }else{ ?>
                        <div id="emheaderlogin" style="right: 119px !important;">
                            <div id="oxwidget_headerlogin" class="">
                                <div class="oxwidget_headerlogin_header">
                                    <div class="oxwidget_headerlogin_title1 ">Ingresar</div>
                                </div>
                                <div class="oxwidget_headerlogin_popup">
                                    <div class="oxwidget_headerlogin_helper "></div>
                                    <div class="oxwidget_headerlogin_loginform">
                                        <form action="<?php echo base_url()?>Account/login" method="post">
                                            <input type="hidden" name="lang" value="0" />
                                            <input type="hidden" name="actcontrol" value="start" />
                                            <input type="hidden" name="pgNr" value="0">
                                            <input type="hidden" name="errorDest" value="loginBoxErrors">
                                            <ul class="form">
                                                <li>
                                                    <label>E-Mail:</label>
                                                    <input id="loginEmail" type="text" name="user" value="" class="textbox" autocomplete="header username" required>
                                                </li>
                                                <li>
                                                    <label>Contraseña:</label>
                                                    <input id="loginPasword" type="password" name="password" class="textbox passwordbox" value="" autocomplete="header password" required>
                                                </li>
                                                <li class="seperatororange"></li>
                                                <div>
                                                    <a style="text-align: center;color:red;font-weight: 700; line-height: 20px;padding: 8px 0 7px 0;" href="olvide-mi-contrasena/index.html" title="Olvidé mi contraseña">Olvidé mi contraseña</a>
                                                </div>
                                                <li class="formSubmit">
                                                    <button class="submitButton largeButton" type="submit" style="margin-right:-3px; ">Iniciar sesión</button>
                                                </li>
                                                <li class="formSubmit">
                                                    <a class="normalButton largeButton oxwidget_headerlogin_register_button" href="crear-cuenta/index.html" style="margin-right:-3px;">Registrarse</a>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>   
                            
                        <?php 
                            if(isset($_SESSION['pro'])){
                                $count_pro=count($_SESSION['pro']);
                                if($count_pro>0){
                                    $class_hass='has';
                                }else{
                                    $class_hass='';
                                }
                            }else{
                                $count_pro=0;
                                $class_hass='';
                            }
                        ?>
                        <div id="emheaderminibasket" class=" ">
                            <div id="oxwidget_headerminibasket" class="emsmoothhover  <?php echo $class_hass;?>">
                                <a class="oxwidget_headerminibasket_header" href="carrito-de-compras/index.html">
                                    <span class="count"><?php echo $count_pro;?></span>
                                    <span class="icon"></span>
                                    <span class="total totalgeneraltotal">$0.00</span>
                                </a>
                                <?php if($count_pro>0){ 
                                    $totalgeneral=0;
                                    $totalcostoenvio=99;
                                    ?>
                                    <div class="oxwidget_headerminibasket_popup">
                                        <div class="oxwidget_headerminibasket_popup_connect "></div>
                                        <div class="oxwidget_headerminibasket_popup_header">
                                            <div class="oxwidget_headerminibasket_popup_title">Tienes <span class="bigtext"><?php echo $count_pro;?> artículos</span> en tu carrito</div>
                                            <a href="<?php echo base_url();?>Account/clearprocard"><button class="oxwidget_headerminibasket_popup_emptyBtn">Borrar todos</button></a>
                                        </div>
                                        <div class="row">
                                            <?php 
                                                $prorow=0;
                                                foreach ($_SESSION['pro'] as $fila){ 
                                                    $url_image=base_url().'public/img/producto-sin-imagen.png';
                                                    $Cantidad=$_SESSION['can'][$prorow]; 
                                                    if($fila['typeitem']==1){
                                                        $idcode=intval($fila['iditem']);
                                                        $url="https://altaproductividadapr.com/index.php/restserver/productosinfo/".$idcode;
                                                        $equiposrow=$this->ModeloRest->consultaapiget($url);
                                                        foreach ($equiposrow as $item) {
                                                            $noparte=$item['noparte'];
                                                            $costo_venta=$item['costo_venta'];
                                                            $title_equipo=$item['modelo'].' '.$item['especificaciones'];
                                                            $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                            if($item['foto']!=''){
                                                                $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['foto'];
                                                            }
                                                            ?>
                                                                
                                                                <div class="col-md-12">
                                                                    <div class="iconremove" onclick="deletepro(<?php echo $prorow;?>)">X<i class="fa fa-trash fa-fw"></i></div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div style="width: 95px;height: 85px; float: left; background: url(<?php echo $url_image;?>);background-size: 100%;background-repeat: no-repeat;">
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div >
                                                                        <?php echo $title_equipo;?>
                                                                    </div>
                                                                    <div style="color: grey;">
                                                                        <?php echo $noparte;?>
                                                                    </div>
                                                                    <div style="width:50%; float:left; ">
                                                                        <?php echo $Cantidad;?> Piezas
                                                                    </div>
                                                                    <div style="width:50%; float:left; ">
                                                                        C/U $ <?php echo number_format($costo_venta,2,".",",");?>
                                                                    </div>

                                                                </div>
                                                                
                                                                    
                                                                    
                                                            <?php
                                                            $prorow++;
                                                        }
                                                    
                                                    }
                                                    if($fila['typeitem']==2){
                                                        $idcode=intval($fila['iditem']);
                                                        //===========================================================================
                                                            $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                            $arraycols=array();
                                                            $arraycols[]=array('name'=>'id','value'=>$idcode);

                                                            $array=array('tabla'=>'catalogo_accesorios','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                            $equipos=$this->ModeloRest->consultaapipost($url,$array);
                                                        //=================================================================
                                                        //==============================================================

                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'idaccesorio','value'=>$idcode);

                                                        $array=array('tabla'=>'catalogo_accesorios_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $equiposimg=$this->ModeloRest->consultaapipost($url,$array);

                                                        $equiposimg=$equiposimg;
                                                        foreach ($equiposimg as $item) { 
                                                           $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['imagen'];
                                                        }
                                                        //==============================================================

                                                        foreach ($equipos as $item) {
                                                            $noparte=$item['no_parte'];
                                                            $costo_venta=$item['costo'];
                                                            $title_equipo=$item['nombre'];
                                                            $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                            
                                                            ?>
                                                                
                                                                <div class="col-md-12">
                                                                    <div class="iconremove" onclick="deletepro(<?php echo $prorow;?>)">X<i class="fa fa-trash fa-fw"></i></div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div style="width: 95px;height: 85px; float: left; background: url(<?php echo $url_image;?>);background-size: 100%;background-repeat: no-repeat;">
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div >
                                                                        <?php echo $title_equipo;?>
                                                                    </div>
                                                                    <div style="color: grey;">
                                                                        <?php echo $noparte;?>
                                                                    </div>
                                                                    <div style="width:50%; float:left; ">
                                                                        <?php echo $Cantidad;?> Piezas
                                                                    </div>
                                                                    <div style="width:50%; float:left; ">
                                                                        C/U $ <?php echo number_format($costo_venta,2,".",",");?>
                                                                    </div>

                                                                </div>
                                                                
                                                                    
                                                                    
                                                            <?php
                                                            $prorow++;
                                                        }
                                                    
                                                    }
                                                    if($fila['typeitem']==3){
                                                        $idcode=intval($fila['iditem']);
                                                        //===========================================================================
                                                            $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                            $arraycols=array();
                                                            $arraycols[]=array('name'=>'id','value'=>$idcode);

                                                            $array=array('tabla'=>'consumibles','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                            $equipos=$this->ModeloRest->consultaapipost($url,$array);
                                                        //=================================================================
                                                        //==============================================================

                                                        $arraycols=array();
                                                        $arraycols[]=array('name'=>'idconsumible','value'=>$idcode);

                                                        $array=array('tabla'=>'consumibles_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                        $equiposimg=$this->ModeloRest->consultaapipost($url,$array);

                                                        $equiposimg=$equiposimg;
                                                        foreach ($equiposimg as $item) { 
                                                           $url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['imagen'];
                                                        }
                                                        //==============================================================

                                                        foreach ($equipos as $item) {
                                                            $noparte=$item['parte'];
                                                            
                                                            //================================
                                                                //===========================================================================
                                                                    $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
                                                                    $arraycols=array();
                                                                    $arraycols[]=array('name'=>'consumibles_id','value'=>$idcode);

                                                                    $array=array('tabla'=>'consumibles_costos','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
                                                                    $costo_lis_precio=$this->ModeloRest->consultaapipost($url,$array);
                                                                    foreach ($costo_lis_precio as $itemp) {
                                                                        $costo_venta=$itemp['general'];
                                                                    }
                                                                //=================================================================
                                                            //================================
                                                            $title_equipo=$item['modelo'];
                                                            $totalgeneral=$totalgeneral+($Cantidad*$costo_venta);
                                                            
                                                            ?>
                                                                
                                                                <div class="col-md-12">
                                                                    <div class="iconremove" onclick="deletepro(<?php echo $prorow;?>)">X<i class="fa fa-trash fa-fw"></i></div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div style="width: 95px;height: 85px; float: left; background: url(<?php echo $url_image;?>);background-size: 100%;background-repeat: no-repeat;">
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div >
                                                                        <?php echo $title_equipo;?>
                                                                    </div>
                                                                    <div style="color: grey;">
                                                                        <?php echo $noparte;?>
                                                                    </div>
                                                                    <div style="width:50%; float:left; ">
                                                                        <?php echo $Cantidad;?> Piezas
                                                                    </div>
                                                                    <div style="width:50%; float:left; ">
                                                                        C/U $ <?php echo number_format($costo_venta,2,".",",");?>
                                                                    </div>

                                                                </div>
                                                                
                                                                    
                                                                    
                                                            <?php
                                                            $prorow++;
                                                        }
                                                    
                                                    }

                                            } 
                                                $totalgeneraltotal=$totalgeneral+$totalcostoenvio;
                                            ?>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                
                                            </div>
                                            <div class="col-md-4 card_montos">Subtotal:</div>
                                            <div class="col-md-4 card_montos"><b>$ <?php echo number_format($totalgeneral,2,".",",");?></b></div>

                                            <div class="col-md-4">
                                                
                                            </div>
                                            <div class="col-md-4 card_montos">Costo envio: </div>
                                            <div class="col-md-4 card_montos"><b>$ <?php echo number_format($totalcostoenvio,2,".",",");?></b></div>

                                            <div class="col-md-4">
                                                
                                            </div>
                                            <div class="col-md-4 card_montos">Total: </div>
                                            <div class="col-md-4 card_montos"><b>$ <?php echo number_format($totalgeneraltotal,2,".",",");?></b></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a type="button" class="btn btn-secondary" href="<?php echo base_url();?>Carrito">Ir a caja</a>
                                            </div>
                                            
                                        </div>

                                    </div>
                                    <script type="text/javascript">
                                        $(document).ready(function($) {
                                            $('.totalgeneraltotal').html('$ <?php echo number_format($totalgeneraltotal,2,".",",");?>');
                                        });
                                    </script>
                                <?php } ?>
                            </div>
                        </div>
                        <!----------------->

                        <!----------------->
                        <div class="cp-header-overlay"></div>
                    </div>
                </div>            </div>
                
                        