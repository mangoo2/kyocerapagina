<style type="text/css">
	.emproduct.listitemline .emproduct_right .emproduct_right_attribute li:before {
content: "•";
}
</style>


				<!--<div id="breadCrumb"><span class="breadCrumb first  "><a href="https://www.kyoceraap.com/" title="Inicio"><span>Inicio</span></a></span><span class="breadCrumb  last "><a href="https://www.kyoceraap.com/Nuevos-articulos/" title="Nuevos artículos"><span> artículos</span></a></span></div>-->
				<div class="start_topbox_right">
					<div class="row">
						<div class="col-md-12">
							<div id="breadCrumb"><span class="breadCrumb first  "><a href="https://www.kyoceraap.com/" title="Inicio"><span>Inicio</span></a></span><span class="breadCrumb  last "><a href="https://www.kyoceraap.com/Nuevos-articulos/" title="Nuevos artículos"><span> artículos</span></a></span></div>
						</div>
					</div>
					<div class="row bannerarea clear">
						<div class="col-md-12">
							<section class="lazy slider" id="lazy" data-sizes="50vw" style="height:auto;">
								<?php foreach ($infobaner as $itemb) { 
									$url_image='https://altaproductividadapr.com/uploads/baner/'.$itemb['imagen'];
									?>
									<div>
								      <img data-lazy="<?php echo $url_image;?>" data-srcset="<?php echo $url_image;?>" data-sizes="100vw">
								    </div>
								<?php }?>
							    
							  </section>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="margin-bottom: 113px;">
							<table class="table" id="tablepro">
								<thead>
									<tr><th></th></tr>
								</thead>
								<!----------------------------------------->
									<?php
										foreach ($accesoriosall as $item) {
											$iditem=$item['id'];
											$title_equipo=$item['nombre'];
											//if($item['foto']==''){
												$url_image=base_url().'public/img/impresora.png';
											//}else{
											//	$url_image='https://altaproductividadapr.com/uploads/equipos/'.$item['foto'];	
											//}
											
											$url_pro=base_url().'Accesorios/articulo/'.$item['uuid'].'/'.$item['no_parte'];
									?>
									<tr><td>
									<li class="cell productData small-12 small-order-1">
										<div class="emproduct clear listitemline">
											<!--<form name="tobasket.productList-1" class="js-oxProductForm" action="<?php echo base_url();?>Account/addcar" method="post">-->
												<?php echo form_open('Account/addcar'); ?>
												<input type="hidden" name="typeitem" value="2" wtx-context="40E8F617-9938-42C6-BEDC-DAEE4FCAF3B5">
			                        			<input type="hidden" name="iditem" value="<?php echo $iditem;?>" wtx-context="40E8F617-9938-42C6-BEDC-DAEE4FCAF3B5">
			                        			<input type="hidden" name="urlitem" value="<?php echo base_url();?>Accesorios" wtx-context="40E8F617-9938-42C6-BEDC-DAEE4FCAF3B5">
												<input id="am_productList-1" type="hidden" name="am" value="1" wtx-context="EA720FF7-BC22-478A-8602-4764E297D1B0">
												<div class="emproduct_left_shadow"></div>
												<div class="emproduct_left">
													<a class="cp-picture-container cpGaProdproductList-1" href="<?php echo $url_pro;?>">
														<div class="picture-wrapper">
															<div class="line-cat-slider">
																<div  class="catSlider">
																	<div data-cp-img-container="" class="cs-image" style="background-image: url(<?php echo $url_image;?>);"></div>
																</div>
															</div>
														</div>
													</a>
												</div>
												<div class="emproduct_right">
													<a id="productList-1" href="<?php echo $url_pro;?>" class="emproduct_right_title emsmoothtext cpGaProdproductList-1 productList-1" title="<?php echo $title_equipo;?>" style="margin-right: 40px; color: rgb(51, 51, 51);" data-color="rgb(51, 51, 51)">
														<?php echo $title_equipo;?>
													</a>
													<div class="clear emproduct_right_artnum_review">
														<div class="emproduct_right_artnum">
															SKU: <?php echo $item['no_parte'];?>
														</div>
													</div>
													<div class="clear emproduct_left_attribute_price">
														<div class="emproduct_right_attribute">
															<ul><?php echo str_replace(array('<col1>','<col2>','<col3>'),array('<li><span class="title">',': </span><span class="content">','</span></li>'), $item['caracte'])?></ul>
														</div>
														<div class="emproduct_right_price">
															<?php if($this->session->userdata('logeado')==true){ ?>
															<div class="clear">
																<!--sse-->
																<div class="emproduct_right_price_left">
																	<label class="price">$<?php echo round($item['costo']*1.16,2);?></label>
																	<div class="emdeliverycost">
																		<span class="deliverytext">Costo de envío:</span>
																		<span class="deliveryvalue">$99.00</span>
																	</div>
																	<div class="emstock">Disponibles:<span><?php echo $item['stock'];?></span>pzas.</div>
																</div>
																<!--/sse-->
																<div class="emproduct_right_price_right">
																	<div class="tobasketFunction clear">
																		<div class="emproduct_tobasketbox  clear">
																			<input type="hidden" name="am" value="1" wtx-context="B1FB0674-D7AC-49F3-8C6B-73DC054462C3">
																			<?php if($item['stock']>0 and $item['costo']>0){ ?>
																				<button id="toBasket_productList-1" type="submit" class="submitButton largeButton cartIcon">Agregar al carrito</button>
																			<?php }else{ ?>
																				<a class="btn btn-primary btn-sm" onclick="addproconti(2,<?php echo $iditem;?>)">Solicitar Cotización</a>
																			<?php } ?>
																		</div>
																		<div class="emproduct_comparebox clear">
																			<div class="emcomparelinkbox clear" id="emcomparelinkboxlistproduct_productList-1" data-id="listproduct_productList-1"></div>
																		</div>
																	</div>
																</div>
															</div>
															<?php }else{ ?>
								                                <div class="main-info clientelogueo">
								                                    Debes ser cliente para ver precios y existencias
								                                </div>
								                            <?php } ?> 
															
														</div>
													</div>
												</div>
											<!--</form>-->
											<?php echo form_close(); ?>
										</div>
											
										</li></td></tr>
										<?php
										}
										?>
								<!----------------------------------------->
							</table>
						</div>
					</div>
					
					
					
					
					
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function($) {
				$(".lazy").slick({
				        autoplay: true,
				  autoplaySpeed: 10000,
      			});
				$('#tablepro').dataTable();
			});
		</script>