<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrito extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        if($this->session->userdata('logeado')==true){
            $this->idemp = $this->session->userdata('idemp');
            $this->emp = $this->session->userdata('emp');
            $this->correo = $this->session->userdata('correo');
            $this->load->library('openpay');
            $this->openpay = Openpay::getInstance('mzuolrwtlcn4ttmv4jvz', 'sk_552c80a7da564eee8200ffd8214b0c70', 'MX');
        }else{
            redirect('Inicio'); 
        }
        $this->client_id_paypay='AW9RhmXeNrX2903kBNCmfhKIR0WDV-tPuLPbLZjMTKqmDYD_5H6JiPr7daoXPOu3iTcUo61zyHJjC3vK';
    }

	public function index(){
    $result_car=$this->ModeloConsultas->listado_carrito();
    $data['result_car']=$result_car;
    if(count($result_car)>0){

    }else{
      redirect('Inicio'); 
    }
    $this->load->view('theme/pagedemo_h');
		$this->load->view('carrito',$data);
    $this->load->view('theme/pagedemo_f');
		$this->load->view('iniciojs');

	}
  public function direccion(){
    $result_car=$this->ModeloConsultas->listado_carrito();
    $data['result_car']=$result_car;
    if(count($result_car)>0){

    }else{
      redirect('Inicio'); 
    }
    $idemp = $this->session->userdata('idemp');
    $url="https://altaproductividadapr.com/index.php/restserver/sisuserdireccion/".$this->idemp;
    $data['direcciones']=$this->ModeloRest->consultaapiget($url);

    $url="https://altaproductividadapr.com/index.php/restserver/sisusercontacto/".$this->idemp;
    $data['contactos']=$this->ModeloRest->consultaapiget($url);


    $this->load->view('theme/pagedemo_h');
    $this->load->view('direccion',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');

  }
  function direccionsave(){
    $params = $this->input->post();
    $dir=$params['dir'];
    $entre=$params['entre'];
    $cp=$params['cp'];
    if($this->idemp>0){
      $url="https://altaproductividadapr.com/index.php/restserver/sisuserdireccionadd";
      $array=array('dir'=>$dir,'entrecalles'=>$entre,'cp'=>$cp,'cli'=>$this->idemp);
      $this->ModeloRest->consultaapipost($url,$array);
    }

  }
  function contactosave(){
    $params = $this->input->post();
    $per=$params['per'];
    $tel=$params['tel'];
    $cel=$params['cel'];
    if($this->idemp>0){
      $url="https://altaproductividadapr.com/index.php/restserver/sisusercontactoadd";
      $array=array('cli'=>$this->idemp,'atencionpara'=>$per,'telefono'=>$tel,'celular'=>$cel);
      $this->ModeloRest->consultaapipost($url,$array);
    }
  }
  function enviopago(){
    $params = $this->input->post();
    $direccion = $params['direccion'];
    $contacto = $params['contacto'];
    $_SESSION['direccion'] =$direccion;
    $_SESSION['contacto'] =$contacto;
    $data['x']='0';

    //============================================================
      $arraypro=array();
      $result_car=$this->ModeloConsultas->listado_carrito();
      foreach ($result_car as $fila) {
        $arraypro[]=array('tipo'=>$fila['typeitem'],'iditem'=>$fila['iditem'],'can'=>$fila['cant']);
      }
      /*
      
      $prorow=0;
      foreach ($_SESSION['pro'] as $fila){
        $Cantidad=$_SESSION['can'][$prorow];
        $arraypro[]=array('tipo'=>$fila['typeitem'],'iditem'=>$fila['iditem'],'can'=>$Cantidad);
        $prorow++;
      }
      */

    //============================================================




    $url="https://altaproductividadapr.com/index.php/restserver/addventa";
    $array=array('contacto'=>$contacto,'direccion'=>$direccion,'idCliente'=>$this->idemp,'pro'=>json_encode($arraypro));


      $idventa=$this->ModeloRest->consultaapipost($url,$array);
      log_message('error','idventa '.$idventa);
      //$idventa=19412;
      $_SESSION['idventa']=$idventa;
      $data['idventa']=$idventa;
    $data['client_id_paypay']=$this->client_id_paypay;
    $this->load->view('theme/pagedemo_h');
    $this->load->view('enviopago',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
    $this->load->view('enviopagojs');
  }
  function deletedir(){
    $params = $this->input->post();
    $id=$params['idi'];

    $url="https://altaproductividadapr.com/index.php/restserver/updatetable";

    $columns=array();
    $columns[]=array('name'=>'status','value'=>0);

    $columnsw=array();
    $columnsw[]=array('name'=>'idclientedirecc ','value'=>$id);

    $array=array('table'=>'clientes_direccion','columns'=>json_encode($columns),'columnsw'=>json_encode($columnsw));
  }
  function addcargotargeta(){
        $respuesta =array();
        $params = $this->input->post();
        $token = $params['token'];
        //$amount = $params['amount'];
        $amount = $_SESSION['totalgeneraltotal'];
        //$description = $params['description'];
        $description = 'Cargo por venta'.$_SESSION['idventa'];
        $device_session_id = $params['device_session_id'];
        $name = $params['name'];
        $email = $params['email'];
        $customer = array(
         'name' => $this->session->userdata('emp'),
         'email' => $this->session->userdata('correo'));

        $chargeRequest = array(
        'method' => 'card',
        'source_id' => $token, //idtarjeta o token
        'amount' => $amount,
        'currency' => 'MXN',
        'description' => $description,
        //'order_id' => 'oid-00051',
        'device_session_id' => $device_session_id,//id identificador del dispotitivo se devera agrear dinamicamente desde la api https://github.com/open-pay/openpay-js#fraud-detection-using-device-data
        'customer' => $customer);

        $charge = $this->openpay->charges->create($chargeRequest);
        if($charge){
            if($charge->status=="completed"){
                $numerstuatus=1;
                $respuesta =array('status'=>$numerstuatus,'amount'=>$charge->amount,'authorization'=>$charge->authorization,'fecha'=>$charge->creation_date,'description'=>$charge->description);
                log_message('error', 'estatus pago 1');
                $this->ventarealizada($respuesta);
            }else{
                //var_dump($charge);
                log_message('error', 'estatus pago 0');
                $numerstuatus=0;
                $respuesta =array('status'=>$numerstuatus);
            }
        }else{
            $numerstuatus=0;
            $respuesta =array('status'=>$numerstuatus);
        }
        //echo $numerstuatus; 

        echo json_encode($respuesta);
  }
  function ventarealizada($params){

    $url_update="https://altaproductividadapr.com/index.php/restserver/updatetable";
    $url_insert="https://altaproductividadapr.com/index.php/restserver/inserttable";
    //==============================================================================================
      $columns=array();
      $columns[]=array('name'=>'activo','value'=>1);
      $columns[]=array('name'=>'estatus','value'=>3);


      $columnsw=array();
      $columnsw[]=array('name'=>'id ','value'=>$_SESSION['idventa']);

      $array=array('table'=>'ventas','columns'=>json_encode($columns),'columnsw'=>json_encode($columnsw));
      $this->ModeloRest->consultaapipost($url_update,$array);
    //==================================================================================================
    //==============================================================================================
      $columns=array();
      $columns[]=array('name'=>'idventa','value'=>$_SESSION['idventa']);
      $columns[]=array('name'=>'pago','value'=>$_SESSION['totalgeneraltotal']);
      $columns[]=array('name'=>'fecha','value'=>$params['fecha']);
      $columns[]=array('name'=>'observacion','value'=>'Pago realizado, Autorizacion:'.$params['authorization']);
      $columns[]=array('name'=>'idmetodo','value'=>4);
      $columns[]=array('name'=>'idpersonal','value'=>1);


      $array=array('table'=>'pagos_ventas','columns'=>json_encode($columns));
      $this->ModeloRest->consultaapipost($url_insert,$array);
    //==================================================================================================
      $this->ModeloConsultas->deletecarrito();//limpia la tabla del carrito
  }
  function paypalorder(){
    $idventa=$_SESSION['idventa'];

    echo json_encode(array('id'=>$idventa,'price'=>$_SESSION['totalgeneraltotal']));
  }
	

}