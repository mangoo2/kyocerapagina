<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Icha extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloGeneral');
        date_default_timezone_set('America/Mexico_City');
        if($this->session->userdata('logeado')==true){
            $this->idemp = $this->session->userdata('idemp');
            $this->emp = $this->session->userdata('emp');

            //$this->correo = $this->session->userdata('correo');
            //$this->load->library('openpay');
            //$this->openpay = Openpay::getInstance('mzuolrwtlcn4ttmv4jvz', 'sk_552c80a7da564eee8200ffd8214b0c70', 'MX');
        }else{
            $this->emp='';
        }
        $this->fecha_cod = date('Ymd');
        $this->fecha_cod_m = date('ymd');
    }

	public function index(){
    if(isset($_GET['ttp'])){
      $ttp=$_GET['ttp'];
    }else{
      $ttp=0;
    }
    if($ttp==0){
      if(isset($_SESSION['url_actual'])){
        
        redirect($_SESSION['url_actual']);
      }
    }
    if(isset($_GET['ses'])){
      $ses=1;
    }else{
      $ses=0;
    }
    if($ses==0){
      if($ttp==0){
        if($this->session->userdata('logeado')==true){
          $uuid=$this->session->userdata('uuidemp');
          redirect('Icha/cli_verif/'.$uuid);
        }
      }else{
        if($this->session->userdata('logeado')==true){
          $uuid=$this->session->userdata('uuidemp');
          redirect('Icha/cli_servicio_group/'.$uuid);
        }else{
          redirect('Icha/verif_cli');
        }
      }
    } 
    
		$this->load->view('icha/icha');
	}
  function verif_cli(){
    $request_uri=(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $_SESSION['url_actual']=$request_uri;

    $params = $this->input->post();
    $data['estatus']='';
    if(isset($params['codigo'])){
      //echo 'existe';
      $codigo=$params['codigo'];

      $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
      $arraycols=array();
      $arraycols[]=array('name'=>'uuid','value'=>$codigo,'activo'=>1);

      $array=array('tabla'=>'clientes','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
      $equipos=$this->ModeloRest->consultaapipost($url,$array);
      $data['estatus']=0;
      foreach ($equipos as $item) {
        $data['estatus']=1;
        $data['codigo']=$codigo;
        $data['idcliente']=$item['id'];
      }
    }
    $this->load->view('icha/verif_cli',$data);
  }
  function cli_verif($codigo=0){
      $request_uri=(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $_SESSION['url_actual']=$request_uri;
      if($codigo==0){
        

        //  $request_uri=base_url().'index.php/Icha?ses=1&ttp=1';
        //$request_uri.'<br>';
        //$_SESSION['url_actual']=$request_uri;
        //unset($_SESSION['url_actual']);
        redirect('Icha?ses=1&ttp=1');
      }
      
      
      
      //$codigo=$params['codigo'];

      $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
      $arraycols=array();
      $arraycols[]=array('name'=>'uuid','value'=>$codigo,'activo'=>1);

      $array=array('tabla'=>'clientes','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
      $equipos=$this->ModeloRest->consultaapipost($url,$array);
      $data['estatus']=0;
      foreach ($equipos as $item) {
        $data['estatus']=1;
        $data['codigo']=$codigo;
        $data['idcliente']=$item['id'];
      }
    $this->load->view('icha/cli_verif',$data);
  }
  function cli_servicio_group($codigo){
      $request_uri=(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $_SESSION['url_actual']=$request_uri;
      //$codigo=$params['codigo'];
      $idcliente=0;
      $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
      $arraycols=array();
      $arraycols[]=array('name'=>'uuid','value'=>$codigo,'activo'=>1);

      $array=array('tabla'=>'clientes','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
      $equipos=$this->ModeloRest->consultaapipost($url,$array);
      $data['estatus']=0;
      foreach ($equipos as $item) {
        $data['estatus']=1;
        $data['codigo']=$codigo;
        $data['idcliente']=$item['id'];
        $idcliente=$item['id'];
      }
      $url="https://altaproductividadapr.com/index.php/restserver/equiposegeneral/$idcliente/1/1/0";
      $result_eq=$this->ModeloRest->consultaapiget($url);
      $data['result_eq_c']=$result_eq;

      $url="https://altaproductividadapr.com/index.php/restserver/equiposegeneral/$idcliente/2/1/0";
      $result_eq=$this->ModeloRest->consultaapiget($url);
      $data['result_eq_p']=$result_eq;

      $url="https://altaproductividadapr.com/index.php/restserver/equiposegeneral/$idcliente/3/1/0";
      $result_eq=$this->ModeloRest->consultaapiget($url);
      $data['result_eq_h']=$result_eq;

      $url="https://altaproductividadapr.com/index.php/restserver/equiposegeneral/$idcliente/4/1/0";
      $result_eq=$this->ModeloRest->consultaapiget($url);
      $data['result_eq_v']=$result_eq;

      $this->load->view('icha/cli_servicio_group',$data);
  }
	function cli_servicio_pre($codigo,$tiporcv,$group,$idg){
    $request_uri=(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $_SESSION['url_actual']=$request_uri;

      $data['tiporcv']=$tiporcv;
      $data['group']=$group;
      $data['idg']=$idg;
      if(isset($_GET['row'])){
        $data['row_reg']=$_GET['row'];
      }else{
        $data['row_reg']=1;
      }
      //$codigo=$params['codigo'];
      $idcliente=0;
      $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
      $arraycols=array();
      $arraycols[]=array('name'=>'uuid','value'=>$codigo,'activo'=>1);

      $array=array('tabla'=>'clientes','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
      $equipos=$this->ModeloRest->consultaapipost($url,$array);
      $data['estatus']=0;
      foreach ($equipos as $item) {
        $data['estatus']=1;
        $data['codigo']=$codigo;
        $data['idcliente']=$item['id'];
        $idcliente=$item['id'];
      }
      $this->load->view('icha/cli_servicio_pre',$data);
  }
  function cli_servicio_ss($codigo,$tiporcv,$group,$idg){
    $request_uri=(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $_SESSION['url_actual']=$request_uri;

      $data['tiporcv']=$tiporcv;
      $data['group']=$group;
      $data['idg']=$idg;
      if(isset($_GET['row'])){
        $data['row_reg']=$_GET['row'];
      }else{
        $data['row_reg']=1;
      }
      //$codigo=$params['codigo'];
      $idcliente=0;
      $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
      $arraycols=array();
      $arraycols[]=array('name'=>'uuid','value'=>$codigo,'activo'=>1);

      $array=array('tabla'=>'clientes','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
      $equipos=$this->ModeloRest->consultaapipost($url,$array);
      $data['estatus']=0;
      foreach ($equipos as $item) {
        $data['estatus']=1;
        $data['codigo']=$codigo;
        $data['idcliente']=$item['id'];
        $idcliente=$item['id'];
      }
      $this->load->view('icha/cli_servicio_ss',$data);
  }
  function cli_servicio($codigo,$tiporcv,$group,$idg){
      $request_uri=(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $_SESSION['url_actual']=$request_uri;

      $data['tiporcv']=$tiporcv;
      //$codigo=$params['codigo'];
      $idcliente=0;
      $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
      $arraycols=array();
      $arraycols[]=array('name'=>'uuid','value'=>$codigo,'activo'=>1);

      $array=array('tabla'=>'clientes','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
      $equipos=$this->ModeloRest->consultaapipost($url,$array);
      $data['estatus']=0;
      foreach ($equipos as $item) {
        $data['estatus']=1;
        $data['codigo']=$codigo;
        $data['idcliente']=$item['id'];
        $idcliente=$item['id'];
      }
      $url="https://altaproductividadapr.com/index.php/restserver/equiposegeneral/$idcliente/$tiporcv/0/$idg";
      $result_eq=$this->ModeloRest->consultaapiget($url);
      $data['result_eq']=$result_eq;
      if($tiporcv==1){
        $this->load->view('icha/cli_servicio_1',$data);
      }else{
        $this->load->view('icha/cli_servicio',$data);
      }
      
  }
  function savesol(){
    $params=$this->input->post();
    $params['tipo']=1;


    $url="https://altaproductividadapr.com/index.php/restserver/generalpost";
    $array=$params;
    $idventa=$this->ModeloRest->consultaapipost($url,$array);

    echo $idventa;
  }
  function cli_chat($codigo){
      $request_uri=(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $_SESSION['url_actual']=$request_uri;

    //$codigo=$params['codigo'];
      $idcliente=0;
      $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
      $arraycols=array();
      $arraycols[]=array('name'=>'uuid','value'=>$codigo,'activo'=>1);

      $array=array('tabla'=>'clientes','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
      $equipos=$this->ModeloRest->consultaapipost($url,$array);
      $data['estatus']=0;
      foreach ($equipos as $item) {
        $data['estatus']=1;
        $data['codigo']=$codigo;
        $data['idcliente']=$item['id'];
        $idcliente=$item['id'];
      }
      //$url="https://altaproductividadapr.com/index.php/restserver/equiposclientes/$idcliente";
      //$result_eq=$this->ModeloRest->consultaapiget($url);
      //$data['result_eq']=$result_eq;

      $this->load->view('icha/cli_chat',$data);
  }
  function cli_chat_select($tipo,$codigo,$nom='',$email='',$telefono=''){
      $request_uri=(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $_SESSION['url_actual']=$request_uri;

      $data['codigo']=$codigo;
      $idcliente=0;
      $tipo_dep='';
      //================================================
        if($tipo==1){ $tipo_dep='Ventas';}
        if($tipo==2){ $tipo_dep='Contratos / Arrendamiento';}
        if($tipo==3){ $tipo_dep='Pólizas';}
        if($tipo==4){ $tipo_dep='Servicio Técnico';}

      //================================================
      if($codigo==0){
        $label='Propesto';
      }else{
        $label='cliente';
        $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
        $arraycols=array();
        $arraycols[]=array('name'=>'uuid','value'=>$codigo);
        //$arraycols[]=array('name'=>'estatus','value'=>1);
        
        $array=array('tabla'=>'clientes','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
        $equipos=$this->ModeloRest->consultaapipost($url,$array);
        $data['estatus']=0;
        foreach ($equipos as $item) {
              $idcliente=$item['id'];
              $nom = $item['empresa'];
              $email = '';
        }
      }


      //=======================================
          $url_insert="https://altaproductividadapr.com/index.php/restserver/inserttable";

          $columns=array();
          $columns[]=array('name'=>'tipo','value'=>1);
          $columns[]=array('name'=>'idcliente','value'=>$idcliente);
          $columns[]=array('name'=>'departamento','value'=>$tipo);
          $columns[]=array('name'=>'nombre','value'=>$nom);
          //$columns[]=array('name'=>'correo','value'=>$email);
          //$columns[]=array('name'=>'telefono','value'=>'');
          $columns[]=array('name'=>'motivo','value'=>'');
          $columns[]=array('name'=>'codigo','value'=>$this->fecha_cod);
          $codigol=$this->fecha_cod_m.$this->generarCadenaAleatoria();
          $columns[]=array('name'=>'codigol','value'=>$codigol);

          $array=array('table'=>'chat_ini','columns'=>json_encode($columns));
          $id_result=$this->ModeloRest->consultaapipost($url_insert,$array);
          log_message('error','$id_result: '.$id_result);
          if(intval($id_result)>0){
            /*
              c_t : es el tipo de cliente 0 es nuevo,1 es un cliente existente
              idc : es el id del cliente pero si es nuevo sera el el id del resgistro 
            */
              //$cadena=$this->ModeloGeneral->generarcodigo();
            $request_uri="https://chat.altaproductividadapr.com?c_t=0&idc=$id_result&test=$codigol";
            $_SESSION['url_actual']=$request_uri;
            redirect('Icha');
            echo 1;
          }else{
            echo 0;
          }
      //=======================================
      $data['tipo_dep']=$tipo_dep;
      $data['label']=$label;
      $data['idcliente']=$idcliente;
      $data['nom']=$nom;
      $data['email']=$email;
      $data['telefono']=$telefono;
      $this->load->view('icha/cli_chat_select',$data);
  }
  function chat_no_cli(){
    $request_uri=(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $_SESSION['url_actual']=$request_uri;

    $this->load->view('icha/chat_no_cli');
  }
  function cli_estatus_ser($codigo){
    $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
      $arraycols=array();
      $arraycols[]=array('name'=>'uuid','value'=>$codigo,'activo'=>1);

      $array=array('tabla'=>'clientes','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
      $equipos=$this->ModeloRest->consultaapipost($url,$array);
      $data['estatus']=0;
      foreach ($equipos as $item) {
        $data['estatus']=1;
        $data['codigo']=$codigo;
        $data['idcliente']=$item['id'];
        $idcliente=$item['id'];
      }
      $this->load->view('icha/cli_estatus_ser',$data);
  }
  function cli_estatus_servicio(){
    $params = $this->input->post();
    $codigo = $params['codigo'];
    $idcliente = $params['idcliente'];
    $idservicio = $params['idservicio'];

    $url="https://altaproductividadapr.com/index.php/restserver/doccontrato/$idservicio/$idcliente";
    $infoser_c=$this->ModeloRest->consultaapiget($url);
    $url="https://altaproductividadapr.com/index.php/restserver/docpoliza/$idservicio/$idcliente";
    $infoser_p=$this->ModeloRest->consultaapiget($url);
    $url="https://altaproductividadapr.com/index.php/restserver/docservicio/$idservicio/$idcliente";
    $infoser_e=$this->ModeloRest->consultaapiget($url);
    $url="https://altaproductividadapr.com/index.php/restserver/getdatosventas/$idservicio/$idcliente";
    $infoser_v=$this->ModeloRest->consultaapiget($url);
    $data['idcliente']=$idcliente;
    $tipo=0;
    $data['idservicio']=$idservicio;
    $g_status=0;
    $personalid=49;
    $idser=0;
    $tecnico='';
    $fecha='';
    if(count($infoser_c)>0){
      $tipo=1;
      foreach ($infoser_c as $item) {
        if($item['asignacionId']>0){
          $g_status = $this->ModeloGeneral->get_info_status($item['g_status']);
          $personalid=$item['personalId'];
          $idser=$item['asignacionId'];
          $tecnico=$item['tecnico'];
          $fecha=$item['fecha'];
        }
      }
    }
    if(count($infoser_p)>0){
      $tipo=2;
      foreach ($infoser_p as $item) {
        if($item['asignacionId']>0){
          $g_status = $this->ModeloGeneral->get_info_status($item['g_status']);
          $personalid=$item['personalId'];
          $idser=$item['asignacionId'];
          $tecnico=$item['tecnico'];
          $fecha=$item['fecha'];
        }
      }
    }
    if(count($infoser_e)>0){
      $tipo=3;
      foreach ($infoser_e as $item) {
        if($item['asignacionId']>0){
          $g_status = $this->ModeloGeneral->get_info_status($item['g_status']);
          $personalid=$item['personalId'];
          $idser=$item['asignacionId'];
          $tecnico=$item['tecnico'];
          $fecha=$item['fecha'];
        }
      }
    }
    if(count($infoser_v)>0){
      $tipo=4;
      foreach ($infoser_v as $item) {
        if($item['asignacionId']>0){
          $g_status = $this->ModeloGeneral->get_info_status($item['g_status']);
          $personalid=$item['personalId'];
          $idser=$item['asignacionId'];
          $tecnico=$item['tecnico'];
          $fecha=$item['fecha'];
        }
      }
    }
    if(intval($idser)>0){
      
    }else{
      redirect('Icha/cli_estatus_ser/'.$codigo.'?staser=0');
    }
    $data['idser']=$idser;
    $data['tipo']=$tipo;
    $data['g_status']=$g_status;
    $data['personalid']=$personalid;
    $data['tecnico']=$tecnico;
    $data['fecha']=$fecha;


    $this->load->view('icha/cli_estatus_servicio',$data);
  }
  function newclientesave(){
    $params=$this->input->post();
      
      $url_insert="https://altaproductividadapr.com/index.php/restserver/inserttable";

      $columns=array();
      $columns[]=array('name'=>'tipo','value'=>0);
      $columns[]=array('name'=>'idcliente','value'=>0);
      $columns[]=array('name'=>'departamento','value'=>$params['servicio_c']);
      $columns[]=array('name'=>'nombre','value'=>$params['nom']);
      $columns[]=array('name'=>'correo','value'=>$params['email']);
      $columns[]=array('name'=>'telefono','value'=>$params['tel']);
      $columns[]=array('name'=>'motivo','value'=>$params['motivo']);
      $columns[]=array('name'=>'codigo','value'=>$this->fecha_cod);
      $codigol=$this->fecha_cod_m.$this->generarCadenaAleatoria();
      $columns[]=array('name'=>'codigol','value'=>$codigol);

      $array=array('table'=>'chat_ini','columns'=>json_encode($columns));
      $id_result=$this->ModeloRest->consultaapipost($url_insert,$array);
      //log_message('error','$id_result: '.$id_result);
      if(intval($id_result)>0){
        /*
          c_t : es el tipo de cliente 0 es nuevo,1 es un cliente existente
          idc : es el id del cliente pero si es nuevo sera el el id del resgistro 
        */
          //$cadena=$this->ModeloGeneral->generarcodigo();
        $request_uri="https://chat.altaproductividadapr.com?c_t=0&idc=$id_result&test=$codigol";
        $_SESSION['url_actual']=$request_uri;
        echo 1;
      }else{
        echo 0;
      }


      https://chat.altaproductividadapr.com/
  }
  function generarCadenaAleatoria($longitud = 6) {
    $caracteres = '0123456789abcdefghijklmnopqrstuvwxyz';
    $cadenaAleatoria = '';
    $maxIndex = strlen($caracteres) - 1;

    for ($i = 0; $i < $longitud; $i++) {
        $indiceAleatorio = rand(0, $maxIndex);
        $cadenaAleatoria .= $caracteres[$indiceAleatorio];
    }

    return $cadenaAleatoria;
  }
 

}