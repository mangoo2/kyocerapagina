<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accesorios extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->helper('url');
    }

  public function index(){
    $url="https://altaproductividadapr.com/index.php/restserver/accesorios";
    $data['accesoriosall']=$this->ModeloRest->consultaapiget($url);

    $url="https://altaproductividadapr.com/index.php/restserver/baner";
    $data['infobaner']=$this->ModeloRest->consultaapiget($url);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('accesoriosall',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
  }
  function articulo($idcode,$numparte){
    //$id=base64_decode($idcode);
    //$idcode=intval($idcode);
    
    //==============================================================
    $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
    $arraycols=array();
    $arraycols[]=array('name'=>'uuid','value'=>$idcode);

    $array=array('tabla'=>'catalogo_accesorios','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
    $equipos=$this->ModeloRest->consultaapipost($url,$array);
    foreach ($equipos as $item) {
      $idcode =$item['id'];
    }
    $data['equipos']=$equipos;
    //==============================================================

    $arraycols=array();
    $arraycols[]=array('name'=>'idaccesorios','value'=>$idcode);

    $array=array('tabla'=>'catalogo_accesorios_caracteristicas','cols'=>json_encode($arraycols),'orderbyname'=>'orden','orderby'=>'ASC');
    $equiposc=$this->ModeloRest->consultaapipost($url,$array);

    $data['equiposc']=$equiposc;
    //==============================================================

    $arraycols=array();
    $arraycols[]=array('name'=>'idaccesorio','value'=>$idcode);

    $array=array('tabla'=>'catalogo_accesorios_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
    $equiposimg=$this->ModeloRest->consultaapipost($url,$array);

    $data['equiposimg']=$equiposimg;
    //==============================================================


    $this->load->view('theme/pagedemo_h');
    $this->load->view('accesorioinfo',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
  }

  
 
}