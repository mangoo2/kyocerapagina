<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consumibles extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->helper('url');
    }

  public function index(){
    //==============================================================
    $url="https://altaproductividadapr.com/index.php/restserver/consumiblesall";
    $data['equipos']=$this->ModeloRest->consultaapiget($url);
    //==============================================================

    $url="https://altaproductividadapr.com/index.php/restserver/baner";
    $data['infobaner']=$this->ModeloRest->consultaapiget($url);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('consumibles',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
    $this->load->view('consumiblesjs');
  }
  public function listcon(){
    $params=$this->input->post();
    log_message('error', 'cc '.$params);

    $url="https://altaproductividadapr.com/index.php/restserver/getlistconsumibles";
    log_message('error', $params);
    //$equipos=$this->ModeloRest->consultaapipost($url,$params);
    echo json_encode($equipos);
  }
  function articulo($idcode){
    //$id=base64_decode($idcode);
    //$idcode=intval($idcode);
    $url="https://altaproductividadapr.com/index.php/restserver/consumibleinfo/$idcode";
    $equipos_c=$this->ModeloRest->consultaapiget($url);
    foreach ($equipos_c as $item) {
      $idcode=$item['id'];
    }
    $data['equipos']=$equipos_c;

    $urlc="https://altaproductividadapr.com/index.php/restserver/consumibleinfocaract/$idcode";
    $data['equiposc']=$this->ModeloRest->consultaapiget($urlc);

    $urlc="https://altaproductividadapr.com/index.php/restserver/consumibleimg/$idcode";
    $data['equiposimg']=$this->ModeloRest->consultaapiget($urlc);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('consumibleinfo',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
    $this->load->view('consumibleinfojs');
  }
  
 
}