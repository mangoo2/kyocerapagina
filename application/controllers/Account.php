<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        if($this->session->userdata('logeado')==true){
            $this->idemp = $this->session->userdata('idemp');
            $this->emp = $this->session->userdata('emp');
        }else{
            $this->idemp=0;
            $this->emp='';
        }
    }

  public function index(){
    redirect('Inicio');
  }
  function login(){
    $params = $this->input->post();
    $user=$params['user'];
    $pass=$params['password'];
    $url="https://altaproductividadapr.com/index.php/restserver/sisuser";//gerardo@mangoo.com.mx admin123
    $array=array('user'=>$user);
    $results=$this->ModeloRest->consultaapipost($url,$array);
    $count=0;
    foreach ($results as $item) {
        log_message('error',$user);
        log_message('error',$pass);
        $verificar = password_verify($pass,$item['pass']);
        if($verificar){
          $count=1;
          $data = array(
                        'logeado' => true,
                        'idemp'=>$item['id'],
                        'emp'=>$item['empresa'],
                        'correo'=>$user,
                        'uuidemp'=>$item['uuid'],

                      );
          unset($_SESSION['pro']);
          $_SESSION['pro']=array(); 
          unset($_SESSION['can']);
          $_SESSION['can']=array(); 

          unset($_SESSION['cotpro']);
          $_SESSION['cotpro']=array(); 
          unset($_SESSION['cotcan']);
          $_SESSION['cotcan']=array(); 

          $_SESSION['errorlogin']=1;  
        }
    }
    if($count==0){
      $data = array('logeado' => false);
      $_SESSION['errorlogin']=0;
    }
    $this->session->set_userdata($data);
    //redirect('Inicio');
    if(isset($params['lses'])){
      echo $count;
    }else{
      redirect('Inicio');
    }
  
  }
  function exit(){
    $this->session->sess_destroy();
    redirect(base_url(), 'refresh');
  }
  function addcar(){
    $params = $this->input->post();
    $typeitem = $params['typeitem'];
    $iditem = $params['iditem'];
    $urlitem = $params['urlitem'];
    $cant = $params['am'];

    /*
    $oProducto=array("typeitem"=>$typeitem,"iditem"=>$iditem);

    if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
            $idx = array_search($oProducto, $_SESSION['pro']);
            $_SESSION['can'][$idx]+=$cant;
    }
    else{ //sino lo agrega
        array_push($_SESSION['pro'],$oProducto);
        array_push($_SESSION['can'],$cant);
    }
    */
    //============================Insert=======================
      $url_insert="https://altaproductividadapr.com/index.php/restserver/inserttable";
      $columns=array();
      $columns[]=array('name'=>'clienteid','value'=>$this->idemp);
      $columns[]=array('name'=>'typeitem','value'=>$typeitem);
      $columns[]=array('name'=>'iditem','value'=>$iditem);
      $columns[]=array('name'=>'cant','value'=>1);

      $array=array('table'=>'cliente_carrito','columns'=>json_encode($columns));
      $this->ModeloRest->consultaapipost($url_insert,$array);
    //===================================================


    redirect($urlitem);
  }
  function clearprocard(){
    $this->ModeloConsultas->deletecarrito();
    /*
    unset($_SESSION['pro']);
        $_SESSION['pro']=array();
    unset($_SESSION['can']);
        $_SESSION['can']=array();
        redirect('Inicio');
    */
    redirect('Inicio');
  }
  function deletepro(){
    $params = $this->input->post();
    $row = $params['row'];
    /*
    unset($_SESSION['pro'][$row]);
    unset($_SESSION['can'][$row]);
    $_SESSION['pro'] = array_values($_SESSION['pro']); 
    $_SESSION['can'] = array_values($_SESSION['can']); 
    */
    //============================Delete item=======================
      $url_insert="https://altaproductividadapr.com/index.php/restserver/deletecarrito";
      $columns=array();
      $columns[]=array('name'=>'id','value'=>$row);

      //$array=array('table'=>'cliente_carrito','columns'=>json_encode($columns));
      $array=array('columns'=>json_encode($columns));
      $this->ModeloRest->consultaapipost($url_insert,$array);
    //===================================================
  }
  function down_up(){
    $params = $this->input->post();
    $type=$params['type'];
    $idx=$params['row'];
    if($type=='down'){
      //$_SESSION['can'][$idx]-=1;
      $this->ModeloConsultas->update_cant_carrito(1,'-',$idx);
    }
    if($type=='up'){
      //$_SESSION['can'][$idx]+=1;
      $this->ModeloConsultas->update_cant_carrito(1,'+',$idx);
    }
  }
  //================= cotizaciones =====================
    function addcar_c(){
      $params = $this->input->post();
      $typeitem = $params['typeitem'];
      $iditem = $params['iditem'];
      $cant = $params['am'];
      log_message('error','addcar_c typeitem:'.$typeitem.' iditem:'.$iditem);

      $oProducto=array("typeitem"=>$typeitem,"iditem"=>$iditem);

      if(in_array($oProducto, $_SESSION['cotpro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
              $idx = array_search($oProducto, $_SESSION['cotpro']);
              $_SESSION['cotcan'][$idx]+=$cant;
              log_message('error','ya se encuentra');
      }
      else{ //sino lo agrega
          array_push($_SESSION['cotpro'],$oProducto);
          array_push($_SESSION['cotcan'],$cant);
          log_message('error','se crea');
      }

      log_message('error',json_decode($_SESSION['cotpro']));
    }
    function clearprocard_c(){
      unset($_SESSION['cotpro']);
          $_SESSION['cotpro']=array();
      unset($_SESSION['cotcan']);
          $_SESSION['cotcan']=array();
          //redirect('Inicio');
    }
    function deletepro_c(){
      $params = $this->input->post();
      $row = $params['row'];
      unset($_SESSION['cotpro'][$row]);
      unset($_SESSION['c_can'][$row]);
      $_SESSION['cotpro'] = array_values($_SESSION['cotpro']); 
      $_SESSION['cotcan'] = array_values($_SESSION['cotcan']); 
    }
    function down_up_c(){
      $params = $this->input->post();
      $type=$params['type'];
      $idx=$params['row'];
      if($type=='down'){
        $_SESSION['cotcan'][$idx]-=1;
      }
      if($type=='up'){
        $_SESSION['cotcan'][$idx]+=1;
      }
    }
  //====================================================

  
  
 
}