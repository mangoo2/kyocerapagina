<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cotizacion extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->idemp = $this->session->userdata('idemp');
        $this->emp = $this->session->userdata('emp');
    }

  public function index(){
    $data['idemp']=$this->idemp;
    $data['emp']=$this->emp;

    $this->load->view('theme/pagedemo_h');
    $this->load->view('cotizacion',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
    $this->load->view('cotizacionjs');
  }
  function realizarcot(){
    $params = $this->input->post();
    $arraypro = $params['arraypro'];
    $idemp    = $params['idemp'];
    $DATAe = json_decode($arraypro);

    //============================================================
      $arraypro=array();
      $prorow=0;
      for ($i=0;$i<count($DATAe);$i++) {
        $arraypro[]=array('tipo'=>$DATAe[$i]->tipo,'item'=>$DATAe[$i]->item,'can'=>$DATAe[$i]->cant);
      }

    //============================================================
      $url="https://altaproductividadapr.com/index.php/restserver/addcotizacion";
      $array=array('idemp'=>$idemp,'pro'=>json_encode($arraypro));
      $this->ModeloRest->consultaapipost($url,$array);
  }

  
 
}