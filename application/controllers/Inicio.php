<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        
        /*$this->load->model('ModeloIdiomas');
        if(!$this->session->userdata('idioma')){
    			$this->idioma=1;
    		}elseif($this->session->userdata('idioma')==0){
    			$this->idioma=0;
    		}else{
    			$this->idioma=1;
    		}*/
    }

	public function index(){
		//$result = $this->ModeloIdiomas->idioma($this->idioma);
		//$data['textoesen']=$result;
    $url="https://altaproductividadapr.com/index.php/restserver/baner";
    $data['infobaner']=$this->ModeloRest->consultaapiget($url);

    $url="https://altaproductividadapr.com/index.php/restserver/productosdestacado";
    $data['prodestacado']=$this->ModeloRest->consultaapiget($url);
    $this->load->view('theme/pagedemo_h');
		$this->load->view('inicio',$data);//,$data);
    $this->load->view('theme/pagedemo_f');
		$this->load->view('iniciojs');

	}
	function cambioidioma(){
		$idioma = $this->input->post('idioma');//0 ingles 1 español
		$data = array(
                        'idioma' => $idioma
                    );
		$this->session->set_userdata($data);
	}
    

  function enviar(){
  	$nombre = $this->input->post('nombre');
  	$correo = $this->input->post('correo');
  	$telefono = $this->input->post('telefono');
  	$comentario = $this->input->post('comentario');
      //================================
          //cargamos la libreria email
          $this->load->library('email');
          /*
          * Configuramos los parámetros para enviar el email,
          * las siguientes configuraciones es recomendable
          * hacerlas en el fichero email.php dentro del directorio config,
          * en este caso para hacer un ejemplo rápido lo hacemos 
          * en el propio controlador
          */
          
          //Indicamos el protocolo a utilizar
          $config['protocol'] = 'smtp';
           
          //El servidor de correo que utilizaremos
          $config["smtp_host"] ='mail.redapol.com'; 
           
          //Nuestro usuario
          $config["smtp_user"] = 'contacto@redapol.com';

          //Nuestra contraseña
          $config["smtp_pass"] = 'bbRpczbLHHPv';

          //Puerto
          $config["smtp_port"] = '465';

          $config["smtp_crypto"] = 'ssl';
                  
          //El juego de caracteres a utilizar
          $config['charset'] = 'utf-8'; 
   
          //Permitimos que se puedan cortar palabras
          $config['wordwrap'] = TRUE;
           
          //El email debe ser valido  
          $config['validate'] = true;

          $config['mailtype'] = 'html';

          //Establecemos esta configuración
          $this->email->initialize($config);
   
          //Ponemos la dirección de correo que enviará el email y un nombre
          $this->email->from('contacto@redapol.com','REDAPOL');

           
            /*
             * Ponemos el o los destinatarios para los que va el email
             * en este caso al ser un formulario de contacto te lo enviarás a ti
             * mismo
             */
      //======================
      $this->email->to('leon1255555@gmail.com','contacto');
      //$this->email->bcc('contacto@redapol.com');
      $this->email->bcc('soporte@mangoo.mx');
      //$this->email->to('andres@mangoo.mx',$paciente);
      $asunto='CONTACTO REDAPOL';

    //Definimos el asunto del mensaje
      $this->email->subject($asunto);
       
    //Definimos el mensaje a enviar
    //$this->email->message($body)
      $message  = "<div>
                    <div style='background: black; border: 1px solid #F8F8F8; border-radius: 15px; padding: 25px; width: 60%; margin-left: auto; margin-right: auto; position: absolute; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                    -moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                    box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);'>
                      <table style='width: 100%;'>
                         <thead>
                          <tr>
                              <th colspan='4' align='center'>
                                <img src='".base_url()."public/img/L001.png' style='width: 250px;'>
                              </th>
                          </tr>
                          <tr>
                            <th>
                              <br>
                            </th>
                          </tr>
                          <tr>
                            <th colspan='4' style='font-family: sans-serif; font-size:23px; color: white; -webkit-print-color-adjust: exact;' align='left'>Nombre: ".$nombre."<span style='font-size:10px;'><br><br></span>
                            </th>
                          </tr>
                          <tr>
                            <th colspan='4' style='font-family: sans-serif; font-size:23px; color: white; -webkit-print-color-adjust: exact;' align='left'>Correo: ".$correo."<span style='font-size:10px;'><br><br></span>
                            </th>
                          </tr>
                          <tr>
                            <th colspan='4' style='font-family: sans-serif; font-size:23px; color: white; -webkit-print-color-adjust: exact;' align='left'>Teléfono: ".$telefono."<span style='font-size:10px;'><br><br></span>
                            </th>
                          </tr>
                          <tr>
                            <th colspan='4' style='font-family: sans-serif; font-size:23px; color: white; -webkit-print-color-adjust: exact;' align='left'>Asunto: ".$comentario." 
                            </th>
                          </tr>
                        </thead>
                    </table>    
                    </div>
                </div>";


      $this->email->message($message);

      //Enviamos el email y si se produce bien o mal que avise con una flasdata
      if($this->email->send()){
          //$this->session->set_flashdata('envio', 'Email enviado correctamente');
      }else{
          //$this->session->set_flashdata('envio', 'No se a enviado el email');
      }
      //==================
  }

  public function correo(){
		$this->load->view('correo');
	}
   
  function quienes_somos(){
    $data['data']='';
    $this->load->view('theme/pagedemo_h');
    $this->load->view('quienessomos',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
  }

  function servicios(){
    $data['data']='';
    $this->load->view('theme/head');
    $this->load->view('servicios',$data);
    $this->load->view('theme/footer');
    $this->load->view('iniciojs');
  }

  function rentaequipos(){
    $data['data']='';



    //==============================================================
      $equipos=$this->ModeloConsultas->list_eq_arrendamiento();
      $data['lis_equi']=$equipos;
    //=======================================================
    $this->load->view('theme/pagedemo_h');
    $this->load->view('rentaequipos',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('rentaequiposjs');
  }

  function contacto(){
    $data['data']='';
    $this->load->view('theme/pagedemo_h');
    $this->load->view('contacto',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
  }
  /*
  function consultaapi($url){
    $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $login='admin';
        $password='123Admin.';
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
        //================================================
        // para consultar sin ssl
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //================================================
        
        $res = curl_exec($ch);
        if(curl_errno($ch)){
          echo curl_error($ch);
        }else{
          $decoded = json_decode($res,true);
          //echo $decoded;
        }
        curl_close($ch);
    return $decoded;
  }
  */
  function pruebas(){
    //==============================================================
    $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
    $arraycols=array();
    $arraycols[]=array('name'=>'id','value'=>55);
    //$arraycols[]=array('name'=>'status','value'=>1);

    $array=array('tabla'=>'catalogo_accesorios','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
    $equipos=$this->ModeloRest->consultaapipost($url,$array);
    var_dump($equipos);
    //log_message('error',$equipos);
    //$data['equipos']=$equipos;
    //==============================================================
  }
  function pagedemo(){
    $url="https://altaproductividadapr.com/index.php/restserver/baner";
    $data['infobaner']=$this->ModeloRest->consultaapiget($url);

    $url="https://altaproductividadapr.com/index.php/restserver/productosdestacado";
    $data['prodestacado']=$this->ModeloRest->consultaapiget($url);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('pagedemo',$data);
    
    $this->load->view('theme/pagedemo_b');
    $this->load->view('theme/pagedemo_f');
  }
  function prueba($text){
    echo $text;
  }
  function search(){
    $pro = $this->input->get('search');

    $url="https://altaproductividadapr.com/index.php/restserver/searchproductos/$pro";
    $equiposresult=$this->ModeloRest->consultaapiget($url);
        //echo $results;
        echo json_encode($equiposresult);
  }
  function consumibles(){
    if($this->session->userdata('logeado')==true){}else{
      //redirect('Inicio'); 
    }
    $data['data']='';
    //==============================================================
      $equipos=$this->ModeloConsultas->list_equipos();
      $data['lis_equi']=$equipos;
    //=======================================================
    $this->load->view('theme/pagedemo_h');
    $this->load->view('consu/index',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('consu/indexjs');
  }
  function consumiblesxequipo(){
    $params = $this->input->post();
    $idequipo = $params['ideq'];

    $result = $this->ModeloConsultas->list_consumibles_equipos($idequipo);
    $html='';
    foreach ($result as $item) {
        //===============================================
            $idequipo = $item['id'];
                  $span_tipo_m='<button style="background: url('.base_url().'public/img/eq_mono.png);background-repeat: no-repeat;background-size: 100%;"></button> Imp Mono';//1
                  $span_tipo_c='<button style="background: url('.base_url().'public/img/eq_color.jpg);background-repeat: no-repeat;background-size: 100%;"></button> Imp Color';//2
                  //$span_tipo_h='<button style="background: url('.base_url().'public/img/eq_hib.jpg);background-repeat: no-repeat;background-size: 100%;"></button> Imp Hibrida';//3
                 
                      $tipo_c=1;
                  if($item['tipo']==1){//black
                    $tipo_c=1;
                  }
                  if($item['tipo']==2){//c
                    $tipo_c=2;
                  }
                  if($item['tipo']==3){//m
                    $tipo_c=2;
                  }
                  if($item['tipo']==4){//y
                    $tipo_c=2;
                  }
                  if($item['tipo']==5){//mk
                    $tipo_c=1;
                  }

                  $html.='<div class="impresoras imp_cat_'.$tipo_c.' imp_id_'.$item['id'].'">';
                    $html.='<div class="card_eq_contect">';
                      $html.='<div class="p_cont">';
                        $html.='<div class="price bshadow">';
                          $html.='<div class="p_c_1">';
                            $html.='$ '.number_format(($item['general']*1.16),2,'.',',');
                          $html.='</div>';
                          $html.='<div class="p_c_2">';
                            $html.='Renta mensual +IVA';
                          $html.='</div>';
                        $html.='</div>';
                      $html.='</div>';
                      $html.='<div class="p_mod bshadow">'.$item['modelo'].'</div>';
                      $html.='<div class="card_eq_body bshadow">';
                        $html.='<div class="eq_c_1">';
                          $html.='<div class="img_modelo" style="background:url('.base_url().'public/img/toner.png);background-repeat: no-repeat;background-size: 70%;background-position: center;"></div>';
                          $html.='<div class="eq_t_mc">';
                              if($tipo_c==1){
                                $html.=$span_tipo_m;
                              }
                              if($tipo_c==2){
                                $html.=$span_tipo_c;
                              }
                          $html.='</div>';
                        $html.='</div>';
                        /*
                        $html.='<div class="eq_c_2">';
                          $html.='<div><div class="btn_i_red2">Incluye</div></div>';
                          $html.='<div>';
                            $html.='<ul>';
                              $html.='<li>Tóner, consumibles y refacciones.</li>';
                              $html.='<li>Un servicio mensual preventivo.</li>';
                              $html.='<li>Servicios correctivos ilimitados.</li>';
                              $html.='<li>Tiempo de respuesta 6 hrs. máximo.</li>';
                              $html.='<li>Capacitación de uso del equipo.</li>';
                              $html.='<li>Servicio técnico certificado por la marca.</li>';
                            $html.='</ul>';
                          $html.='</div>';
                          $html.='<div><div class="btn_i_red2">Extras</div></div>';
                          $html.='<div>';
                            $html.='<ul>';
                              $html.='<li>(excedente monocromático o color)</li>';
                              $html.='<li>‍‍Condiciones</li>';
                            $html.='</ul>';
                          $html.='</div>';
                          $html.='<div><div class="btn_i_red2">Condiciones</div></div>';
                          $html.='<div>';
                            $html.='<ul>';
                              $html.='<li>Contrato por 12, 24 o 36 meses</li>';
                              $html.='<li>(renta en depósito en garantía)</li>';
                            $html.='</ul>';
                          $html.='</div>';
                        $html.='</div>';
                        */
                        $html.='<div class="footer_eq">';
                          $html.='<div class="btns-infos">';
                            //$html.='<button class="btn_i_red" onclick="view_dll('.$item['id'].',1)"><i class="fa fa-plus"></i> Info</button>';
                          $html.='</div>';
                          $html.='<div class="cinta_espe"></div>';
                          $html.='<div class="cinta_espe0">';
                            $html.='<div class="cinta_espe2">Precio especial en línea</div>';
                          $html.='</div>';
                        $html.='</div>';
                        

                        
                      $html.='</div>';
                      $html.='<div class="p_cont">';
                          $html.='<div class="cotiza bshadow">Solicitar consumible</div>';
                        $html.='</div>';
                    $html.='</div>';
                  $html.='</div>';
        //===============================================
    }
    echo $html;
  }

}