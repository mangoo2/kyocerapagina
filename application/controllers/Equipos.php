<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipos extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->helper('url');
    }

  public function index(){
    $this->load->view('theme/head');
    $this->load->view('inicio2');
    $this->load->view('theme/footer');
    $this->load->view('iniciojs');
  }
  function Monocromaticos(){
    $url="https://altaproductividadapr.com/index.php/restserver/productosmono";
    $data['equipos']=$this->ModeloRest->consultaapiget($url);

    $url="https://altaproductividadapr.com/index.php/restserver/baner";
    $data['infobaner']=$this->ModeloRest->consultaapiget($url);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('equipos',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
  }
  function Color(){
    $url="https://altaproductividadapr.com/index.php/restserver/productoscolor";
    $data['equipos']=$this->ModeloRest->consultaapiget($url);
    
    $url="https://altaproductividadapr.com/index.php/restserver/baner";
    $data['infobaner']=$this->ModeloRest->consultaapiget($url);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('equipos',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
  }
  function Multifuncional_monocromatico(){
    $url="https://altaproductividadapr.com/index.php/restserver/productosmmono";
    $data['equipos']=$this->ModeloRest->consultaapiget($url);
    
    $url="https://altaproductividadapr.com/index.php/restserver/baner";
    $data['infobaner']=$this->ModeloRest->consultaapiget($url);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('equipos',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
  }
  function Multifuncional_de_color(){
    $url="https://altaproductividadapr.com/index.php/restserver/productosmcolor";
    $data['equipos']=$this->ModeloRest->consultaapiget($url);
    
    $url="https://altaproductividadapr.com/index.php/restserver/baner";
    $data['infobaner']=$this->ModeloRest->consultaapiget($url);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('equipos',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
  }
  function Multifuncional_Hibrido_de_color(){
    $url="https://altaproductividadapr.com/index.php/restserver/productosmhibrida";
    $data['equipos']=$this->ModeloRest->consultaapiget($url);
    
    $url="https://altaproductividadapr.com/index.php/restserver/baner";
    $data['infobaner']=$this->ModeloRest->consultaapiget($url);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('equipos',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
  }
  function articulo_equipo($idcode){
    //$id=base64_decode($idcode);
    //$idcode=intval($idcode);
    $url="https://altaproductividadapr.com/index.php/restserver/productosinfo/$idcode";
    $equiposresult=$this->ModeloRest->consultaapiget($url);
    $data['equipos']=$equiposresult;
    //var_dump($equiposresult);
    foreach ($equiposresult as $item) {
      $idequipo=$item['id'];
    }

    $urlc="https://altaproductividadapr.com/index.php/restserver/productosinfocaract/$idequipo";
    $data['equiposc']=$this->ModeloRest->consultaapiget($urlc);

    $urlc="https://altaproductividadapr.com/index.php/restserver/productosimg/$idequipo";
    $data['equiposimg']=$this->ModeloRest->consultaapiget($urlc);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('equiposinfo',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
    $this->load->view('equiposinfojs');
  }
  /*
  function prueba(){
    $prueba=1;
      $url="https://altaproductividadapr.com/index.php/restserver/productosmono";
      $url="https://altaproductividadapr.com/index.php/Restserver/productoss";
      //$url="https://reqres.in/api/users?page=2";
    if($prueba==0){
      $url="https://altaproductividadapr.com/index.php/restserver/productosmono";
      $ch2 = curl_init();
      //curl_setopt($ch2, CURLOPT_HEADER, false); 
      //curl_setopt($ch2, CURLOPT_HTTPHEADER, $autori );
      curl_setopt($ch2, CURLOPT_URL, $url); 
      curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true); 
      curl_setopt($ch2, CURLOPT_POST, 1 );
      //curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($parametros));
      $res = curl_exec($ch2); 
      if(curl_errno($ch2)){
          echo curl_error($ch2);
        }else{
          $decoded = json_decode($res,true);
          var_dump($decoded);
        }
      curl_close($ch2);
      $ddata = json_decode($res);
    }
    if($prueba==1){
      $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        if(curl_errno($ch)){
          echo curl_error($ch);
        }else{
          $decoded = json_decode($res,true);
          //$decoded = json_encode($res,true);
          //echo $decoded;
          var_dump($decoded);
        }
        curl_close($ch);
    }
    if($prueba==2){
      $Usuario='admin'; //datos de prueba
      $Password='1234'; //datos de prueba
      $jsonData = array(
        'Username'=> $Usuario,
        'Password' => $Password
      );
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'
      ));
      curl_setopt($ch, CURLOPT_URL, "http://altaproductividadapr.com/index.php/restserver/productos" );
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
      //curl_setopt($ch, CURLOPT_POST, 1 );
      //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonData));
      $result=curl_exec ($ch);
      if(curl_errno($ch)){
          echo curl_error($ch);
        }else{
          $decoded = json_decode($result,true);
          var_dump($decoded);
        }
      //echo $result;
      curl_close($ch);
      //log_message('error', 'result: '.$result);
      
      //$token = json_encode($data->token);
      //$this->session->set_userdata("token_ws",str_replace('"', '',$token));
      //log_message('error', 'token: '.$token);
    }
  }
  function consultaapi($url){
    $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $login='admin';
        $password='123Admin.';
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
        //================================================
        // para consultar sin ssl
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //================================================
        
        $res = curl_exec($ch);
        if(curl_errno($ch)){
          echo curl_error($ch);
        }else{
          $decoded = json_decode($res,true);
          //echo $decoded;
        }
        curl_close($ch);
    return $decoded;
  }
  */
  
 
}