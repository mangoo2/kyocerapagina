<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mi_perfil extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloGeneral');
        if($this->session->userdata('logeado')==true){
            $this->idemp = $this->session->userdata('idemp');
            $this->emp = $this->session->userdata('emp');
            $this->correo = $this->session->userdata('correo');
            $this->uuidemp=$this->session->userdata('uuidemp');
            //$this->load->library('openpay');
            //$this->openpay = Openpay::getInstance('mzuolrwtlcn4ttmv4jvz', 'sk_552c80a7da564eee8200ffd8214b0c70', 'MX');
        }else{
            redirect('Inicio'); 
        }
    }

  public function index(){
    $data['idemp']=$this->idemp;
    $data['emp']=$this->emp;

    $url="https://altaproductividadapr.com/index.php/restserver/optencionserviciosclinte/$this->idemp/3";
    $data['servicioslimit']=$this->ModeloRest->consultaapiget($url);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('miperfil/miperfil',$data);
    $this->load->view('theme/pagedemo_f');
    //$this->load->view('iniciojs');
    //$this->load->view('cotizacionjs');
  }
  function servicios(){
    $data['idemp']=$this->idemp;
    $data['emp']=$this->emp;
    $url="https://altaproductividadapr.com/index.php/restserver/optencionserviciosclinte/$this->idemp/0";
    $data['serviciosall']=$this->ModeloRest->consultaapiget($url);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('miperfil/servicios',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('miperfil/serviciosjs');
  }
  function servicios_doc($id_servicio,$tipo){
    if($tipo==1){
      $url="https://altaproductividadapr.com/index.php/restserver/doccontrato/$id_servicio/0";
      $infoser=$this->ModeloRest->consultaapiget($url);
      $data['infoser']=$infoser;

      $this->load->view('reportes/prefactura_servicio_c',$data);
    }
    if($tipo==2){
      $url="https://altaproductividadapr.com/index.php/restserver/docpoliza/$id_servicio/0";
      $infoser=$this->ModeloRest->consultaapiget($url);
      $data['infoser']=$infoser;

      $this->load->view('reportes/prefactura_servicio_p',$data);
    }
    if($tipo==3){
      $url="https://altaproductividadapr.com/index.php/restserver/docservicio/$id_servicio/0";
      $infoser=$this->ModeloRest->consultaapiget($url);
      $data['infoser']=$infoser;

      $this->load->view('reportes/prefactura_servicio_e',$data);
    }
    if($tipo==4){
      $url="https://altaproductividadapr.com/index.php/restserver/getdatosventas/$id_servicio/0";
      $infoser=$this->ModeloRest->consultaapiget($url);
      $data['infoser']=$infoser;

      $this->load->view('reportes/prefactura_servicio_v',$data);
    }
  }
  

  
 
}