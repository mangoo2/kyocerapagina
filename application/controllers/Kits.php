<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kits extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->url_consult="https://altaproductividadapr.com/index.php/restserver/consultatabla";
    }

  public function index(){
    $url="https://altaproductividadapr.com/index.php/restserver/refaccionesall";
    $data['kitsall']=$this->ModeloRest->consultaapiget($url);



    $url="https://altaproductividadapr.com/index.php/restserver/baner";
    $data['infobaner']=$this->ModeloRest->consultaapiget($url);

    $this->load->view('theme/pagedemo_h');
    $this->load->view('kitsall',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
    $this->load->view('kitsalljs');
  }
  function articulo($idcode,$numparte){
    //$id=base64_decode($idcode);
    //$idcode=intval($idcode);
    //log_message('error',$idcode);
    //==============================================================
    $url="https://altaproductividadapr.com/index.php/restserver/consultatabla";
    $arraycols=array();
    $arraycols[]=array('name'=>'uuid','value'=>$idcode);

    $array=array('tabla'=>'refacciones','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
    $equipos=$this->ModeloRest->consultaapipost($url,$array);
    //var_dump($equipos);
    foreach ($equipos as $item) {
      $idcode=$item['id'];
    }


    //=======================================================
      $arraycols=array();
      $arraycols[]=array('name'=>'refacciones_id','value'=>$idcode);

      $array=array('tabla'=>'refacciones_costos','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
      $costo_ref=$this->ModeloRest->consultaapipost($url,$array);
      //var_dump($equipos);
      $general=0;
      foreach ($costo_ref as $item) {
        $general=$item['general'];
      }
      if($general>0){
        $general=round(($general*1.16),2);
      }
      $data['general']=$general;
    //=======================================================
    $data['equipos']=$equipos;
    //==============================================================

    $arraycols=array();
    $arraycols[]=array('name'=>'idrefaccion','value'=>$idcode);

    $array=array('tabla'=>'refacciones_caracteristicas','cols'=>json_encode($arraycols),'orderbyname'=>'orden','orderby'=>'ASC');
    $equiposc=$this->ModeloRest->consultaapipost($url,$array);

    $data['equiposc']=$equiposc;
    //==============================================================

    $arraycols=array();
    $arraycols[]=array('name'=>'idrefaccion','value'=>$idcode);

    $array=array('tabla'=>'refacciones_imgs','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
    $equiposimg=$this->ModeloRest->consultaapipost($url,$array);

    $data['equiposimg']=$equiposimg;
    //==============================================================


    $this->load->view('theme/pagedemo_h');
    $this->load->view('refaccionesinfo',$data);
    $this->load->view('theme/pagedemo_f');
    $this->load->view('iniciojs');
  }
  function getListado(){
    $url="https://altaproductividadapr.com/index.php/restserver/refaccionesall";
    $kitsall=$this->ModeloRest->consultaapiget($url);

    $json_data = array("data" => $kitsall);
    echo json_encode($json_data);
  }

  
 
}