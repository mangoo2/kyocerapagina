<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloGeneral extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->fecha_cod = date('Ymd');
    }

    function get_info_status($numeros){
        $numeros = explode(",", $numeros);
        $conteo = array_count_values($numeros);
        $status_num=0;
        // Verificar si todos los números son iguales a 2
        if (isset($conteo[2]) && count($numeros) == $conteo[2]) {
            //echo "Todos los números son iguales a 2.";
            $status_num=2;
        } 
        // Verificar si todos los números son ceros
        elseif (isset($conteo[0]) && count($numeros) == $conteo[0]) {
            //echo "Todos los números son ceros.";
            $status_num=0;
        } 
        // Verificar si al menos uno de los números es 2
        elseif (isset($conteo[2])) {
            //echo "Al menos uno de los números es 2.";
            $status_num=1;
        } 
        else {
            //echo "No todos los números son iguales a 2, no todos son ceros y no hay ninguno que sea 2.";
            $status_num=0;
        }
        return $status_num;
    }
    function get_info_status_nr($numeros){
        $numeros = explode(",", $numeros);
        $conteo = array_count_values($numeros);
        $status_num=0;
        // Verificar si todos los números son iguales a 1
        if (isset($conteo[1]) && count($numeros) == $conteo[1]) {
            //echo "Todos los números son iguales a 1.";
            $status_num=1;
        } 
        
        else {
            //echo "No todos los números son iguales a 1, no todos son ceros y no hay ninguno que sea 1.";
            $status_num=0;
        }
        return $status_num;
    }
    function generarcodigo(){
        //$cadena = date('Ymd');
        $cadena = $this->fecha_cod;
        //echo $cadena . '<br>';

        do {
            $cadenaEncriptada = password_hash($cadena, PASSWORD_BCRYPT);
        } while (strpos($cadenaEncriptada, '/') !== false || strpos($cadenaEncriptada, '+') !== false || strpos($cadenaEncriptada, '&') !== false || strpos($cadenaEncriptada, '.') !== false);//esta parte es para descartar entre la cadena estos caracteres

        return $cadenaEncriptada;
    }
}

?>