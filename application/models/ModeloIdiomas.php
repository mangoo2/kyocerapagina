<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloIdiomas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function idioma($ididioma){
        if($ididioma==0){
            $menu1='Directors';
        }else{
            $menu1='Directores';
        }
    	$array=array(
                    'menu1'=>$menu1
                    );
	    return $array; 
    }

   	public function getTiposCambio()
   	{
   		$sql = "SELECT * FROM tiposCambio";
        $query = $this->db->query($sql);
        return $query->result();
   	}
}

?>