<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloConsultas extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->fecha_cod = date('Ymd');
        $this->load->model('ModeloRest');
        if($this->session->userdata('logeado')==true){
            $this->idemp = $this->session->userdata('idemp');
            $this->emp = $this->session->userdata('emp');
        }else{
            $this->idemp=0;
            $this->emp='';
        }
        $this->url_consultatabla="https://altaproductividadapr.com/index.php/restserver/consultatabla";
        $this->url_update="https://altaproductividadapr.com/index.php/restserver/updatetable";
    }
    function listado_carrito(){
        $url=$this->url_consultatabla;
        $arraycols=array();
        $arraycols[]=array('name'=>'clienteid','value'=>$this->idemp);

        $array=array('tabla'=>'cliente_carrito','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
        $equipos=$this->ModeloRest->consultaapipost($url,$array);

        return $equipos;
    }
    function update_cant_carrito($cant,$masmenos,$id){
        log_message('error','update_cant_carrito');
        $url_update="https://altaproductividadapr.com/index.php/restserver/updatecantcarrito";

        $array=array('cant'=>$cant,'masmenos'=>$masmenos,'id'=>$id);
        $this->ModeloRest->consultaapipost($url_update,$array);
    }
    function deletecarrito(){
        //============================Delete all=======================
        $url_insert="https://altaproductividadapr.com/index.php/restserver/deletecarrito";
        $columns=array();
        $columns[]=array('name'=>'clienteid','value'=>$this->idemp);

        //$array=array('table'=>'cliente_carrito','columns'=>json_encode($columns));
        $array=array('columns'=>json_encode($columns));
        $this->ModeloRest->consultaapipost($url_insert,$array);
        //===================================================
    }
    function list_eq_arrendamiento(){
        $url=$this->url_consultatabla;
        $arraycols=array();
        $arraycols[]=array('name'=>'paginaweb','value'=>1);
        $arraycols[]=array('name'=>'pw_rentas','value'=>1);
        $arraycols[]=array('name'=>'estatus','value'=>1);

        $array=array('tabla'=>'equipos','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
        $result=$this->ModeloRest->consultaapipost($url,$array);
        return $result;
    }
    function equipo_caracteristica($idequipo){
        $url=$this->url_consultatabla;
        $arraycols=array();
        $arraycols[]=array('name'=>'idequipo','value'=>$idequipo);
        $arraycols[]=array('name'=>'activo','value'=>1);

        $array=array('tabla'=>'equipos_caracteristicas','cols'=>json_encode($arraycols),'orderbyname'=>'orden','orderby'=>'ASC');
        $result=$this->ModeloRest->consultaapipost($url,$array);
        return $result;
    }
    function list_equipos(){
        $url=$this->url_consultatabla;
        $arraycols=array();
        $arraycols[]=array('name'=>'paginaweb','value'=>1);
        //$arraycols[]=array('name'=>'pw_rentas','value'=>1);
        $arraycols[]=array('name'=>'estatus','value'=>1);

        $array=array('tabla'=>'equipos','cols'=>json_encode($arraycols),'orderbyname'=>'','orderby'=>'x');
        $result=$this->ModeloRest->consultaapipost($url,$array);
        return $result;
    }
    function list_consumibles_equipos($idequipo){
        $url="https://altaproductividadapr.com/index.php/restserver/getconsumiblesequipo/$idequipo";
        $infoser=$this->ModeloRest->consultaapiget($url);
        return $infoser;
    }
    
}

?>