<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloRest extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function consultaapiget($url){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $login='admin';
        $password='123Admin.';
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
        //================================================
        // para consultar sin ssl
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //================================================
        
        $res = curl_exec($ch);
        if(curl_errno($ch)){
          echo curl_error($ch);
        }else{
          $decoded = json_decode($res,true);
          //echo $decoded;
        }
        curl_close($ch);
        return $decoded;
    }
    public function consultaapipost($url,$array){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $login='admin';
        $password='123Admin.';
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");

        //===============================================
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array );
        //================================================
        // para consultar sin ssl
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //================================================
        
        $res = curl_exec($ch);
        if(curl_errno($ch)){
          echo curl_error($ch);
        }else{
          $decoded = json_decode($res,true);
          //echo $decoded;
        }
        curl_close($ch);
        return $decoded;
    }
}

?>