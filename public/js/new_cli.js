var base_url=$('#base_url').val();
/*
document.addEventListener('DOMContentLoaded', function() {
        const checkboxes = document.querySelectorAll('.id_equipo_ser');

        checkboxes.forEach(function(checkbox) {
            checkbox.addEventListener('change', function() {
                const optionCt = checkbox.closest('.option_ct');
                if (checkbox.checked) {
                    optionCt.classList.add('selected');
                } else {
                    optionCt.classList.remove('selected');
                }

                const optiontde = checkbox.closest('.td_equipo');
                if (checkbox.checked) {
                    optiontde.classList.add('selected');
                    optiontde.classList.remove('ocultartd');
                } else {
                    optiontde.classList.remove('selected');
                    optiontde.classList.add('ocultartd');
                }
            });
        });
    });
*/
$(document).ready(function($) {
    $('#iniciarchat').click(function(event) {
        $( "#iniciarchat" ).prop( "disabled", true );
        setTimeout(function(){ 
             $("#iniciarchat" ).prop( "disabled", false );
        }, 3000);
        var form=$('#form_newcli');
        var ser = $('input[name=servicio_c]:checked').val();
        if(parseInt(ser)>0){
            if(form.valid()){
                var datos=form.serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+"Icha/newclientesave",
                    data: datos,
                    success: function (data){
                        var resp=parseInt(data);
                        if(resp==1){
                            location.href ="https://kyoceraap.com/index.php/Icha?ttp=0";   
                        }else{
                            toastr.error('vuelva a verificar');
                        }
                        

                    }
                });
            }else{
                toastr.error('Acomplete los campos requeridos');
            }
        }else{
            toastr.error('Seleccione un tipo de servicio');
        }
    });
    
});