var base_url = $('#base_url').val();
var csrfName = $('#csrfName').val();
var csrfHash = $('#csrfHash').val();
$(document).ready(function($) {
	
});
function deleteproc(id){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminación?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
			    $.ajax({
			        type:'POST',
			        url: base_url+"Account/deletepro_c",
			        data: {
			            row:id,
                        [csrfName]: csrfHash, // Include the CSRF token
			        },
			        success: function (data){
			            location.reload(); 
			        }
			    });
				
                
            },
            cancelar: function (){   
            }
        }
    }); 
}
function realizarcot(){
	var idemp = $('#idemp').val();
	var rentaslis = $("#table_cotizacion tbody > tr");
    var DATAr  = [];

    rentaslis.each(function(){      
        item = {};
        item ["tipo"]  = $(this).find("input[id*='tipo']").val();
        item ["item"]  = $(this).find("input[id*='item']").val();
        item ["cant"]  = $(this).find("input[id*='cant']").val();
        
        DATAr.push(item);
            
    });
    arraypro   = JSON.stringify(DATAr);
    var datos = 'arraypro='+arraypro+'&idemp='+idemp+'&'+[csrfName]+'='+csrfHash;
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizacion/realizarcot",
        data: datos,
        success: function (response){
            toastr.success('Se realizo la solicitud');    
            limpiarcotizacion();    
            setTimeout(function(){ 
                location.reload(); 
            }, 2000);/*
            setTimeout(function(){ 
                window.location.href = base_url+"index.php/Cotizaciones/listaCotizacionesPorCliente/"+idCliente; 
            }, 2000);
            */
            
        },
        error: function(response){
            toastr.error('A ocurrido un error');
        }
    });
}
function limpiarcotizacion(){
	$.ajax({
			        type:'POST',
			        url: base_url+"Account/clearprocard_c",
			        data: {
			            row:0,
                        [csrfName]: csrfHash,
			        },
			        success: function (data){
			        }
			    });
}