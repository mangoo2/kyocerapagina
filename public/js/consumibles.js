var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table = $('#table_lis').DataTable();
	//load();
});
function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#table_lis').DataTable({
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url":  base_url+"Consumibles/listcon",
            type: "post",
            "data": {
                'paginaweb':1
            },
        },
        "columns": [

            {"data": "id",
                render:function(data,type,row){
                    var html =''
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        //"dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            /*
            {   
                extend: 'excelHtml5',
                text: ' Descargar Excel <i class="fa fa-download"></i>',
                className: 'btn classBotonFratsa'
            }
            */
        ],
        
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}