var base_url = $('#base_url').val();
$(document).ready(function($) {
	//===============================================================================
			OpenPay.setId('mzuolrwtlcn4ttmv4jvz');
            OpenPay.setApiKey('pk_a877c06890124d46833eaa759fc05094');
            OpenPay.setSandboxMode(true);
            //Se genera el id de dispositivo
            var deviceSessionId = OpenPay.deviceData.setup("payment-form", "deviceIdHiddenFieldName");
            /*
            $('#pay-button').on('click', function(event) {
                event.preventDefault();
                $('.pay-buttonloader').html('<div class="d-flex justify-content-center"><div class="spinner-border" role="status" style="width: 2rem; height: 2rem;"><span class="sr-only"></span></div></div>');
                $("#pay-button").prop( "disabled", true);
                OpenPay.token.extractFormAndCreate('payment-form', sucess_callbak, error_callbak);                
            });
            */

            var sucess_callbak = function(response) {
              var token_id = response.data.id;
              $('#token_id').val(token_id);
              enviapago(token_id);
              //$('#payment-form').submit();
            };

            var error_callbak = function(response) {
                var desc = response.data.description != undefined ? response.data.description : response.message;
                alert("ERROR [" + response.status + "] " + desc);
                $("#pay-button").prop("disabled", false);
            };
    //==================================================================================
	$("input:radio[name=formap]").change(function(event) {
		var formap=$("input:radio[name=formap]:checked").val();
		$('.addformpago').html('');
		var html='';
		if(formap==1){
			
			html='<form action="#" method="POST" id="payment-form">\
            <div class="row">\
                <div class="col-md-12" style="background: #e51f04;">\
                    <h2 style="color:white;">Tarjeta de crédito o débito</h2>\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-6">\
                    <label>Nombre del titular</label>\
                    <input  class="form-control" placeholder="Como aparece en la tarjeta" autocomplete="off" data-openpay-card="holder_name">\
                </div>\
                <div class="col-md-6">\
                    <label>Número de tarjeta</label>\
                    <input type="number" class="form-control" autocomplete="off" data-openpay-card="card_number">\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-12">\
                    <label>Fecha de expiración</label>\
                </div>\
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">\
                	<select class="form-control" placeholder="Mes" data-openpay-card="expiration_month">\
                    	<option value="01">01</option>\
                    	<option value="02">02</option>\
                    	<option value="03">03</option>\
                    	<option value="04">04</option>\
                    	<option value="05">05</option>\
                    	<option value="06">06</option>\
                    	<option value="07">07</option>\
                    	<option value="08">08</option>\
                    	<option value="09">09</option>\
                    	<option value="10">10</option>\
                    	<option value="11">11</option>\
                    	<option value="12">12</option>\
                    </select>\
                </div>\
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">\
                    <select class="form-control" placeholder="Año" data-openpay-card="expiration_year">\
                    	<option value="23">2023</option>\
                    	<option value="24">2024</option>\
                    	<option value="25">2025</option>\
                    	<option value="26">2026</option>\
                    	<option value="27">2027</option>\
                    	<option value="28">2028</option>\
                    	<option value="29">2029</option>\
                    	<option value="30">2030</option>\
                    	<option value="31">2031</option>\
                    	<option value="32">2032</option>\
                    </select>\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-12">\
                    <label>Código de seguridad</label>\
                </div>\
                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">\
                    <input type="number" class="form-control" placeholder="3 dígitos" autocomplete="off" data-openpay-card="cvv2" maxlength="3">\
                </div>\
                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6" style="background-image: url('+base_url+'public/img/cvv.png); background-repeat: no-repeat; background-position: center;">\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6" style="background-image: url('+base_url+'public/img/openpay.png); background-repeat: no-repeat; background-position: center;height: 70px;">\
                    Transacciones realizadas vía:\
                </div>\
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6" style="background-image: url('+base_url+'public/img/security.png); background-repeat: no-repeat; background-position: left;height: 70px;padding-left: 40px;">\
                    Tus pagos se realizan de forma segura con encriptación de 256 bits\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-7">\
                </div>\
                <div class="col-md-5 pay-button_div">\
                    <a class="button rght" id="pay-button" style="text-decoration: none;color: white;">Pagar</a>\
                </div>\
                <div class="col-md-5 pay-buttonloader_div" style="display:none">\
                    <div class="d-flex justify-content-center pay-buttonloader" ><div class="spinner-border" role="status" style="width: 2rem; height: 2rem;"><span class="sr-only"></span></div></div>\
                </div>\
            </div>\
        </form>';
        $('.addformpago').html(html);
        $('#pay-button').on('click', function(event) {
            event.preventDefault();
            $('.pay-buttonloader_div').show();
            $('.pay-button_div').hide();
            $("#pay-button").prop( "disabled", true);
            setTimeout(function(){
            	$('.pay-buttonloader_div').hide();
            	$('.pay-button_div').show();
            }, 3000);
            OpenPay.token.extractFormAndCreate('payment-form', sucess_callbak, error_callbak);                
        });
		}
        if(formap==2){
            html='<div id="paypal-button-container"></div>';
            $('.addformpago').html(html);
            activapaypay();
        }
	});
	
});
function mesajedepago0(){
        var html='<div class="container">\
           <div class="row">\
              <div class="col-md-6 mx-auto mt-5">\
                 <div class="payment">\
                    <div class="payment_header red">\
                       <div class="check"><i class="fa fa-check" aria-hidden="true"></i></div>\
                    </div>\
                    <div class="content">\
                       <h1>Payment Error !</h1>\
                       <p>No fue posible prosesar el pago, Intente mas tarde.</p>\
                    </div>\
                    \
                 </div>\
              </div>\
           </div>\
        </div>';
        $('.addformpago').html(html);
    
}
function mesajedepago1(array){
        var html='<div class="container">\
           <div class="row">\
              <div class="col-md-6 mx-auto mt-5">\
                 <div class="payment">\
                    <div class="payment_header green">\
                       <div class="check"><i class="fa fa-check" aria-hidden="true"></i></div>\
                    </div>\
                    <div class="content">\
                       <h1>Pago Aplicado !</h1>\
                       <p>Monto: '+array.amount+'</p>\
                       <p>Descripción: '+array.description+'</p>\
                       <p>Fecha: '+array.fecha+'</p>\
                       <p>Autorización: '+array.authorization+'</p>\
                    </div>\
                    \
                 </div>\
              </div>\
           </div>\
        </div>';
        $('.addformpago').html(html);
    
}
function enviapago(token){
            var device_session_id = OpenPay.deviceData.setup("payment-form");
            var urlenvio=base_url+"index.php/Carrito/addcargotargeta";
            console.log(urlenvio);

            var amount='';
            var description = '';
            var name = '';
            var email = '';
            
            $.ajax({
                type:'POST',
                url: urlenvio,
                data: {
                  token: token,
                  amount:amount,
                  description:description,
                  name:name,
                  email:email,
                  device_session_id:device_session_id
                },
                success: function (data){
                    //alert(data);
                  console.log(data);
                  var array = $.parseJSON(data);
                  if(array.status==1){
                    mesajedepago1(array);
                    console.log('pago generado');
                  }else{
                    mesajedepago0();
                    console.log('no fue posible prosesar');
                  }
                },
                error: function(response){
                    console.log(response)
                }
            });
        }
function activapaypay(){
    paypal.Buttons({

            // Call your server to set up the transaction
            createOrder: function(data, actions) {
                return fetch(base_url+'Carrito/paypalorder', {
                    method: 'post'
                }).then(function(res) {
                    return res.json();
                }).then(function(orderData) {
                    console.log(orderData);
                    console.log(orderData.id);
                    return orderData.id;
                });
            },

            // Call your server to finalize the transaction
            onApprove: function(data, actions) {
                return fetch('/demo/checkout/api/paypal/order/' + data.orderID + '/capture/', {
                    method: 'post'
                }).then(function(res) {
                    return res.json();
                }).then(function(orderData) {
                    // Three cases to handle:
                    //   (1) Recoverable INSTRUMENT_DECLINED -> call actions.restart()
                    //   (2) Other non-recoverable errors -> Show a failure message
                    //   (3) Successful transaction -> Show confirmation or thank you

                    // This example reads a v2/checkout/orders capture response, propagated from the server
                    // You could use a different API or structure for your 'orderData'
                    var errorDetail = Array.isArray(orderData.details) && orderData.details[0];

                    if (errorDetail && errorDetail.issue === 'INSTRUMENT_DECLINED') {
                        return actions.restart(); // Recoverable state, per:
                        // https://developer.paypal.com/docs/checkout/integration-features/funding-failure/
                    }

                    if (errorDetail) {
                        var msg = 'Sorry, your transaction could not be processed.';
                        if (errorDetail.description) msg += '\n\n' + errorDetail.description;
                        if (orderData.debug_id) msg += ' (' + orderData.debug_id + ')';
                        return alert(msg); // Show a failure message (try to avoid alerts in production environments)
                    }

                    // Successful capture! For demo purposes:
                    console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
                    var transaction = orderData.purchase_units[0].payments.captures[0];
                    alert('Transaction '+ transaction.status + ': ' + transaction.id + '\n\nSee console for all available details');

                    // Replace the above to show a success message within this page, e.g.
                    // const element = document.getElementById('paypal-button-container');
                    // element.innerHTML = '';
                    // element.innerHTML = '<h3>Thank you for your payment!</h3>';
                    // Or go to another URL:  actions.redirect('thank_you.html');
                });
            }

        }).render('#paypal-button-container');
}