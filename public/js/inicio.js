var base_url = $('#base_url').val();
var csrfName = $('#csrfName').val();
var csrfHash = $('#csrfHash').val();
$(document).ready(function($) {
	$('.closems').click(function(event) {
        $('.modalesss').hide('show');
    });
    $('#savedir').click(function(event) {
        $.ajax({
            type:'POST',
            url: base_url+"Carrito/direccionsave",
            data: {
                dir:$('#adddireccion').val(),
                entre:$('#entrecalles').val(),
                cp:$('#cp').val(),
                [csrfName]: csrfHash, // Include the CSRF token
            },
            success: function (data){
                window.location.reload();

            }
        });
    });
    $('#savecont').click(function(event) {
        $.ajax({
            type:'POST',
            url: base_url+"Carrito/contactosave",
            data: {
                per:$('#atencionpara').val(),
                tel:$('#telefono').val(),
                cel:$('#celular').val(),
                [csrfName]: csrfHash, // Include the CSRF token
            },
            success: function (data){
                window.location.reload();

            }
        });
    });
    $('#savedir_close').click(function(event) {
        $('#direccionModal').modal('hide');
    });
    $('#savecont_close').click(function(event) {
        $('#contactoModal').modal('hide');
    });
    $('#searchparam').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        templateResult: formatState_input_searchparam,
        ajax: {
            url: base_url+'Inicio/search',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.modelo+' '+element.noparte,
                        uuid: element.uuid,
                        tipo: element.tipo,
                        mod: element.modelo,
                        npar: element.noparte

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        selected_url(data.uuid,data.tipo);
    });
    $('#inicarsesion').click(function(event) {
        var form_s = $('#form_sesion');
        if(form_s.valid()){
            var datos=form_s.serialize()+'&lses=1';
            $.ajax({
                type:'POST',
                url: base_url+"Account/login",
                data: datos,
                success: function (data){
                    var log = parseInt(data);
                    //window.location.reload();
                    if(log==1){
                        $('.info_login_error').html('<div class="not_sucess_login">¡Inicio de sesión correcto!</div>');
                        setTimeout(function(){ 
                            window.location.reload();
                        }, 1000);
                    }else{
                        $('.info_login_error').html('<div class="not_error_login">¡E-mail ó contraseña errónea!</div>');
                    }

                }
            });
        }else{
            toastr.error('Favor de completar los campos requeridos');
        }
        
    });
});
function formatState_input_searchparam (state) {
  if (!state.id) {
    return state.text;
  }
  
  var $state = $(
    '<span class="searchparam_in_1">' + state.mod + '</span> <span class="searchparam_in_2">No. de parte: ' + state.npar + '</span>'
  );
  return $state;
};
function idioma(idi){
	$.ajax({
        type:'POST',
        url: base_url+"Inicio/cambioidioma",
        data: {
        	idioma:idi,
            [csrfName]: csrfHash, // Include the CSRF token
        },
        success: function (data){
        	location.reload(); 
        }
    });
}
function modal(num){
    $('#myModal_'+num).show('show');
    if(num==3){
        var html='<div class="col-md-12">\
            <div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url('+base_url+'public/img/perceptual-standard.jpg); background-size:cover;" onclick="vercatalogo(1)">\
                  <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">\
                    <p class="text-white" style="margin:0px">ver catalogo</p>\
                  </div>\
                </div>\
            <div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url('+base_url+'public/img/perceptual-standard.jpg); background-size:cover;" onclick="vercatalogo(2)">\
                <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">\
                    <p class="text-white" style="margin:0px">ver catalogo</p>\
                </div>\
            </div>\
        </div>\
        <div class="col-md-12 text-white" style="text-align:center;">\
            <p style="font-size: 18px;">¿ Necesitas producir algo en alguna parte de México ? no te preocupes, Redapol cuenta con su propia agencia de locaciones</p>\
            <p style="font-size: 18px;">Gestionamos permisos en cualquier parte del país, contamos con nuestro propio personal de locaciones y equipo de seguridad fílmica, ideal para cualquier base camp y cierre de vialidades. </p>\
            <p style="font-size: 18px;">¿ Solo necesitas el servicio de locaciones ? nosotros podemos hacerlo.</p>\
        </div>';
        $('.infoclass_modal3').html(html);
    }
}

function  vercatalogo(num) {
    var html='';
    if(num==1){
        var html='<div class="col-md-12">\
            <div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url('+base_url+'public/img/perceptual-standard.jpg); background-size:cover;" onclick="vercatalogo(1)">\
                  <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">\
                    <p class="text-white" style="margin:0px">ver catalogo</p>\
                  </div>\
                </div>\
            <div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url('+base_url+'public/img/perceptual-standard.jpg); background-size:cover;" onclick="vercatalogo(2)">\
                <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">\
                    <p class="text-white" style="margin:0px">ver catalogo</p>\
                </div>\
            </div>\
            <div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url('+base_url+'public/img/perceptual-standard.jpg); background-size:cover;" >\
                  <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">\
                  </div>\
                </div>\
            <div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url('+base_url+'public/img/perceptual-standard.jpg); background-size:cover;" >\
                <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">\
                </div>\
            </div>\
        </div>';
    }
    if(num==2){
        var html='<div class="col-md-12">\
            <div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url('+base_url+'public/img/perceptual-standard.jpg); background-size:cover;" onclick="vercatalogo(1)">\
                  <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">\
                  </div>\
                </div>\
            <div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url('+base_url+'public/img/perceptual-standard.jpg); background-size:cover;" onclick="vercatalogo(2)">\
                <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">\
                </div>\
            </div>\
            <div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url('+base_url+'public/img/perceptual-standard.jpg); background-size:cover;" >\
                  <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">\
                  </div>\
                </div>\
            <div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url('+base_url+'public/img/perceptual-standard.jpg); background-size:cover;" >\
                <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">\
                </div>\
            </div>\
        </div>';
    }
    $('.infoclass_modal3').html(html);
    
}

function enviar_correo(){
    var nombre_t = $('#nombre_t').val();
    var correo_t = $('#correo_t').val();
    var telefono_t = $('#telefono_t').val();
    var comentario_t = $('#comentario_t').val();
    $('.text_enviar').html('Enviando correo . . .');
    $.ajax({
        type:'POST',
        url: base_url+"Inicio/enviar",
        data: {
            nombre:$('#nombre_t').val(),
            correo:$('#correo_t').val(),
            telefono:$('#telefono_t').val(),
            comentario:$('#comentario_t').val(),
            [csrfName]: csrfHash, // Include the CSRF token
        },
        success: function (data){
            setTimeout(function(){ 
                $('.text_enviar').html('Enviado con éxito<br>En breve nos comunicamos contigo');
            }, 1000);
            setTimeout(function(){ 
                $('.text_enviar').html('Enviar correo');
                $('#nombre_t').val('');
                $('#correo_t').val('');
                $('#telefono_t').val('');
                $('#comentario_t').val('');
            }, 10000);
        }
    });
    
    //alert(nombre_t+'-'+correo_t+'-'+telefono_t+'-'+comentario_t);

}
function deletepro(row){
   $.ajax({
        type:'POST',
        url: base_url+"Account/deletepro",
        data: {
            row:row,
            [csrfName]: csrfHash, // Include the CSRF token
        },
        success: function (data){
            window.location.reload();

        }
    }); 
}
function down_up(row,type){
    $.ajax({
        type:'POST',
        url: base_url+"Account/down_up",
        data: {
            row:row,type:type,
            [csrfName]: csrfHash, // Include the CSRF token
        },
        success: function (data){
            window.location.reload();

        }
    });
}
function agregardir(){
    $('#direccionModal').modal('show');
    $('#adddireccion').val('');
}
function agregarcon(){
    $('#contactoModal').modal('show');
    $('#atencionpara').val('');
    $('#telefono').val('');
    $('#celular').val('');
}
function deletedir(idi){
    $.ajax({
        type:'POST',
        url: base_url+"Carrito/deletedir",
        data: {
            idi:idi,
            [csrfName]: csrfHash, // Include the CSRF token
        },
        success: function (data){
            location.reload(); 
        }
    });
}
function addproconti(tipo,item){
    $.ajax({
            type:'POST',
            url: base_url+"Account/addcar_c",
            data: {
                typeitem:tipo,
                iditem:item,
                am:1,
                [csrfName]: csrfHash, // Include the CSRF token
            },
            success: function (data){
                toastr.success('Producto agregado para cotizar');

            }
        });
}
function selected_url(uuid,tipo){
    if(tipo==1){
        $(location).attr('href',base_url+'Equipos/articulo_equipo/'+uuid);
    }
    if(tipo==2){
        $(location).attr('href',base_url+'Accesorios/articulo/'+uuid+'/0');
    }
    if(tipo==3){
        $(location).attr('href',base_url+'Kits/articulo/'+uuid+'/0');
    }
    if(tipo==4){
        $(location).attr('href',base_url+'Consumibles/articulo/'+uuid);
    }
}