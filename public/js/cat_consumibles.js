var base_url = $('#base_url').val();
var csrfName = $('#csrfName').val();
var csrfHash = $('#csrfHash').val();
function consu_eq(id){
	$('.consu_eq').removeClass('active');
	$('.consu_eq_'+id).addClass('active');
	$.ajax({
        type:'POST',
        url: base_url+"Inicio/consumiblesxequipo",
        data: {
            ideq:id,
            [csrfName]: csrfHash, // Include the CSRF token
        },
        success: function (data){
            console.log(data);
            $('.view_consumibles').html(data);
        }
    });
}
function view_eq(tipo){
	if(tipo==0){
		$('.impresoras').show('show');
	}
	if(tipo==1){
		$('.imp_cat_2').hide('show');
		$('.imp_cat_3').hide('show');
		$('.imp_cat_1').show('show');
	}
	if(tipo==2){
		$('.imp_cat_1').hide('show');
		$('.imp_cat_3').hide('show');
		$('.imp_cat_2').show('show');
	}
	if(tipo==3){
		$('.imp_cat_2').hide('show');
		$('.imp_cat_1').hide('show');
		$('.imp_cat_3').show('show');
	}
}
function view_dll(id,view){
	if(view==0){
		$('.imp_id_'+id+' .eq_c_2').hide('show');
		$('.imp_id_'+id+' .eq_c_1').show('show');
		$('.imp_id_'+id+' .btns-infos').html('<button class="btn_i_red" onclick="view_dll('+id+',1)"><i class="fa fa-plus"></i> Info</button>');
	}
	if(view==1){
		$('.imp_id_'+id+' .eq_c_1').hide('show');
		$('.imp_id_'+id+' .eq_c_2').show('show');
		$('.imp_id_'+id+' .btns-infos').html('<button class="btn_i_grey" onclick="view_dll('+id+',0)"><i class="fa fa-minus"></i> Cerrar</button>');
	}
}