const tiporcv = $('#tiporcv').val();
//================================================ agrega o quita classes al seleccionar checkbox
if(tiporcv==1){
    document.addEventListener('DOMContentLoaded', function() {
        const checkboxes = document.querySelectorAll('.id_equipo_ser');

        checkboxes.forEach(function(checkbox) {
            checkbox.addEventListener('change', function() {
                const optionCt = checkbox.closest('.option_ct_serie');
                if (checkbox.checked) {
                    optionCt.classList.add('selected');
                } else {
                    optionCt.classList.remove('selected');
                }

                const optiontde = checkbox.closest('.option_ct_serie');
                if (checkbox.checked) {
                    optiontde.classList.add('selected');
                    optiontde.classList.remove('ocultartd');
                } else {
                    optiontde.classList.remove('selected');
                    optiontde.classList.add('ocultartd');
                }
            });
        });
    });
}else{
    document.addEventListener('DOMContentLoaded', function() {
        const checkboxes = document.querySelectorAll('.id_equipo_ser');

        checkboxes.forEach(function(checkbox) {
            checkbox.addEventListener('change', function() {
                const optionCt = checkbox.closest('.option_ct');
                if (checkbox.checked) {
                    optionCt.classList.add('selected');
                } else {
                    optionCt.classList.remove('selected');
                }

                const optiontde = checkbox.closest('.td_equipo');
                if (checkbox.checked) {
                    optiontde.classList.add('selected');
                    optiontde.classList.remove('ocultartd');
                } else {
                    optiontde.classList.remove('selected');
                    optiontde.classList.add('ocultartd');
                }
            });
        });
    });
}
//=================================================
var base_url=$('#base_url').val();
var csrfName = $('#csrfName').val();
var csrfHash = $('#csrfHash').val();
var codigo = $('#codigo').val();
var table_equipos;
$(document).ready(function($) {
    var table_equipos=$('#table_equipos').DataTable({
        bLengthChange : false, //thought this line could hide the LengthMenu
        bInfo:false, 
        //searching: false,
        lengthMenu: [
            [ -1],
            [ 'All']
        ]
    });
    $('#search_input').on('keyup', function() {
        table_equipos.search(this.value).draw();
    });
    $('.id_equipo_ser').click(function(event) {
    	verificarestatus();
    });
    $('.next_1').click(function(event) {
    	$('.ocultartd').hide('show');
    	$('.td_equipo.selected .div_motivo').show('show');
        $('.option_ct_serie.selected .div_motivo').show('show');
    	$('.row_fixed_date').show('show');
    	$('#table_equipos').addClass('margin-add');

    	$('.next_1').hide('show');
    	$('.next_2').show('show');
        if(tiporcv==1){
            //si para los equipos de rentas si alguno de los bloque no seleccionaron por lo menos un equipo , que todo el bloque se oculte
            $(".td_equipo").each(function() {
                var isUnchecked = true; // Variable para verificar si todos los checkboxes están desmarcados
                
                $(this).find('input[type="checkbox"]').each(function() {
                    if ($(this).is(':checked')) {
                        isUnchecked = false; // Si alguno está marcado, cambiamos la variable
                    }
                });
                
                if (isUnchecked) {
                    console.log($(this)); // Aquí puedes realizar la acción que desees, como $(this).hide();
                    $(this).hide();
                }
            });
        }
    });
    $('.next_2').click(function(event) {
        view_gene(0);
        if(tiporcv==1){
            var DATAa  = [];
            $(".option_ct_serie").each(function() {
                var checked = $(this).find("input[class*='id_equipo_ser']").is(':checked');
                console.log(checked);
                if(checked){
                    item = {};
                    item ["ideq"]   = $(this).find("input[class*='id_equipo_ser']").data('ideq');
                    item ["eqmod"]   = $(this).find("input[class*='id_equipo_ser']").data('modelo');
                    item ["idser"]  = $(this).find("input[class*='id_equipo_ser']").data('idser');
                    item ["ser"]  = $(this).find("input[class*='id_equipo_ser']").data('serie');
                    item ["tcv"]  = $(this).find("input[class*='id_equipo_ser']").data('tiporcv');// es del tipo de donde viene la informacion 1 rentas 2 poliza 3 cliente, 4venta
                    item ["idsv"]  = $(this).find("input[class*='id_equipo_ser']").data('idserv');//id, actualmente solo tiene poliza
                    item ["idsvd"]  = $(this).find("input[class*='id_equipo_ser']").data('idservd');//id, actualmente solo tiene poliza, es el id de detallle del equipo
                    item ["vc"]  = $(this).find("input[class*='id_equipo_ser']").data('vc');//solo aplica para las polizas que tengan mas de un servico(modulo polizas contratos)
                    item ["inf"]  = $(this).find("input[id*='motivo_equipo']").val();
                    DATAa.push(item);
                }
            });
        }else{
        	var equipos_lis = $("#table_equipos tbody > tr");
    	    var DATAa  = [];
    	    equipos_lis.each(function(){  
    	        if ($(this).find("input[class*='id_equipo_ser']").is(':checked')) {
    	            item = {};
                    item ["ideq"]   = $(this).find("input[class*='id_equipo_ser']").data('ideq');
                    item ["eqmod"]   = $(this).find("input[class*='id_equipo_ser']").data('modelo');
                    item ["idser"]  = $(this).find("input[class*='id_equipo_ser']").data('idser');
                    item ["ser"]  = $(this).find("input[class*='id_equipo_ser']").data('serie');
                    item ["tcv"]  = $(this).find("input[class*='id_equipo_ser']").data('tiporcv');// es del tipo de donde viene la informacion 1 rentas 2 poliza 3 cliente, 4venta
                    item ["idsv"]  = $(this).find("input[class*='id_equipo_ser']").data('idserv');//id, actualmente solo tiene poliza
                    item ["idsvd"]  = $(this).find("input[class*='id_equipo_ser']").data('idservd');//id, actualmente solo tiene poliza, es el id de detallle del equipo
                    item ["vc"]  = $(this).find("input[class*='id_equipo_ser']").data('vc');//solo aplica para las polizas que tengan mas de un servico(modulo polizas contratos)
                    item ["inf"]  = $(this).find("input[id*='motivo_equipo']").val();
                    DATAa.push(item);
    	        }  
    	    });
        }
	    INFOa  = new FormData();
        aInfoa   = JSON.stringify(DATAa);
    	$.ajax({
            type:'POST',
            url: base_url+"index.php/Icha/savesol",
            async: false,
            statusCode:{
                404: function(data){
                },
                403: function(data){
                    toastr.error("Algo salió mal");
                    setTimeout(function(){ 
                        location.reload(); 
                    }, 2000);
                },
                500: function(){
                    
                }
            },
            data: {
            	[csrfName]: csrfHash, // Include the CSRF token
                idc:$('#idc').val(),
                fech:$('#date_servicio').val(),
                equipos:aInfoa
            },
            success: function (response){
                console.log(response);
                var idv = parseInt(response);
                if(idv>0){
                    view_gene(1);
                }
            },
            error: function(response){
                toastr.error("Algo salió mal, intente de nuevo o contacte al administrador del sistema..");
                setTimeout(function(){ 
                    location.reload(); 
                }, 2000);
            }
        });
    });
});
function verificarestatus(){
	var equipos_lis = $("#table_equipos tbody > tr");
    var num_equipos_selected=0;
    equipos_lis.each(function(){  
        if ($(this).find("input[class*='id_equipo_ser']").is(':checked')) {
            num_equipos_selected++;   
        }  
    });
    if(num_equipos_selected>0){
    	$('.next_1').show('show');
    }else{
    	$('.next_1').hide('show');
    }
}
function view_gene(tipo){
    $('.row_fixed_notificacion').html('');
    
    if(tipo==0){
        $('.row_fixed_notificacion').show('show');
        var html='';
            html+='<div class="col-md-12">';
            html+='<img src="'+base_url+'public/img/ring.gif" class="loading">';
            html+='</div>';
            html+='<div class="col-md-12" style="color:red">';
                html+='Generando solicitud de servicio...';
            html+='</div>';
        $('.row_fixed_notificacion').html(html);    
    }
    if(tipo==1){
        var html='';
            html+='<div class="col-md-12">';
            html+='<img src="'+base_url+'public/img/success.svg" class="success">';
            html+='</div>';
            html+='<div class="col-md-12" style="color:red">';
                html+='¡Solicitud generada con éxito, en breve uno de nuestros ejecutivos entrara en ontacto con usted!';
            html+='</div>';
            html+='<div class="col-md-12" style="font-size: 11px;">';
                html+='¿Desea realizar algun otro movimiento o solicitud?';
            html+='</div>';
            html+='<div class="col-md-6 col-sm-6 col-6" style="float:left">';
                    html+='<a href="'+base_url+'Icha/cli_verif/'+codigo+'" class="btn btn-danger btn-sm shadowx " >Si</a>';
            html+='</div>';
            html+='<div class="col-md-6 col-sm-6 col-6" style="float:left">';
                    html+='<a href="'+base_url+'Icha" class="btn btn-danger btn-sm shadowx " >No</a>';
            html+='</div>';
        $('.row_fixed_notificacion').html(html);   
    }
}
function search_view(){
    var isclass =$('#btb-search').hasClass('search_view');
    if(isclass){
        $('#btb-search .btb-search_content').html('<i class="fas fa-search"></i>');
        $('#btb-search').removeClass('search_view');
        $('#search_input').hide('show').val('');
    }else{
        $('#btb-search .btb-search_content').html('<i class="fas fa-times"></i>');
        $('#btb-search').addClass('search_view');
        $('#search_input').show('show').val('');
    }
}
function buscarequipo(){
    var search_value=$('#search_input').val();
    table_equipos.search(search_value).draw();
}
function show_hide_ser(classe){
    var div = $('div.'+classe+'.activo');

    // Verifica si el div existe y haz algo con él
    if (div.length > 0) {
      $('.'+classe).removeClass('activo');
      //$('.'+classe+' .form-check-input').prop('checked',false); le quita la seleccion a los input cuando se ocultan, se deja comentado por si no se ocupa
    } else {
      $('.'+classe).addClass('activo');
    }
}