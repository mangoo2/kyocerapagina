(function ($) {

    function setImageSize(area) {
        if (area == null)
            return;

        area.css({'line-height': area.height() + 'px'});

        var w = area.attr('data-emzoompam_imagewidth');
        var h = area.attr('data-emzoompam_imageheight');

        var neww = w;
        var newh = h;

        var maxwidth  = area.width();
        var maxheight = area.height();
        if (neww > maxwidth) {
            var factor = maxwidth / neww;
            neww *= factor;
            newh *= factor;
        }
        if (newh > maxheight) {
            var factor = maxheight / newh;
            neww *= factor;
            newh *= factor;
        }

        var img = $('img', area);
        img.css({
                    'width' : neww + 'px',
                    'height': newh + 'px'
                });
    }

    function setZoom(area) {
        var w  = area.width();
        var h  = area.height();
        var iw = area.attr('data-emzoompam_imagewidth');
        var ih = area.attr('data-emzoompam_imageheight');
        if (w < iw || h < ih) {
            area.removeClass('emzoompamdisabled');
        }
        else {
            area.addClass('emzoompamdisabled');
        }
    }

    function enableZoom(area) {
        var img     = $('img', area);
        var tmpimgw = $(img).width();
        var tmpimgh = $(img).height();
        var iw      = area.attr('data-emzoompam_imagewidth');
        var ih      = area.attr('data-emzoompam_imageheight');
        var tmpimgt = $(img).position().top;
        var tmpimgl = $(img).position().left;

        $(img)
            .css({
                     'left'    : tmpimgl + 'px',
                     'top'     : tmpimgt + 'px',
                     'position': 'absolute'
                 })
            .animate({
                         'left'  : tmpimgl - ((iw - tmpimgw) / 2),
                         'top'   : tmpimgt - ((ih - tmpimgh) / 2),
                         'width' : iw + 'px',
                         'height': ih + 'px'
                     }, 100, '', function () {
                area.css({
                             'background-image'   : 'url("' + $(img).attr('src') + '")',
                             'background-position': 'center center'
                         });
                img.blur().css({
                                   'left'    : 'auto',
                                   'top'     : 'auto',
                                   'position': 'static',
                                   'display' : 'none',
                                   'width'   : tmpimgw + 'px',
                                   'height'  : tmpimgh + 'px'
                               });
                area.addClass('emzoompamenabled');
            });
    }

    function disableZoom(area) {
        var p = area.css('background-position');
        p     = p.split(" ");

        var img     = $('img', area);
        var tmpimgw = $(img).width();
        var tmpimgh = $(img).height();
        var iw      = area.attr('data-emzoompam_imagewidth');
        var ih      = area.attr('data-emzoompam_imageheight');

        var nl = p[0];
        var nt = p[1];
        if (nl.substr(nl.length - 1, 1) == "%") {
            nl = ((area.width() / 2) - (iw / 2)) + 'px';
            nt = ((area.height() / 2) - (ih / 2)) + 'px';
        }

        $(img)
            .css({
                     'left'    : nl,
                     'top'     : nt,
                     'position': 'absolute',
                     'width'   : iw + 'px',
                     'height'  : ih + 'px',
                     'display' : 'inline'
                 });

        area.css('background-color', 'white');
        area.css('background-image', 'none');

        var nl = ((area.width() / 2) - (tmpimgw / 2));
        var nt = ((area.height() / 2) - (tmpimgh / 2));

        $(img)
            .animate({
                         'left'  : nl + 'px',
                         'top'   : nt + 'px',
                         'width' : tmpimgw + 'px',
                         'height': tmpimgh + 'px'
                     }, 100, '', function () {
                img.css({
                            'left'    : 'auto',
                            'top'     : 'auto',
                            'position': 'static',
                            'display' : 'inline',
                            'width'   : tmpimgw + 'px',
                            'height'  : tmpimgh + 'px',
                            'display' : 'inline'
                        }).blur();
                area.removeClass('emzoompamenabled');
            });
    }

    function changeImage(area) {
        var img = $('img', area);
        area.css('background-color', 'white');
        area.css('background-image', 'none');
        img.css('display', 'inline').blur();
        area.removeClass('emzoompamenabled');
    }


    var emzoompam_current     = null;
    var emzoompam_loadedindex = 0;

    $.fn.emzoompam = function (options) {
        options = $.extend({
                               athumbclass   : 'emzoompam_image',
                               dmousemovearea: 0.7
                           }, options);

        $('body').append('<div id="emzoompam_pixel" style="width:0px; height:0px; overflow:hidden; "></div>');

        var zoomarea = $(this);

        $.fn.doresize = function () {
            if (emzoompam_current != null) {
                setImageSize(emzoompam_current);
                changeImage(emzoompam_current);
                setZoom(emzoompam_current);
            }
        };

        $('.' + options.athumbclass).each(function () {

            var loader = $('<div />').addClass("emzoompam_loader");

            a = $(this);
            a.attr({
                       'data-emzoompamloaded': '-1',
                       'data-emzoompamindex' : emzoompam_loadedindex
                   })
             .click(function () {
                 $('.' + options.athumbclass).removeClass('selected');
                 $('div', zoomarea).stop().fadeOut(300, function () {
                     $(this).css({'display': 'none'});
                 });
                 if ($(this).attr('data-emzoompamloaded') == "1") {
                     var area = $('div[data-emzoompamindex="' + $(this).attr('data-emzoompamindex') + '"]');
                     changeImage(area);
                     emzoompam_current = area;

                     area.removeClass('emzoompamenabled');
                     setImageSize(area);
                     setZoom(area);

                     if ($(this).attr('id')=='modal-thumbnail-360') {
                        area.stop().fadeIn(300, function () {
                            $(this).css({'display': 'none', 'opacity': 1});
                            $('#modal-img-360').css({'display':'block','cursor': 'ew-resize'});
                            var width_360 = $('#emzoompam_zoomarea').width();
                            var height_360 = $('#emzoompam_zoomarea').height();
                            var dataslidescount = $('#modal-thumbnail-360').attr('data-slidescount');
                            var dataurlmask = $('#modal-thumbnail-360').attr('data-urlmask');
                            var arreglo360 = [];
                            for (var i = 0; i < dataslidescount; i++) {
                                let temp = dataurlmask;
                                arreglo360.push(temp.replace('%d', i));
                            }
                            $('#modal-img-360').spritespin({
                                source: arreglo360,
                                width: width_360,
                                height: height_360,
                                animate: false,
                                sense: -1,
                                sizeMode: "fit",
                            });
                        });
                    } else {
                         area.stop().fadeIn(300, function () {
                             $(this).css({'display': 'block', 'opacity': 1});
                         });
                    }
                    $(this).addClass('selected');
                 }
                 return false;
             }).append(loader);


            var img = $('<img />')
                .attr({
                          'src'                : a.attr('href'),
                          'data-emzoompamindex': emzoompam_loadedindex
                      })
                .css({
                         'vertical-align': 'middle',
                         'position'      : 'relative',
                         'text-align'    : 'center'
                     })
                .focus(function () {
                })
                .load(function () {

                    var img = $(this);

                    $('div#emzoompam_pixel').html($(this));
                    var w = img.width();
                    var h = img.height();
                    $('div#emzoompam_pixel').html('');

                    img.attr({
                                 'data-emzoompam_imagewidth' : w,
                                 'data-emzoompam_imageheight': h
                             });


                    var area = $('<div />')
                        .css({
                                 'background-repeat'  : 'no-repeat',
                                 'background-position': '0px 0px',
                                 'background-color'   : 'white',
                                 'position'           : 'absolute',
                                 'top'                : '0px',
                                 'left'               : '0px',
                                 'right'              : '0px',
                                 'bottom'             : '0px',
                                 'text-align'         : 'center',
                                 'display'            : 'none'
                             })
                        .focus(function () {
                        })
                        .attr({
                                  'data-emzoompamindex'       : img.attr('data-emzoompamindex'),
                                  'data-emzoompam_imagewidth' : w,
                                  'data-emzoompam_imageheight': h
                              })
                        .click(function () {
                            if ($(this).hasClass('emzoompamdisabled') == false) {
                                if ($(this).hasClass('emzoompamenabled')) {
                                    disableZoom($(this));
                                    $(this).blur();
                                }
                                else {
                                    enableZoom($(this));
                                }
                            }
                        })
                        .mouseleave(function (e) {
                            if ($(this).hasClass('emzoompamdisabled') == false) {
                                if ($(this).hasClass('emzoompamenabled')) {
                                    disableZoom($(this));
                                    $(this).blur();
                                }
                            }
                        })
                        .mousemove(function (e) {
                            if ($(this).hasClass('emzoompamdisabled') == false && $(this).hasClass('emzoompamenabled') == true) {
                                var offsetfactor = 20;

                                var w = $(this).width();
                                var h = $(this).height();

                                var iw = $(this).attr('data-emzoompam_imagewidth');
                                var ih = $(this).attr('data-emzoompam_imageheight');

                                var bw  = Math.ceil(w / 100 * offsetfactor / 2);
                                var bh  = Math.ceil(h / 100 * offsetfactor / 2);
                                var ibw = Math.ceil(iw / 100 * offsetfactor / 2);
                                var ibh = Math.ceil(ih / 100 * offsetfactor / 2);

                                var newleft = 0;
                                var newtop  = 0;
                                if (w > iw) {
                                    newleft = (w / 2) - (iw / 2);
                                }
                                else {
                                    w           = w - (bw * 2);
                                    var factorw = (iw - w) / w;
                                    var l       = $(this).offset().left;
                                    var mousex  = e.pageX - l - bw;
                                    newleft     = ((mousex * factorw) * -1);
                                    newleft     = newleft + bw;
                                }
                                if (h > ih) {
                                    newtop = (h / 2) - (ih / 2);
                                }
                                else {
                                    h           = h - (bh * 2);
                                    var factorh = (ih - h) / h;
                                    var t       = $(this).offset().top;
                                    var mousey  = e.pageY - t - bh;
                                    newtop      = (mousey * factorh) * -1;
                                    newtop      = newtop + bh;
                                }

                                $(this).css('background-position', newleft + 'px ' + newtop + 'px');
                            }
                        })
                        .append(img)
                        .append(' ');

                    zoomarea.append(area);

                    var a = $('a.emzoompam_image[data-emzoompamindex="' + img.attr('data-emzoompamindex') + '"]');
                    a.attr('data-emzoompamloaded', '1')
                     .addClass('emzoompamloaded');

                    $('div.emzoompam_loader', a).css('display', 'none');

                    if (a.attr('data-emzoompam_selected') == "1")
                        a.click();
                });


            emzoompam_loadedindex++;
        });


    };

})(jQuery);